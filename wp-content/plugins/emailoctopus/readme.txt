=== Email Marketing by EmailOctopus ===

Contributors: EmailOctopus
Tags: email marketing, collect emails, emailoctopus, emailoctopus form, emailoctopus plugin, email, email octopus, email form, marketing, newsletter, signup form, subscribers, subscription, widget, email plugin, list builder
Requires at least: 3.7
Tested up to: 5.0
Stable tag: 2.0.3
Requires PHP: 5.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Email marketing widget; easily add subscribers to an EmailOctopus list. Fully customisable.

== Description ==

[EmailOctopus](https://emailoctopus.com) is the cheapest way to send your email marketing campaigns. Utilising the power of Amazon SES, the EmailOctopus platform allows you to send email marketing campaigns at a cost of up to 10x cheaper than other email marketing platforms.

The official [EmailOctopus](https://emailoctopus.com) Wordpress plugin allows you to add multilingual forms to your Wordpress site. These forms allow you to collect your visitors email addresses, allowing you to contact them through the [EmailOctopus](https://emailoctopus.com) marketing platform.

= About EmailOctopus =
[vimeo https://vimeo.com/145924374]

= EmailOctopus Features =
* **Low Cost, Unlimited Email Marketing Campaigns.** Send your email marketing campaigns at a fraction of the cost of other providers — every plan comes with unlimited emails.
* **High Deliverability.** Because we use Amazon's infrastructure to send your emails, you never have to worry about them not reaching their destination.
* **Email Marketing Personalisation.** We allow you to add customer data, such as First and Last name direct into the EmailOctopus marketing platform. Personalise your email marketing campaigns, and see your metrics skyrocket.
* **Real-time Reports & Insights.** Track exactly how your email marketing campaign performed with its advanced report. View statistics on who opened, clicked, unsubscribed, bounced, and more.
* **WYSIWYG Email Editor.** Copy and paste your HTML email templates for newsletters and marketing campaigns. Our incredibly simple to use WYSIWYG editor will allow you to edit content without needing to know HTML.
* **Automatic Bounce, Complaint & Unsubscribe Handling.** All bounces, complaints or unsubscribes are automatically cleaned from your lists.
* **Email Marketing Automation/Drips.** Trigger a series of emails to be sent to your list at specific intervals. Great for onboarding your customers.

= EmailOctopus Plugin Features =
* Allows you to enter your API key.
* Integrated form builder.
* Advanced style customization.
* Gutenberg option available.

== Installation ==

1. In your WordPress admin panel, go to Plugins > New Plugin, search for EmailOctopus and click 'Install now'. Alternatively, download the plugin and upload the contents of emailoctopus.zip to your plugins directory, which is usually located at '/wp-content/plugins/'.
2. Activate the plugin and enter your API key from EmailOctopus.
3. Create a form using the built-in form builder.
4. Enter a shortcode onto a page, or use the EmailOctopus widget to select a form to display.
5. If using Gutenberg, you'll find EmailOctopus under widgets.

== Frequently Asked Questions ==

= How can I contact you? =

You can email us on contact@emailoctopus.com.

== Screenshots ==

1. An EmailOctopus form in the sidebar of the Twenty Sixteen theme.
2. An EmailOctopus form in the body of a post or page.
3. API key entry screen.
4. Form builder main screen.
5. Form builder settings screen.
6. Form builder appearance screen with custom colors.
7. Form builder messages screen.
8. Save button with shortcode output.
9. Saved forms screen.
10. Gutenberg block.

== Changelog ==

= 2.0.3 =
* Improves the UX for managing lists.
* Fixes bugs preventing some forms from being properly updated.

= 2.0.2 =
* Fixes a bug that causes form submissions to fail.

= 2.0.1 =
* Improved UX in the classic and Gutenberg form builders.

= 2.0.0 =
* Add your EmailOctopus API key and interact directly with your lists.
* A brand new form builder with advanced styling options.
* Shortcode support, meaning you can add your forms anywhere.
* Customise form messages.
* Support the new WordPress editing experience, Gutenberg.

= 1.4.0 =
* Add an option to include a consent checkbox. Useful for GDPR!
* Reduce false positives when detecting bots.
* Bug fixes.

= 1.3.5 =
* Allow the first and last name fields to be toggled individually.

= 1.3.4 =
* Bug fixes.

= 1.3.3 =
* Further improve support for multiple forms.

= 1.3.2 =
* Improve support for multiple forms.
* Improve setup process.

= 1.3.1 =
* Improve bot detection.

= 1.3.0 =
* Support multiple forms on a page.
* Trigger a JavaScript event (emailoctopus.success) on successful form submission.
* Bug fixes.

= 1.2.0 =
* Multilingual support.
* Improved documentation.

= 1.1.0 =
* Add an option to set a URL that a new subscriber is redirected to.

= 1.0.1 =
* Prevent browser autocomplete from looking like a bot submission.

= 1.0.0 =
* First release.

== Upgrade Notice ==

= 2.0.3 =
Improves the UX for managing lists. Fixes bugs preventing some forms from being properly updated.

= 2.0.2 =
Fixes a bug that causes form submissions to fail.

= 2.0.1 =
Improved UX in the classic and Gutenberg form builders.

= 2.0.0 =
Major release. Adds a brand new form builder with advanced customisation options. Supports the new WordPress editing experience, Gutenberg.
