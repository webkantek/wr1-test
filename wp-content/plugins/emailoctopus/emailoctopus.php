<?php
/**
 * The plugin bootstrap file.
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @category  EmailOctopus
 * @package   EmailOctopus
 * @author    EmailOctopus <contact@emailoctopus.com>
 * @copyright 2018 EmailOctopus
 * @license   https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPL-2.0+
 * @link      https://emailoctopus.com
 *
 * @wordpress-plugin
 * Plugin Name:       EmailOctopus
 * Plugin URI:        https://emailoctopus.com
 * Description:       Email marketing widget; easily add subscribers to an EmailOctopus list. Fully customisable.
 * Version:           2.0.3
 * Author:            EmailOctopus
 * Author URI:        https://emailoctopus.com
 * Text Domain:       emailoctopus
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

if (!defined('ABSPATH')) {
    die('No direct access.');
}

define('EMAILOCTOPUS_VERSION', '2.0.3');

/**
 * Main initialization class for EmailOctopus.
 *
 * @category EmailOctopus
 * @package  EmailOctopus
 * @author   EmailOctopus <contact@emailoctopus.com>
 * @license  GPL-2.0+
 * @link     https://emailoctopus.com
 *
 * @since 2.0.0
 */
class EmailOctopus
{
    /**
     * Holds the class instance.
     *
     * @since  2.0.0
     * @access static
     * @var    EmailOctopus $instance
     */
    private static $instance = null;

    /**
     * Holds the slug to the admin panel page
     *
     * @since 2.0.0
     * @access static
     * @var string $slug
     */
    private static $slug = 'emailoctopus';

    /**
     * Holds the URL to the admin panel page
     *
     * @since 2.0.0
     * @access static
     * @var string $url
     */
    private static $url = '';

    /**
     * Holds the URL to the admin panel page
     *
     * @since 2.0.0
     * @access static
     * @var string $basename
     */
    private static $basename = '';

    /**
     * Retrieve a class instance.
     *
     * Retrieve a class instance.
     *
     * @since 2.0.0
     * @access static
     *
     * @return MPSUM_Updates_Manager Instance of the class.
     */
    public static function get_instance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Class constructor.
     *
     * Set up internationalization, auto-loader, and plugin initialization.
     *
     * @since  2.0.0
     * @access private
     */
    private function __construct()
    {
        // Register loader for class instantiation
        spl_autoload_register(array($this, 'loader'));

        // Notice for API key
        add_action('admin_notices', array($this, 'api_key_admin_notice'));

        // Init internationalization
        load_plugin_textdomain('emailoctopus', false, dirname(plugin_basename(__FILE__)) . '/languages/');

        // Add Ajax param to saved dismised notice
        add_action('wp_ajax_emailoctopus_dismissed_notice', array($this, 'ajax_save_dismissed_state'));

        // Perform admin functions
        new EmailOctopus_Admin();

        // Register shortcode
        add_shortcode('emailoctopus', array($this, 'shortcode'));

        // Register custom content filter
        add_action('init', array($this, 'custom_content'));

        // Include widget
        add_action('widgets_init', array($this, 'include_widget'));

        // Register custom scripts and styles for the front end
        add_action('wp_enqueue_scripts', array($this, 'add_scripts_and_styles'));

        // Ajax for front-end form submission
        add_action('wp_ajax_submit_frontend_form', array($this, 'ajax_form_submission'));
        add_action('wp_ajax_nopriv_submit_frontend_form', array($this, 'ajax_form_submission'));
    }

    /**
     * Retrieve the plugin basename.
     *
     * Retrieve the plugin basename.
     *
     * @since 2.0.0
     * @access static
     *
     * @return string plugin basename
     */
    public static function get_plugin_basename()
    {
        return plugin_basename(__FILE__);
    }

    /**
     * Add an API key dismissable notice if no API is set
     *
     * @since  2.0.0
     * @access public
     */
    public function api_key_admin_notice()
    {
        $api_key = get_option('emailoctopus_api_key', false);
        $dismissed = get_option('emailoctopus_admin_dismiss', false);
        if ($dismissed === 'dismissed') {
            return;
        }
        if ($api_key) {
            return;
        } ?>
        <div class="emailoctopus-notice notice notice-warning is-dismissible">
            <p><?php printf(__('Please enter an API key for %s', 'emailoctopus'), sprintf('<a href="%s">EmailOctopus</a>', esc_url(self::get_url()))); ?></p>
        </div>
        <?php
    }

    public function shortcode($atts)
    {
        $args = shortcode_atts(array(
            'form_id' => 0,
            'show_title' => 'true',
            'show_summary' => 'true',
        ), $atts);
        $form_id = absint($args['form_id']);
        $show_title = $args['show_title'];
        $show_description = $args['show_summary'];
        $tabindex = 100;

        // Pre load some variables
        $form_data = EmailOctopus_Utils::get_form_data($form_id);
        if (!empty($form_data)):
        $appearance = EmailOctopus_Utils::get_form_meta($form_id, 'appearance');
        $consent = EmailOctopus_Utils::get_form_meta($form_id, 'consent_checkbox');
        $redirect = EmailOctopus_Utils::get_form_meta($form_id, 'redirect_checkbox');
        $redirect_url = EmailOctopus_Utils::get_form_meta($form_id, 'redirect_url');
        $consent_content = EmailOctopus_Utils::get_form_meta($form_id, 'consent_content');
        $classname = EmailOctopus_Utils::get_form_meta($form_id, 'form_class');
        $branding = EmailOctopus_Utils::get_form_meta($form_id, 'branding');
        $button_color = EmailOctopus_Utils::get_form_meta($form_id, 'button_color');
        $button_text_color = EmailOctopus_Utils::get_form_meta($form_id, 'button_text_color');
        $custom_fields = EmailOctopus_Utils::get_custom_fields($form_id);
        $list_id = $form_data['list_id'];

        // Get Messages
        $message_submit = EmailOctopus_Utils::get_form_meta($form_id, 'message_submit');
        $message_success = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_success'));
        $message_missing_email = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_missing_email'));
        $message_invalid_email = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_invalid_email'));
        $message_bot = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_bot'));
        $message_consent_required = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_consent_required'));
        $message_unknown = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_unknown'));

        // Get class names and allow them to be filterable
        $main_class_names = array(
            'emailoctopus-form-wrapper'
        );

        // Adding custom styles
        $main_class_styles = array();
        switch ($appearance) {
            case 'custom':
                $main_class_names[] = 'emailoctopus-custom-colors';
                $main_class_styles[] = sprintf(
                    'background: %s !important; ',
                    EmailOctopus_Utils::get_form_meta($form_id, 'background_color')
                );
                $main_class_styles[] = sprintf(
                    'color: %s !important;',
                    EmailOctopus_Utils::get_form_meta($form_id, 'text_color')
                );
                break;
            case 'light':
                $main_class_names[] = 'emailoctopus-theme-light';
                break;
            case 'dark':
                $main_class_names[] = 'emailoctopus-theme-dark';
                break;
        }
        if (!empty($classname)) {
            $classname = explode(' ', $classname);
            foreach ($classname as $class) {
                $main_class_names[] = $class;
            }
        }
        ob_start();
        include $this::get_plugin_dir('templates/form.php');

        return ob_get_clean();
        endif;
    }

    /**
    * Saves dismissed state in an option
    *
    * @since  2.0.0
    * @access public
    */
    public function ajax_save_dismissed_state()
    {
        update_option('emailoctopus_admin_dismiss', 'dismissed');
        die('');
    }

    /**
     * Return the URL to the admin panel page.
     *
     * Return the URL to the admin panel page.
     *
     * @since 2.0.0
     * @access static
     *
     * @return string URL to the admin panel page.
     */
    public static function get_url()
    {
        $url = self::$url;
        if (empty($url)) {
            $url = add_query_arg(array('page' => self::$slug), admin_url('index.php'));
            self::$url = $url;
        }

        return $url;
    }

    /**
     * Return the absolute path to an asset.
     *
     * Return the absolute path to an asset based on a relative argument.
     *
     * @param string $path Relative path to the asset.
     *
     * @since  5.0.0
     * @access static
     *
     * @return string Absolute path to the relative asset.
     */
    public static function get_plugin_dir($path = '')
    {
        $dir = rtrim(plugin_dir_path(__FILE__), '/');
        if (!empty($path) && is_string($path)) {
            $dir .= '/' . ltrim($path, '/');
        }

        return $dir;
    }

    /**
     * Return the web path to an asset.
     *
     * Return the web path to an asset based on a relative argument.
     *
     * @since 2.0.0
     * @access static
     *
     * @param string $path Relative path to the asset.
     * @return string Web path to the relative asset.
     */
    public static function get_plugin_url($path = '')
    {
        $dir = rtrim(plugin_dir_url(__FILE__), '/');
        if (!empty($path) && is_string($path)) {
            $dir .= '/' . ltrim($path, '/');
        }

        return $dir;
    }

    /**
     * Auto-loads classes.
     *
     * Auto-load classes that belong to this plugin.
     *
     * @since 2.0.0
     * @access private
     *
     * @param string $class_name The name of the class.
     */
    private function loader($class_name)
    {
        $class_name = 'class-' . str_replace('_', '-', strtolower($class_name));
        if (
            class_exists($class_name, false) ||
            !strpos($class_name, 'emailoctopus')
        ) {
            return;
        }

        $file = self::get_plugin_dir("includes/{$class_name}.php");
        if (file_exists($file)) {
            include_once($file);
        }
    }

    /**
     * Custom content filter.
     *
     * Allow the_content filtering without other plugins injecting content.
     *
     * @since 2.0.0
     * @access public
     * @see __construct
     */
    public function custom_content()
    {
        if (!has_filter('emailoctopus_the_content', 'wptexturize')) {
            add_filter('emailoctopus_the_content', 'wptexturize');
            add_filter('emailoctopus_the_content', 'convert_smilies');
            add_filter('emailoctopus_the_content', 'convert_chars');
            add_filter('emailoctopus_the_content', 'wpautop');
            add_filter('emailoctopus_the_content', 'shortcode_unautop');
            add_filter('emailoctopus_the_content', 'prepend_attachment');
            $vidembed = new WP_Embed();
            add_filter(
                'emailoctopus_the_content',
                array(&$vidembed, 'run_shortcode'),
                8
            );
            add_filter(
                'emailoctopus_the_content',
                array(&$vidembed, 'autoembed'),
                8
            );
            add_filter('emailoctopus_the_content', 'do_shortcode', 11);
        }
    }

    /**
     * Custom content filter.
     *
     * Allow the_content filtering without other plugins injecting content.
     *
     * @since 2.0.0
     * @access public
     * @see __construct
     */
    public function include_widget()
    {
        $widget = new EmailOctopus_Widget();
        register_widget($widget);

        if (get_option('widget_emailoctopus', false)) {
            $legacy_widget = new EmailOctopus_Widget_Legacy;
            register_widget($legacy_widget);
        }
    }

    /**
     * Inject custom styles and scripts on the front-end
     *
     * @since 2.0.0
     * @access public
     * @see __construct
     */
    public function add_scripts_and_styles()
    {
        // Ensure script debug allows non-minified scripts
        $is_minified = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

        // Load frontend script
        wp_enqueue_script(
            'emailoctopus_front_end',
            EmailOctopus::get_plugin_url('js/front-end' . $is_minified . '.js'),
            array('jquery'),
            EMAILOCTOPUS_VERSION,
            true
        );
        wp_localize_script('emailoctopus_front_end', 'emailoctopus', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'sending' => __('Sending', 'emailoctopus'),
        ));

        // Load frontend style
        wp_enqueue_style(
            'emailoctopus_front_end',
            EmailOctopus::get_plugin_url('css/front-end' . $is_minified . '.css'),
            array(),
            EMAILOCTOPUS_VERSION
        );
    }

    public function ajax_form_submission()
    {
        $api = sprintf(
            'https://emailoctopus.com/api/1.5/lists/%s/contacts',
            sanitize_text_field($_POST['list_id'])
        );
        $form_data = $_POST['form_data'];
        $body = array(
            'api_key' => get_option('emailoctopus_api_key'),
        );
        $fields = array();
        $custom_fields = array();
        foreach ($form_data as $index => $data) {
            if ($data['name'] === 'EmailAddress') {
                $email = $data['value'];
                if (!is_email($email)) {
                    wp_send_json(array(
                        'errors' => true,
                        'message' => 'message_invalid_email'
                    ));
                    exit;
                }
                $body['email_address'] = $data['value'];
            } else {
                $fields[$data['name']] = $data['value'];
            }
        }
        if (!empty($fields)) {
            $body['fields'] = $fields;
        }
        $response = wp_remote_post($api, array('body' => $body));
        $response_body = wp_remote_retrieve_body($response);
        $response_body = json_decode($response_body);

        // Error detected - send message
        if (isset($response_body->error)) {
            wp_send_json(array(
                'errors' => true,
                'message' => $response_body->error->message
            ));
        }

        // Contact pending, send response
        wp_send_json(array('errors' => false, 'message' => 'message_success'));
    }
}
add_action('plugins_loaded', 'emailoctopus_loaded');
function emailoctopus_loaded()
{
    EmailOctopus::get_instance();
    new EmailOctopus_Gutenberg();
}
// Perform redirect upon activation
add_action('activated_plugin', 'emailoctopus_redirect_to_plugin');
/**
 * Redirect when plugin is activated
 *
 * @since  2.0.0
 * @access public
 *
 * @param string $plugin Plugin slug
 */
 function emailoctopus_redirect_to_plugin($plugin)
 {
     if ($plugin === plugin_basename(__FIlE__)) {
         exit(wp_redirect(admin_url('admin.php?page=emailoctopus')));
     }
 }
