<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

class EmailOctopus_Widget extends WP_Widget
{
    /**
     * Unique identifier for the widget, used as the text domain when
     * internationalizing strings of text.
     *
     * @var string
     */
    protected $widget_slug = 'emailoctopus_2';

    /**
     * Specifies the classname and description, instantiates the widget, loads
     * localization files, and includes necessary stylesheets and JavaScript.
     */
    public function __construct()
    {
        // Instantiate main class
        parent::__construct(
            $this->get_widget_slug(),
            __('EmailOctopus Form', 'emailoctopus'),
            array(
                'classname'  => $this->get_widget_slug().'-class',
                'description' => __(
                    'A widget to display your EmailOctopus forms.',
                    'emailoctopus'
                )
            )
        );
    }


    /**
     * Returns the widget slug.
     *
     * @return Plugin slug.
     */
    public function get_widget_slug()
    {
        return $this->widget_slug;
    }

    public function form($instance)
    {
        $forms = EmailOctopus_Utils::get_forms();

        // Check API
        $api_key = get_option('emailoctopus_api_key', false);
        if (!EmailOctopus_Utils::is_valid_api_key($api_key)) {
            ?>
            <p>
                <?php esc_html_e('Your API key is invalid.', 'emailoctopus');
            echo "&nbsp;";
            printf('<a href="%s">%s</a>', esc_url(EmailOctopus::get_url()), __('Please connect to our API.', 'emailoctopus')); ?>
            </p>
            <?php
            return;
        }

        // Check forms
        if (empty($forms)) {
            echo __('There are no forms to select', 'emailoctopus');

            return;
        }

        $title = !empty($instance['title']) ? $instance['title'] : '';
        $selected_form = !empty($instance['form_id']) ? $instance['form_id'] : 0;
        $show_description = !empty($instance['description']) ? $instance['description'] : 'true'; ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'emailoctopus'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <select name="<?php echo esc_attr($this->get_field_name('emailoctopus-form-select')); ?>">
                <option value="0"><?php esc_html_e('Select a form', 'emailoctopus'); ?></option>
                <?php
                foreach ($forms as $index => $form_data) {
                    printf('<option value="%s" %s">%s</option>', $form_data['form_id'], selected($selected_form, $form_data['form_id'], false), $form_data['title']);
                } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('description')); ?>">
            <input type="hidden" name="<?php echo esc_attr($this->get_field_name('description')); ?>" value="false">
            <?php esc_html_e('Show form summary', 'emailoctopus'); ?> <input class="widefat" id="<?php echo esc_attr($this->get_field_id('description')); ?>" name="<?php echo esc_attr($this->get_field_name('description')); ?>" type="checkbox" value="true" <?php checked('true', $show_description); ?> value="false"></label>
        </p>
        <?php
    }

    /**
     * Outputs the content of the widget.
     *
     * @param array args     The array of form elements
     * @param array instance The current instance of the widget
     */
    public function widget($args, $instance)
    {
        // Check if there is a cached output
        $cache = wp_cache_get($this->get_widget_slug(), 'emailoctopus_widget');

        if (!is_array($cache)) {
            $cache = array();
        }

        extract($args, EXTR_SKIP);

        if (isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }

        if (isset($cache[$args['widget_id']])) {
            return print $cache[$args['widget_id']];
        }

        $show_widget_title = $instance['title'];
        $form_id = absint($instance['form_id']);
        $show_description = sanitize_text_field($instance['description']);
        $tabindex = 200;
        $widget_output = $before_widget;
        $widget_output .= '<div class="emailoctopus-email-widget">';
        if (0 !== $form_id) {
            // Preload some variables
            $form_data = EmailOctopus_Utils::get_form_data($form_id);
            if (!empty($form_data)):
                $appearance = EmailOctopus_Utils::get_form_meta($form_id, 'appearance');
            $consent = EmailOctopus_Utils::get_form_meta($form_id, 'consent_checkbox');
            $redirect = EmailOctopus_Utils::get_form_meta($form_id, 'redirect_checkbox');
            $redirect_url = EmailOctopus_Utils::get_form_meta($form_id, 'redirect_url');
            $consent_content = EmailOctopus_Utils::get_form_meta($form_id, 'consent_content');
            $classname = EmailOctopus_Utils::get_form_meta($form_id, 'form_class');
            $branding = EmailOctopus_Utils::get_form_meta($form_id, 'branding');
            $button_color = EmailOctopus_Utils::get_form_meta($form_id, 'button_color');
            $button_text_color = EmailOctopus_Utils::get_form_meta($form_id, 'button_text_color');
            $custom_fields = EmailOctopus_Utils::get_custom_fields($form_id);
            $list_id = $form_data['list_id'];

            // Get Messages
            $message_submit = EmailOctopus_Utils::get_form_meta($form_id, 'message_submit');
            $message_success = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_success'));
            $message_missing_email = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_missing_email'));
            $message_invalid_email = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_invalid_email'));
            $message_bot = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_bot'));
            $message_consent_required = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_consent_required'));
            $message_unknown = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_unknown'));

            // Get class names and allow them to be filterable
            $main_class_names = array(
                    'emailoctopus-form-wrapper'
                );

            // Adding custom styles
            $main_class_styles = array();
            switch ($appearance) {
                case 'custom':
                    $main_class_names[] = 'emailoctopus-custom-colors';
                    $main_class_styles[] = sprintf(
                        'background: %s !important; ',
                        EmailOctopus_Utils::get_form_meta(
                            $form_id,
                            'background_color'
                        )
                    );
                    $main_class_styles[] = sprintf(
                        'color: %s !important;',
                        EmailOctopus_Utils::get_form_meta(
                            $form_id,
                            'text_color'
                        )
                    );
                    break;
                case 'light':
                    $main_class_names[] = 'emailoctopus-theme-light';
                    break;
                case 'dark':
                    $main_class_names[] = 'emailoctopus-theme-dark';
                    break;
            }
            if (!empty($classname)) {
                $classname = explode(' ', $classname);
                foreach ($classname as $class) {
                    $main_class_names[] = $class;
                }
            }

            $show_title = empty($instance['title']) ? 'false' : 'true';
            ob_start();
            include EmailOctopus::get_plugin_dir('templates/form.php');
            $widget_output .= ob_get_clean();
            endif;
        }
        $widget_output .= '</div>';
        $widget_output .= $after_widget;

        $cache[$args['widget_id']] = $widget_output;

        wp_cache_set($this->get_widget_slug(), $cache, 'emailoctopus_widget');

        echo $widget_output;
    }

    /**
     * Flushes the widget's cache.
     */
    public function flush_widget_cache()
    {
        wp_cache_delete($this->get_widget_slug(), 'emailoctopus_widget');
    }

    /**
     * Processes the widget's options to be saved.
     *
     * @param array new_instance The new instance of values to be generated via the update.
     * @param array old_instance The previous instance of values before the update.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array(
            'title' =>
                sanitize_text_field($new_instance['title']),
            'form_id' =>
                sanitize_text_field($new_instance['emailoctopus-form-select']),
            'description' =>
                sanitize_text_field($new_instance['description']),
        );

        return $instance;
    }
}
