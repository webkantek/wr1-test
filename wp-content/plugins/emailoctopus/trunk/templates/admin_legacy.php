<?php
$title = isset($instance['title']) ? $instance['title'] : __('Subscribe to my list', 'emailoctopus');
$list_id = isset($instance['list_id']) ? $instance['list_id'] : '';
$include_referral = isset($instance['include_referral']) ? (bool) $instance['include_referral'] : true;
$success_redirect_url = isset($instance['success_redirect_url']) ? $instance['success_redirect_url'] : '';
$redirect_on_success = isset($instance['redirect_on_success']) ? (bool) $instance['redirect_on_success'] : false;
$consent_message = isset($instance['consent_message']) ? strip_tags($instance['consent_message']) : 'I consent to receiving your weekly newsletter and special offers via email.';
$include_consent = isset($instance['include_consent']) ? (bool) $instance['include_consent'] : false;

$include_first_name_field = false;
$include_last_name_field = false;

if (array_key_exists('include_first_name_field', $instance)) {
    $include_first_name_field = (bool) $instance['include_first_name_field'];
}

if (array_key_exists('include_last_name_field', $instance)) {
    $include_last_name_field = (bool) $instance['include_last_name_field'];
}

settings_errors();
?>
<div class="emailoctopus-widget-options">
    <p>
        <label for="<?php echo $this->get_field_id('list_id'); ?>"><?php _e('List ID:', 'emailoctopus'); ?></label>
        <input id="<?php echo $this->get_field_id('list_id'); ?>" name="<?php echo $this->get_field_name('list_id'); ?>" type="text" value="<?php echo esc_attr($list_id); ?>" class="widefat">
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'emailoctopus'); ?></label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" class="widefat">
    </p>
    <p>
        <input type="hidden" name="<?php echo $this->get_field_name('include_first_name_field'); ?>" value="false"  />
        <input type="checkbox" <?php checked($include_first_name_field, true); ?> id="<?php echo $this->get_field_id('include_first_name_field'); ?>" name="<?php echo $this->get_field_name('include_first_name_field'); ?>" class="checkbox" value="true" />
        <label for="<?php echo $this->get_field_id('include_first_name_field'); ?>"><?php _e('Include optional first name field', 'emailoctopus'); ?></label>
    </p>
    <p>
        <input type="hidden" name="<?php echo $this->get_field_name('include_last_name_field'); ?>" value="false"  />
        <input type="checkbox" <?php checked($include_last_name_field, true); ?> id="<?php echo $this->get_field_id('include_last_name_field'); ?>" name="<?php echo $this->get_field_name('include_last_name_field'); ?>" class="checkbox" value="true" />
        <label for="<?php echo $this->get_field_id('include_last_name_field'); ?>"><?php _e('Include optional last name field', 'emailoctopus'); ?></label>
    </p>
    <p>
        <input type="hidden" name="<?php echo $this->get_field_name('include_consent'); ?>" value="false"  />
        <input type="checkbox" <?php checked($include_consent, true); ?> id="<?php echo $this->get_field_id('include_consent'); ?>" name="<?php echo $this->get_field_name('include_consent'); ?>" class="checkbox include-consent" value="true" />
        <label for="<?php echo $this->get_field_id('include_consent'); ?>"><?php _e('Include a consent checkbox', 'emailoctopus'); ?></label>
    </p>
    <p>
        <textarea id="<?php echo $this->get_field_id('consent_message'); ?>"
                  name="<?php echo $this->get_field_name('consent_message'); ?>"
                  class="widefat consent-message" rows="5"
        ><?php echo $consent_message; ?></textarea>
    </p>
    <p>
        <input type="hidden" name="<?php echo $this->get_field_name('redirect_on_success'); ?>" value="false"  />
        <input type="hidden" name="<?php echo $this->get_field_name('include_referral'); ?>" value="true"  />
        <input type="checkbox" <?php checked($redirect_on_success, true); ?> id="<?php echo $this->get_field_id('redirect_on_success'); ?>" name="<?php echo $this->get_field_name('redirect_on_success'); ?>" class="checkbox redirect-on-success" value="true" />
        <label for="<?php echo $this->get_field_id('redirect_on_success'); ?>"><?php _e('Instead of thanking the user for subscribing, redirect them to a URL', 'emailoctopus'); ?></label>
    </p>
    <p class="success-redirect-url-wrapper">
        <input type="hidden" name="<?php echo $this->get_field_name('success_redirect_url'); ?>" value=""  />
        <input id="<?php echo $this->get_field_id('success_redirect_url'); ?>" name="<?php echo $this->get_field_name('success_redirect_url'); ?>" type="text" value="<?php echo esc_attr($success_redirect_url); ?>" placeholder="URL" class="widefat success-redirect-url">
    </p>
</div>
