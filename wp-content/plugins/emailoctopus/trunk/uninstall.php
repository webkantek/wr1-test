<?php
/**
 * Uninstall script
 *
 * Uninstall script for EmailOctopus.
 *
 * @package WordPress
 * @since 2.0.0
 */
if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit();
}
delete_option('emailoctopus_api_key');
delete_option('emailoctopus_admin_dismiss');
delete_option('emailoctopus_table_version');

// Get rid of the tables used
global $wpdb;
$tables = array(
    $wpdb->prefix . 'emailoctopus_forms',
    $wpdb->prefix . 'emailoctopus_forms_meta',
    $wpdb->prefix . 'emailoctopus_custom_fields',
);
foreach ($tables as $table) {
    $sql = "drop table if exists $table";
    $wpdb->query($sql);
}
