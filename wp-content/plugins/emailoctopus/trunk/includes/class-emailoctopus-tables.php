<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

/**
 * EmailOctopus Table Creation and storing
 *
 * @package WordPress
 * @since 2.0.0
 * @access public
 */
class EmailOctopus_Tables
{
    /**
     * Holds version number of the table
     *
     * @since 2.0.0
     * @access private
     * @var string $version
     */
    private $version = '1.0.3';

    /**
     * Holds version number of the table
     *
     * @since 2.0.0
     * @access private
     * @var bool $table_exists
     */
    private static $table_exists = false;

    public function __construct()
    {
        $table_version = get_option('emailoctopus_table_version', '0');
        if (version_compare($table_version, $this->version) < 0) {
            if (!$this->tables_exists()) {
                $this->build_tables();
                update_option('emailoctopus_table_version', $this->version);
            }
        }
    }

    /**
     * Creates the EmailOctopus tables
     *
     * Creates the EmailOctopus tables
     *
     * @since 2.0.0
     * @access private
     */
    private function build_tables()
    {
        global $wpdb;
        $table_forms = $wpdb->prefix . 'emailoctopus_forms';
        $table_meta = $wpdb->prefix . 'emailoctopus_forms_meta';
        $table_custom_fields = $wpdb->prefix . 'emailoctopus_custom_fields';

        // Get collation - From /wp-admin/includes/schema.php
        $charset_collate = '';
        if (!empty($wpdb->charset)) {
            $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
        }
        if (!empty($wpdb->collate)) {
            $charset_collate .= " COLLATE $wpdb->collate";
        }

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $sql = "CREATE TABLE {$table_forms} (
                        form_id BIGINT(20) NOT NULL AUTO_INCREMENT,
                        user_id BIGINT(20) NOT NULL DEFAULT 0,
                        title longtext NOT NULL,
                        description longtext NOT NULL,
                        list VARCHAR(255) NOT NULL,
                        list_id VARCHAR(255) NOT NULL,
                        status VARCHAR(255) NOT NULL,
                        date DATETIME NOT NULL,
                        PRIMARY KEY  (form_id)
                        ) {$charset_collate};";
        dbDelta($sql);

        $sql = "CREATE TABLE {$table_meta} (
            meta_id BIGINT(20) NOT NULL AUTO_INCREMENT,
            form_id BIGINT(20) NOT NULL DEFAULT 0,
            meta_key VARCHAR(255) NOT NULL,
            meta_value longtext NOT NULL,
            PRIMARY KEY  (meta_id)
            ) {$charset_collate};";
        dbDelta($sql);

        $sql = "CREATE TABLE {$table_custom_fields} (
            id BIGINT(20) NOT NULL AUTO_INCREMENT,
            form_id BIGINT(20) NOT NULL DEFAULT 0,
            tag VARCHAR(255) NOT NULL,
            type VARCHAR(255) NOT NULL,
            label VARCHAR(255) NOT NULL,
            `order` BIGINT(20) NOT NULL DEFAULT 0,
            PRIMARY KEY  (id)
            ) {$charset_collate};";
        dbDelta($sql);
    }

    /**
     * Checks whether the EmailOctopus tables exist
     *
     * @return boolean True if tables exists, otherwise false
     */
    private function tables_exists()
    {
        if (self::$table_exists) {
            return;
        }

        global $wpdb;
        $table_forms = $wpdb->prefix . 'emailoctopus_forms';
        $table_meta = $wpdb->prefix . 'emailoctopus_forms_meta';
        $table_custom_fields = $wpdb->prefix . 'emailoctopus_custom_fields';
        $database_name = DB_NAME;
        $query = "SHOW TABLES FROM $database_name WHERE Tables_in_$database_name LIKE '$table_forms' AND Tables_in_$database_name LIKE '$table_meta' and Tables_in_$database_name LIKE '$table_custom_fields'";
        self::$table_exists = (bool)$wpdb->get_var($query);

        return self::$table_exists;
    }
}
