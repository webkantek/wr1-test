��    ;      �  O   �           	       
     *   )     T     e     m     z     �     �  
   �  #   �     �     �       	   0     :     B     I     Q     X     d     i  	   w  
   �  
   �     �  	   �     �     �  
   �     �     �     �  
   �     �     �     �       	             )     7     E     M  	   V     `     g     n  	   |     �  
   �  
   �     �     �     �     �  
   �  �  �  
   �
     �
     �
  +   �
     �
               *     D     T     j     v     �     �     �     �     �     �     �     �     �     �     �     �     �                      
   $     /     >     G     U  
   b     m  	   y     �     �     �     �     �  0   �             	             #     )  	   8     B     X     e     v     �     �     �     �         *   7          !             %           1         &   "   #                          3      :       )                9               ;      $   6      
             4   (   /   +   -   	   8   2           5   .                  ,       0              '                            API Key Add new form Appearance Are you sure you want to delete this form? Background Color Builder Button Color Button Text Color Button color Button text color Class name Column header for formsDescription Column header for formsID Column header for formsList Column header for formsTitle Connected Copied! Custom Default Delete Deleting... Edit Email address Enter URL First name Form title Forms Last name List List ID: Loading... Messages No title Not Connected Powered by Powered by  Preview Save Save Changes Saving... Select a Form Select a form Select a list Sending Settings Shortcode Status Submit Submit button Subscribe Success message Text Color Text color Thanks for subscribing! Theme Title: Unknown error subscribed Project-Id-Version: EmailOctopus
Report-Msgid-Bugs-To: https://wordpress.org/plugins/emailoctopus/
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-11-14 17:52-0800
Language-Team: EmailOctopus <contact@emailoctopus.com>
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
POT-Creation-Date: 
X-Generator: Poedit 2.2
Last-Translator: 
Language: it
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Chiave API Aggiungi nuovo form Aspetto Sei sicuro di voler cancellare questo form? Colore di sfondo Costruttore Colore Pulsante Colore testo del pulsante Colore pulsante Colore testo pulsante Nome classe Descrizione ID Lista Titolo Connesso Copiato! Personalizzato Predefinito Elimina Cancellando... Modifica Indirizzo email Inserisci URL Nome Titolo Modulo Forms Cognome Lista ID Elenco: Caricamento... Messaggi Nessun titolo Non connesso Offerto da Powered by  Anteprima Salva Salva modifiche Sto salvando... Seleziona un Form Seleziona un Modulo Selezionare lista stella di statica e spettacolo Invio Impostazioni Shortcode Stato Invia Pulsante Invio Iscriviti Messaggio di Successo Colore testo Colore del testo Grazie per essersi iscritto! Tema Titolo: Errore sconosciuto iscritto 