<?php


include_once('InstagramCarouselFeed_LifeCycle.php');

class InstagramCarouselFeed_Plugin extends InstagramCarouselFeed_LifeCycle {


    /******************************************
    * Instagram Feed Carousel - settings page data
    * @return array of option meta data.
    * See: http://plugin.michael-simpson.com/?page_id=31
    ******************************************/
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades. 
            /*'ATextInput' => array(__('Enter in some text', 'my-awesome-plugin')),
            'AmAwesome' => array(__('I like this awesome plugin', 'my-awesome-plugin'), 'false', 'true'),
            'CanDoSomething' => array(__('Which user role can do something', 'my-awesome-plugin'),
                                        'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone')*/

            'Username' => array(__('Enter your Instagram username (e.g. "JohnSmith_23")', 'my-awesome-plugin')),
            'WR1ClubID' => array(__('Enter WR1 Club ID (e.g. "155")', 'my-awesome-plugin')),
            'ShowFeed' => array(__('Show Instagram feed?', 'my-awesome-plugin'), 'false', 'true')
        );
    }


    //    protected function getOptionValueI18nString($optionValue) {
    //        $i18nValue = parent::getOptionValueI18nString($optionValue);
    //        return $i18nValue;
    //    }


    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }


    public function getPluginDisplayName() {
        return 'Instagram Carousel Feed';
    }


    protected function getMainPluginFileName() {
        return 'instagram-carousel-feed.php';
    }


    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('instagram_carousel');
        //        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
        //            `id` INTEGER NOT NULL");
    }


    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade() {
    }


    /******************************************
    * Instagram Feed Carousel - Get all data 
    ******************************************/
    public function getInstagramInfo() {

        /******************************************
        * Call .js file 
        ******************************************/
        /*wp_enqueue_script(
            'instagram-js',
            plugins_url( 'js/instagram-feed.js', __FILE__)
        );*/

        /******************************************
        * Database labels - located in wp-options table
        ******************************************/
        $username = 'InstagramCarouselFeed_Plugin_Username';
        $wr1_clubID = 'InstagramCarouselFeed_Plugin_WR1ClubID';
        $showFeed = 'InstagramCarouselFeed_Plugin_ShowFeed';


        /******************************************
        * Check database to see if Club ID exists
        ******************************************/
        $clubID = get_option($wr1_clubID);

        if ( $clubID != FALSE ) {
            //echo 'value exists';

            /******************************************
            * Get data
            ******************************************/
            /*$api_test_url = 'https://mobile-test.wr1.com/webapi/clubs/' . $clubID . '/posts?source=artist';
            $api_url = 'https://mobile.wr1.com/webapi/clubs/' . $clubID . '/posts?source=artist';*/

            $api_test_url = 'https://mobile-test.wr1.com/webapi/clubs/' . $clubID . '/posts/instagram/';
            $api_url = 'https://mobile.wr1.com/webapi/clubs/' . $clubID . '/posts/instagram/';

            // Create a stream
            $opts = array(
                'http' => array(
                    'method' => "GET",
                    //'header' => "Application-Key: 6CAEA9D43AC5DF96CEF21C78A73A3",
                    'header' => "Web-Key: CAA857EE6819CC968752C98A9DB43",
                    //'header' => "Content-Type: application/json"
                )
            );
            
            $context = stream_context_create($opts);
            
            // Open the file using the HTTP headers set above
            $file = file_get_contents($api_url, false, $context);
            $jsonObj = json_encode($file);
            //echo $jsonObj;

            $posts_data_parse = (array) json_decode($file);
            //echo $posts_data_parse;
            //print_r($posts_data_parse);


            /******************************************
            * Get posts array
            ******************************************/
            $instagram_posts_array = $posts_data_parse['items'];
            //var_dump($instagram_posts_array);
            $club_id_error = 'false';

            $b = sizeof($instagram_posts_array);
            //echo 'array size: '.$b;

            if (empty($instagram_posts_array)) {
                //echo 'Error: No posts or wrong Club ID';
                $club_id_error = 'true';
            }


        
            $script_params = array(
                'username' => get_option($username),
                'wr1_clubID' => get_option($wr1_clubID),
                'showFeed' => get_option($showFeed)
            );

            wp_localize_script('admin-js', 'instagram_options', $script_params);
            wp_localize_script('instagram-js', 'instagram_params', $script_params);
            wp_localize_script('instagram-js', 'instagram_options', $instagram_posts_array);
            wp_localize_script('instagram-js', 'club_id_error', $club_id_error);

        }


        /******************************************
        * OLD - Check database to see if a username exists
        ******************************************/
        //if ( isset($username) ) {
            //echo 'value exists';

            /******************************************
            * Add all data to an array
            * get_option('') => wp native db function
            ******************************************/
            /*$script_params = array(
                'username' => get_option($username),
                'showFeed' => get_option($showFeed)
            );*/

            /******************************************
            * Make php array avaialbe to front-end or admin page
            * wp_localize_script('', '', ''); => wp native function
            ******************************************/
            //echo json_encode($script_params);

            /*wp_localize_script('admin-js', 'instagram_options', $script_params);
            wp_localize_script('instagram-js', 'instagram_options', $script_params);*/
        //}

    }


    /******************************************
    * Instagram Feed Carousel - Add actions for plug-in
    ******************************************/
    public function addActionsAndFilters() {

        /******************************************
        * Add options ADMIN page
        * http://plugin.michael-simpson.com/?page_id=47
        ******************************************/
        // Examples:
            //add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));
            //add_action('admin_menu', array(&$this, 'addSettingsSubMenuPageToSettingsMenu'));

        add_action('admin_menu', array(&$this, 'addSettingsToDashboardMenuPage'));
        //add_action('admin_menu', 'my_plugin_menu');
        //add_action('admin_menu', array(&$this, 'getInstagramInfo'));
        

        /******************************************
        * Enqueue scripts and styles for ADMIN pages
        ******************************************/ 
        add_action('admin_enqueue_scripts', array(&$this, 'enqueueAdminPageStylesAndScripts'));
        add_action('admin_enqueue_scripts', array(&$this, 'getInstagramInfo'));
        // Example adding a script & style just for the options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        //        if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
        //            wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        //            wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        }


        /******************************************
        * Enqueue scripts and styles for PLUG-IN pages
        ******************************************/
        add_action('wp_enqueue_scripts', array(&$this, 'enqueueStylesAndScripts'));
        add_action('wp_enqueue_scripts', array(&$this, 'getInstagramInfo'));


        /******************************************
        * Add Actions & Filters 
        * Adding scripts & styles to all pages
        * http://plugin.michael-simpson.com/?page_id=37
        ******************************************/
        // Examples:
                //wp_enqueue_script('jquery');
                //wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
                //wp_enqueue_script('my-script', plugins_url('/js/instagram-feed.js', __FILE__));


        /******************************************
        * Register short codes
        * http://plugin.michael-simpson.com/?page_id=39
        ******************************************/
        add_shortcode('instagram-feed', array($this, 'addShortCode'));

        /******************************************
        * Register AJAX hooks
        * http://plugin.michael-simpson.com/?page_id=41
        ******************************************/
 
    }

    
    /******************************************
    * Instagram Feed Carousel - ALL
    * Add styles & scripts to ALL pages where feed will be rendered
    ******************************************/
    public function enqueueStylesAndScripts() {
        
        // Examples:
            //wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
            //wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        
        // CSS files:
        wp_enqueue_style('slick-css', plugins_url('/js/vendor/slick/slick.css', __FILE__));
        wp_enqueue_style('custom-css', plugins_url('/css/style.css', __FILE__));

        // JS files:
        wp_enqueue_script('jquery');
        wp_enqueue_script('slick', plugins_url('/js/vendor/slick/slick.min.js', __FILE__));
        wp_enqueue_script('instagram-js', plugins_url('/js/instagram-feed.js', __FILE__));

    }
     

    /******************************************
    * Instagram Feed Carousel - ADMIN
    * Add styles & scripts to ADMIN pages that plugin-in will be rendered
    ******************************************/
    public function enqueueAdminPageStylesAndScripts() {
        
        // Examples:
            //wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
            //wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));

        // CSS files:
        //wp_enqueue_style('slick-css', plugins_url('/js/vendor/slick/slick.css', __FILE__));
        wp_enqueue_style('custom-css', plugins_url('/css/style.css', __FILE__));

        // JS files:
        wp_enqueue_script('jquery');
        wp_enqueue_script('admin-js', plugins_url('/js/instagram-admin.js', __FILE__));
        //wp_enqueue_script('slick', plugins_url('/js/vendor/slick/slick.min.js', __FILE__));

    }


}
