/******************************************
* Instagram Feed Carousel
* Author: Ryan Mahabir
* Date: 2017-20-11
******************************************/
function instagramCarousel(element){
    
    /******************************************
	* Variables
	******************************************/
    this.username = 'username';
    this.$element = jQuery( element );
    this.$domNode = '#instagram-feed';
    this.$instagramCarousel = this.$element.find( this.$domNode );

    // 12 recent posts is the limit without Instagram access-token
    this.instagramLimit = 12;
    this.feedLimitDesktop = 6;
    this.feedLimitTablet = 4;
    this.feedLimitMobile = 1;

    this.instagramCaptionFlag = true;
    //this.instagramCaptionArray = [];

    /******************************************
	* WordPress user settings
	******************************************/
    this.init = function(name, feed) {
    	//console.log('username: ' + name + ' feed: ' + feed);
    	var flag = true;

    	//WIP - Need to solidify booleans.
    	if (feed === 'false') {
    		flag = false;
    		return;
    	}

		if (this.$instagramCarousel.length) {
			//console.log('found dom node');
			this.getInstagram(name, flag);
			return;
		}
        
    };

    /******************************************
	* WIP - Create DOM nodes
	******************************************/
	/*this.createDOM = function() {
		var container = document.createElement('div'),
			btn = document.createElement('a'),
			video = document.createElement('div'),
			img = document.createElement('img'),

			overlay = document.createElement('div'),
			overlayInfo = document.createElement('div'),
			caption = document.createElement('p'),
			comments = document.createElement('p'),
			likes = document.createElement('p');

		return;
	};*/


	/******************************************
	* Caption character limit global var
	******************************************/
	this.getCharacterFlag = function(flag) {
		this.instagramCaptionFlag = flag;
		return this.instagramCaptionFlag;
	};


	/******************************************
	* Caption rerender on window resize
	******************************************/
	/*this.reRenderInstagramCaption = function(flag) {
		var self = this;

		if (flag === true) {
			//console.log('Mobile orientation change. Truncate caption: ', flag);
		}
	};*/



	/******************************************
	* Add events
	******************************************/
	this.addEvents = function() {

		var self = this;

		jQuery(self.$instagramCarousel).on('init', function(breakpoint, slick){
			var clientWidth = breakpoint.target.clientWidth;

			console.log('Slider was initialized. Width:', clientWidth);


			if (clientWidth < 735) {
				var flag = self.getCharacterFlag(false);
				console.log('We are showing one slide, caption truncation: ', flag);
			}

		});

		
		/******************************************
		* Caption truncation on MOBILE ONLY orientation.
		******************************************/
		// Mobile orientation change
		/*jQuery( window ).on( "orientationchange", function( event ) {
			console.log(event.orientation);
		});

		jQuery( window ).resize(function() {
			//console.log(window.innerWidth);
		 	var windowWidth = window.innerWidth;

			if (windowWidth > 700) {
				self.reRenderInstagramCaption(true);
			}
		  
		});*/

	};


    /******************************************
	* Get JSON from Instagram API
	******************************************/
    this.getInstagram = function(username, flag) {

		var url = 'https://api.instagram.com/'+ username + '?__a=1',
			//url = 'https://api.instagram.com/v1/users/search?user_id=37980436&client_id=48f55ad9719140eea4769f8f6fae6b29',
    		self = this;

    	/******************************************
		* Get JSON feed from Instagram
		******************************************/
    	jQuery.getJSON( url, {
			format: 'json'
		})
		.fail(function(err) {
			console.log( 'Instagram error:', err.status );
		})
		.done(function( data ) {
			//console.log(data.graphql.user.id);

			/******************************************
			* Instagram data
			******************************************/
			var userID = data.graphql.user.id,
				username = data.graphql.user.username,
				userImages = data.graphql.user.edge_owner_to_timeline_media.edges,
				isPrivate = data.graphql.user.is_private;

			//console.log(userImages);
			/******************************************
			* Private Instagram account message.
			******************************************/
			if (isPrivate) {
				console.log('Private Instagram account: ', isPrivate);
				return;
			}

			/******************************************
			* Init Slick.js carousel.
			******************************************/
			self.$instagramCarousel.slick({
				slidesToShow: self.feedLimitDesktop,
				slidesToScroll: 3,
				autoplay: true,
				autoplaySpeed: 4000,

				// To use lazy loading, set a data-lazy attribute
				// on your img tags and leave off the src
				lazyLoad: 'ondemand',
				adaptiveHeight: false,
				centerMode: false,
				centerPadding: '0px',
  				variableWidth: false,
  				fade: false,
  				cssEase: 'linear',

				dots: false,
				infinite: true,
				speed: 300,

				responsive: [{
					// Desktop
					breakpoint: 1024,
					  	settings: {
					    	slidesToShow: self.feedLimitDesktop,
					    	slidesToScroll: 3,
					    	//infinite: false,
					    	//dots: false
					  	}
					},

					// Tablet - landscape
					{breakpoint: 800,
						settings: {
					    	slidesToShow: self.feedLimitTablet,
					    	slidesToScroll: 2
					  	}
					},

					// Mobile - landscape
					{breakpoint: 700,
						settings: {
					    	slidesToShow: self.feedLimitTablet,
					    	slidesToScroll: 2
					  	}
					},

					// Mobile - portrait
					{breakpoint: 636,
					  	settings: {
					    	slidesToShow: self.feedLimitMobile,
					   		slidesToScroll: 1
					  	}
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				]
			});


			/******************************************
			* Loop through each media asset.
			******************************************/
			jQuery.each( userImages, function( i, item ) {
				//console.log('Images: ' + item);

				var imageLink = userImages[i].node.thumbnail_src,
					
					//imageCaption = userImages[i].node.edge_media_to_caption.edges,
					imageCaption = '',
					postCaption = userImages[i].node.edge_media_to_caption.edges,
					
					imageComments = userImages[i].node.edge_media_to_comment.count,
					imageLikes = userImages[i].node.edge_liked_by.count,
					isVideo = userImages[i].node.is_video;

				if (postCaption.length) {
					//console.log(postCaption[0].node.text);
					imageCaption = postCaption[0].node.text;
				}
				
				/*jQuery.each( imageCaption, function( i, item ) {
					console.log('Caption: ' + item);
				});*/

				/*var videoLinkCode = userImages[i].code,
					videoLink = userImages[i].display_src,
					videoResultPre = videoLink.replace('t51', 'vp/t50'),
					videoResultFinal = videoResultPre.replace('jpg', 'mp4');*/
					//truncateCaption = self.truncate(imageCaption);	


				/******************************************
				* Caption truncation based on instagramCaptionFlag.
				******************************************/
				// Save entire caption for mobile view.
				//self.instagramCaptionArray.push(imageCaption);
				//console.log(self.instagramCaptionArray);

				//console.log('Should limit: ', self.instagramCaptionFlag);
				if (typeof imageCaption === 'undefined') {
					imageCaption = '';
				}

				if (self.instagramCaptionFlag) {
					//console.log(imageCaption);
					imageCaption = self.truncate(imageCaption);

					/*if (typeof imageCaption === 'undefined') {
						console.log('NO COMMENT BY OWNER');
						imageCaption = '';
					} else {
						//console.log('COMMENT BY OWNER');
						imageCaption = self.truncate(imageCaption);
					}*/
				}


				//console.log("userID: " + userID + " username: " + username + " imageCaption: " + imageCaption + " imageLink: " + imageLink + " comments: " + comments + " likes: " + likes);


				/******************************************
				* Render ONLY where DOM element lives.
				******************************************/
				if ( self.$instagramCarousel.length && flag ) {
					//console.log('Found the DOM element');

					if (isVideo) {

						/*jQuery(self.$instagramCarousel).slick('slickAdd',
							'<div class="instagram-post">
								<a href="https://instagram.com/' + username + '" class="user">
									<div class="video"></div>
									<img data="' + imageLink + '" src="' + imageLink + '" class="mediaImg"/>
									
									<div class="overlay">
										<div class="info">
											<p class="caption">' + imageCaption + '</p>
											<p class="comments">Comments: ' + comments + '</p>
											<p class="likes">Likes: ' + likes + '</p>
										</div>
									</div>
								</a>
							</div>');*/

						/******************************************
						* Create DOM nodes for carousel
						******************************************/
						var container = document.createElement('div'),
							btn = document.createElement('a'),
							video = document.createElement('div'),
							img = document.createElement('img'),

							overlay = document.createElement('div'),
							overlayInfo = document.createElement('div'),
							caption = document.createElement('p'),
							comments = document.createElement('p'),
							commentsIcon = document.createElement('span'),
							likes = document.createElement('p'),
							likesIcon = document.createElement('span');


						/* console.log(videoResultFinal);
						t50. = Video mp4
						image
						https://scontent-lga3-1.cdninstagram.com/t51.2885-15/e15/23668084_132422104124705_5493594476771278848_n.jpg
						
						video
						https://scontent-lga3-1.cdninstagram.com/vp/t50.2886-16/23727841_131340950908418_8435699043635560448_n.mp4

						<video class="_l6uaz" playsinline="" 
							poster="https://scontent-lga3-1.cdninstagram.com/t51.2885-15/e15/23668084_132422104124705_5493594476771278848_n.jpg" 
							preload="none" 
							src="https://scontent-lga3-1.cdninstagram.com/vp/ec5e962533d95397e16c3344f9159643/5A1864E7/t50.2886-16/23727841_131340950908418_8435699043635560448_n.mp4" 
							type="video/mp4">
						</video>
						*/


						// Instagram individual posts
						container.setAttribute('class', 'instagram-post');
						video.setAttribute('class', 'instagram-video');
						btn.setAttribute('class', 'instagram-user-link');
						btn.setAttribute('target', '_blank');
						
						// Link to open video in Instagram
						//btn.href = 'https://www.instagram.com/p/' + videoLinkCode + '/?taken-by=' + username;

						// Link to open video only
						//btn.href = videoResultFinal;

						img.setAttribute('class', 'instagram-media');
						img.src = imageLink;

						// To stop anchor, add 'img' to container instead of 'btn'
						//btn.appendChild(img);
						container.appendChild(video);
						container.appendChild(img);

						// Instagram individual overlays
						overlay.setAttribute('class', 'instagram-overlay');
						overlayInfo.setAttribute('class', 'instagram-info');
						caption.setAttribute('class', 'instagram-caption');
						comments.setAttribute('class', 'instagram-comments');
						commentsIcon.setAttribute('class', 'instagram-comments-icon');
						likes.setAttribute('class', 'instagram-likes');
						likesIcon.setAttribute('class', 'instagram-likes-icon');

						//caption.textContent = '"' + imageCaption + '"';
						if (imageCaption === '') {
							caption.textContent = '';
						} else {
							caption.textContent = '"' + imageCaption + '"';
						}
						
						comments.textContent = imageComments;
						likes.textContent = imageLikes;

						comments.appendChild(commentsIcon);
						likes.appendChild(likesIcon);

						overlayInfo.appendChild(caption);
						overlayInfo.appendChild(comments);
						overlayInfo.appendChild(likes);
						overlay.appendChild(overlayInfo);

						// Add overlay to post
						container.appendChild(overlay);

						// Add posts to carousel
						jQuery(self.$instagramCarousel).slick('slickAdd', container);

					} else {

						/*jQuery(self.$instagramCarousel).slick('slickAdd',
							'<div class="instagram-post">
								<a href="https://instagram.com/' + username + '" class="user">
									<img data="' + imageLink + '" src="' + imageLink + '" class="mediaImg"/>
									<div class="overlay">
										<div class="info">
											<p class="caption">' + imageCaption + '</p>
											<p class="comments">Comments: ' + comments + '</p>
											<p class="likes">Likes: ' + likes + '</p>
										</div>
									</div>
								</a>
							</div>');*/

						/******************************************
						* Create DOM nodes for carousel
						******************************************/
						var container = document.createElement('div'),
							btn = document.createElement('a'),
							img = document.createElement('img'),

							overlay = document.createElement('div'),
							overlayInfo = document.createElement('div'),
							caption = document.createElement('p'),
							comments = document.createElement('p'),
							commentsIcon = document.createElement('span'),
							likes = document.createElement('p'),
							likesIcon = document.createElement('span');

						// Instagram individual posts
						container.setAttribute('class', 'instagram-post');
						btn.setAttribute('class', 'instagram-user');
						btn.setAttribute('target', '_blank');
						btn.href = 'https://instagram.com/' + username;
						img.setAttribute('class', 'instagram-media');
						img.src = imageLink;

						btn.appendChild(img);

						// To stop anchor, add 'img' to container instead of 'btn'
						container.appendChild(img);

						// Instagram individual overlays
						overlay.setAttribute('class', 'instagram-overlay');
						overlayInfo.setAttribute('class', 'instagram-info');
						caption.setAttribute('class', 'instagram-caption');
						comments.setAttribute('class', 'instagram-comments');
						commentsIcon.setAttribute('class', 'instagram-comments-icon');
						likes.setAttribute('class', 'instagram-likes');
						likesIcon.setAttribute('class', 'instagram-likes-icon');

						//caption.textContent = '"' + imageCaption + '"';
						if (imageCaption === '') {
							caption.textContent = '';
						} else {
							caption.textContent = '"' + imageCaption + '"';
						}
						
						comments.textContent = imageComments;
						likes.textContent = imageLikes;

						comments.appendChild(commentsIcon);
						likes.appendChild(likesIcon);

						overlayInfo.appendChild(caption);
						overlayInfo.appendChild(comments);
						overlayInfo.appendChild(likes);
						overlay.appendChild(overlayInfo);

						// Add overlay to post
						container.appendChild(overlay);

						// Add posts to carousel
						jQuery(self.$instagramCarousel).slick('slickAdd', container);

					}
				}

				/******************************************
				* To limit the amount of images rendered.
				******************************************/
				if ( i === self.instagramLimit ) {
				  return false;
				}

			});

			/******************************************
			* Add header to Instagram feed.
			******************************************/
			jQuery(self.$instagramCarousel).before('<div class="instagram-follow"><h3><a href="https://instagram.com/' + username + '" target="_blank">@ Follow me on Instagram</a></h3></div>');
			
		});

		/******************************************
		* Add events
		******************************************/
		this.addEvents();
    };


    /******************************************
	* Truncate string with ellipsis (...)
	******************************************/
	this.truncate = function(string) {

		var self = this,
			characters = 50;
		
		//console.log(string);

		if ( typeof(string) !== 'undefined' && string.length > characters ) {
			return string.substring(0, characters) + '...';
		} else if ( typeof(string) === 'undefined' ) {
			string = ' ';
			return string;
		} else {
			return string;
		}

	};

};


/******************************************
* Start the magic
******************************************/
(function($) {
	$(document).ready(function($) {
		//console.log('ready');
		var username,
		  	feed;

		$.each( instagram_options, function( key, value ) {
		  	//console.log( key + ": " + value );

		  	if (key === 'username') {
		  		//console.log(value);
		  		username = value;
		  	}

		  	if (key === 'showFeed') {
		  		//console.log(value);
		  		feed = value;
		  	}
		});

		function init(el, username, feed) {
			var newInstagramFeed = new instagramCarousel( el );
			newInstagramFeed.init(username, feed);
		}

		init('body', username, feed);

	});
}(jQuery));



