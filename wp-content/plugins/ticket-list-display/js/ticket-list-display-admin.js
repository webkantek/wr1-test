/******************************************
* Ticket List Display
* Author: Ryan Mahabir
* Date: 2018-01-28
******************************************/


/******************************************
* Parse URL
******************************************/
function parseURL(url) {
    var parser = document.createElement('a'),
        searchObject = {},
		queries, split, i;
		
    // Let the browser do the work
    parser.href = url;
	
	// Convert query string to object
    queries = parser.search.replace(/^\?/, '').split('&');
	
	for( i = 0; i < queries.length; i++ ) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
	}
	
    return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        search: parser.search,
        searchObject: searchObject,
        hash: parser.hash
    };
}



/******************************************
* Hide custom fields if post type is not Event Ticket
******************************************/
function hideCustomFields() {
    var el = document.getElementById('product-type'),
        customFields = document.getElementById('acf_after_title-sortables')
        postType = el.options[el.selectedIndex].value;
        
    //console.log(postType, customFields);

    if (postType !== 'ticket-event') {
        $(customFields).addClass('inactive');
    }
}


/******************************************
* Start the magic
******************************************/
(function($) {
	$(document).ready(function($) {
        //console.log('ready');

        var path = parseURL(window.location.href),
            artistName = path.pathname.indexOf(1);
        
        //console.log(path.pathname);

        artistName = path.pathname.split("/")[1];
        //console.log(artistName);
        
        var postURL = '/' + artistName + '/wp-admin/post.php';
        
        if (path.pathname === postURL) {
            //console.log('Post type path');
            hideCustomFields();
        }

	});
}(jQuery));


