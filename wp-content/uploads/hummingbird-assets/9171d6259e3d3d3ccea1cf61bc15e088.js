/**handles:music-detail-page-js**/
/******************************************
* Music - Detail Page JS
* Author: Ryan Mahabir
* Date: 2018-27-03
******************************************/
function newMusicAlbum( element ){
    
    /******************************************
	* Variables
	******************************************/
    this.$element = jQuery( element );
    this.$domNode = '#album-content';
    this.$albumEl = this.$element.find( this.$domNode );
    this.$lyricsLink = '.track-lyric-link';
    this.$lyricsLinkEl = this.$element.find( this.$lyricsLink );

    this.$stars = '.star';
    this.$starsEl = this.$element.find( this.$stars );


    // Video Carousel
    this.$videoDomNode = '#video-carousel';
    this.$videoCarousel = this.$element.find( this.$videoDomNode );
    //this.instagramLimit = 12;
    this.feedLimitDesktop = 1;
    this.feedLimitTablet = 2;
    this.feedLimitMobile = 1;

    

    /******************************************
	* WordPress user settings
	******************************************/
    this.init = function(vid_count) {
        //console.log('array: ' + array);
        //console.log('video count: ' + vid_count);

		if (this.$albumEl.length) {
			//console.log('found dom node');
            //this.createDom( array );
            this.addEvents();
			//return;
        }
        
        if (this.$videoCarousel.length) {
            //console.log('found video carousel dom node');
            var self = this;

            if (vid_count > 3) {
                vid_count = 3;
                
            }
            self.initCarousel(vid_count);
        }
        
    };


    /******************************************
	* Get JSON from PHP
	******************************************/
    this.createDom = function( array ) {
        //console.log('creating dom');
        var self = this;

        jQuery.each( music_album_data, function( key, value ) {
            //console.log( key + ": " + value );

            if (key === 'album_count') {
                //console.log(value);
                album_count = value;
            } else if (key === 'album_id') {
                //console.log(value);
                album_id = value;
            } else if (key === 'album_link') {
                //console.log(value);
                album_link = value;
            } else if (key === 'album_date') {
                //console.log(value);
                album_date = value;
            } else if (key === 'album_title') {
                //console.log(value);
                album_title = value;
            } else if (key === 'album_content') {
                //console.log(value);
                album_content = value;
            } else if (key === 'album_release_date') {
                //console.log(value);
                album_release_date = value;
            } else if (key === 'album_featured_image') {
                //console.log(value);
                album_featured_image = value;
            } else if (key === 'album_background_image') {
                //console.log(value);
                album_background_image = value;
            } else if (key === 'album_links_count') {
                //console.log(value);
                album_links_count = value;
            } else if (key === 'album_external_link_hostname') {
                //console.log(value);
                album_external_link_hostname = value;
            } else if (key === 'album_link_url') {
                //console.log(value);
                album_link_url = value;
            } else if (key === 'album_track_count') {
                //console.log(value);
                album_track_count = value;
            } else if (key === 'album_track_title') {
                //console.log(value);
                album_track_title = value;
            } else if (key === 'album_track_duration') {
                //console.log(value);
                album_track_duration = value;
            } else if (key === 'album_track_lyric') {
                //console.log(value);
                album_track_lyric = value;
            } else if (key === 'album_track_external_link_amount') {
                //console.log(value);
                album_track_external_link_amount = value;
            } else if (key === 'album_track_external_link_hostname') {
                //console.log(value);
                album_track_external_link_hostname = value;
            } else if (key === 'album_track_external_link') {
                //console.log(value);
                album_track_external_link = value;
            }
        });

        //console.log(album_count, album_id, album_link, album_date, album_title, album_content, album_release_date, album_featured_image, album_background_image, album_links_count, album_external_link_hostname, album_link_url, album_track_count, album_track_title, album_track_duration, album_track_lyric, album_track_external_link_amount, album_track_external_link_hostname, album_track_external_link);
        console.log(album_release_date);


        


        var album = [],
            startEl = '<div class="album-info">',
            endEl = '</div>',

            albumSleeve = '<div class="album-cover"><img src="' + album_featured_image[0] + '" atr="' + album_title + '" /></div>',
            album_share_links_container = '<div id="album-share"></div>',
            album_tracks_container = '<div id="album-tracks"></div>',

            albumInfo = '<h3>' + album_title + '</h3><h6>Release Date: ' + album_release_date + '</h6>' + album_share_links_container,

            content = albumSleeve + startEl + albumInfo + endEl + album_tracks_container;
        
        album.push(content);
        this.$albumEl.append(album);
            



        // Render album share links
        var album_share_links = [],
            $albumShare = '#album-share',
            $albumShareEl = this.$element.find( $albumShare );

        for (i = 0; i < album_links_count; i++) {
            var link = '<a href="' + album_link_url[i] + '" title="' + album_external_link_hostname[i] + '" target="_blank">' + album_external_link_hostname[i] + '</a>'
            //album_share_links.push(link);
            $albumShareEl.append(link);
        }
        //console.log(album_share_links);



        // Render album tracks
        var album_tracks = [],
            $albumTracksContainer = '#album-tracks',
            $albumTracksEl = this.$element.find( $albumTracksContainer ),
            $accordionContainer = '<div id="accordion"></div>';

        $albumTracksEl.append($accordionContainer);
        var $accordionEl = this.$element.find( '#accordion' );

        for (i = 0; i < album_track_count; i++) {
            var track = '<div class="track"><h3>' + album_track_title[i] + ' <a class="track-lyric-link">Lyrics</a><span> ' + album_track_duration[i] + '</span></h3>',

                accordion = track + '<div class="track-lyrics"><p>' + album_track_lyric[i] + '</p></div></div>';

            //album_share_links.push(link);
            $accordionEl.append(accordion);
        }
        //console.log(album_share_links);
        

        this.addEvents();
        
    };

    
    /******************************************
	* WIP - Create DOM nodes
	******************************************/
	/*this.createDOM = function() {
		var container = document.createElement('div'),
			btn = document.createElement('a'),
			video = document.createElement('div'),
			img = document.createElement('img'),

			overlay = document.createElement('div'),
			overlayInfo = document.createElement('div'),
			caption = document.createElement('p'),
			comments = document.createElement('p'),
			likes = document.createElement('p');

		return;
	};*/


	/******************************************
	* Add events
	******************************************/
	this.addEvents = function() {

        var self = this,
            tabcontent = jQuery('.tabcontent'),
            stars = jQuery('.full');

		jQuery('.track-lyric-link p').on('click', function(){
            //console.log('clicked');
            
            //var lyricsContainer = jQuery(this).siblings('div.track-lyrics');
            var lyricsContainer = jQuery(this).parent().parent().next('tr').find('div.track-lyrics');
            //console.log(lyricsContainer);

            if (lyricsContainer.length) {
                //console.log('found lyrics');

                var currentContainer = jQuery(this).parent().parent().next('tr').find('div.track-lyrics');

                if ( currentContainer.hasClass('active') ) {
                    jQuery(currentContainer).removeClass('active');
                } else {
                    jQuery('.track-lyrics').removeClass('active');
                    jQuery(this).parent().parent().next('tr').find('div.track-lyrics').addClass('active');
                }

                

            }
            
            /*if ( this.hasClass('active') ) {
                console.log('active class');
                this.removeClass('active');
            }*/
            
        });


        jQuery('.track-lyric-link a.more-icon').on('click', function(){
            //console.log('clicked');
            
            //var lyricsContainer = jQuery(this).siblings('div.track-lyrics');
            var lyricsContainer = jQuery(this).parent().parent().next('tr').find('div.track-lyrics');
            //console.log(lyricsContainer);

            if (lyricsContainer.length) {
                //console.log('found lyrics');

                var currentContainer = jQuery(this).parent().parent().next('tr').find('div.track-lyrics');

                if ( currentContainer.hasClass('active') ) {
                    jQuery(currentContainer).removeClass('active');
                } else {
                    jQuery('.track-lyrics').removeClass('active');
                    jQuery(this).parent().parent().next('tr').find('div.track-lyrics').addClass('active');
                }

                

            }
            
            /*if ( this.hasClass('active') ) {
                console.log('active class');
                this.removeClass('active');
            }*/
            
        });


        jQuery('.tablinks').on('click', function(){
            //console.log('clicked');
            var currentLink = jQuery(this),
                name = jQuery(this).data('value'),
                str = '#value-' + name,
                res = str.toLowerCase();

            //console.log(res);
            jQuery(self.$starsEl).removeClass('animate');

            if ( currentLink.hasClass('active') ) {
                return;
            } else {
                // Remove all active classes
                jQuery('.tablinks').removeClass('active');
                jQuery('.tabcontent').removeClass('active');
                
                jQuery(this).addClass('active');
                jQuery(res).addClass('active');
            }

            if ( res === '#value-reviews' ) {
                
                if (self.$starsEl.length) {
                    setTimeout( function() { 
                        //alert('Carousel ready');
                        //jQuery(self.$starsEl).addClass('star-colour');
                        jQuery(self.$starsEl).addClass('animate');
                        //jQuery(self.$starsEl).siblings('.half').addClass('star-colour');
                        //jQuery(self.$starsEl).children('.selected').addClass('is-animated');
                        //jQuery(self.$starsEl).children('.selected').addClass('pulse');
                    }, 300);
        
                }
            }

        });

        
        /*jQuery('#video-carousel').on('init', function(event, slick){
            console.log('slider was initialized');
            //jQuery('#video-carousel').addClass('active');
        });*/


		/******************************************
		* Caption truncation on MOBILE ONLY orientation.
		******************************************/
		// Mobile orientation change
		/*jQuery( window ).on( "orientationchange", function( event ) {
			console.log(event.orientation);
		});

		jQuery( window ).resize(function() {
			//console.log(window.innerWidth);
		 	var windowWidth = window.innerWidth;

			if (windowWidth > 700) {
				self.reRenderInstagramCaption(true);
			}
		  
        });*/

    };


    /******************************************
	* Start video carousel
	******************************************/
    this.initCarousel = function(vid_count) {

        var self = this;
        //console.log(vid_count);

        /******************************************
        * Init Slick.js carousel.
        ******************************************/
        self.$videoCarousel.slick({
            slidesToShow: vid_count,
            //slidesToScroll: 1,
            //autoplay: true,
            autoplaySpeed: 4000,

            // To use lazy loading, set a data-lazy attribute
            // on your img tags and leave off the src
            //lazyLoad: 'ondemand',
            //adaptiveHeight: true,
            centerMode: false,
            centerPadding: '0px',
            variableWidth: true,
            fade: false,
            cssEase: 'linear',

            dots: true,
            //infinite: true,
            //speed: 300,

            responsive: [{
                // Desktop
                breakpoint: 1024,
                    settings: {
                        //slidesToShow: self.feedLimitDesktop,
                        //slidesToScroll: 3,
                        //infinite: false,
                        //dots: false
                    }
                },

                // Tablet - landscape
                {breakpoint: 800,
                    settings: {
                        //slidesToShow: self.feedLimitTablet,
                        //slidesToScroll: 2
                    }
                },

                // Mobile - landscape
                {breakpoint: 700,
                    settings: {
                        //slidesToShow: self.feedLimitTablet,
                        //slidesToScroll: 2
                    }
                },

                // Mobile - portrait
                {breakpoint: 636,
                    settings: {
                        //slidesToShow: self.feedLimitMobile,
                        //slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        /*setTimeout( function() { 
            //alert('Carousel ready');
            //jQuery('#video-carousel').addClass('active');
        }, 4000);*/
    };


    /******************************************
	* Truncate string with ellipsis (...)
	******************************************/
	this.truncate = function(string) {

		var self = this,
			characters = 50;
		
		//console.log(string);

		if ( typeof(string) !== 'undefined' && string.length > characters ) {
			return string.substring(0, characters) + '...';
		} else if ( typeof(string) === 'undefined' ) {
			string = ' ';
			return string;
		} else {
			return string;
		}

	};

};


/******************************************
* Start the magic
******************************************/
(function($) {
	$(document).ready(function($) {
        //console.log('ready');

        var // Get album data
            username = 'ryan',
            feed = 'false',

            album_count,
            album_id,
            album_link,
            album_date,
            album_title,
            album_content,
            album_release_date,
            album_featured_image,
            album_background_image,

            // Get album external links count
            album_links_count,
            album_external_link_hostname,
            album_link_url,

            // Get album tracks
            album_track_count,
            album_track_title,
            album_track_duration,
            album_track_lyric,

            // Get album track external links
            album_track_external_link_amount,
            album_track_external_link_hostname,
            album_track_external_link;

              
        //console.log(videos_count);
        
        /*$.each( music_album_data, function( key, value ) {
		  	//console.log( key + ": " + value );

		  	if (key === 'album_count') {
		  		//console.log(value);
		  		album_count = value;
		  	} else if (key === 'album_id') {
		  		//console.log(value);
		  		album_id = value;
		  	} else if (key === 'album_link') {
                //console.log(value);
                album_link = value;
            } else if (key === 'album_date') {
                //console.log(value);
                album_date = value;
            } else if (key === 'album_title') {
                //console.log(value);
                album_title = value;
            } else if (key === 'album_content') {
                //console.log(value);
                album_content = value;
            } else if (key === 'album_featured_image') {
                //console.log(value);
                album_featured_image = value;
            } else if (key === 'album_links_count') {
                //console.log(value);
                album_links_count = value;
            } else if (key === 'album_link_icon') {
                //console.log(value);
                album_link_icon = value;
            } else if (key === 'album_link_url') {
                //console.log(value);
                album_link_url = value;
            } else if (key === 'album_track_count') {
                //console.log(value);
                album_track_count = value;
            } else if (key === 'album_track_title') {
                //console.log(value);
                album_track_title = value;
            } else if (key === 'album_track_duration') {
                //console.log(value);
                album_track_duration = value;
            } else if (key === 'album_track_lyric') {
                //console.log(value);
                album_track_lyric = value;
            } else if (key === 'album_track_external_link_amount') {
                //console.log(value);
                album_track_external_link_amount = value;
            } else if (key === 'album_track_external_link_hostname') {
                //console.log(value);
                album_track_external_link_hostname = value;
            } else if (key === 'album_track_external_link') {
                //console.log(value);
                album_track_external_link = value;
            }
        });*/
        
        //console.log(album_count, album_id, album_link, album_date, album_title, album_content, album_featured_image, album_links_count, album_link_icon, album_link_url, album_track_count, album_track_title, album_track_duration, album_track_lyric, album_track_external_link_amount, album_track_external_link_hostname, album_track_external_link);


        // Add jQuery Accordion
        /*var imported = document.createElement('script');
        imported.src = '/wp-content/themes/claue-child-artist-template/custom_js/vendor/jquery-ui.min.js';
        document.head.appendChild(imported);
    

        setTimeout(function(){ 
            $( "#accordion" ).accordion({
                collapsible: true,
                active: 'none'
              });
        }, 300);*/


		/*function init(el, array) {
			var newAlbum = new newMusicAlbum( el );
			newAlbum.init(array);
        }

        init('body', music_album_data);*/

        function init(el, vid_count) {
			var newAlbum = new newMusicAlbum( el );
			newAlbum.init(vid_count);
        }
        
        init('body', videos_count);

	});
}(jQuery));



// Event listener for DOM
document.addEventListener("DOMContentLoaded", theDOMHasLoaded, false);

// array of audio files (stored in a folder called music)
var files = ["interlude.mp3", // 0
            "chooseyourweapon.mp3", // 1
            "interlude.mp3", // 2
            "scriptures.mp3" // 3
            ];

///////////////////////////////////////////////
// Find and store audio info
///////////////////////////////////////////////

// array for AudioObjects
var audioList = [];
// components and the index for their AudioObject
var componentDict = {};
// store AudioObject that is currently playing
var playingAudio = null;
// store playhead id if one is being dragged
var onplayhead = null;

/* AudioObject Constructor */
function AudioObject(audio, duration) {
    this.audio = audio;
    this.id = audio.id;
    this.duration = duration;
}
/* bindAudioPlayer
 * Store audioplayer components in correct AudioObject
 * num identifes correct audioplayer
 */
AudioObject.prototype.bindAudioPlayer = function (num) {
    this.track = document.getElementById("track-" + num);
    this.tracknumber = document.getElementById("track-number-" + num);
    //console.log(this.track);
    
    this.audioplayer = document.getElementById("audioplayer-" + num);
    this.playbutton = document.getElementById("playbutton-" + num);
    this.timeline = document.getElementById("timeline-" + num);
    this.playhead = document.getElementById("playhead-" + num);
    this.timelineWidth = this.timeline.offsetWidth - this.playhead.offsetWidth
}

/* addEventListeners() */
AudioObject.prototype.addEventListeners = function () {
    this.audio.addEventListener("timeupdate", AudioObject.prototype.timeUpdate, false);
    this.audio.addEventListener("durationchange", AudioObject.prototype.durationChange, false);
    this.timeline.addEventListener("click", AudioObject.prototype.timelineClick, false);
    this.playbutton.addEventListener("click", AudioObject.prototype.pressPlay, false);
    // Makes playhead draggable 
    this.playhead.addEventListener('mousedown', AudioObject.prototype.mouseDown, false);
    window.addEventListener('mouseup', mouseUp, false);
}

/* populateAudioList */
function populateAudioList() {
    var audioElements = document.getElementsByClassName("audio");
    for (i = 0; i < audioElements.length; i++) {
        audioList.push(
            new AudioObject(audioElements[i], 0)
        );
        audioList[i].bindAudioPlayer(i);
        audioList[i].addEventListeners();
    }
}

/* populateComponentDictionary() 
 * {key=element id : value=index of audioList} */
function populateComponentDictionary() {
    for (i = 0; i < audioList.length; i++) {
        componentDict[audioList[i].audio.id] = i;
        componentDict[audioList[i].playbutton.id] = i;
        componentDict[audioList[i].timeline.id] = i;
        componentDict[audioList[i].playhead.id] = i;
    }
}

///////////////////////////////////////////////
// Update Audio Player
///////////////////////////////////////////////

/* durationChange
 * set duration for AudioObject */
AudioObject.prototype.durationChange = function () {
    var ao = audioList[getAudioListIndex(this.id)];
    ao.duration = this.duration;
}

/* pressPlay() 
 * call play() for correct AudioObject
 */
AudioObject.prototype.pressPlay = function () {
    var index = getAudioListIndex(this.id);
    audioList[index].play();
}

/* play() 
 * play or pause selected audio, if there is a song playing pause it
 */
AudioObject.prototype.play = function () {
    if (this == playingAudio) {
        playingAudio = null;
        this.audio.pause();
        changeClass(this.playbutton, "playbutton play");
        
        changeClass(this.track, "track inactive");
        changeClass(this.tracknumber, "inactive");
    }
    // else check if playing audio exists and pause it, then start this
    else {
        if (playingAudio != null) {
            playingAudio.audio.pause();
            changeClass(playingAudio.playbutton, "playbutton play");

            changeClass(playingAudio.track, "track inactive");
            changeClass(playingAudio.tracknumber, "inactive");
        }
        this.audio.play();
        playingAudio = this;
        changeClass(this.playbutton, "playbutton pause active");

        changeClass(this.track, "track active");
        changeClass(this.tracknumber, "active");
    }
}

/* timelineClick()
 * get timeline's AudioObject
 */
AudioObject.prototype.timelineClick = function (event) {
    var ao = audioList[getAudioListIndex(this.id)];
    ao.audio.currentTime = ao.audio.duration * clickPercent(event, ao.timeline, ao.timelineWidth);
}

/* mouseDown */
AudioObject.prototype.mouseDown = function (event) {
    onplayhead = this.id;
    var ao = audioList[getAudioListIndex(this.id)];
    window.addEventListener('mousemove', AudioObject.prototype.moveplayhead, true);
    ao.audio.removeEventListener('timeupdate', AudioObject.prototype.timeUpdate, false);
}

/* mouseUp EventListener
 * getting input from all mouse clicks */
function mouseUp(e) {
    if (onplayhead != null) {
        var ao = audioList[getAudioListIndex(onplayhead)];
        window.removeEventListener('mousemove', AudioObject.prototype.moveplayhead, true);
        // change current time
        ao.audio.currentTime = ao.audio.duration * clickPercent(e, ao.timeline, ao.timelineWidth);
        ao.audio.addEventListener('timeupdate', AudioObject.prototype.timeUpdate, false);
    }
    onplayhead = null;
}

/* mousemove EventListener
 * Moves playhead as user drags */
AudioObject.prototype.moveplayhead = function (e) {
    var ao = audioList[getAudioListIndex(onplayhead)];
    var newMargLeft = e.clientX - getPosition(ao.timeline);

  if (newMargLeft >= 0 && newMargLeft <= ao.timelineWidth) {
        document.getElementById(onplayhead).style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead.style.marginLeft = "0px";
    }
    if (newMargLeft > ao.timelineWidth) {
        playhead.style.marginLeft = ao.timelineWidth + "px";
    }
}

/* timeUpdate 
 * Synchronizes playhead position with current point in audio 
 * this is the html audio element
 */
AudioObject.prototype.timeUpdate = function () {
    // audio element's AudioObject
    var ao = audioList[getAudioListIndex(this.id)];
    var playPercent = ao.timelineWidth * (ao.audio.currentTime / ao.duration);
    ao.playhead.style.marginLeft = playPercent + "px";
    // If song is over
    if (ao.audio.currentTime == ao.duration) {
        changeClass(ao.playbutton, "playbutton play");

        changeClass(ao.track, "track inactive");
        changeClass(ao.tracknumber, "inactive");

        ao.audio.currentTime = 0;
        ao.audio.pause();
        playingAudio = null;
    }
}

///////////////////////////////////////////////
// Utility Methods
///////////////////////////////////////////////

/* changeClass 
 * overwrites element's class names */
function changeClass(element, newClasses) {
    element.className = newClasses;
}

function addClass(element, newClasses) {
    element.className += ' ' + newClasses;
}

/* getAudioListIndex
 * Given an element's id, find the index in audioList for the correct AudioObject */
function getAudioListIndex(id) {
    return componentDict[id];
}

/* clickPercent()
 * returns click as decimal (.77) of the total timelineWidth */
function clickPercent(e, timeline, timelineWidth) {
   return (event.clientX - getPosition(timeline)) / timelineWidth;
}

// getPosition
// Returns elements left position relative to top-left of viewport
function getPosition(el) {
    return el.getBoundingClientRect().left;
}
///////////////////////////////////////////////
// GENERATE HTML FOR AUDIO ELEMENTS AND PLAYERS
///////////////////////////////////////////////

/* createAudioElements
 * create audio elements for each file in files */
function createAudioElements() {
    for (f in files) {
        //var audioString = "<audio id=\"audio-" + f + "\" class=\"audio\" preload=\"true\"><source src=\"http://www.alexkatz.me/codepen/music/" + files[f] + "\"></audio>";
        //jQuery("#audio-players").append(audioString);

        var audioString = '<audio id="audio-"' + f + '" class="audio" preload="true"><source src="http://www.alexkatz.me/codepen/music/"' + music_album_data[f] + '"></audio>';
        jQuery("#album-tracks").append(audioString);
    }
}

/* createAudioPlayers
 * create audio players for each file in files */
function createAudioPlayers() {
    for (f in files) {
        /*var playerString = "<div id=\"audioplayer-" + f + "\" class=\"audioplayer\"><button id=\"playbutton-" + f + "\" class=\"play playbutton\"></button><div id=\"timeline-" + f + "\" class=\"timeline\"><div id=\"playhead-" + f + "\" class=\"playhead\"></div></div></div>";
        jQuery("#audio-players").append(playerString);*/
    }
}

/* theDOMHasLoaded()
 * Execute when DOM is loaded */
function theDOMHasLoaded(e) {
    // Generate HTML for audio elements and audio players
    //createAudioElements();
    //createAudioPlayers();

    // Populate Audio List
    populateAudioList();
    populateComponentDictionary();
}