/** Wonderplugin 3D Carousel Plugin Commercial Version
 * Copyright 2017 Magic Hills Pty Ltd All Rights Reserved
 * Website: http://www.wonderplugin.com
 * Version 2.2 
 */
(function(){var scripts=document.getElementsByTagName("script");var jsFolder="";for(var i=0;i<scripts.length;i++)if(scripts[i].src&&scripts[i].src.match(/html5lightbox\.js/i))jsFolder=scripts[i].src.substr(0,scripts[i].src.lastIndexOf("/")+1);var loadjQuery=false;if(typeof jQuery=="undefined")loadjQuery=true;else{var jVersion=jQuery.fn.jquery.split(".");if(jVersion[0]<1||jVersion[0]==1&&jVersion[1]<6)loadjQuery=true}if(loadjQuery){var head=document.getElementsByTagName("head")[0];var script=document.createElement("script");
script.setAttribute("type","text/javascript");if(script.readyState)script.onreadystatechange=function(){if(script.readyState=="loaded"||script.readyState=="complete"){script.onreadystatechange=null;loadHtml5LightBox(jsFolder)}};else script.onload=function(){loadHtml5LightBox(jsFolder)};script.setAttribute("src",jsFolder+"jquery.js");head.appendChild(script)}else loadHtml5LightBox(jsFolder)})();
function loadHtml5LightBox(jsFolder){(function($){$.fn.wp3dcarousellightbox=function(options){var inst=this;inst.options=$.extend({freelink:"https://www.wonderplugin.com/wordpress-3dcarousel/",defaultvideovolume:1,autoclose:false,autoclosedelay:0,resizedelay:100,insideiframe:false,autoresizecontent:true,defaultwidth:960,defaultheight:540,usedefaultsizeforcontent:false,preload:true,preloadallonpageload:false,preloadalldelay:5E3,autoplay:true,loopvideo:false,html5player:true,responsive:true,nativehtml5controls:false,videohidecontrols:false,
nativecontrolsonfirefox:false,nativecontrolsonie:false,nativecontrolsoniphone:true,nativecontrolsonipad:true,nativecontrolsonandroid:true,nativecontrolsonfullscreen:true,nativecontrolsnodownload:true,noresizecallback:true,imagekeepratio:true,maxheight:false,elemautoheight:false,useflashonie9:true,useflashonie10:true,useflashonie11:false,useflashformp4onfirefox:false,transition:"none",transitionduration:400,enablepdfjs:true,pdfjsengine:"",openpdfinnewtaboniphone:false,openpdfinnewtabonipad:false,googleanalyticsaccount:"",
arrowloop:true,showall:false,userelforgroup:true,shownavigation:true,thumbwidth:96,thumbheight:72,thumbgap:4,thumbtopmargin:12,thumbbottommargin:12,thumbborder:1,thumbbordercolor:"transparent",thumbhighlightbordercolor:"#fff",thumbopacity:1,navbuttonwidth:32,navbgcolor:"rgba(0,0,0,0.2)",shownavcontrol:true,navcontrolimage:"lightbox-navcontrol.png",hidenavdefault:false,overlaybgcolor:"#000",overlayopacity:0.9,bgcolor:"#fff",bordersize:8,borderradius:0,bordermargin:16,bordertopmargin:48,barautoheight:true,
barheight:64,smallscreenheight:415,responsivebarheight:false,barheightonsmallheight:64,notkeepratioonsmallheight:false,bordertopmarginsmall:36,loadingwidth:64,loadingheight:64,resizespeed:400,fadespeed:0,jsfolder:jsFolder,skinsfoldername:"skins/default/",loadingimage:"lightbox-loading.gif",nextimage:"lightbox-next.png",previmage:"lightbox-prev.png",closeimage:"lightbox-close.png",playvideoimage:"lightbox-playvideo.png",titlebgimage:"lightbox-titlebg.png",navarrowsprevimage:"lightbox-navprev.png",
navarrowsnextimage:"lightbox-navnext.png",navarrowsalwaysshowontouch:true,navarrowsbottomscreenwidth:479,closeonoverlay:true,alwaysshownavarrows:false,showplaybutton:true,playimage:"lightbox-play.png",pauseimage:"lightbox-pause.png",fullscreenmode:false,fullscreencloseimage:"lightbox-close-fullscreen.png",fullscreennextimage:"lightbox-next-fullscreen.png",fullscreenprevimage:"lightbox-prev-fullscreen.png",fullscreennomargin:false,fullscreenmodeonsmallscreen:false,fullscreennomarginonsmallscreen:false,
fullscreensmallscreenwidth:800,fullscreenbgcolor:"rgba(0, 0, 0, 0.9)",fullscreenbordersize:0,fullscreentextinside:false,videobgcolor:"#000",html5videoposter:"",showtitle:true,titlestyle:"bottom",titleinsidecss:"color:#fff; font-size:16px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 8px;",titlebottomcss:"color:#333; font-size:16px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left;",showonmouseoverinside:false,showinsidetitleforimageonly:true,
showdescription:true,descriptioninsidecss:"color:#fff; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;",descriptionbottomcss:"color:#333; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;",fullscreentitlebottomcss:"color:#fff; font-size:16px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 8px 8px;",fullscreendescriptionbottomcss:"color:#fff; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;",
showsocialmedia:true,socialmediaposition:"position:absolute;top:8px;right:8px;",showtitleprefix:true,titleprefix:"%NUM / %TOTAL",autoslide:false,slideinterval:5E3,showtimer:true,timerposition:"bottom",timerheight:2,timercolor:"#dc572e",timeropacity:1,initvimeo:true,inityoutube:true,swipepreventdefaultonandroid:false,initsocial:true,showsocial:false,socialposition:"position:absolute;top:100%;right:0;",socialpositionsmallscreen:"position:absolute;top:100%;right:0;left:0;",socialdirection:"horizontal",
socialbuttonsize:32,socialbuttonfontsize:18,socialrotateeffect:true,showfacebook:true,showtwitter:true,showpinterest:true,showemail:false,imagepercentage:75,sidetobottomscreenwidth:479,errorwidth:280,errorheight:48,errorcss:"text-align:center; color:#ff0000; font-size:14px; font-family:Arial, sans-serif;",enabletouchswipe:true,mobileresizeevent:false,swipedistance:0,bodynoscroll:false,useabsolutepos:false,useabsoluteposonmobile:false,supportesckey:true,supportarrowkeys:true,version:"3.3",stamp:false,
freemark:"87,111,114,100,80,114,101,115,115,32,51,68,32,67,97,114,111,117,115,101,108,32,70,114,101,101,32,86,101,114,115,105,111,110",watermark:"",watermarklink:""},options);if(typeof wp3dcarousellightbox_options!="undefined"&&wp3dcarousellightbox_options)$.extend(inst.options,wp3dcarousellightbox_options);if($("div.wp3dcarousellightbox_options").length)$.each($("div.wp3dcarousellightbox_options").data(),function(key,value){inst.options[key.toLowerCase()]=value});if($("div#wp3dcarousellightbox_options").length)$.each($("div#wp3dcarousellightbox_options").data(),function(key,
value){inst.options[key.toLowerCase()]=value});if($("div#html5lightbox_general_options").length)$.each($("div#html5lightbox_general_options").data(),function(key,value){inst.options[key.toLowerCase()]=value});var ELEM_TYPE=0,ELEM_HREF=1,ELEM_TITLE=2,ELEM_GROUP=3,ELEM_WIDTH=4,ELEM_HEIGHT=5,ELEM_HREF_WEBM=6,ELEM_HREF_OGG=7,ELEM_THUMBNAIL=8,ELEM_DESCRIPTION=9,ELEM_DIV=10,ELEM_ORIGINALWIDTH=11,ELEM_ORIGINALHEIGHT=12,ELEM_SOCIALMEDIA=13,ELEM_WEBLINK=14,ELEM_WEBLINKTARGET=15,ELEM_WEBLINKTEXT=16;inst.options.types=
["IMAGE","FLASH","VIDEO","YOUTUBE","VIMEO","PDF","MP3","WEB","FLV","DAILYMOTION","DIV","WISTIA","IFRAMEVIDEO"];inst.options.htmlfolder=window.location.href.substr(0,window.location.href.lastIndexOf("/")+1);inst.options.skinsfolder=inst.options.skinsfoldername;if(inst.options.skinsfolder.length>0&&inst.options.skinsfolder[inst.options.skinsfolder.length-1]!="/")inst.options.skinsfolder+="/";if(inst.options.skinsfolder.charAt(0)!="/"&&inst.options.skinsfolder.substring(0,5)!="http:"&&inst.options.skinsfolder.substring(0,
6)!="https:")inst.options.skinsfolder=inst.options.jsfolder+inst.options.skinsfolder;var image_list=["loadingimage","nextimage","previmage","closeimage","playvideoimage","titlebgimage","navarrowsprevimage","navarrowsnextimage","navcontrolimage","playimage","pauseimage","fullscreencloseimage","fullscreennextimage","fullscreenprevimage"];for(var i=0;i<image_list.length;i++)if(inst.options[image_list[i]])if(inst.options[image_list[i]].substring(0,7).toLowerCase()!="http://"&&inst.options[image_list[i]].substring(0,
8).toLowerCase()!="https://")inst.options[image_list[i]]=inst.options.skinsfolder+inst.options[image_list[i]];var i;var l;var mark="";var bytes=inst.options.freemark.split(",");for(i=0;i<bytes.length;i++)mark+=String.fromCharCode(bytes[i]);inst.options.freemark=mark;var d0="wmoangdiecrpluginh.iclolms";for(i=1;i<=5;i++)d0=d0.slice(0,i)+d0.slice(i+1);l=d0.length;for(i=0;i<5;i++)d0=d0.slice(0,l-9+i)+d0.slice(l-8+i);if(inst.options.htmlfolder.indexOf(d0)!=-1)inst.options.stamp=false;inst.options.flashInstalled=
false;try{if(new ActiveXObject("ShockwaveFlash.ShockwaveFlash"))inst.options.flashInstalled=true}catch(e){if(navigator.mimeTypes["application/x-shockwave-flash"])inst.options.flashInstalled=true}inst.options.html5VideoSupported=!!document.createElement("video").canPlayType;inst.options.isChrome=navigator.userAgent.match(/Chrome/i)!=null;inst.options.isFirefox=navigator.userAgent.match(/Firefox/i)!=null;inst.options.isOpera=navigator.userAgent.match(/Opera/i)!=null||navigator.userAgent.match(/OPR\//i)!=
null;inst.options.isSafari=navigator.userAgent.match(/Safari/i)!=null;inst.options.isIE11=navigator.userAgent.match(/Trident\/7/)!=null&&navigator.userAgent.match(/rv:11/)!=null;inst.options.isIE=navigator.userAgent.match(/MSIE/i)!=null&&!inst.options.isOpera;inst.options.isIE10=navigator.userAgent.match(/MSIE 10/i)!=null&&!this.options.isOpera;inst.options.isIE9=navigator.userAgent.match(/MSIE 9/i)!=null&&!inst.options.isOpera;inst.options.isIE8=navigator.userAgent.match(/MSIE 8/i)!=null&&!inst.options.isOpera;
inst.options.isIE7=navigator.userAgent.match(/MSIE 7/i)!=null&&!inst.options.isOpera;inst.options.isIE6=navigator.userAgent.match(/MSIE 6/i)!=null&&!inst.options.isOpera;inst.options.isIE678=inst.options.isIE6||inst.options.isIE7||inst.options.isIE8;inst.options.isIE6789=inst.options.isIE6||inst.options.isIE7||inst.options.isIE8||inst.options.isIE9;inst.options.isAndroid=navigator.userAgent.match(/Android/i)!=null;inst.options.isIPad=navigator.userAgent.match(/iPad/i)!=null;inst.options.isIPhone=
navigator.userAgent.match(/iPod/i)!=null||navigator.userAgent.match(/iPhone/i)!=null;inst.options.isIOS=inst.options.isIPad||inst.options.isIPhone;inst.options.isMobile=inst.options.isAndroid||inst.options.isIPad||inst.options.isIPhone;inst.options.isIOSLess5=inst.options.isIPad&&inst.options.isIPhone&&(navigator.userAgent.match(/OS 4/i)!=null||navigator.userAgent.match(/OS 3/i)!=null);inst.options.supportCSSPositionFixed=!inst.options.isIE6&&!inst.options.isIOSLess5;inst.options.iequirksmode=inst.options.isIE6789&&
document.compatMode!="CSS1Compat";inst.options.isTouch="ontouchstart"in window;if(inst.options.isChrome){var match=navigator.userAgent.match(/Chrome\/([0-9]+)/);inst.options.chromeVersion=match&&match.length>=2?parseInt(match[1],10):0}if(inst.options.isAndroid){var match=navigator.userAgent.match(/Android\s([0-9\.]*)/i);inst.options.androidVersion=match&&match.length>=2?parseInt(match[1],10):-1}var v=document.createElement("video");inst.options.canplaymp4=v&&v.canPlayType&&v.canPlayType("video/mp4").replace(/no/,
"");if(inst.options.isFirefox&&inst.options.nativecontrolsonfirefox||(inst.options.isIE6789||inst.options.isIE10||inst.options.isIE11)&&inst.options.nativecontrolsonie||inst.options.isIPhone&&inst.options.nativecontrolsoniphone||inst.options.isIPad&&inst.options.nativecontrolsonipad||inst.options.isAndroid&&inst.options.nativecontrolsonandroid)inst.options.nativehtml5controls=true;if(inst.options.isIOS||inst.options.isAndroid)inst.options.nativecontrolsonfullscreen=true;inst.options.navheight=0;inst.options.thumbgap+=
2*inst.options.thumbborder;inst.options.resizeTimeout=-1;inst.slideTimeout=null;inst.autosliding=false;inst.existingElem=-1;inst.direction=-3;inst.elemArray=new Array;inst.options.curElem=-1;inst.defaultoptions=$.extend({},inst.options);if(inst.options.googleanalyticsaccount&&!window._gaq){window._gaq=window._gaq||[];window._gaq.push(["_setAccount",inst.options.googleanalyticsaccount]);window._gaq.push(["_trackPageview"]);$.getScript("https://ssl.google-analytics.com/ga.js")}if(inst.options.initvimeo){var tag=
document.createElement("script");tag.src=inst.options.jsfolder+"froogaloop2.min.js";var firstScriptTag=document.getElementsByTagName("script")[0];firstScriptTag.parentNode.insertBefore(tag,firstScriptTag)}if(inst.options.inityoutube){var tag=document.createElement("script");tag.src="https://www.youtube.com/iframe_api";var firstScriptTag=document.getElementsByTagName("script")[0];firstScriptTag.parentNode.insertBefore(tag,firstScriptTag)}if(inst.options.initsocial)$("head").append('<link rel="stylesheet" href="'+
inst.options.jsfolder+'icons/css/fontello.css" type="text/css" />');inst.showing=false;inst.navvisible=false;inst.disableEscKey=function(isFullscreen){if(isFullscreen)inst.disableesckeyinfullscreen=true;else setTimeout(function(){inst.disableesckeyinfullscreen=false},1E3)};inst.supportKeyboard=function(){inst.disableesckeyinfullscreen=false;$(document).keyup(function(e){if(!inst.showing)return;if(!inst.disableesckeyinfullscreen&&inst.options.supportesckey&&e.keyCode==27)inst.finish();else if(inst.options.supportarrowkeys)if(e.keyCode==
39)inst.gotoSlide(-1);else if(e.keyCode==37)inst.gotoSlide(-2)});if(inst.options.supportesckey){document.addEventListener("MSFullscreenChange",function(){inst.disableEscKey(document.msFullscreenElement!=null)},false);document.addEventListener("webkitfullscreenchange",function(){inst.disableEscKey(document.webkitIsFullScreen)},false)}};inst.supportKeyboard();inst.init=function(){inst.showing=false;inst.readData();inst.createMarkup();inst.initSlide()};inst.checkParentData=function(node,parentNode,attr){return node.data(attr)?
node.data(attr):parentNode&&parentNode.data(attr)?parentNode.data(attr):null};inst.readNodeData=function(node,parentNode){var fileType="mediatype"in node.data()?node.data("mediatype"):inst.checkType(node.attr("href"));if(fileType<0)return;var title=node.data("title")?node.data("title"):node.attr("title");var group=node.data("group")?node.data("group"):inst.options.userelforgroup?node.attr("rel"):null;if(!group&&parentNode)group=parentNode.data("group")?parentNode.data("group"):inst.options.userelforgroup?
parentNode.attr("rel"):null;for(var i=0;i<inst.elemArray.length;i++)if(node.attr("href")==inst.elemArray[i][ELEM_HREF]){inst.elemArray[i][ELEM_TITLE]=title;inst.elemArray[i][ELEM_GROUP]=group;return}inst.elemArray.push(new Array(fileType,node.attr("href"),title,group,inst.checkParentData(node,parentNode,"width"),inst.checkParentData(node,parentNode,"height"),inst.checkParentData(node,parentNode,"webm"),inst.checkParentData(node,parentNode,"ogg"),inst.checkParentData(node,parentNode,"thumbnail"),inst.checkParentData(node,
parentNode,"description"),null,null,null,inst.checkParentData(node,parentNode,"socialmedia"),inst.checkParentData(node,parentNode,"weblink"),inst.checkParentData(node,parentNode,"weblinktarget"),inst.checkParentData(node,parentNode,"weblinktext")))};inst.readData=function(){inst.each(function(){var self=$(this);if(this.nodeName.toLowerCase()=="a"||this.nodeName.toLowerCase()=="area")inst.readNodeData(self);else self.find("a,area").each(function(){inst.readNodeData($(this),self)})})};inst.createMarkup=
function(){if($(window).width()<=inst.options.fullscreensmallscreenwidth){if(inst.options.fullscreenmodeonsmallscreen){inst.options.fullscreenmode=true;if(inst.options.fullscreennomarginonsmallscreen)inst.options.fullscreennomargin=true}if(inst.options.fullscreenmode&&inst.options.fullscreennomarginonsmallscreen)inst.options.fullscreennomargin=true}if(inst.options.fullscreenmode){inst.options.bgcolor=inst.options.fullscreenbgcolor;inst.options.bordersize=inst.options.fullscreenbordersize;if(inst.options.fullscreennomargin){inst.options.bordersize=
0;inst.options.bordermargin=0;inst.options.bordertopmargin=0;inst.options.bordertopmarginsmall=0}if(inst.options.fullscreentextinside){inst.options.titlestyle="inside";inst.options.titlecss=inst.options.titleinsidecss;inst.options.descriptioncss=inst.options.descriptioninsidecss}else{inst.options.titlebottomcss=inst.options.fullscreentitlebottomcss;inst.options.descriptionbottomcss=inst.options.fullscreendescriptionbottomcss}}inst.options.barheightoriginal=inst.options.barheight;if(inst.options.responsivebarheight){var winH=
$(window).height();if(winH<=inst.options.smallscreenheight)inst.options.barheight=inst.options.barheightonsmallheight}if(!inst.options.titlecss)inst.options.titlecss=inst.options.titlestyle=="inside"?inst.options.titleinsidecss:inst.options.titlebottomcss;if(!inst.options.descriptioncss)inst.options.descriptioncss=inst.options.titlestyle=="inside"?inst.options.descriptioninsidecss:inst.options.descriptionbottomcss;inst.options.titlecss=$.trim(inst.options.titlecss);if(inst.options.titlecss.length>
1){if(inst.options.titlecss.charAt(0)=="{")inst.options.titlecss=inst.options.titlecss.substring(1);if(inst.options.titlecss.charAt(inst.options.titlecss.length-1)=="}")inst.options.titlecss=inst.options.titlecss.substring(0,inst.options.titlecss.length-1)}inst.options.descriptioncss=$.trim(inst.options.descriptioncss);if(inst.options.descriptioncss.length>1){if(inst.options.descriptioncss.charAt(0)=="{")inst.options.descriptioncss=inst.options.descriptioncss.substring(1);if(inst.options.descriptioncss.charAt(inst.options.descriptioncss.length-
1)=="}")inst.options.descriptioncss=inst.options.descriptioncss.substring(0,inst.options.descriptioncss.length-1)}inst.options.errorcss=$.trim(inst.options.errorcss);if(inst.options.errorcss.length>1){if(inst.options.errorcss.charAt(0)=="{")inst.options.errorcss=inst.options.errorcss.substring(1);if(inst.options.errorcss.charAt(inst.options.errorcss.length-1)=="}")inst.options.errorcss=inst.options.errorcss.substring(0,inst.options.errorcss.length-1)}var styleCss=".bodynoscroll {height:100%;overflow:hidden;}";
styleCss+=".html5-hide {display:none !important;} #threedcarousel-html5-lightbox .html5-text {"+inst.options.titlecss+"}";styleCss+="#threedcarousel-html5-lightbox .html5-description {"+inst.options.descriptioncss+"}";styleCss+="#threedcarousel-html5-lightbox .html5-error {"+inst.options.errorcss+"}";if(inst.options.navarrowsalwaysshowontouch||inst.options.alwaysshownavarrows){styleCss+="#threedcarousel-html5-lightbox .html5-prev-touch {left:0px;top:50%;margin-top:-16px;margin-left:-32px;} #threedcarousel-html5-lightbox .html5-next-touch {right:0px;top:50%;margin-top:-16px;margin-right:-32px;}";
styleCss+="@media (max-width: "+inst.options.navarrowsbottomscreenwidth+"px) { #threedcarousel-html5-lightbox .html5-prev-touch {top:100%;left:0;margin:0;} #threedcarousel-html5-lightbox .html5-next-touch {top:100%;right:0;margin:0;} }"}styleCss+="#threedcarousel-html5-lightbox .html5-prev-fullscreen {display:block;} #threedcarousel-html5-lightbox .html5-next-fullscreen {display:block;} #threedcarousel-html5-lightbox .html5-prev-bottom-fullscreen {display:none;} #threedcarousel-html5-lightbox .html5-next-bottom-fullscreen {display:none;}";
styleCss+="@media (max-width: "+inst.options.navarrowsbottomscreenwidth+"px) {#threedcarousel-html5-lightbox .html5-prev-fullscreen {display:none;} #threedcarousel-html5-lightbox .html5-next-fullscreen {display:none;} #threedcarousel-html5-lightbox .html5-prev-bottom-fullscreen {display:block;} #threedcarousel-html5-lightbox .html5-next-bottom-fullscreen {display:block;} }";if(inst.options.titlestyle=="right"){styleCss+="#threedcarousel-html5-lightbox .html5-elem-wrap {width:"+inst.options.imagepercentage+"%;height:100%;} #threedcarousel-html5-lightbox .html5-elem-data-box {min-height:100%;}";
styleCss+="@media (max-width: "+inst.options.sidetobottomscreenwidth+"px) {#threedcarousel-html5-lightbox .html5-elem-wrap {width:100%;height:auto;} #threedcarousel-html5-lightbox .html5-elem-data-box {width:100%;height:auto;min-height:0;}}"}else if(inst.options.titlestyle=="left"){styleCss+="#threedcarousel-html5-lightbox .html5-elem-wrap {height:100%;} #threedcarousel-html5-lightbox .html5-elem-data-box {width:"+String(100-inst.options.imagepercentage)+"%;min-height:100%;}";styleCss+="@media (max-width: "+inst.options.sidetobottomscreenwidth+
"px) {#threedcarousel-html5-lightbox .html5-elem-wrap {width:100%;height:auto;} #threedcarousel-html5-lightbox .html5-elem-data-box {width:100%;height:auto;min-height:0;}}"}styleCss+=".html5-rotate { border-radius:50%; -webkit-transition:-webkit-transform .4s ease-in; transition: transform .4s ease-in; } .html5-rotate:hover { -webkit-transform: rotate(360deg); transform: rotate(360deg); }";styleCss+="@media (max-width: "+inst.options.navarrowsbottomscreenwidth+"px) {#html5-social {"+inst.options.socialpositionsmallscreen+
"}}";$("head").append("<style type='text/css' data-creator='threedcarousel-html5-lightbox'>"+styleCss+"</style>");var elemheight=inst.options.elemautoheight?"auto":"100%";inst.$lightbox=$("<div id='threedcarousel-html5-lightbox' style='display:none;top:0px;left:0px;width:100%;height:100%;z-index:9999998;text-align:center;'>"+"<div id='html5-lightbox-overlay' style='display:block;position:absolute;top:0px;left:0px;width:100%;min-height:100%;background-color:"+inst.options.overlaybgcolor+";opacity:"+inst.options.overlayopacity+
";filter:alpha(opacity="+Math.round(inst.options.overlayopacity*100)+");'></div>"+"<div id='html5-lightbox-box' style='display:block;position:relative;margin:0px auto;'>"+"<div class='html5-elem-box' style='display:block;position:relative;width:100%;overflow-x:hidden;overflow-y:auto;height:"+elemheight+";margin:0px auto;text-align:center;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;'>"+"<div class='html5-elem-wrap' style='display:block;position:relative;margin:0px auto;text-align:center;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;background-color:"+
inst.options.bgcolor+";'>"+"<div class='html5-loading' style='display:none;position:absolute;top:0px;left:0px;text-align:center;width:100%;height:100%;background:url(\""+inst.options.loadingimage+"\") no-repeat center center;'></div>"+"<div class='html5-error-box html5-error' style='display:none;position:absolute;padding:"+inst.options.bordersize+"px;text-align:center;width:"+inst.options.errorwidth+"px;height:"+inst.options.errorheight+"px;'>"+"The requested content cannot be loaded.<br />Please try again later."+
"</div>"+"<div class='html5-image' style='display:none;position:relative;top:0px;left:0px;width:100%;height:100%;"+(inst.options.iequirksmode?"margin":"padding")+":"+inst.options.bordersize+"px;text-align:center;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;'></div>"+"</div>"+"</div>"+"<div id='html5-watermark' style='display:none;position:absolute;left:"+String(inst.options.bordersize+2)+"px;top:"+String(inst.options.bordersize+2)+"px;'></div>"+"</div>"+"</div>");
inst.options.positionFixed=inst.options.supportCSSPositionFixed&&inst.options.responsive&&!inst.options.iequirksmode;if(inst.options.useabsolutepos||inst.options.useabsoluteposonmobile&&inst.options.isMobile)inst.options.positionFixed=false;if(!inst.options.positionFixed)inst.options.bodynoscroll=true;inst.$lightbox.css({position:inst.options.positionFixed?"fixed":"absolute"});inst.$lightbox.appendTo("body");inst.$lightboxBox=$("#html5-lightbox-box",inst.$lightbox);inst.$elem=$(".html5-elem-box",
inst.$lightbox);inst.$elemWrap=$(".html5-elem-wrap",inst.$lightbox);inst.$loading=$(".html5-loading",inst.$lightbox);inst.$error=$(".html5-error-box",inst.$lightbox);inst.$image=$(".html5-image",inst.$lightbox);if(inst.options.fullscreenmode&&inst.options.fullscreennomargin)inst.$elem.css({overflow:"hidden"});var elemText="<div class='html5-elem-data-box' style='display:none;box-sizing:border-box;'><div class='html5-text' style='display:block;overflow:hidden;'></div></div>";if(inst.options.titlestyle==
"left")inst.$elem.prepend(elemText);else inst.$elem.append(elemText);inst.$elemData=$(".html5-elem-data-box",inst.$lightbox);inst.$text=$(".html5-text",inst.$lightbox);if(inst.options.borderradius>0){inst.$elem.css({"border-radius":inst.options.borderradius+"px","-moz-border-radius":inst.options.borderradius+"px","-webkit-border-radius":inst.options.borderradius+"px"});if(inst.options.titlestyle=="inside")inst.$elemWrap.css({"border-radius":inst.options.borderradius+"px","-moz-border-radius":inst.options.borderradius+
"px","-webkit-border-radius":inst.options.borderradius+"px"});else if(inst.options.titlestyle=="bottom"){inst.$elemWrap.css({"border-top-left-radius":inst.options.borderradius+"px","-moz-top-left-border-radius":inst.options.borderradius+"px","-webkit-top-left-border-radius":inst.options.borderradius+"px","border-top-right-radius":inst.options.borderradius+"px","-moz-top-right-border-radius":inst.options.borderradius+"px","-webkit-top-right-border-radius":inst.options.borderradius+"px"});inst.$elemData.css({"border-bottom-left-radius":inst.options.borderradius+
"px","-moz-top-bottom-border-radius":inst.options.borderradius+"px","-webkit-bottom-left-border-radius":inst.options.borderradius+"px","border-bottom-right-radius":inst.options.borderradius+"px","-moz-bottom-right-border-radius":inst.options.borderradius+"px","-webkit-bottom-right-border-radius":inst.options.borderradius+"px"})}}if(inst.options.titlestyle=="right"||inst.options.titlestyle=="left"){inst.$lightboxBox.css({"background-color":inst.options.bgcolor});if(inst.options.titlestyle=="right"){inst.$elemWrap.css({position:"relative",
"float":"left"});inst.$elemData.css({position:"relative",overflow:"hidden",padding:inst.options.bordersize+"px"})}else{inst.$elemWrap.css({position:"relative",overflow:"hidden"});inst.$elemData.css({position:"relative","float":"left",padding:inst.options.bordersize+"px"})}}else if(inst.options.titlestyle=="inside"){inst.$elemData.css({position:"absolute",margin:inst.options.bordersize+"px",bottom:0,left:0,"background-color":"#333","background-color":"rgba(51, 51, 51, 0.6)"});if(inst.options.showonmouseoverinside)inst.$elemData.css({opacity:0});
inst.$text.css({padding:inst.options.bordersize+"px "+2*inst.options.bordersize+"px"})}else{inst.$elemData.css({position:"relative",width:"100%",height:inst.options.barautoheight?"auto":inst.options.barheight+"px","padding":"0 0 "+inst.options.bordersize+"px"+" 0","background-color":inst.options.bgcolor,"text-align":"left"});if(!inst.options.fullscreenmode||!inst.options.fullscreennomargin)inst.$text.css({"margin":"0 "+inst.options.bordersize+"px"})}if(inst.options.showsocial){var socialCode='<div id="html5-social" style="display:none;'+
inst.options.socialposition+'">';var socialBtnCSS=(inst.options.socialdirection=="horizontal"?"display:inline-block;":"display:block;")+"margin:4px;";var socialCSS="display:table-cell;width:"+inst.options.socialbuttonsize+"px;height:"+inst.options.socialbuttonsize+"px;font-size:"+inst.options.socialbuttonfontsize+"px;border-radius:50%;color:#fff;vertical-align:middle;text-align:center;cursor:pointer;padding:0;";if(inst.options.showemail)socialCode+='<div class="html5-social-btn'+(inst.options.socialrotateeffect?
" html5-rotate":"")+' html5-social-email" style="'+socialBtnCSS+'"><div class="mh-icon-mail" style="'+socialCSS+'background-color:#4d83ff;"></div></div>';if(inst.options.showfacebook)socialCode+='<div class="html5-social-btn'+(inst.options.socialrotateeffect?" html5-rotate":"")+' html5-social-facebook" style="'+socialBtnCSS+'"><div class="mh-icon-facebook" style="'+socialCSS+'background-color:#3b5998;"></div></div>';if(inst.options.showtwitter)socialCode+='<div class="html5-social-btn'+(inst.options.socialrotateeffect?
" html5-rotate":"")+' html5-social-twitter" style="'+socialBtnCSS+'"><div class="mh-icon-twitter" style="'+socialCSS+'background-color:#03b3ee;"></div></div>';if(inst.options.showpinterest)socialCode+='<div class="html5-social-btn'+(inst.options.socialrotateeffect?" html5-rotate":"")+' html5-social-pinterest" style="'+socialBtnCSS+'"><div class="mh-icon-pinterest" style="'+socialCSS+'background-color:#c92228;"></div></div>';socialCode+='<div style="clear:both;"></div></div>';inst.$lightboxBox.append(socialCode);
$(".html5-social-btn",inst.$lightbox).click(function(){var shareUrl=window.location.href+(window.location.href.indexOf("?")<0?"?":"&")+"wp3dcarousellightboxshare="+encodeURIComponent(inst.currentElem[ELEM_HREF]);var shareTitle=inst.currentElem[ELEM_TITLE];var shareMedia=inst.currentElem[ELEM_HREF];if(inst.currentElem[ELEM_TYPE]==0)shareMedia=inst.absoluteUrl(inst.currentElem[ELEM_HREF]);else if(inst.currentElem[ELEM_TYPE]==3)shareMedia="https://img.youtube.com/vi/"+inst.getYoutubeId(inst.currentElem[ELEM_HREF])+
"/0.jpg";else{var lightboxLink=$('.wp3dcarousellightbox[href="'+inst.currentElem[ELEM_HREF]+'"]');if(lightboxLink.length>0)if(lightboxLink.data("shareimage")&&lightboxLink.data("shareimage").length>0)shareMedia=inst.absoluteUrl(lightboxLink.data("shareimage"));else if(lightboxLink.data("thumbnail")&&lightboxLink.data("thumbnail").length>0)shareMedia=inst.absoluteUrl(lightboxLink.data("thumbnail"));else{var lightboxImg=$("img",lightboxLink);if(lightboxImg.length>0)shareMedia=inst.absoluteUrl(lightboxImg.attr("src"))}}var isVideo=
inst.currentElem[ELEM_TYPE]==2||inst.currentElem[ELEM_TYPE]==3||inst.currentElem[ELEM_TYPE]==4||inst.currentElem[ELEM_TYPE]==8||inst.currentElem[ELEM_TYPE]==9||inst.currentElem[ELEM_TYPE]==11||inst.currentElem[ELEM_TYPE]==12;if(!shareTitle)shareTitle="";else shareTitle=inst.html2Text(shareTitle);if($(this).hasClass("html5-social-facebook"))window.open("https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(shareUrl)+"&t="+encodeURIComponent(shareTitle),"_blank");else if($(this).hasClass("html5-social-twitter"))window.open("https://twitter.com/share?url="+
encodeURIComponent(shareUrl)+"&text="+encodeURIComponent(shareTitle),"_blank");else if($(this).hasClass("html5-social-pinterest"))window.open("https://pinterest.com/pin/create/bookmarklet/?media="+encodeURIComponent(shareMedia)+"&url="+encodeURIComponent(shareUrl)+"&description="+encodeURIComponent(shareTitle)+"&is_video="+(isVideo?"true":"false"),"_blank");else if($(this).hasClass("html5-social-email"))window.open("mailto:?subject="+encodeURIComponent(shareTitle)+"&body=Check out this: "+encodeURIComponent(shareUrl));
return false})}if(inst.options.fullscreenmode){inst.$lightbox.append("<div class='html5-next-fullscreen' style='cursor:pointer;position:absolute;right:"+inst.options.bordersize+"px;top:50%;margin-top:-16px;'><img alt='' src='"+inst.options.fullscreennextimage+"'></div>"+"<div class='html5-prev-fullscreen' style='cursor:pointer;position:absolute;left:"+inst.options.bordersize+"px;top:50%;margin-top:-16px;'><img alt='' src='"+inst.options.fullscreenprevimage+"'></div>");inst.$next=$(".html5-next-fullscreen",
inst.$lightbox);inst.$prev=$(".html5-prev-fullscreen",inst.$lightbox);inst.$lightboxBox.append("<div class='html5-next-bottom-fullscreen' style='cursor:pointer;position:absolute;top:100%;right:0;margin-top:8px;'><img alt='' src='"+inst.options.fullscreennextimage+"'></div>"+"<div class='html5-prev-bottom-fullscreen' style='cursor:pointer;position:absolute;top:100%;left:0;margin-top:8px;'><img alt='' src='"+inst.options.fullscreenprevimage+"'></div>");inst.$nextbottom=$(".html5-next-bottom-fullscreen",
inst.$lightbox);inst.$prevbottom=$(".html5-prev-bottom-fullscreen",inst.$lightbox);inst.$nextbottom.click(function(){inst.nextArrowClicked()});inst.$prevbottom.click(function(){inst.prevArrowClicked()});inst.$lightbox.append("<div id='html5-close-fullscreen' style='display:block;cursor:pointer;position:absolute;top:0;right:0;margin-top:0;margin-right:0;'><img alt='' src='"+inst.options.fullscreencloseimage+"'></div>");inst.$close=$("#html5-close-fullscreen",inst.$lightbox)}else{inst.$lightboxBox.append("<div class='html5-next' style='display:none;cursor:pointer;position:absolute;right:"+
inst.options.bordersize+"px;top:50%;margin-top:-32px;'><img alt='' src='"+inst.options.nextimage+"'></div>"+"<div class='html5-prev' style='display:none;cursor:pointer;position:absolute;left:"+inst.options.bordersize+"px;top:50%;margin-top:-32px;'><img alt='' src='"+inst.options.previmage+"'></div>");inst.$next=$(".html5-next",inst.$lightbox);inst.$prev=$(".html5-prev",inst.$lightbox);if(inst.options.isTouch&&inst.options.navarrowsalwaysshowontouch||inst.options.alwaysshownavarrows){inst.$lightboxBox.append("<div class='html5-next-touch' style='display:block;cursor:pointer;position:absolute;'><img alt='' src='"+
inst.options.nextimage+"'></div>"+"<div class='html5-prev-touch' style='display:block;cursor:pointer;position:absolute;'><img alt='' src='"+inst.options.previmage+"'></div>");inst.$nexttouch=$(".html5-next-touch",inst.$lightbox);inst.$prevtouch=$(".html5-prev-touch",inst.$lightbox);inst.$nexttouch.click(function(){inst.nextArrowClicked()});inst.$prevtouch.click(function(){inst.prevArrowClicked()})}inst.$lightboxBox.append("<div id='html5-close' style='display:none;cursor:pointer;position:absolute;top:0;right:0;margin-top:-16px;margin-right:-16px;'><img alt='' src='"+
inst.options.closeimage+"'></div>");inst.$close=$("#html5-close",inst.$lightbox)}if(inst.options.titlestyle=="inside"&&inst.options.showonmouseoverinside)inst.$lightboxBox.hover(function(){if(inst.currentElem[ELEM_TYPE]==0||!inst.options.showinsidetitleforimageonly)inst.$elemData.animate({opacity:1},400)},function(){inst.$elemData.animate({opacity:0},400)});inst.$watermark=$("#html5-watermark",inst.$lightbox);if(inst.options.stamp)inst.$watermark.html("<a href='"+inst.options.freelink+"' style='text-decoration:none;' title='WordPress 3D Carousel'><div style='display:block;width:210px;height:20px;text-align:center;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;background-color:#fff;color:#333;font:12px Arial,sans-serif;'><div style='line-height:20px;'>"+
inst.options.freemark+"</div></div></a>");else if(inst.options.watermark){var html="<img alt='' src='"+inst.options.watermark+"' style='border:none;' />";if(inst.options.watermarklink)html="<a href='"+inst.options.watermarklink+"' target='_blank'>"+html+"</a>";inst.$watermark.html(html)}if(inst.options.closeonoverlay)$("#html5-lightbox-overlay",inst.$lightbox).click(inst.finish);inst.$close.click(inst.finish);inst.$next.click(function(){inst.nextArrowClicked()});inst.$prev.click(function(){inst.prevArrowClicked()});
$(window).resize(function(){if(inst.options.isIOS&&!inst.options.mobileresizeevent)return;clearTimeout(inst.options.resizeTimeout);inst.options.resizeTimeout=setTimeout(function(){inst.resizeWindow()},inst.options.resizedelay)});$(window).scroll(function(){if(inst.options.isIOS&&!inst.options.mobileresizeevent)return;inst.scrollBox()});$(window).on("orientationchange",function(e){if(inst.options.isMobile)inst.resizeWindow()});if(inst.options.enabletouchswipe)inst.enableSwipe()};inst.html2Text=function(html){var tag=
document.createElement("div");tag.innerHTML=html;return tag.innerText};inst.slideTimer=function(interval,callback,updatecallback){var timerInstance=this;timerInstance.timeout=interval;var updateinterval=50;var updateTimerId=null;var runningTime=0;var paused=false;var started=false;var startedandpaused=false;this.pause=function(){if(started){paused=true;clearInterval(updateTimerId)}};this.resume=function(forceresume){if(startedandpaused&&!forceresume)return;startedandpaused=false;if(started&&paused){paused=
false;updateTimerId=setInterval(function(){runningTime+=updateinterval;if(runningTime>timerInstance.timeout){clearInterval(updateTimerId);if(callback)callback()}if(updatecallback)updatecallback(runningTime/timerInstance.timeout)},updateinterval)}};this.stop=function(){clearInterval(updateTimerId);if(updatecallback)updatecallback(-1);runningTime=0;paused=false;started=false};this.start=function(){runningTime=0;paused=false;started=true;updateTimerId=setInterval(function(){runningTime+=updateinterval;
if(runningTime>timerInstance.timeout){clearInterval(updateTimerId);if(callback)callback()}if(updatecallback)updatecallback(runningTime/timerInstance.timeout)},updateinterval)};this.startandpause=function(){runningTime=0;paused=true;started=true;startedandpaused=true};return this};inst.updateTimer=function(percent){var w=Math.round(percent*100);if(w>100)w=100;if(w<0)w=0;$(".html5-timer",inst.$lightbox).css({display:"block",width:w+"%"})};inst.initSlide=function(){inst.autosliding=false;inst.slideTimeout=
inst.slideTimer(inst.options.slideinterval,function(){inst.gotoSlide(-1)},inst.options.showtimer?function(percent){inst.updateTimer(percent)}:null);if(inst.options.autoslide){inst.slideTimeout.stop();inst.autosliding=true}};inst.nextArrowClicked=function(){if(inst.options.nextElem<=inst.options.curElem)if(inst.options.onlastarrowclicked&&window[inst.options.onlastarrowclicked]&&typeof window[inst.options.onlastarrowclicked]=="function")window[inst.options.onlastarrowclicked]();inst.gotoSlide(-1)};
inst.prevArrowClicked=function(){if(inst.options.prevElem>=inst.options.curElem)if(inst.options.onfirstarrowclicked&&window[inst.options.onfirstarrowclicked]&&typeof window[inst.options.onfirstarrowclicked]=="function")window[inst.options.onfirstarrowclicked]();inst.gotoSlide(-2)};inst.calcNextPrevElem=function(){inst.options.nextElem=-1;inst.options.prevElem=-1;inst.options.inGroup=false;inst.options.groupIndex=0;inst.options.groupCount=0;var group=inst.elemArray[inst.options.curElem][ELEM_GROUP];
for(var i=0;i<inst.elemArray.length;i++)if(inst.matchGroup(group,inst.elemArray[i][ELEM_GROUP])){if(i==inst.options.curElem)inst.options.groupIndex=inst.options.groupCount;inst.options.groupCount++}var j,curGroup=inst.elemArray[inst.options.curElem][ELEM_GROUP];if(curGroup!=undefined&&curGroup!=null){for(j=inst.options.curElem+1;j<inst.elemArray.length;j++)if(inst.matchGroup(curGroup,inst.elemArray[j][ELEM_GROUP])){inst.options.nextElem=j;break}if(inst.options.nextElem<0)for(j=0;j<inst.options.curElem;j++)if(inst.matchGroup(curGroup,
inst.elemArray[j][ELEM_GROUP])){inst.options.nextElem=j;break}if(inst.options.nextElem>=0){for(j=inst.options.curElem-1;j>=0;j--)if(inst.matchGroup(curGroup,inst.elemArray[j][ELEM_GROUP])){inst.options.prevElem=j;break}if(inst.options.prevElem<0)for(j=inst.elemArray.length-1;j>inst.options.curElem;j--)if(inst.matchGroup(curGroup,inst.elemArray[j][ELEM_GROUP])){inst.options.prevElem=j;break}}}if(inst.options.nextElem>=0||inst.options.prevElem>=0)inst.options.inGroup=true};inst.calcBoxPosition=function(initW,
initH){var boxW=initW+2*inst.options.bordersize;var boxH=initH+2*inst.options.bordersize;var navH=inst.options.shownavigation&&inst.navvisible?inst.options.navheight:0;var winH=$(window).height();var boxT=Math.round((winH-navH)/2-boxH/2);if(inst.options.titlestyle=="bottom")boxT-=Math.round(inst.options.barheight/2);var topmargin=$(window).height()<inst.options.smallscreenheight?inst.options.bordertopmarginsmall:inst.options.bordertopmargin;if(boxT<topmargin)boxT=topmargin;if(inst.options.insideiframe&&
window.self!=window.top)if(parent.window.jQuery&&parent.window.jQuery("#"+inst.options.iframeid).length){var iframetop=parent.window.jQuery("#"+inst.options.iframeid).offset().top;var parentscroll=parent.window.document.body.scrollTop;boxT=topmargin;boxT+=parentscroll>iframetop?parentscroll-iframetop:0}return[boxW,boxH,boxT]};inst.hideNavArrows=function(){var showPrev=false;var showNext=false;if(inst.options.inGroup){if(inst.options.arrowloop||!inst.options.arrowloop&&inst.options.prevElem<inst.options.curElem)showPrev=
true;if(inst.options.arrowloop||!inst.options.arrowloop&&inst.options.nextElem>inst.options.curElem)showNext=true}if(showPrev){inst.$prev.removeClass("html5-hide");if(inst.$prevbottom)inst.$prevbottom.removeClass("html5-hide");if(inst.$prevtouch)inst.$prevtouch.removeClass("html5-hide")}else{inst.$prev.addClass("html5-hide");if(inst.$prevbottom)inst.$prevbottom.addClass("html5-hide");if(inst.$prevtouch)inst.$prevtouch.addClass("html5-hide")}if(showNext){inst.$next.removeClass("html5-hide");if(inst.$nextbottom)inst.$nextbottom.removeClass("html5-hide");
if(inst.$nexttouch)inst.$nexttouch.removeClass("html5-hide")}else{inst.$next.addClass("html5-hide");if(inst.$nextbottom)inst.$nextbottom.addClass("html5-hide");if(inst.$nexttouch)inst.$nexttouch.addClass("html5-hide")}};inst.clickHandler=function(){var $this=$(this);var dataoptions={};$.each($this.data(),function(key,value){dataoptions[key.toLowerCase()]=value});inst.options=$.extend(inst.options,inst.defaultoptions,dataoptions);$(window).trigger("html5lightbox.lightboxshow");inst.init();if(inst.elemArray.length<=
0)return true;inst.hideObjects();for(var i=0;i<inst.elemArray.length;i++)if(inst.elemArray[i][ELEM_HREF]==$this.attr("href"))break;if(i==inst.elemArray.length)return true;inst.options.curElem=i;inst.calcNextPrevElem();inst.reset();inst.$lightbox.show();var boxPos=inst.calcBoxPosition(inst.options.loadingwidth,inst.options.loadingheight);var boxW=boxPos[0];var boxH=boxPos[1];var boxT=boxPos[2];if(inst.options.iequirksmode)inst.$lightboxBox.css({"top":boxT});else inst.$lightboxBox.css({"margin-top":boxT});
if(inst.options.titlestyle=="left"||inst.options.titlestyle=="right")inst.$lightboxBox.css({"width":boxW,"height":boxH});else{inst.$lightboxBox.css({"width":boxW,"height":"auto"});inst.$elemWrap.css({"width":boxW,"height":boxH})}inst.loadCurElem();return false};inst.loadThumbnail=function(src,index,title){var imgLoader=new Image;$(imgLoader).on("load",function(){var style;if(this.width/this.height<=inst.options.thumbwidth/inst.options.thumbheight)style="width:100%;";else style="height:100%;";$(".html5-nav-thumb").eq(index).html("<img alt='"+
inst.html2Text(title)+"' style='"+style+"' src='"+src+"' />")});imgLoader.src=src};inst.matchGroup=function(curGroup,elemGroup){if(inst.options.showall)return true;if(!curGroup||!elemGroup)return false;var curs=curGroup.split(":");var elems=elemGroup.split(":");var result=false;for(var i in curs)if($.inArray(curs[i],elems)>-1){result=true;break}return result};inst.showNavigation=function(){if(!inst.options.shownavigation)return;if(!inst.currentElem||!inst.currentElem[ELEM_GROUP])return;var i;var showNav=
false;var group=inst.currentElem[ELEM_GROUP];for(i=0;i<inst.elemArray.length;i++)if(inst.matchGroup(group,inst.elemArray[i][ELEM_GROUP]))if(inst.elemArray[i][ELEM_THUMBNAIL]&&inst.elemArray[i][ELEM_THUMBNAIL].length>0){showNav=true;break}if(!showNav)return;inst.options.navheight=inst.options.thumbheight+inst.options.thumbtopmargin+inst.options.thumbbottommargin;if($(".html5-nav").length>0)return;var posCss=inst.options.hidenavdefault?"top:100%;bottom:auto;left:0;right:0;":"top:auto;bottom:0;left:0;right:0;";
var posType=inst.options.positionFixed?"fixed":"absolute";$("body").append("<div class='html5-nav' style='display:block;position:"+posType+";"+posCss+"width:100%;height:"+inst.options.navheight+"px;z-index:9999999;"+(inst.options.navbgcolor?"background-color:"+inst.options.navbgcolor+";":"")+"'>"+"<div class='html5-nav-container' style='position:relative;margin:"+inst.options.thumbtopmargin+"px auto "+inst.options.thumbbottommargin+"px;'>"+"<div class='html5-nav-prev' style='display:block;position:absolute;cursor:pointer;width:"+
inst.options.navbuttonwidth+'px;height:100%;left:0;top:0;background:url("'+inst.options.navarrowsprevimage+"\") no-repeat left center;'></div>"+"<div class='html5-nav-mask' style='display:block;position:relative;margin:0 auto;overflow:hidden;'>"+"<div class='html5-nav-list'></div>"+"</div>"+"<div class='html5-nav-next' style='display:block;position:absolute;cursor:pointer;width:"+inst.options.navbuttonwidth+'px;height:100%;right:0;top:0;background:url("'+inst.options.navarrowsnextimage+"\") no-repeat right center;'></div>"+
"</div>"+"</div>");inst.navvisible=inst.options.hidenavdefault?false:true;if(inst.options.shownavcontrol){$(".html5-nav").append('<div class="html5-nav-showcontrol" style="position:absolute;display:block;cursor:pointer;bottom:100%;right:12px;margin:0;padding:0;"><img alt="" src="'+inst.options.navcontrolimage+'"></div>');$(".html5-nav-showcontrol").click(function(){var winH=$(window).height();var navH=$(".html5-nav").height();if(inst.navvisible){inst.navvisible=false;$(".html5-nav").css({top:winH-
navH+"px",bottom:"auto"}).animate({top:winH+"px"},function(){$(this).css({top:"100%",bottom:"auto"})})}else{inst.navvisible=true;var navH=$(".html5-nav").height();$(".html5-nav").css({top:winH+"px",bottom:"auto"}).animate({top:winH-navH+"px"},function(){$(this).css({top:"auto",bottom:0})})}inst.resizeWindow()})}var index=0;for(i=0;i<inst.elemArray.length;i++)if(inst.matchGroup(group,inst.elemArray[i][ELEM_GROUP]))if(inst.elemArray[i][ELEM_THUMBNAIL]&&inst.elemArray[i][ELEM_THUMBNAIL].length>0){$(".html5-nav-list").append("<div class='html5-nav-thumb' data-arrayindex='"+
i+"' style='float:left;overflow:hidden;cursor:pointer;opacity:"+inst.options.thumbopacity+";margin: 0 "+inst.options.thumbgap/2+"px;width:"+inst.options.thumbwidth+"px;height:"+inst.options.thumbheight+"px;border:"+inst.options.thumbborder+"px solid "+inst.options.thumbbordercolor+";'></div>");this.loadThumbnail(inst.elemArray[i][ELEM_THUMBNAIL],index,inst.elemArray[i][ELEM_TITLE]);index++}$(".html5-nav-thumb").hover(function(){$(this).css({opacity:1});$(this).css({border:inst.options.thumbborder+
"px solid "+inst.options.thumbhighlightbordercolor})},function(){$(this).css({opacity:inst.options.thumbopacity});$(this).css({border:inst.options.thumbborder+"px solid "+inst.options.thumbbordercolor})});$(".html5-nav-thumb").click(function(){var index=$(this).data("arrayindex");if(index>=0)inst.gotoSlide(index)});inst.options.totalwidth=index*(inst.options.thumbgap+inst.options.thumbwidth+2*inst.options.thumbborder);$(".html5-nav-list").css({display:"block",position:"relative","margin-left":0,width:inst.options.totalwidth+
"px"}).append("<div style='clear:both;'></div>");var $navMask=$(".html5-nav-mask");var $navPrev=$(".html5-nav-prev");var $navNext=$(".html5-nav-next");$navPrev.click(function(){var $navList=$(".html5-nav-list");var $navNext=$(".html5-nav-next");var winWidth=$(window).width();var maskWidth=winWidth-2*inst.options.navbuttonwidth;var marginLeft=parseInt($navList.css("margin-left"))+maskWidth;if(marginLeft>=0){marginLeft=0;$(this).css({"background-position":"center left"})}else $(this).css({"background-position":"center right"});
if(marginLeft<=maskWidth-inst.options.totalwidth)$navNext.css({"background-position":"center left"});else $navNext.css({"background-position":"center right"});$navList.animate({"margin-left":marginLeft})});$navNext.click(function(){var $navList=$(".html5-nav-list");var $navPrev=$(".html5-nav-prev");var winWidth=$(window).width();var maskWidth=winWidth-2*inst.options.navbuttonwidth;var marginLeft=parseInt($navList.css("margin-left"))-maskWidth;if(marginLeft<=maskWidth-inst.options.totalwidth){marginLeft=
maskWidth-inst.options.totalwidth;$(this).css({"background-position":"center left"})}else $(this).css({"background-position":"center right"});if(marginLeft>=0)$navPrev.css({"background-position":"center left"});else $navPrev.css({"background-position":"center right"});$navList.animate({"margin-left":marginLeft})});var winWidth=$(window).width();if(inst.options.totalwidth<=winWidth){$navMask.css({width:inst.options.totalwidth+"px"});$navPrev.hide();$navNext.hide()}else{$navMask.css({width:winWidth-
2*inst.options.navbuttonwidth+"px"});$navPrev.show();$navNext.show()}};inst.loadElem=function(elem){inst.currentElem=elem;inst.showing=true;if(inst.options.bodynoscroll)$("html,body").addClass("bodynoscroll");if(!(inst.options.showtitle&&inst.currentElem[ELEM_TITLE]&&inst.currentElem[ELEM_TITLE].length>0||inst.options.showdescription&&inst.currentElem[ELEM_DESCRIPTION]&&inst.currentElem[ELEM_DESCRIPTION].length>0||inst.options.inGroup&&(inst.options.showplaybutton||inst.options.showtitleprefix)))inst.options.barheight=
0;inst.showNavigation();inst.$elem.off("mouseenter").off("mouseleave").off("mousemove");inst.$loading.show();if(inst.options.onshowitem&&window[inst.options.onshowitem]&&typeof window[inst.options.onshowitem]=="function")window[inst.options.onshowitem](elem);if((inst.options.transition=="slide"||inst.options.transition=="crossfade")&&inst.existingElem>=0){$(".html5-elem-box-previous").remove();var newitem=inst.$elem.clone();newitem.insertAfter(inst.$elem);inst.$prevelem=inst.$elem;inst.$elem=newitem;
inst.$prevelem.addClass("html5-elem-box-previous");inst.$elem.addClass("html5-elem-box-current");inst.$elemWrap=$(".html5-elem-wrap",inst.$elem);inst.$loading=$(".html5-loading",inst.$elem);inst.$error=$(".html5-error-box",inst.$elem);inst.$image=$(".html5-image",inst.$elem);inst.$elemData=$(".html5-elem-data-box",inst.$elem);inst.$text=$(".html5-text",inst.$elem);inst.$elem.css({position:"absolute",top:0,left:inst.options.transition=="slide"?inst.direction==-1?"100%":"-100%":0,opacity:0,height:"auto"});
inst.$prevelem.css({width:inst.$prevelem.width()+"px",height:inst.$prevelem.height()+"px"})}switch(elem[ELEM_TYPE]){case 0:var imgLoader=new Image;$(imgLoader).on("load",function(){elem[ELEM_ORIGINALWIDTH]=imgLoader.width;elem[ELEM_ORIGINALHEIGHT]=imgLoader.height;inst.showImage(elem,imgLoader.width,imgLoader.height)});$(imgLoader).on("error",function(){inst.showError()});imgLoader.src=elem[ELEM_HREF];break;case 1:inst.showSWF(elem);break;case 2:case 8:inst.showVideo(elem);break;case 3:case 4:case 9:case 11:case 12:inst.showYoutubeVimeo(elem);
break;case 5:inst.showPDF(elem);break;case 6:inst.showMP3(elem);break;case 7:inst.showWeb(elem);break;case 10:inst.showDiv(elem);break}if(inst.options.googleanalyticsaccount&&window._gaq)window._gaq.push(["_trackEvent","Lightbox","Open",elem[ELEM_HREF]]);if(inst.options.preload){if(inst.options.nextElem>=0&&inst.elemArray[inst.options.nextElem][ELEM_TYPE]==0)(new Image).src=inst.elemArray[inst.options.nextElem][ELEM_HREF];if(inst.options.prevElem>=0&&inst.elemArray[inst.options.prevElem][ELEM_TYPE]==
0)(new Image).src=inst.elemArray[inst.options.prevElem][ELEM_HREF]}};inst.loadCurElem=function(){inst.loadElem(inst.elemArray[inst.options.curElem])};inst.showError=function(){inst.$loading.hide();inst.resizeLightbox(inst.options.errorwidth,inst.options.errorheight,true,function(){inst.$loading.hide();inst.$error.show();inst.$elem.fadeIn(inst.options.fadespeed,function(){inst.showData()})})};inst.calcTextWidth=function(objW){return objW-36};inst.showTitle=function(w,t,description){if(inst.options.titlestyle==
"inside")inst.$elemData.css({width:w+"px"});var text="";if(inst.options.showtitle)if(t&&t.length>0)text+=t;if(inst.options.inGroup){if(inst.options.showtitleprefix)text="<span class='html5-title-prefix'>"+inst.options.titleprefix.replace("%NUM",inst.options.groupIndex+1).replace("%TOTAL",inst.options.groupCount)+"</span> <span class='html5-title-caption'>"+text+"</span>";if(inst.options.showplaybutton)text="<div class='html5-playpause' style='display:inline-block;cursor:pointer;vertical-align:middle;'><div class='html5-play' style='display:block;'><img alt='' src='"+
inst.options.playimage+"'></div><div class='html5-pause' style='display:none;'><img alt='' src='"+inst.options.pauseimage+"'></div></div> "+text}if(text.length>0)text='<div class="html5-title">'+text+"</div>";if(inst.options.showdescription&&description&&description.length>0)text+='<div class="html5-description">'+description+"</div>";inst.$text.html(text);if(inst.options.inGroup&&inst.options.showplaybutton){if(inst.autosliding){$(".html5-play",inst.$lightbox).hide();$(".html5-pause",inst.$lightbox).show()}else{$(".html5-play",
inst.$lightbox).show();$(".html5-pause",inst.$lightbox).hide()}$(".html5-play",inst.$lightbox).click(function(){$(".html5-play",inst.$lightbox).hide();$(".html5-pause",inst.$lightbox).show();if(inst.slideTimeout){inst.slideTimeout.stop();inst.slideTimeout.start();inst.autosliding=true}});$(".html5-pause",inst.$lightbox).click(function(){$(".html5-play",inst.$lightbox).show();$(".html5-pause",inst.$lightbox).hide();if(inst.slideTimeout){inst.slideTimeout.stop();inst.autosliding=false}})}$("#html5-social",
inst.$lightbox).show();if(inst.options.showsocialmedia)if(inst.currentElem[ELEM_SOCIALMEDIA])if($("#html5-socialmedia",inst.$lightboxBox).length>0)$("#html5-socialmedia",inst.$lightboxBox).html(inst.currentElem[ELEM_SOCIALMEDIA]);else inst.$lightboxBox.append('<div id="html5-socialmedia" style="'+inst.options.socialmediaposition+'">'+inst.currentElem[ELEM_SOCIALMEDIA]+"</div>");else if($("#html5-socialmedia",inst.$lightboxBox).length>0)$("#html5-socialmedia",inst.$lightboxBox).remove()},inst.showImage=
function(elem,imgW,imgH){var elemW,elemH;if(elem[ELEM_WIDTH])elemW=elem[ELEM_WIDTH];else{elemW=imgW;elem[ELEM_WIDTH]=imgW}if(elem[ELEM_HEIGHT])elemH=elem[ELEM_HEIGHT];else{elemH=imgH;elem[ELEM_HEIGHT]=imgH}var sizeObj=inst.calcElemSize({w:elemW,h:elemH},inst.options.imagekeepratio);inst.resizeLightbox(sizeObj.w,sizeObj.h,true,function(){inst.$loading.hide();inst.showTitle(sizeObj.w,elem[ELEM_TITLE],elem[ELEM_DESCRIPTION]);var timercode=!inst.options.showtimer||!inst.options.inGroup?"":"<div class='html5-timer' style='display:none;position:absolute;"+
inst.options.timerposition+":0;left:0;width:0;height:"+inst.options.timerheight+"px;background-color:"+inst.options.timercolor+";opacity:"+inst.options.timeropacity+";'></div>";var titlecode=elem[ELEM_WEBLINKTEXT]&&elem[ELEM_WEBLINKTEXT].length>0?" title='"+elem[ELEM_WEBLINKTEXT].replace(/'/g,"&#39;")+"'":"";var targetcode=elem[ELEM_WEBLINKTARGET]&&elem[ELEM_WEBLINKTARGET].length>0?" target='"+elem[ELEM_WEBLINKTARGET]+"'":"";var linkcode=elem[ELEM_WEBLINK]&&elem[ELEM_WEBLINK].length>0?"<a href='"+
elem[ELEM_WEBLINK]+"'"+titlecode+targetcode+">":"";var linkcodeafter=elem[ELEM_WEBLINK]&&elem[ELEM_WEBLINK].length>0?"</a>":"";inst.$image.hide();inst.$image.html("<div class='html5-image-container' style='display:block;position:relative;width:100%;height:100%;"+(inst.options.imagekeepratio?"overflow:hidden;":"overflow:auto;")+"'>"+linkcode+"<img class='html5-image-img' alt='"+inst.html2Text(elem[ELEM_TITLE])+"' src='"+elem[ELEM_HREF]+"' width='100%' height='"+(inst.options.imagekeepratio?"100%":
"auto")+"' />"+linkcodeafter+timercode+"</div>");inst.$image.fadeIn(inst.options.fadespeed);inst.showData();if(inst.autosliding){inst.slideTimeout.stop();inst.slideTimeout.start()}})};inst.showSWF=function(elem){var dataW=elem[ELEM_WIDTH]?elem[ELEM_WIDTH]:inst.options.defaultwidth;var dataH=elem[ELEM_HEIGHT]?elem[ELEM_HEIGHT]:inst.options.defaultheight;var sizeObj=inst.calcElemSize({w:dataW,h:dataH},true);dataW=sizeObj.w;dataH=sizeObj.h;inst.resizeLightbox(dataW,dataH,true,function(){inst.$loading.hide();
inst.showTitle(sizeObj.w,elem[ELEM_TITLE],elem[ELEM_DESCRIPTION]);inst.$image.html("<div class='html5lightbox-swf' style='display:block;width:100%;height:100%;'></div>").show();inst.embedFlash($(".html5lightbox-swf",inst.$image),elem[ELEM_HREF],"window",{width:dataW,height:dataH});inst.$elem.show();inst.showData();if(inst.autosliding){inst.slideTimeout.stop();inst.slideTimeout.start()}})};inst.showVideo=function(elem){inst.slideTimeout.stop();var dataW=elem[ELEM_WIDTH]?elem[ELEM_WIDTH]:inst.options.defaultwidth;
var dataH=elem[ELEM_HEIGHT]?elem[ELEM_HEIGHT]:inst.options.defaultheight;var sizeObj=inst.calcElemSize({w:dataW,h:dataH},true);dataW=sizeObj.w;dataH=sizeObj.h;inst.resizeLightbox(dataW,dataH,true,function(){inst.$loading.hide();inst.showTitle(sizeObj.w,elem[ELEM_TITLE],elem[ELEM_DESCRIPTION]);inst.$image.html("<div class='html5lightbox-video' style='display:block;width:100%;height:100%;overflow:hidden;background-color:"+inst.options.videobgcolor+";'></div>").show();var isHTML5=false;if(inst.options.isIE678||
elem[ELEM_TYPE]==8||inst.options.isIE9&&inst.options.useflashonie9||inst.options.isIE10&&inst.options.useflashonie10||inst.options.isIE11&&inst.options.useflashonie11)isHTML5=false;else if(inst.options.isMobile)isHTML5=true;else if((inst.options.html5player||!inst.options.flashInstalled)&&inst.options.html5VideoSupported){isHTML5=true;if(inst.options.isFirefox||inst.options.isOpera)if(!elem[ELEM_HREF_WEBM]&&!elem[ELEM_HREF_OGG]&&(!inst.options.canplaymp4||inst.options.useflashformp4onfirefox))isHTML5=
false}if(isHTML5){var videoSrc=elem[ELEM_HREF];if(inst.options.isFirefox||inst.options.isOpera)if(elem[ELEM_HREF_WEBM])videoSrc=elem[ELEM_HREF_WEBM];else if(elem[ELEM_HREF_OGG])videoSrc=elem[ELEM_HREF_OGG];inst.embedHTML5Video($(".html5lightbox-video",inst.$image),videoSrc,inst.options.autoplay,inst.options.loopvideo);if(elem[ELEM_WEBLINK]&&elem[ELEM_WEBLINK].length>0)$(".html5-lightbox-video",inst.$image).css({cursor:"pointer"}).click(function(){if(elem[ELEM_WEBLINKTARGET]&&elem[ELEM_WEBLINKTARGET].length>
0)window.open(elem[ELEM_WEBLINK],elem[ELEM_WEBLINKTARGET]);else window.open(elem[ELEM_WEBLINK])})}else{var videoFile=elem[ELEM_HREF];if(videoFile.charAt(0)!="/"&&videoFile.substring(0,5)!="http:"&&videoFile.substring(0,6)!="https:")videoFile=inst.options.htmlfolder+videoFile;inst.embedFlash($(".html5lightbox-video",inst.$image),inst.options.jsfolder+"html5boxplayer.swf","transparent",{width:dataW,height:dataH,jsobjectname:"wp3DCarouselLightboxObject",hidecontrols:inst.options.videohidecontrols?"1":"0",hideplaybutton:"0",
videofile:videoFile,hdfile:"",ishd:"0",defaultvolume:inst.options.defaultvideovolume,autoplay:inst.options.autoplay?"1":"0",loop:inst.options.loopvideo?"1":"0",errorcss:".html5box-error"+inst.options.errorcss,id:0})}inst.$elem.show();inst.showData()})};inst.loadNext=function(){$(window).trigger("html5lightbox.videofinished");if(inst.autosliding)inst.gotoSlide(-1);else if(inst.options.autoclose)setTimeout(function(){inst.finish()},inst.options.autoclosedelay)};inst.getYoutubeParams=function(href){var result=
{};if(href.indexOf("?")<0)return result;var params=href.substring(href.indexOf("?")+1).split("&");for(var i=0;i<params.length;i++){var value=params[i].split("=");if(value&&value.length==2&&value[0].toLowerCase()!="v")result[value[0].toLowerCase()]=value[1]}return result};inst.getYoutubeId=function(href){var youtubeId="";var regExp=/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\??v?=?))([^#\&\?]*).*/;var match=href.match(regExp);if(match&&match[7]&&match[7].length==11)youtubeId=match[7];return youtubeId};
inst.prepareYoutubeHref=function(href){var youtubeId=inst.getYoutubeId(href);var protocol="https:";var result=protocol+"//www.youtube.com/embed/"+youtubeId;var params=this.getYoutubeParams(href);var first=true;for(var key in params){if(first){result+="?";first=false}else result+="&";result+=key+"="+params[key]}return result};inst.prepareDailymotionHref=function(href){if(href.match(/\:\/\/.*(dai\.ly)/i)){var protocol="https:";var id=href.match(/(dai\.ly\/)([a-zA-Z0-9\-\_]+)/)[2];href=protocol+"//www.dailymotion.com/embed/video/"+
id}return href};inst.showYoutubeVimeo=function(elem){inst.slideTimeout.stop();var dataW=elem[ELEM_WIDTH]?elem[ELEM_WIDTH]:inst.options.defaultwidth;var dataH=elem[ELEM_HEIGHT]?elem[ELEM_HEIGHT]:inst.options.defaultheight;var sizeObj=inst.calcElemSize({w:dataW,h:dataH},true);dataW=sizeObj.w;dataH=sizeObj.h;if(inst.options.noresizecallback){inst.resizeLightbox(dataW,dataH,true,function(){inst.showData()});inst.showYouTubeVimeoCallback(elem,sizeObj)}else inst.resizeLightbox(dataW,dataH,true,function(){inst.showYouTubeVimeoCallback(elem,
sizeObj);inst.showData()})};inst.showYouTubeVimeoCallback=function(elem,sizeObj){inst.$loading.hide();inst.showTitle(sizeObj.w,elem[ELEM_TITLE],elem[ELEM_DESCRIPTION]);inst.$image.html("<div class='html5lightbox-video' style='display:block;width:100%;height:100%;overflow:hidden;'></div>").show();var href=elem[ELEM_HREF];var youtubeid="";if(elem[ELEM_TYPE]==3){youtubeid=inst.getYoutubeId(href);href=inst.prepareYoutubeHref(href)}if(elem[ELEM_TYPE]==9)href=inst.prepareDailymotionHref(href);if(inst.options.autoplay){href+=
href.indexOf("?")<0?"?":"&";if(elem[ELEM_TYPE]==11)href+="autoPlay=true";else href+="autoplay=1"}if(inst.options.loopvideo){href+=href.indexOf("?")<0?"?":"&";switch(elem[ELEM_TYPE]){case 3:href+="loop=1&playlist="+youtubeid;break;case 4:case 9:href+="loop=1";break;case 11:href+="endVideoBehavior=loop";break}}if(elem[ELEM_TYPE]==3){if(href.indexOf("?")<0)href+="?wmode=transparent&rel=0";else href+="&wmode=transparent&rel=0";if(inst.options.videohidecontrols)href+="&controls=0&showinfo=0";href+="&enablejsapi=1&origin="+
document.location.protocol+"//"+document.location.hostname}else if(elem[ELEM_TYPE]==4){href+=href.indexOf("?")<0?"?":"&";href+="api=1&player_id=html5boxiframevideo"+inst.options.curElem}$(".html5lightbox-video",inst.$image).html("<iframe style='margin:0;padding:0;border:0;' class='html5boxiframevideo' id='html5boxiframevideo"+inst.options.curElem+"' width='100%' height='100%' src='"+href+"' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>");inst.$elem.show();if(elem[ELEM_TYPE]==
3&&typeof YT==="object"&&typeof YT.Player==="function")inst.ytplayer=new YT.Player("html5boxiframevideo"+inst.options.curElem,{events:{"onStateChange":function(event){if(event.data==YT.PlayerState.ENDED){$(window).trigger("html5lightbox.videofinished");if(inst.autosliding)inst.gotoSlide(-1);else if(inst.options.autoclose)setTimeout(function(){inst.finish()},inst.options.autoclosedelay)}}}});else if(elem[ELEM_TYPE]==4&&typeof $f==="function"){var vimeoIframe=$("#html5boxiframevideo"+inst.options.curElem)[0];
inst.vimeoPlayer=$f(vimeoIframe);inst.vimeoPlayer.addEvent("ready",function(){inst.vimeoPlayer.addEvent("finish",function(id){$(window).trigger("html5lightbox.videofinished");if(inst.autosliding)inst.gotoSlide(-1);else if(inst.options.autoclose)setTimeout(function(){inst.finish()},inst.options.autoclosedelay)})})}};inst.showPDF=function(elem){if(inst.options.enablepdfjs){if(inst.options.isIPhone&&inst.options.openpdfinnewtaboniphone||inst.options.isIPad&&inst.options.openpdfinnewtabonipad){var win=
window.open(elem[ELEM_HREF],"_blank");win.focus();inst.finish();return}if(!inst.options.pdfjsengine)inst.options.pdfjsengine=inst.options.jsfolder+"pdfjs/web/viewer.html";var href=elem[ELEM_HREF];if(href.substring(0,5)!="http:"&&href.substring(0,6)!="https:")href=inst.absoluteUrl(href);var pdfelem=jQuery.extend(true,{},elem);pdfelem[ELEM_HREF]=inst.options.pdfjsengine+(inst.options.pdfjsengine.indexOf("?")<0?"?":"&")+"file="+encodeURIComponent(href);inst.showWeb(pdfelem)}else if(inst.options.isIPhone||
inst.options.isIPad||inst.options.isAndroid||inst.options.isIE||inst.options.isIE11){var win=window.open(elem[ELEM_HREF],"_blank");win.focus();inst.finish();return}else inst.showWeb(elem)};inst.showMP3=function(elem){};inst.showDiv=function(elem){var winWidth=$(window).width();var winH=$(window).height();var navH=inst.options.shownavigation&&inst.navvisible?inst.options.navheight:0;var dataW=elem[ELEM_WIDTH]?elem[ELEM_WIDTH]:inst.options.usedefaultsizeforcontent?inst.options.defaultwidth:winWidth;
var dataH=elem[ELEM_HEIGHT]?elem[ELEM_HEIGHT]:inst.options.usedefaultsizeforcontent?inst.options.defaultheight:winH-navH;var sizeObj=inst.calcElemSize({w:dataW,h:dataH},false);dataW=sizeObj.w;dataH=sizeObj.h;inst.resizeLightbox(dataW,dataH,true,function(){inst.$loading.hide();inst.showTitle(sizeObj.w,elem[ELEM_TITLE],elem[ELEM_DESCRIPTION]);inst.$image.html("<div class='html5lightbox-div' id='html5lightbox-div"+inst.options.curElem+"' style='display:block;width:100%;height:"+(inst.options.autoresizecontent?
"auto":"100%")+";"+(inst.options.isIOS?"-webkit-overflow-scrolling:touch;overflow-y:scroll;":"overflow:auto;")+"'></div>").show();var divID=elem[ELEM_HREF];if($(divID).length>0)$(divID).children().appendTo($("#html5lightbox-div"+inst.options.curElem,inst.$image));else $("#html5lightbox-div"+inst.options.curElem,inst.$image).html("<div class='html5-error'>The specified div ID does not exist.</div>");inst.$elem.show();inst.showData();if(inst.options.autoresizecontent)inst.resizeWindow();if(inst.autosliding){inst.slideTimeout.stop();
inst.slideTimeout.start()}})};inst.isSameDomain=function(href){if(href.substring(0,5)!="http:"&&href.substring(0,6)!="https:")return true;var link=document.createElement("a");link.setAttribute("href",href);var result=link.protocol==document.location.protocol&&link.host==document.location.host&&link.port==document.location.port;link=null;return result};inst.showWeb=function(elem){var winWidth=$(window).width();var winH=$(window).height();var navH=inst.options.shownavigation&&inst.navvisible?inst.options.navheight:
0;var dataW=elem[ELEM_WIDTH]?elem[ELEM_WIDTH]:inst.options.usedefaultsizeforcontent?inst.options.defaultwidth:winWidth;var dataH=elem[ELEM_HEIGHT]?elem[ELEM_HEIGHT]:inst.options.usedefaultsizeforcontent?inst.options.defaultheight:winH-navH;var sizeObj=inst.calcElemSize({w:dataW,h:dataH},false);dataW=sizeObj.w;dataH=sizeObj.h;inst.resizeLightbox(dataW,dataH,true,function(){inst.$loading.hide();inst.showTitle(sizeObj.w,elem[ELEM_TITLE],elem[ELEM_DESCRIPTION]);inst.$image.html("<div class='html5lightbox-web' style='display:block;width:100%;height:100%;"+
(inst.options.isIOS?"-webkit-overflow-scrolling:touch;overflow-y:scroll;":"")+"'></div>").show();$(".html5lightbox-web",inst.$image).html("<iframe style='margin:0;padding:0;border:0;' class='html5lightbox-web-iframe' width='100%' height='100%' src='"+elem[ELEM_HREF]+"' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>");inst.$elem.show();inst.showData();if(inst.options.autoresizecontent&&inst.isSameDomain(elem[ELEM_HREF])){$(".html5lightbox-web-iframe",inst.$image).data("sameorigin",
true);$(".html5lightbox-web-iframe",inst.$image).on("load",function(){$(this).data("sameoriginloaded",true);inst.resizeWindow()})}if(inst.autosliding){inst.slideTimeout.stop();inst.slideTimeout.start()}})};inst.scrollBox=function(){};inst.resizeWindow=function(){if(!inst.currentElem)return;if(!inst.options.responsive)return;var winWidth=$(window).width();var winH=$(window).height();if(inst.options.responsivebarheight){if(winH<=inst.options.smallscreenheight)inst.options.barheight=inst.options.barheightonsmallheight;
else inst.options.barheight=inst.options.barheightoriginal;if(inst.options.titlestyle=="bottom"&&inst.options.barautoheight!="auto")inst.$elemData.css({height:inst.options.barheight+"px","max-height":inst.options.barheight+"px"})}if(!(inst.options.showtitle&&inst.currentElem[ELEM_TITLE]&&inst.currentElem[ELEM_TITLE].length>0||inst.options.showdescription&&inst.currentElem[ELEM_DESCRIPTION]&&inst.currentElem[ELEM_DESCRIPTION].length>0||inst.options.inGroup&&(inst.options.showplaybutton||inst.options.showtitleprefix)))inst.options.barheight=
0;var elemW,elemH,keepratio;if(inst.currentElem[ELEM_TYPE]==5||inst.currentElem[ELEM_TYPE]==7||inst.currentElem[ELEM_TYPE]==10){var navH=inst.options.shownavigation&&inst.navvisible?inst.options.navheight:0;elemW=inst.currentElem[ELEM_WIDTH]?inst.currentElem[ELEM_WIDTH]:inst.options.usedefaultsizeforcontent?inst.options.defaultwidth:winWidth;elemH=inst.currentElem[ELEM_HEIGHT]?inst.currentElem[ELEM_HEIGHT]:inst.options.usedefaultsizeforcontent?inst.options.defaultheight:winH-navH;keepratio=false}else{elemW=
inst.currentElem[ELEM_WIDTH]?inst.currentElem[ELEM_WIDTH]:inst.options.defaultwidth;elemH=inst.currentElem[ELEM_HEIGHT]?inst.currentElem[ELEM_HEIGHT]:inst.options.defaultheight;if(inst.currentElem[ELEM_TYPE]==0)keepratio=inst.options.imagekeepratio;else keepratio=true}var sizeObj=inst.calcElemSize({w:elemW,h:elemH},keepratio);var boxPos=inst.calcBoxPosition(sizeObj.w,sizeObj.h);var boxW=boxPos[0];var boxH=boxPos[1];var boxT=boxPos[2];inst.$lightboxBox.css({"margin-top":boxT});if(inst.options.titlestyle==
"left"||inst.options.titlestyle=="right")inst.$lightboxBox.css({"width":boxW,"height":boxH});else{inst.$lightboxBox.css({"width":boxW,"height":"auto"});inst.$elemWrap.css({"width":boxW,"height":boxH})}if(inst.options.titlestyle=="inside")inst.$elemData.css({width:sizeObj.w+"px"});if(inst.options.autoresizecontent&&(inst.currentElem[ELEM_TYPE]==5||inst.currentElem[ELEM_TYPE]==7||inst.currentElem[ELEM_TYPE]==10)){var resizeHeight=false;if(inst.currentElem[ELEM_TYPE]==7&&$(".html5lightbox-web-iframe",
inst.$lightbox).length>0&&$(".html5lightbox-web-iframe",inst.$lightbox).data("sameoriginloaded")){var iframe=$(".html5lightbox-web-iframe",inst.$lightbox)[0];if(iframe&&iframe.contentWindow&&iframe.contentWindow.document&&iframe.contentWindow.document.documentElement.offsetHeight)if(elemH>iframe.contentWindow.document.documentElement.offsetHeight){elemH=iframe.contentWindow.document.documentElement.offsetHeight;resizeHeight=true}}else if(inst.currentElem[ELEM_TYPE]==10&&$(".html5lightbox-div",inst.$lightbox).length>
0){var divH=$(".html5lightbox-div",inst.$lightbox).height();if(elemH>divH){elemH=divH;resizeHeight=true}}if(resizeHeight){sizeObj=inst.calcElemSize({w:elemW,h:elemH},keepratio);boxPos=inst.calcBoxPosition(sizeObj.w,sizeObj.h);boxW=boxPos[0];boxH=boxPos[1];boxT=boxPos[2];inst.$lightboxBox.css({"margin-top":boxT});if(inst.options.titlestyle=="left"||inst.options.titlestyle=="right")inst.$lightboxBox.css({"height":boxH});else{inst.$lightboxBox.css({"height":"auto"});inst.$elemWrap.css({"height":boxH})}}}if($(".html5-nav").length<=
0)return;$(".html5-nav-list").css({"margin-left":0});var $navMask=$(".html5-nav-mask");var $navPrev=$(".html5-nav-prev");var $navNext=$(".html5-nav-next");var winWidth=$(window).width();if(inst.options.totalwidth<=winWidth){$navMask.css({width:inst.options.totalwidth+"px"});$navPrev.hide();$navNext.hide()}else{$navMask.css({width:winWidth-2*inst.options.navbuttonwidth+"px"});$navPrev.show();$navNext.show()}};inst.calcElemSize=function(sizeObj,keepratio){if(!inst.options.responsive)return sizeObj;
var winWidth=$(window).width();winWidth=winWidth?winWidth:$(document).width();var winH=$(window).height();winH=winH?winH:$(document).height();if((inst.options.titlestyle=="left"||inst.options.titlestyle=="right")&&winWidth>inst.options.sidetobottomscreenwidth)sizeObj.w=sizeObj.w*100/inst.options.imagepercentage;var navH=inst.options.shownavigation&&inst.navvisible?inst.options.navheight:0;var topmargin=$(window).height()<inst.options.smallscreenheight?inst.options.bordertopmarginsmall:inst.options.bordertopmargin;
var h0=winH-navH-2*inst.options.bordersize-2*topmargin;if(inst.options.titlestyle=="bottom")h0-=inst.options.barheight;var w0=winWidth-2*inst.options.bordersize-2*inst.options.bordermargin;if(inst.options.fullscreenmode&&winWidth>inst.options.navarrowsbottomscreenwidth||(inst.options.isTouch&&inst.options.navarrowsalwaysshowontouch||inst.options.alwaysshownavarrows)&&winWidth>inst.options.navarrowsbottomscreenwidth)w0-=64;if((inst.options.titlestyle=="left"||inst.options.titlestyle=="right")&&winWidth<=
inst.options.sidetobottomscreenwidth||inst.options.notkeepratioonsmallheight&&winH<=inst.options.smallscreenheight)keepratio=false;if(inst.currentElem[ELEM_TYPE]==0&&keepratio){var ratio=inst.currentElem[ELEM_ORIGINALWIDTH]/inst.currentElem[ELEM_ORIGINALHEIGHT];sizeObj.h=Math.round(sizeObj.w/ratio);if(sizeObj.h>h0){sizeObj.w=Math.round(ratio*h0);sizeObj.h=h0}if(sizeObj.w>w0){sizeObj.h=Math.round(w0/ratio);sizeObj.w=w0}}else{if(sizeObj.h>h0){if(keepratio)sizeObj.w=Math.round(sizeObj.w*h0/sizeObj.h);
sizeObj.h=h0}else if(inst.options.maxheight)sizeObj.h=h0;if(sizeObj.w>w0){if(keepratio)sizeObj.h=Math.round(sizeObj.h*w0/sizeObj.w);sizeObj.w=w0}}return sizeObj};inst.showData=function(){if(inst.$text.text().length>0)inst.$elemData.show();if(inst.options.titlestyle=="bottom"||inst.options.titlestyle=="inside")inst.$lightboxBox.css({height:"auto"});if(inst.$text.text().length>0&&inst.options.titlestyle=="bottom")inst.$elemData.css({"max-height":inst.options.barheight+"px"});if(inst.options.positionFixed)$("#html5-lightbox-overlay",
inst.$lightbox).css({height:Math.max($(window).height(),$(document).height())});else $("#html5-lightbox-overlay",inst.$lightbox).css({height:"100%"});$(window).trigger("html5lightbox.lightboxopened")};inst.resizeLightbox=function(elemW,elemH,bAnimate,onFinish){inst.hideNavArrows();var boxPos=inst.calcBoxPosition(elemW,elemH);var boxW=boxPos[0];var boxH=boxPos[1];var boxT=boxPos[2];inst.$loading.hide();inst.$watermark.hide();if(inst.options.nextElem<=inst.options.curElem)if(inst.options.onlastitem&&
window[inst.options.onlastitem]&&typeof window[inst.options.onlastitem]=="function")window[inst.options.onlastitem](inst.currentElem);if(inst.options.prevElem>=inst.options.curElem)if(inst.options.onfirstitem&&window[inst.options.onfirstitem]&&typeof window[inst.options.onfirstitem]=="function")window[inst.options.onfirstitem](inst.currentElem);if(!inst.options.fullscreenmode&&(!inst.options.isTouch||!inst.options.navarrowsalwaysshowontouch)&&!inst.options.alwaysshownavarrows){inst.$lightboxBox.on("mouseenter mousemove",
function(){if(inst.options.arrowloop&&inst.options.prevElem>=0||!inst.options.arrowloop&&inst.options.prevElem>=0&&inst.options.prevElem<inst.options.curElem)inst.$prev.fadeIn();if(inst.options.arrowloop&&inst.options.nextElem>=0||!inst.options.arrowloop&&inst.options.nextElem>=0&&inst.options.nextElem>inst.options.curElem)inst.$next.fadeIn()});inst.$lightboxBox.on("mouseleave",function(){inst.$next.fadeOut();inst.$prev.fadeOut()})}var existingBoxT=parseInt(inst.$lightboxBox.css("margin-top"));inst.$lightboxBox.css({"margin-top":boxT});
var speed=bAnimate?inst.options.resizespeed:0;if(inst.options.fullscreenmode)speed=0;if((inst.options.transition=="slide"||inst.options.transition=="crossfade")&&inst.existingElem>=0)speed=0;if(inst.options.titlestyle=="left"||inst.options.titlestyle=="right"){if(boxW==inst.$lightboxBox.width()&&boxH==inst.$lightboxBox.height())speed=0;inst.$lightboxBox.animate({width:boxW},speed).animate({height:boxH},speed,function(){inst.onAnimateFinish(onFinish)})}else{if(boxW==inst.$elemWrap.width()&&boxH==inst.$elemWrap.height())speed=
0;inst.$lightboxBox.css({"width":boxW,"height":"auto"});if((inst.options.transition=="slide"||inst.options.transition=="crossfade")&&inst.existingElem>=0)if($(".html5-elem-box-previous",inst.$lightbox).length>0)$(".html5-elem-box-previous",inst.$lightbox).css({"margin-left":String(boxW/2-$(".html5-elem-box-previous",inst.$lightbox).width()/2)+"px",top:String(existingBoxT-boxT)+"px"});inst.$elemWrap.animate({width:boxW},speed).animate({height:boxH},speed,function(){inst.onAnimateFinish(onFinish)})}};
inst.onAnimateFinish=function(onFinish){inst.$loading.show();inst.$watermark.show();inst.$close.show();inst.$elem.css({"background-color":inst.options.bgcolor});onFinish();inst.finishCallback()};inst.finishCallback=function(){if((inst.options.transition=="slide"||inst.options.transition=="crossfade")&&inst.existingElem>=0)if(inst.options.transition=="slide"){inst.$prevelem.animate({left:inst.direction==-1?"-100%":"100%",opacity:0},{duration:inst.options.transitionduration});inst.$elem.animate({left:0,
opacity:1},{duration:inst.options.transitionduration,always:function(){inst.$prevelem.remove();inst.$elem.removeClass("html5-elem-box-current").css({position:"relative",height:"100%"})}})}else{inst.$prevelem.animate({opacity:0},{duration:inst.options.transitionduration});inst.$elem.animate({opacity:1},{duration:inst.options.transitionduration,always:function(){inst.$prevelem.remove();inst.$elem.removeClass("html5-elem-box-current").css({position:"relative",height:"100%"})}})}},inst.resetDiv=function(elemID){if(inst.elemArray.length>
0&&elemID>=0)if(inst.elemArray[elemID][ELEM_TYPE]==10){var divID=inst.elemArray[elemID][ELEM_HREF];if($(divID).length>0)$("#html5lightbox-div"+elemID).children().appendTo($(divID))}};inst.reset=function(){if(inst.options.stamp)inst.$watermark.hide();inst.showing=false;inst.$image.empty();inst.$text.empty();inst.$error.hide();inst.$loading.hide();inst.$image.hide();if(inst.options.titlestyle=="bottom"||inst.options.titlestyle=="inside")inst.$elemData.hide();if(!inst.options.fullscreenmode)inst.$close.hide();
inst.$elem.css({"background-color":""})};inst.resetNavigation=function(){inst.options.navheight=0;$(".html5-nav").remove();inst.navvisible=false};inst.finish=function(){inst.existingElem=-1;inst.resetDiv(inst.options.curElem);if($(".html5-lightbox-video",inst.$lightbox).length)$(".html5-lightbox-video",inst.$lightbox).attr("src","");$("head").find("style").each(function(){if($(this).data("creator")=="threedcarousel-html5-lightbox")$(this).remove()});if(inst.options.bodynoscroll)$("html,body").removeClass("bodynoscroll");
inst.slideTimeout.stop();inst.reset();inst.resetNavigation();inst.$lightbox.remove();$("#threedcarousel-html5-lightbox").remove();inst.showObjects();if(inst.options.oncloselightbox&&window[inst.options.oncloselightbox]&&typeof window[inst.options.oncloselightbox]=="function")window[inst.options.oncloselightbox](inst.currentElem);if(inst.onLightboxClosed&&typeof inst.onLightboxClosed=="function")inst.onLightboxClosed(inst.currentElem);$(window).trigger("html5lightbox.lightboxclosed")};inst.pauseSlide=function(){};
inst.playSlide=function(){};inst.gotoSlide=function(slide){inst.existingElem=inst.options.curElem;inst.direction=slide;inst.resetDiv(inst.options.curElem);if(slide==-1){if(inst.options.nextElem<0)return;inst.options.curElem=inst.options.nextElem}else if(slide==-2){if(inst.options.prevElem<0)return;inst.options.curElem=inst.options.prevElem}else if(slide>=0){inst.direction=slide>inst.options.curElem?-1:-2;inst.options.curElem=slide}if(inst.autosliding)inst.slideTimeout.stop();inst.calcNextPrevElem();
if(inst.options.transition!="slide"&&inst.options.transition!="crossfade")inst.reset();inst.loadCurElem();if(inst.options.titlestyle=="inside"&&inst.options.showonmouseoverinside&&inst.currentElem[ELEM_TYPE]!==0&&inst.options.showinsidetitleforimageonly)inst.$elemData.css({opacity:0})};inst.enableSwipe=function(){var preventDefault=inst.options.isAndroid&&(inst.options.swipepreventdefaultonandroid||inst.options.androidVersion>=0&&inst.options.androidVersion<=5)?true:false;inst.$lightboxBox.html5lightboxTouchSwipe({preventWebBrowser:preventDefault,
swipeDistance:inst.options.swipedistance,swipeLeft:function(){inst.gotoSlide(-1)},swipeRight:function(){inst.gotoSlide(-2)}})};inst.hideObjects=function(){$("select, embed, object").css({"visibility":"hidden"})};inst.showObjects=function(){$("select, embed, object").css({"visibility":"visible"})};inst.embedHTML5Video=function($container,src,autoplay,loopvideo){$container.html("<div style='display:block;width:100%;height:100%;position:relative;'><video class='html5-lightbox-video' width='100%' height='100%'"+
(inst.options.html5videoposter&&inst.options.html5videoposter.length>0?"poster='"+inst.options.html5videoposter+"'":"")+(autoplay?" autoplay":"")+(loopvideo?" loop":"")+(inst.options.nativehtml5controls&&!inst.options.videohidecontrols?" controls='controls'":"")+(inst.options.nativecontrolsnodownload?' controlsList="nodownload"':"")+" src='"+src+"'></div>");if(!inst.options.nativehtml5controls&&!inst.options.videohidecontrols){$("video",$container).data("src",src);$("video",$container).lightboxHTML5VideoControls(inst.options.skinsfolder,
inst,".html5-lightbox-video",inst.options.videohidecontrols,false,inst.options.defaultvideovolume,inst.options.nativecontrolsonfullscreen,inst.options.nativecontrolsnodownload,null)}$("video",$container).off("ended").on("ended",function(){$(window).trigger("html5lightbox.videofinished");if(inst.autosliding)inst.gotoSlide(-1);else if(inst.options.autoclose)setTimeout(function(){inst.finish()},inst.options.autoclosedelay)})};inst.embedFlash=function($container,src,wmode,flashVars){if(inst.options.flashInstalled){var htmlOptions=
{pluginspage:"http://www.adobe.com/go/getflashplayer",quality:"high",allowFullScreen:"true",allowScriptAccess:"always",type:"application/x-shockwave-flash"};htmlOptions.width="100%";htmlOptions.height="100%";htmlOptions.src=src;htmlOptions.flashVars=$.param(flashVars);htmlOptions.wmode=wmode;var htmlString="";for(var key in htmlOptions)htmlString+=key+"="+htmlOptions[key]+" ";$container.html("<embed "+htmlString+"/>")}else $container.html("<div class='html5lightbox-flash-error' style='display:block; position:relative;text-align:center; width:100%; left:0px; top:40%;'><div class='html5-error'><div>The required Adobe Flash Player plugin is not installed</div><br /><div style='display:block;position:relative;text-align:center;width:112px;height:33px;margin:0px auto;'><a href='http://www.adobe.com/go/getflashplayer'><img src='http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' width='112' height='33'></img></a></div></div>")};
inst.checkType=function(href){if(!href)return-1;if(href.match(/\.(jpg|gif|png|bmp|jpeg)(.*)?$/i))return 0;if(href.match(/[^\.]\.(swf)\s*$/i))return 1;if(href.match(/\.(mp4|m4v|ogv|ogg|webm)(.*)?$/i))return 2;if(href.match(/\:\/\/.*(youtube\.com)/i)||href.match(/\:\/\/.*(youtu\.be)/i))return 3;if(href.match(/\:\/\/.*(vimeo\.com)/i))return 4;if(href.match(/\:\/\/.*(dailymotion\.com)/i)||href.match(/\:\/\/.*(dai\.ly)/i))return 9;if(href.match(/[^\.]\.(pdf)\s*$/i))return 5;if(href.match(/[^\.]\.(mp3)\s*$/i))return 6;
if(href.match(/[^\.]\.(flv)\s*$/i))return 8;if(href.match(/\#\w+/i))return 10;if(href.match(/\:\/\/.*(wistia)/i))return 11;return 7};inst.getURLParams=function(){var result={};var params=window.location.search.substring(1).split("&");for(var i=0;i<params.length;i++){var value=params[i].split("=");if(value&&value.length==2)result[value[0].toLowerCase()]=unescape(value[1])}return result};inst.absoluteUrl=function(href){var link=document.createElement("a");link.href=href;return link.protocol+"//"+link.host+
link.pathname+link.search+link.hash};inst.showLightboxObject=function(obj){if(obj)inst.showLightbox(obj.type,obj.href,obj.title,obj.width,obj.height,obj.webm,obj.ogg,obj.thumbnail,obj.description,obj.div,obj.originalwidth,obj.originalheight,obj.socialmedia,obj.weblink,obj.weblinktarget,obj.weblinktext,obj.group)};inst.showLightbox=function(type,href,title,width,height,webm,ogg,thumbnail,description,div,originalwidth,originalheight,socialmedia,weblink,weblinktarget,weblinktext,group){inst.options=
$.extend(inst.options,inst.defaultoptions);$(window).trigger("html5lightbox.lightboxshow");inst.init();inst.reset();inst.$lightbox.show();var boxPos=inst.calcBoxPosition(inst.options.loadingwidth,inst.options.loadingheight);var boxW=boxPos[0];var boxH=boxPos[1];var boxT=boxPos[2];inst.$lightboxBox.css({"margin-top":boxT});if(inst.options.titlestyle=="left"||inst.options.titlestyle=="right")inst.$lightboxBox.css({"width":boxW,"height":boxH});else{inst.$lightboxBox.css({"width":boxW,"height":"auto"});
inst.$elemWrap.css({"width":boxW,"height":boxH})}inst.loadElem(new Array(type,href,title,group,width,height,webm,ogg,thumbnail,description,div,originalwidth,originalheight,socialmedia,weblink,weblinktarget,weblinktext))};inst.addItem=function(href,title,group,width,height,webm,ogg,thumbnail,description,mediatype){type=mediatype&&mediatype>=0?mediatype:inst.checkType(href);inst.elemArray.push(new Array(type,href,title,group,width,height,webm,ogg,thumbnail,description))};inst.showItem=function(href){inst.options=
$.extend(inst.options,inst.defaultoptions);$(window).trigger("html5lightbox.lightboxshow");inst.init();if(inst.elemArray.length<=0)return true;inst.hideObjects();for(var i=0;i<inst.elemArray.length;i++)if(inst.elemArray[i][ELEM_HREF]==href)break;if(i==inst.elemArray.length)return true;inst.options.curElem=i;inst.calcNextPrevElem();inst.reset();inst.$lightbox.show();var boxPos=inst.calcBoxPosition(inst.options.loadingwidth,inst.options.loadingheight);var boxW=boxPos[0];var boxH=boxPos[1];var boxT=
boxPos[2];inst.$lightboxBox.css({"margin-top":boxT});if(inst.options.titlestyle=="left"||inst.options.titlestyle=="right")inst.$lightboxBox.css({"width":boxW,"height":boxH});else{inst.$lightboxBox.css({"width":boxW,"height":"auto"});inst.$elemWrap.css({"width":boxW,"height":boxH})}inst.loadCurElem();return false};inst.each(function(){var self=$(this);var nodeName=this.nodeName.toLowerCase();if(nodeName=="a"||nodeName=="area")self.off("click").click(inst.clickHandler);else self.find("a,area").each(function(){$(this).off("click").click(inst.clickHandler)});
var autoopen=false;var autoopendelay=0;if(typeof wp3dcarousellightbox_options!="undefined"&&wp3dcarousellightbox_options){if("autoopen"in wp3dcarousellightbox_options)autoopen=wp3dcarousellightbox_options.autoopen;if("autoopendelay"in wp3dcarousellightbox_options)autoopendelay=wp3dcarousellightbox_options.autoopendelay}autoopen=self.data("autoopen")?self.data("autoopen"):autoopen;autoopendelay=self.data("autoopendelay")?self.data("autoopendelay"):autoopendelay;if(autoopen){setTimeout(function(){self.click()},autoopendelay);return false}if(self.data("preloadonpageload")){var fileType=
"mediatype"in self.data()?self.data("mediatype"):inst.checkType(self.attr("href"));if(fileType==0){var preloaddelay=self.data("preloaddelay")?self.data("preloaddelay"):0;setTimeout(function(){(new Image).src=self.attr("href")},preloaddelay)}}});var urlParams=inst.getURLParams();if("wp3dcarousellightboxshare"in urlParams){var shareUrl=decodeURIComponent(urlParams["wp3dcarousellightboxshare"]);var shareLink=$('.wp3dcarousellightbox[href="'+shareUrl+'"]');if(shareLink.length>0)shareLink.click()}if(inst.options.preloadallonpageload)setTimeout(function(){inst.each(function(){if(this.nodeName.toLowerCase()!=
"a"&&this.nodeName.toLowerCase()!="area")return;var fileType="mediatype"in $(this).data()?$(this).data("mediatype"):inst.checkType($(this).attr("href"));if(fileType!==0)return;(new Image).src=$(this).attr("href");if($(this).data("thumbnail"))(new Image).src=$(this).data("thumbnail")})},inst.options.preloadalldelay);return inst}})(jQuery);(function($){$.fn.html5lightboxTouchSwipe=function(options){var defaults={preventWebBrowser:false,swipeDistance:0,swipeLeft:null,swipeRight:null,swipeTop:null,swipeBottom:null};
if(options)$.extend(defaults,options);return this.each(function(){var startX=-1,startY=-1;var curX=-1,curY=-1;function touchStart(event){var e=event.originalEvent;if(e.targetTouches.length>=1){startX=e.targetTouches[0].pageX;startY=e.targetTouches[0].pageY}else touchCancel(event)}function touchMove(event){if(defaults.preventWebBrowser)event.preventDefault();var e=event.originalEvent;if(e.targetTouches.length>=1){curX=e.targetTouches[0].pageX;curY=e.targetTouches[0].pageY}else touchCancel(event)}function touchEnd(event){if(curX>
0||curY>0){triggerHandler();touchCancel(event)}else touchCancel(event)}function touchCancel(event){startX=-1;startY=-1;curX=-1;curY=-1}function triggerHandler(){if(Math.abs(curX-startX)>Math.abs(curY-startY)){if(Math.abs(curX-startX)>defaults.swipeDistance)if(curX>startX){if(defaults.swipeRight)defaults.swipeRight.call()}else if(defaults.swipeLeft)defaults.swipeLeft.call()}else if(Math.abs(curY-startY)>defaults.swipeDistance)if(curY>startY){if(defaults.swipeBottom)defaults.swipeBottom.call()}else if(defaults.swipeTop)defaults.swipeTop.call()}
try{$(this).on("touchstart",touchStart);$(this).on("touchmove",touchMove);$(this).on("touchend",touchEnd);$(this).on("touchcancel",touchCancel)}catch(e){}})}})(jQuery);(function($){$.fn.lightboxHTML5VideoControls=function(skinFolder,parentInst,videoElem,hideControls,hidePlayButton,defaultVolume,fullscreenNativeControls,html5VideoNoDownload,skinImages){var isTouch="ontouchstart"in window;var eStart=isTouch?"touchstart":"mousedown";var eMove=isTouch?"touchmove":"mousemove";var eCancel=isTouch?"touchcancel":
"mouseup";var eClick="click";var BUTTON_SIZE=32;var BAR_HEIGHT=isTouch?48:36;var hideControlsTimerId=null;var hideVolumeBarTimeoutId=null;var sliderDragging=false;var isFullscreen=false;var userActive=true;var isHd=$(this).data("ishd");var hd=$(this).data("hd");var src=$(this).data("src");var $videoObj=$(this);$videoObj.get(0).removeAttribute("controls");var $videoPlay=$("<div class='html5boxVideoPlay'></div>");$videoObj.after($videoPlay);var playbuttonImage=skinImages&&"playbutton"in skinImages&&
skinImages.playbutton.length>0?skinImages.playbutton:skinFolder+"html5boxplayer_playvideo.png";$videoPlay.css({position:"absolute",top:"50%",left:"50%",display:"block",cursor:"pointer",width:64,height:64,"margin-left":-32,"margin-top":-32,"background-image":"url('"+playbuttonImage+"')","background-position":"center center","background-repeat":"no-repeat"}).on(eClick,function(){$videoObj.get(0).play()});var $videoFullscreenBg=$("<div class='html5boxVideoFullscreenBg'></div>");var $videoControls=$("<div class='html5boxVideoControls'>"+
"<div class='html5boxVideoControlsBg'></div>"+"<div class='html5boxPlayPause'>"+"<div class='html5boxPlay'></div>"+"<div class='html5boxPause'></div>"+"</div>"+"<div class='html5boxTimeCurrent'>--:--</div>"+"<div class='html5boxFullscreen'></div>"+"<div class='html5boxHD'></div>"+"<div class='html5boxVolume'>"+"<div class='html5boxVolumeButton'></div>"+"<div class='html5boxVolumeBar'>"+"<div class='html5boxVolumeBarBg'>"+"<div class='html5boxVolumeBarActive'></div>"+"</div>"+"</div>"+"</div>"+"<div class='html5boxTimeTotal'>--:--</div>"+
"<div class='html5boxSeeker'>"+"<div class='html5boxSeekerBuffer'></div>"+"<div class='html5boxSeekerPlay'></div>"+"<div class='html5boxSeekerHandler'></div>"+"</div>"+"<div style='clear:both;'></div>"+"</div>");$videoObj.after($videoControls);$videoObj.after($videoFullscreenBg);$videoFullscreenBg.css({display:"none",position:"fixed",left:0,top:0,bottom:0,right:0});$videoControls.css({display:"block",position:"absolute",width:"100%",height:BAR_HEIGHT,left:0,bottom:0,right:0,margin:"0 auto"});var userActivate=
function(){userActive=true};$videoObj.on("touch click mousemove mouseenter",function(){userActive=true});if(!hideControls)setInterval(function(){if(userActive){$videoControls.show();userActive=false;clearTimeout(hideControlsTimerId);hideControlsTimerId=setTimeout(function(){if(!$videoObj.get(0).paused)$videoControls.fadeOut()},5E3)}},250);$(".html5boxVideoControlsBg",$videoControls).css({display:"block",position:"absolute",width:"100%",height:"100%",left:0,top:0,"background-color":"#000000",opacity:0.7,
filter:"alpha(opacity=70)"});$(".html5boxPlayPause",$videoControls).css({display:"block",position:"relative",width:BUTTON_SIZE+"px",height:BUTTON_SIZE+"px",margin:Math.floor((BAR_HEIGHT-BUTTON_SIZE)/2),"float":"left"});var $videoBtnPlay=$(".html5boxPlay",$videoControls);var $videoBtnPause=$(".html5boxPause",$videoControls);$videoBtnPlay.css({display:"block",position:"absolute",top:0,left:0,width:BUTTON_SIZE+"px",height:BUTTON_SIZE+"px",cursor:"pointer","background-image":"url('"+skinFolder+"html5boxplayer_playpause.png"+
"')","background-position":"top left"}).hover(function(){$(this).css({"background-position":"bottom left"})},function(){$(this).css({"background-position":"top left"})}).on(eClick,function(){$videoObj.get(0).play()});$videoBtnPause.css({display:"none",position:"absolute",top:0,left:0,width:BUTTON_SIZE+"px",height:BUTTON_SIZE+"px",cursor:"pointer","background-image":"url('"+skinFolder+"html5boxplayer_playpause.png"+"')","background-position":"top right"}).hover(function(){$(this).css({"background-position":"bottom right"})},
function(){$(this).css({"background-position":"top right"})}).on(eClick,function(){$videoObj.get(0).pause()});var $videoTimeCurrent=$(".html5boxTimeCurrent",$videoControls);var $videoTimeTotal=$(".html5boxTimeTotal",$videoControls);var $videoSeeker=$(".html5boxSeeker",$videoControls);var $videoSeekerPlay=$(".html5boxSeekerPlay",$videoControls);var $videoSeekerBuffer=$(".html5boxSeekerBuffer",$videoControls);var $videoSeekerHandler=$(".html5boxSeekerHandler",$videoControls);$videoTimeCurrent.css({display:"block",
position:"relative","float":"left","line-height":BAR_HEIGHT+"px","font-weight":"normal","font-size":"12px",margin:"0 8px","font-family":"Arial, Helvetica, sans-serif",color:"#fff"});$videoTimeTotal.css({display:"block",position:"relative","float":"right","line-height":BAR_HEIGHT+"px","font-weight":"normal","font-size":"12px",margin:"0 8px","font-family":"Arial, Helvetica, sans-serif",color:"#fff"});$videoSeeker.css({display:"block",cursor:"pointer",overflow:"hidden",position:"relative",height:"10px",
"background-color":"#222",margin:Math.floor((BAR_HEIGHT-10)/2)+"px 4px"}).on(eStart,function(e){var e0=isTouch?e.originalEvent.touches[0]:e;var pos=e0.pageX-$videoSeeker.offset().left;$videoSeekerPlay.css({width:pos});$videoObj.get(0).currentTime=pos*$videoObj.get(0).duration/$videoSeeker.width();$videoSeeker.on(eMove,function(e){var e0=isTouch?e.originalEvent.touches[0]:e;var pos=e0.pageX-$videoSeeker.offset().left;$videoSeekerPlay.css({width:pos});$videoObj.get(0).currentTime=pos*$videoObj.get(0).duration/
$videoSeeker.width()})}).on(eCancel,function(){$videoSeeker.off(eMove)});$videoSeekerBuffer.css({display:"block",position:"absolute",left:0,top:0,height:"100%","background-color":"#444"});$videoSeekerPlay.css({display:"block",position:"absolute",left:0,top:0,height:"100%","background-color":"#fcc500"});var $videoFSObj=fullscreenNativeControls?$videoObj:$videoObj.parent();if($videoFSObj.get(0).requestFullscreen||$videoFSObj.get(0).webkitRequestFullScreen||$videoFSObj.get(0).mozRequestFullScreen||$videoFSObj.get(0).webkitEnterFullScreen||
$videoFSObj.get(0).msRequestFullscreen){var switchScreen=function(fullscreen){if(fullscreen){if(fullscreenNativeControls){$videoObj.get(0).setAttribute("controls","controls");if(html5VideoNoDownload)$videoObj.get(0).setAttribute("controlsList","nodownload")}if($videoFSObj.get(0).requestFullscreen)$videoFSObj.get(0).requestFullscreen();else if($videoFSObj.get(0).webkitRequestFullScreen)$videoFSObj.get(0).webkitRequestFullScreen();else if($videoFSObj.get(0).mozRequestFullScreen)$videoFSObj.get(0).mozRequestFullScreen();
else if($videoFSObj.get(0).webkitEnterFullScreen)$videoFSObj.get(0).webkitEnterFullScreen();if($videoFSObj.get(0).msRequestFullscreen)$videoFSObj.get(0).msRequestFullscreen()}else if(document.cancelFullScreen)document.cancelFullScreen();else if(document.mozCancelFullScreen)document.mozCancelFullScreen();else if(document.webkitCancelFullScreen)document.webkitCancelFullScreen();else if(document.webkitExitFullscreen)document.webkitExitFullscreen();else if(document.msExitFullscreen)document.msExitFullscreen()};
var switchScreenCSS=function(fullscreen){if(fullscreenNativeControls)if(fullscreen){$videoObj.get(0).setAttribute("controls","controls");if(html5VideoNoDownload)$videoObj.get(0).setAttribute("controlsList","nodownload")}else $videoObj.get(0).removeAttribute("controls");else if(fullscreen){$(document).on("mousemove",userActivate);$videoControls.css({"z-index":2147483647,position:"fixed"});$videoFullscreenBg.css({"z-index":2147483647,display:"block"});$videoPlay.css({"z-index":2147483647})}else{$(document).off("mousemove",
userActivate);$videoControls.css({"z-index":"",position:"absolute"});$videoFullscreenBg.css({"z-index":"",display:"none"});$videoPlay.css({"z-index":""})}};document.addEventListener("MSFullscreenChange",function(){isFullscreen=document.msFullscreenElement!=null;switchScreenCSS(isFullscreen)},false);document.addEventListener("fullscreenchange",function(){isFullscreen=document.fullscreen;switchScreenCSS(isFullscreen)},false);document.addEventListener("mozfullscreenchange",function(){isFullscreen=document.mozFullScreen;
switchScreenCSS(isFullscreen)},false);document.addEventListener("webkitfullscreenchange",function(){isFullscreen=document.webkitIsFullScreen;switchScreenCSS(isFullscreen)},false);$videoFSObj.get(0).addEventListener("webkitbeginfullscreen",function(){isFullscreen=true;switchScreenCSS(isFullscreen)},false);$videoFSObj.get(0).addEventListener("webkitendfullscreen",function(){isFullscreen=false;switchScreenCSS(isFullscreen)},false);if(!fullscreenNativeControls)$("head").append("<style type='text/css'>video"+
videoElem+"::-webkit-media-controls { display:none !important; }</style>");var $videoFullscreen=$(".html5boxFullscreen",$videoControls);$videoFullscreen.css({display:"block",position:"relative","float":"right",width:BUTTON_SIZE+"px",height:BUTTON_SIZE+"px",margin:Math.floor((BAR_HEIGHT-BUTTON_SIZE)/2),cursor:"pointer","background-image":"url('"+skinFolder+"html5boxplayer_fullscreen.png"+"')","background-position":"left top"}).hover(function(){var backgroundPosX=$(this).css("background-position")?
$(this).css("background-position").split(" ")[0]:$(this).css("background-position-x");$(this).css({"background-position":backgroundPosX+" bottom"})},function(){var backgroundPosX=$(this).css("background-position")?$(this).css("background-position").split(" ")[0]:$(this).css("background-position-x");$(this).css({"background-position":backgroundPosX+" top"})}).on(eClick,function(){isFullscreen=!isFullscreen;switchScreen(isFullscreen)})}if(hd){var $videoHD=$(".html5boxHD",$videoControls);$videoHD.css({display:"block",
position:"relative","float":"right",width:BUTTON_SIZE+"px",height:BUTTON_SIZE+"px",margin:Math.floor((BAR_HEIGHT-BUTTON_SIZE)/2),cursor:"pointer","background-image":"url('"+skinFolder+"html5boxplayer_hd.png"+"')","background-position":(isHd?"right":"left")+" center"}).on(eClick,function(){isHd=!isHd;$(this).css({"background-position":(isHd?"right":"left")+" center"});parentInst.isHd=isHd;var isPaused=$videoObj.get(0).isPaused;$videoObj.get(0).setAttribute("src",(isHd?hd:src)+"#t="+$videoObj.get(0).currentTime);
if(!isPaused)$videoObj.get(0).play();else $videoObj.get(0).pause()})}$videoObj.get(0).volume=defaultVolume;var volumeSaved=defaultVolume==0?1:defaultVolume;var volume=$videoObj.get(0).volume;$videoObj.get(0).volume=volume/2+0.1;if($videoObj.get(0).volume===volume/2+0.1){$videoObj.get(0).volume=volume;var $videoVolume=$(".html5boxVolume",$videoControls);var $videoVolumeButton=$(".html5boxVolumeButton",$videoControls);var $videoVolumeBar=$(".html5boxVolumeBar",$videoControls);var $videoVolumeBarBg=
$(".html5boxVolumeBarBg",$videoControls);var $videoVolumeBarActive=$(".html5boxVolumeBarActive",$videoControls);$videoVolume.css({display:"block",position:"relative","float":"right",width:BUTTON_SIZE+"px",height:BUTTON_SIZE+"px",margin:Math.floor((BAR_HEIGHT-BUTTON_SIZE)/2)}).hover(function(){clearTimeout(hideVolumeBarTimeoutId);var volume=$videoObj.get(0).volume;$videoVolumeBarActive.css({height:Math.round(volume*100)+"%"});$videoVolumeBar.show()},function(){clearTimeout(hideVolumeBarTimeoutId);
hideVolumeBarTimeoutId=setTimeout(function(){$videoVolumeBar.hide()},1E3)});$videoVolumeButton.css({display:"block",position:"absolute",top:0,left:0,width:BUTTON_SIZE+"px",height:BUTTON_SIZE+"px",cursor:"pointer","background-image":"url('"+skinFolder+"html5boxplayer_volume.png"+"')","background-position":"top "+(volume>0?"left":"right")}).hover(function(){var backgroundPosX=$(this).css("background-position")?$(this).css("background-position").split(" ")[0]:$(this).css("background-position-x");$(this).css({"background-position":backgroundPosX+
" bottom"})},function(){var backgroundPosX=$(this).css("background-position")?$(this).css("background-position").split(" ")[0]:$(this).css("background-position-x");$(this).css({"background-position":backgroundPosX+" top"})}).on(eClick,function(){var volume=$videoObj.get(0).volume;if(volume>0){volumeSaved=volume;volume=0}else volume=volumeSaved;var backgroundPosY=$(this).css("background-position")?$(this).css("background-position").split(" ")[1]:$(this).css("background-position-y");$videoVolumeButton.css({"background-position":(volume>
0?"left":"right")+" "+backgroundPosY});$videoObj.get(0).volume=volume;$videoVolumeBarActive.css({height:Math.round(volume*100)+"%"})});$videoVolumeBar.css({display:"none",position:"absolute",left:4,bottom:"100%",width:24,height:80,"margin-bottom":Math.floor((BAR_HEIGHT-BUTTON_SIZE)/2),"background-color":"#000000",opacity:0.7,filter:"alpha(opacity=70)"});$videoVolumeBarBg.css({display:"block",position:"relative",width:10,height:68,margin:7,cursor:"pointer","background-color":"#222"});$videoVolumeBarActive.css({display:"block",
position:"absolute",bottom:0,left:0,width:"100%",height:"100%","background-color":"#fcc500"});$videoVolumeBarBg.on(eStart,function(e){var e0=isTouch?e.originalEvent.touches[0]:e;var vol=1-(e0.pageY-$videoVolumeBarBg.offset().top)/$videoVolumeBarBg.height();vol=vol>1?1:vol<0?0:vol;$videoVolumeBarActive.css({height:Math.round(vol*100)+"%"});$videoVolumeButton.css({"background-position":"left "+(vol>0?"top":"bottom")});$videoObj.get(0).volume=vol;$videoVolumeBarBg.on(eMove,function(e){var e0=isTouch?
e.originalEvent.touches[0]:e;var vol=1-(e0.pageY-$videoVolumeBarBg.offset().top)/$videoVolumeBarBg.height();vol=vol>1?1:vol<0?0:vol;$videoVolumeBarActive.css({height:Math.round(vol*100)+"%"});$videoVolumeButton.css({"background-position":"left "+(vol>0?"top":"bottom")});$videoObj.get(0).volume=vol})}).on(eCancel,function(){$videoVolumeBarBg.off(eMove)})}var calcTimeFormat=function(seconds){var h0=Math.floor(seconds/3600);var h=h0<10?"0"+h0:h0;var m0=Math.floor((seconds-h0*3600)/60);var m=m0<10?"0"+
m0:m0;var s0=Math.floor(seconds-(h0*3600+m0*60));var s=s0<10?"0"+s0:s0;var r=m+":"+s;if(h0>0)r=h+":"+r;return r};if(hidePlayButton)$videoPlay.hide();if(hideControls)$videoControls.hide();var onVideoPlay=function(){if(!hidePlayButton)$videoPlay.hide();if(!hideControls){$videoBtnPlay.hide();$videoBtnPause.show()}};var onVideoPause=function(){if(!hidePlayButton)$videoPlay.show();if(!hideControls){$videoControls.show();clearTimeout(hideControlsTimerId);$videoBtnPlay.show();$videoBtnPause.hide()}};var onVideoEnded=
function(){$(window).trigger("html5lightbox.videoended");if(!hidePlayButton)$videoPlay.show();if(!hideControls){$videoControls.show();clearTimeout(hideControlsTimerId);$videoBtnPlay.show();$videoBtnPause.hide()}};var onVideoUpdate=function(){var curTime=$videoObj.get(0).currentTime;if(curTime){$videoTimeCurrent.text(calcTimeFormat(curTime));var duration=$videoObj.get(0).duration;if(duration){$videoTimeTotal.text(calcTimeFormat(duration));if(!sliderDragging){var sliderW=$videoSeeker.width();var pos=
Math.round(sliderW*curTime/duration);$videoSeekerPlay.css({width:pos});$videoSeekerHandler.css({left:pos})}}}};var onVideoProgress=function(){if($videoObj.get(0).buffered&&$videoObj.get(0).buffered.length>0&&!isNaN($videoObj.get(0).buffered.end(0))&&!isNaN($videoObj.get(0).duration)){var sliderW=$videoSeeker.width();$videoSeekerBuffer.css({width:Math.round(sliderW*$videoObj.get(0).buffered.end(0)/$videoObj.get(0).duration)})}};try{$videoObj.on("play",onVideoPlay);$videoObj.on("pause",onVideoPause);
$videoObj.on("ended",onVideoEnded);$videoObj.on("timeupdate",onVideoUpdate);$videoObj.on("progress",onVideoProgress)}catch(e){}}})(jQuery)};
;
/** Wonderplugin 3D Carousel Plugin Commercial Version
 * Copyright 2017 Magic Hills Pty Ltd All Rights Reserved
 * Website: http://www.wonderplugin.com
 * Version 2.2 
 */
(function($){function WP3DCarouselTimer(interval,callback,updatecallback){var timerInstance=this;timerInstance.timeout=interval;var updateinterval=50;var updateTimerId=null;var runningTime=0;var paused=false;var started=false;var startedandpaused=false;this.pause=function(){if(started){paused=true;clearInterval(updateTimerId)}};this.resume=function(forceresume){if(startedandpaused&&!forceresume)return;startedandpaused=false;if(started&&paused){paused=false;clearInterval(updateTimerId);updateTimerId=
setInterval(function(){runningTime+=updateinterval;if(runningTime>timerInstance.timeout){clearInterval(updateTimerId);if(callback)callback()}if(updatecallback)updatecallback(runningTime/timerInstance.timeout)},updateinterval)}};this.stop=function(){clearInterval(updateTimerId);if(updatecallback)updatecallback(-1);runningTime=0;paused=false;started=false};this.start=function(){runningTime=0;paused=false;started=true;clearInterval(updateTimerId);updateTimerId=setInterval(function(){runningTime+=updateinterval;
if(runningTime>timerInstance.timeout){clearInterval(updateTimerId);if(callback)callback()}if(updatecallback)updatecallback(runningTime/timerInstance.timeout)},updateinterval)};this.startandpause=function(){runningTime=0;paused=true;started=true;startedandpaused=true}}$.fn.wonderplugin3dcarousel=function(options){var TYPE_VIDEO_FLASH=5,TYPE_VIDEO_MP4=6,TYPE_VIDEO_OGG=7,TYPE_VIDEO_WEBM=8,TYPE_VIDEO_YOUTUBE=9,TYPE_VIDEO_VIMEO=10;var LegacyCarousel=function(parentObject,container,options,id){this.parentObject=
parentObject;this.container=container;this.options=options;this.id=id;this.curItem=-1};LegacyCarousel.prototype={init:function(){var items=$(".wonderplugin3dcarousel-item",this.container);items.css({"display":"block","opacity":0,"visibility":"hidden"});if(this.curItem<0)this.curItem=this.options.firstitem;this.direction=0;this.totalItems=items.length;this.initClickHandler();this.initSize();this.gotoItem(this.curItem)},resizeObject:function(){var screensize=this.parentObject.checkScreen();if(this.screenstatus!=
screensize){this.screenstatus=screensize;this.initSize();this.parentObject.resizeImages();this.gotoItem(this.curItem)}},initSize:function(){this.screenstatus=this.parentObject.checkScreen();if(this.screenstatus=="small"){this.visibleitems=this.options.small_visibleitems;this.itemW=this.options.small_width;this.itemH=this.options.small_height;this.transition=this.options.small_transition;this.carouselmargin=this.options.small_carouselmargin}else if(this.screenstatus=="medium"){this.visibleitems=this.options.medium_visibleitems;
this.itemW=this.options.medium_width;this.itemH=this.options.medium_height;this.transition=this.options.medium_transition;this.carouselmargin=this.options.medium_carouselmargin}else{this.visibleitems=this.options.visibleitems;this.itemW=this.options.width;this.itemH=this.options.height;this.transition=this.options.transition;this.carouselmargin=this.options.carouselmargin}this.itemImgW=this.itemW-2*this.options.itemborder;this.itemImgH=this.itemH-2*this.options.itemborder;this.sideCount=Math.floor((this.visibleitems-
1)/2);this.container.css({"box-sizing":"border-box"});this.container.find("*").css({"box-sizing":"border-box"});$(".wonderplugin3dcarousel-list-container",this.container).css({display:"block",position:"relative",width:"100%",height:"auto",overflow:"hidden",margin:0,padding:this.carouselmargin});$(".wonderplugin3dcarousel-list",this.container).css({width:"100%",height:this.itemH+"px",margin:0,padding:0});$(".wonderplugin3dcarousel-item",this.container).css({width:this.itemW+"px",position:"absolute",
top:0,left:"50%","margin-left":"-"+this.itemW/2+"px"});$(".wonderplugin3dcarousel-content",this.container).css({"background-color":this.options.itembgcolor,padding:this.options.itemborder+"px"});$(".wonderplugin3dcarousel-image",this.container).css({display:"block",position:"relative",width:this.itemImgW+"px",height:this.itemImgH+"px",overflow:"hidden"});$(".wonderplugin3dcarousel-img-container",this.container).css({display:"block",position:"relative",width:"100%","max-width":"100%",height:"100%",
"max-height":"100%"});$(".wonderplugin3dcarousel-img",this.container).css({display:"inline","max-width":"100%",outline:"1px solid transparent"})},initClickHandler:function(){var instance=this;$(".wonderplugin3dcarousel-item",this.container).click(function(){instance.gotoItem($(this).index())})},gotoPrev:function(){var itemId=this.curItem-1;if(itemId<0)itemId=this.options.loop||this.options.loopslide?this.totalItems-1:0;this.direction=-1;this.gotoItem(itemId)},gotoNext:function(){var itemId=this.curItem+
1;if(itemId>=this.totalItems)itemId=this.options.loop||this.options.loopslide?0:this.totalItems-1;this.direction=1;this.gotoItem(itemId)},gotoItem:function(itemId){this.parentObject.slideTimeout.stop();this.prevItem=this.curItem;this.curItem=itemId;this.container.trigger("wonderplugin3dcarousel.beforeswitch",[this.prevItem,this.curItem]);var items=$(".wonderplugin3dcarousel-item",this.container);items.css({"-webkit-transition":"none","-moz-transition":"none","-o-transition":"none","-ms-transition":"none",
"transition":"none"});items.removeClass("wonderplugin3dcarousel-item-current");items.eq(this.curItem).addClass("wonderplugin3dcarousel-item-current");if(this.options.pausevideonotinmiddle)items.not(".wonderplugin3dcarousel-item-current").each(function(){if($(this).find("video").length>0)$(this).find("video").get(0).pause()});var pos=items.eq(this.curItem).data("pos");var dir=this.visibleitems==1?this.direction:pos;if(this.totalItems>this.visibleitems)if(dir<0)for(var i=0;i<=this.sideCount;i++){var sideItem=
this.curItem-i;if(sideItem<0){if(!this.options.loop)break;sideItem=this.totalItems+sideItem}items.eq(sideItem).css({"margin-left":"-"+(this.itemW/2+this.itemW*(i-dir))+"px"})}else if(dir>0)for(var i=0;i<=this.sideCount;i++){var sideItem=this.curItem+i;if(sideItem>=this.totalItems){if(!this.options.loop)break;sideItem=sideItem-this.totalItems}items.eq(sideItem).css({"margin-left":this.itemW/2+this.itemW*(i+dir-1)+"px"})}var instance=this;setTimeout(function(){instance.goAnimation(dir);instance.container.trigger("wonderplugin3dcarousel.switch",
[instance.prevItem,instance.curItem]);if(instance.parentObject.autosliding&&!instance.parentObject.isMousePaused&&!instance.parentObject.isVideoPaused){var islast=instance.options.autoslidedir=="right"&&instance.curItem>=instance.totalItems-1||instance.options.autoslidedir=="left"&&instance.curItem<=0;if(!islast||instance.options.loop||instance.options.loopslide)instance.parentObject.slideTimeout.start()}},10)},goAnimation:function(pos){var items=$(".wonderplugin3dcarousel-item",this.container);items.css({"-webkit-transition":this.transition,
"-moz-transition":this.transition,"-o-transition":this.transition,"-ms-transition":this.transition,"transition":this.transition});items.removeClass("wonderplugin3dcarousel-item-visible");items.eq(this.curItem).css({"opacity":1,"visibility":"visible","margin-left":"-"+this.itemW/2+"px"});items.eq(this.curItem).data("pos",0).addClass("wonderplugin3dcarousel-item-visible");if(this.totalItems<=1)return;for(var i=1;i<=this.sideCount;i++){var sideItem=this.curItem-i;if(sideItem<0){if(!this.options.loop)break;
sideItem=this.totalItems+sideItem}items.eq(sideItem).css({"opacity":1,"visibility":"visible","margin-left":"-"+(this.itemW/2+this.itemW*i)+"px"});items.eq(sideItem).data("pos",-i).addClass("wonderplugin3dcarousel-item-visible")}for(var i=1;i<=this.sideCount;i++){var sideItem=this.curItem+i;if(sideItem>=this.totalItems){if(!this.options.loop)break;sideItem=sideItem-this.totalItems}items.eq(sideItem).css({"opacity":1,"visibility":"visible","margin-left":this.itemW/2+this.itemW*(i-1)+"px"});items.eq(sideItem).data("pos",
i).addClass("wonderplugin3dcarousel-item-visible")}items.not(".wonderplugin3dcarousel-item-visible").css({"opacity":0,"visibility":"hidden"});if(this.totalItems<=this.visibleitems)return;if(pos<0)for(var i=1;i<=Math.abs(pos);i++){var sideItem=this.curItem+this.sideCount+i;if(sideItem>=this.totalItems){if(!this.options.loop)break;sideItem=sideItem-this.totalItems}if(!items.eq(sideItem).hasClass(".wonderplugin3dcarousel-item-visible"))items.eq(sideItem).css({"margin-left":this.itemW/2+this.itemW*(i+
this.sideCount-1)+"px"})}else for(var i=1;i<=Math.abs(pos);i++){var sideItem=this.curItem-this.sideCount-i;if(sideItem<0){if(!this.options.loop)break;sideItem=this.totalItems+sideItem}if(!items.eq(sideItem).hasClass(".wonderplugin3dcarousel-item-visible"))items.eq(sideItem).css({"margin-left":"-"+(this.itemW/2+this.itemW*(i+this.sideCount))+"px"})}}};var ThreeDSlider=function(parentObject,container,options,id){this.parentObject=parentObject;this.container=container;this.options=options;this.id=id;
this.curItem=-1};ThreeDSlider.prototype={init:function(){var items=$(".wonderplugin3dcarousel-item",this.container);items.css({"display":"block","opacity":0,"visibility":"hidden"});if(this.curItem<0)this.curItem=this.options.firstitem;this.direction=0;this.totalItems=items.length;this.initClickHandler();this.initSize();this.gotoItem(this.curItem)},resizeObject:function(){var screensize=this.parentObject.checkScreen();if(this.screenstatus!=screensize){this.screenstatus=screensize;this.initSize();this.parentObject.resizeImages();
this.gotoItem(this.curItem)}},initSize:function(){this.screenstatus=this.parentObject.checkScreen();if(this.screenstatus=="small"){this.visibleitems=this.options.small_visibleitems;this.perspective=this.options.small_perspective;this.xdis=this.options.small_xdis;this.zdis=this.options.small_zdis;this.yrotate=this.options.small_yrotate;this.transition=this.options.small_transition;this.itemW=this.options.small_width;this.itemH=this.options.small_height;this.carouselmargin=this.options.small_carouselmargin}else if(this.screenstatus==
"medium"){this.visibleitems=this.options.medium_visibleitems;this.perspective=this.options.medium_perspective;this.xdis=this.options.medium_xdis;this.zdis=this.options.medium_zdis;this.yrotate=this.options.medium_yrotate;this.transition=this.options.medium_transition;this.itemW=this.options.medium_width;this.itemH=this.options.medium_height;this.carouselmargin=this.options.medium_carouselmargin}else{this.visibleitems=this.options.visibleitems;this.perspective=this.options.perspective;this.xdis=this.options.xdis;
this.zdis=this.options.zdis;this.yrotate=this.options.yrotate;this.transition=this.options.transition;this.itemW=this.options.width;this.itemH=this.options.height;this.carouselmargin=this.options.carouselmargin}this.itemImgW=this.itemW-2*this.options.itemborder;this.itemImgH=this.itemH-2*this.options.itemborder;this.sideCount=Math.floor((this.visibleitems-1)/2);this.container.css({"box-sizing":"border-box"});this.container.find("*").css({"box-sizing":"border-box"});$(".wonderplugin3dcarousel-list-container",
this.container).css({display:"block",position:"relative",width:"100%",height:"auto",overflow:"hidden",margin:0,padding:this.carouselmargin});$(".wonderplugin3dcarousel-list",this.container).css({width:"100%",height:this.itemH+"px",margin:0,padding:0});$(".wonderplugin3dcarousel-list",this.container).css({"transform-style":this.parentObject.isFirefox?"flat":"preserve-3d",perspective:this.perspective+"px"});$(".wonderplugin3dcarousel-item",this.container).css({width:this.itemW+"px",position:"absolute",
top:0,left:"50%","margin-left":"-"+this.itemW/2+"px"});$(".wonderplugin3dcarousel-content",this.container).css({"background-color":this.options.itembgcolor,padding:this.options.itemborder+"px"});$(".wonderplugin3dcarousel-image",this.container).css({display:"block",position:"relative",width:this.itemImgW+"px",height:this.itemImgH+"px",overflow:"hidden"});$(".wonderplugin3dcarousel-img-container",this.container).css({display:"block",position:"relative",width:"100%","max-width":"100%",height:"100%",
"max-height":"100%"});$(".wonderplugin3dcarousel-img",this.container).css({display:"inline","max-width":"100%",outline:"1px solid transparent"})},initClickHandler:function(){var instance=this;$(".wonderplugin3dcarousel-item",this.container).click(function(){if($(this).index()==instance.curItem)return;instance.gotoItem($(this).index())})},gotoPrev:function(){var itemId=this.curItem-1;if(itemId<0)itemId=this.options.loop||this.options.loopslide?this.totalItems-1:0;this.direction=-1;this.gotoItem(itemId)},
gotoNext:function(){var itemId=this.curItem+1;if(itemId>=this.totalItems)itemId=this.options.loop||this.options.loopslide?0:this.totalItems-1;this.direction=1;this.gotoItem(itemId)},gotoItem:function(itemId){this.parentObject.slideTimeout.stop();this.prevItem=this.curItem;this.curItem=itemId;this.container.trigger("wonderplugin3dcarousel.beforeswitch",[this.prevItem,this.curItem]);var items=$(".wonderplugin3dcarousel-item",this.container);items.css({"-webkit-transition":"none","-moz-transition":"none",
"-o-transition":"none","-ms-transition":"none","transition":"none"});items.removeClass("wonderplugin3dcarousel-item-current");items.eq(this.curItem).addClass("wonderplugin3dcarousel-item-current");if(this.options.pausevideonotinmiddle)items.not(".wonderplugin3dcarousel-item-current").each(function(){if($(this).find("video").length>0)$(this).find("video").get(0).pause()});var pos=items.eq(this.curItem).data("pos");var dir=this.visibleitems==1?this.direction:pos;if(this.totalItems>this.visibleitems)if(dir<
0)for(var i=0;i<=this.sideCount;i++){var sideItem=this.curItem-i;if(sideItem<0){if(!this.options.loop)break;sideItem=this.totalItems+sideItem}items.eq(sideItem).css({transform:"translateX(-"+this.xdis*(i-dir)+"px) translateZ(-"+this.zdis*(i-dir)+"px) rotateY("+this.yrotate+"deg)"})}else if(dir>0)for(var i=0;i<=this.sideCount;i++){var sideItem=this.curItem+i;if(sideItem>=this.totalItems){if(!this.options.loop)break;sideItem=sideItem-this.totalItems}items.eq(sideItem).css({transform:"translateX("+this.xdis*
(i+dir)+"px) translateZ(-"+this.zdis*(i+dir)+"px) rotateY(-"+this.yrotate+"deg)"})}var instance=this;setTimeout(function(){instance.goAnimation(dir);instance.container.trigger("wonderplugin3dcarousel.switch",[instance.prevItem,instance.curItem]);if(instance.parentObject.autosliding&&!instance.parentObject.isMousePaused&&!instance.parentObject.isVideoPaused){var islast=instance.options.autoslidedir=="right"&&instance.curItem>=instance.totalItems-1||instance.options.autoslidedir=="left"&&instance.curItem<=
0;if(!islast||instance.options.loop||instance.options.loopslide)instance.parentObject.slideTimeout.start()}},10)},goAnimation:function(pos){var items=$(".wonderplugin3dcarousel-item",this.container);items.css({"-webkit-transition":this.transition,"-moz-transition":this.transition,"-o-transition":this.transition,"-ms-transition":this.transition,"transition":this.transition});items.removeClass("wonderplugin3dcarousel-item-visible");items.eq(this.curItem).css({"opacity":1,"visibility":"visible","z-index":(this.sideCount+
1)*2,transform:"translateX(0px) translateZ(0px) rotateY(0deg)"});items.eq(this.curItem).data("pos",0).addClass("wonderplugin3dcarousel-item-visible");if(this.totalItems<=1)return;for(var i=1;i<=this.sideCount;i++){var sideItem=this.curItem-i;if(sideItem<0){if(!this.options.loop)break;sideItem=this.totalItems+sideItem}items.eq(sideItem).css({"opacity":1,"visibility":"visible","z-index":(this.sideCount-i+1)*2-1,transform:"translateX(-"+this.xdis*i+"px) translateZ(-"+this.zdis*i+"px) rotateY("+this.yrotate+
"deg)"});items.eq(sideItem).data("pos",-i).addClass("wonderplugin3dcarousel-item-visible")}for(var i=1;i<=this.sideCount;i++){var sideItem=this.curItem+i;if(sideItem>=this.totalItems){if(!this.options.loop)break;sideItem=sideItem-this.totalItems}items.eq(sideItem).css({"opacity":1,"visibility":"visible","z-index":(this.sideCount-i+1)*2,transform:"translateX("+this.xdis*i+"px) translateZ(-"+this.zdis*i+"px) rotateY(-"+this.yrotate+"deg)"});items.eq(sideItem).data("pos",i).addClass("wonderplugin3dcarousel-item-visible")}items.not(".wonderplugin3dcarousel-item-visible").css({"opacity":0,
"visibility":"hidden","z-index":0});if(this.totalItems<=this.visibleitems)return;if(pos<0)for(var i=1;i<=Math.abs(pos);i++){var sideItem=this.curItem+this.sideCount+i;if(sideItem>=this.totalItems){if(!this.options.loop)break;sideItem=sideItem-this.totalItems}if(!items.eq(sideItem).hasClass(".wonderplugin3dcarousel-item-visible"))items.eq(sideItem).css({"z-index":-i,transform:"translateX("+this.xdis*(i+this.sideCount)+"px) translateZ(-"+this.zdis*(i+this.sideCount)+"px) rotateY(-"+this.yrotate+"deg)"})}else for(var i=
1;i<=Math.abs(pos);i++){var sideItem=this.curItem-this.sideCount-i;if(sideItem<0){if(!this.options.loop)break;sideItem=this.totalItems+sideItem}if(!items.eq(sideItem).hasClass(".wonderplugin3dcarousel-item-visible"))items.eq(sideItem).css({"z-index":-i,transform:"translateX(-"+this.xdis*(i+this.sideCount)+"px) translateZ(-"+this.zdis*(i+this.sideCount)+"px) rotateY("+this.yrotate+"deg)"})}}};var ThreeDCarousel=function(parentObject,container,options,id){this.parentObject=parentObject;this.container=
container;this.options=options;this.id=id;this.curItem=-1};ThreeDCarousel.prototype={init:function(){var items=$(".wonderplugin3dcarousel-item",this.container);items.css({"display":"block","backface-visibility":"visible"});this.totalItems=items.length;this.initClickHandler();if(this.curItem<0)this.curItem=this.options.firstitem;this.initSize();this.gotoItem(this.curItem)},initSize:function(){this.screenstatus=this.parentObject.checkScreen();if(this.screenstatus=="small"){this.carouselmargin=this.options.small_carouselmargin;
this.transition=this.options.small_transition;this.itemW=this.options.small_width;this.itemH=this.options.small_height}else if(this.screenstatus=="medium"){this.carouselmargin=this.options.medium_carouselmargin;this.transition=this.options.medium_transition;this.itemW=this.options.medium_width;this.itemH=this.options.medium_height}else{this.carouselmargin=this.options.carouselmargin;this.transition=this.options.transition;this.itemW=this.options.width;this.itemH=this.options.height}this.perspective=
this.options.perspective;this.itemImgW=this.itemW-2*this.options.itemborder;this.itemImgH=this.itemH-2*this.options.itemborder;this.container.css({"box-sizing":"border-box"});this.container.find("*").css({"box-sizing":"border-box"});$(".wonderplugin3dcarousel-list",this.container).css({"transform-style":this.parentObject.isFirefox?"flat":"preserve-3d",perspective:this.perspective+"px"});$(".wonderplugin3dcarousel-list-container",this.container).css({display:"block",position:"relative",width:"100%",
height:"auto",overflow:"hidden",margin:0,padding:this.carouselmargin});$(".wonderplugin3dcarousel-list",this.container).css({width:"100%",height:this.itemH+"px",margin:0,padding:0});$(".wonderplugin3dcarousel-item",this.container).css({width:this.itemW+"px",position:"absolute",top:0,left:"50%","margin-left":"-"+this.itemW/2+"px",transition:this.transition});$(".wonderplugin3dcarousel-content",this.container).css({"background-color":this.options.itembgcolor,padding:this.options.itemborder+"px"});$(".wonderplugin3dcarousel-image",
this.container).css({display:"block",position:"relative",width:this.itemImgW+"px",height:this.itemImgH+"px",overflow:"hidden"});$(".wonderplugin3dcarousel-img-container",this.container).css({display:"block",position:"relative",width:"100%","max-width":"100%",height:"100%","max-height":"100%"});$(".wonderplugin3dcarousel-img",this.container).css({display:"inline","max-width":"100%",outline:"1px solid transparent"})},resizeObject:function(){var screensize=this.parentObject.checkScreen();if(this.screenstatus!=
screensize){this.screenstatus=screensize;this.initSize()}this.gotoItem(this.curItem)},initClickHandler:function(){var instance=this;$(".wonderplugin3dcarousel-item",this.container).click(function(){instance.gotoItem($(this).index())})},gotoPrev:function(){var itemId=this.curItem-1;if(itemId<0)itemId=this.options.loop||this.options.loopslide?this.totalItems-1:0;this.gotoItem(itemId)},gotoNext:function(){var itemId=this.curItem+1;if(itemId>=this.totalItems)itemId=this.options.loop||this.options.loopslide?
0:this.totalItems-1;this.gotoItem(itemId)},gotoItem:function(itemId){var instance=this;this.parentObject.slideTimeout.stop();this.prevItem=this.curItem;this.curItem=itemId;this.container.trigger("wonderplugin3dcarousel.beforeswitch",[this.prevItem,this.curItem]);var items=$(".wonderplugin3dcarousel-item",this.container);items.removeClass("wonderplugin3dcarousel-item-current");items.eq(this.curItem).addClass("wonderplugin3dcarousel-item-current");if(this.options.pausevideonotinmiddle)items.not(".wonderplugin3dcarousel-item-current").each(function(){if($(this).find("video").length>
0)$(this).find("video").get(0).pause()});items.css({transition:this.transition});this.angle=180/this.totalItems;this.axisd=(this.itemW+this.options.itemspace)/2/Math.tan(this.angle*Math.PI/180);this.carouselWidth=0;items.each(function(index){var pos=index-instance.curItem;if(pos<0)pos+=instance.parentObject.elemLength;var xdis=2*instance.axisd*Math.sin(pos*instance.angle*Math.PI/180)*Math.cos(pos*instance.angle*Math.PI/180);var zdis=-2*instance.axisd*Math.sin(pos*instance.angle*Math.PI/180)*Math.sin(pos*
instance.angle*Math.PI/180);var ydis=zdis*Math.sin(-instance.options.rotatex*Math.PI/180);if(instance.carouselWidth<Math.abs(xdis))instance.carouselWidth=Math.abs(xdis);var rotatey=0;if(instance.options.facemode=="circle"){rotatey=instance.angle*2*pos;if(typeof $(this).data("rotatey")!=="undefined"){var r0=$(this).data("rotatey");while(Math.abs(rotatey-r0)>180)if(rotatey>r0)rotatey-=360;else rotatey+=360}}$(this).css({transform:"translateX("+xdis+"px) translateY("+ydis+"px) translateZ("+zdis+"px) rotateX("+
instance.options.rotatex+"deg) rotateY("+rotatey+"deg)","z-index":pos<=instance.parentObject.elemLength/2?Math.ceil(instance.parentObject.elemLength/2-pos)*2:Math.ceil(pos-instance.parentObject.elemLength/2)*2-1});$(this).data("translatex",xdis).data("translatey",ydis).data("translatez",zdis).data("rotatex",instance.options.rotatex).data("rotatey",rotatey)});this.carouselWidth*=2;if(this.options.facemode=="front")this.carouselWidth+=this.itemW;this.containerWidth=this.container.width();var scale=
this.containerWidth>0&&this.carouselWidth>this.containerWidth?this.containerWidth/this.carouselWidth:1;scale*=this.options.scaleratio;if(scale>1)scale=1;$(".wonderplugin3dcarousel-list",this.container).css({transform:"scale("+scale+")"});this.container.trigger("wonderplugin3dcarousel.switch",[this.prevItem,this.curItem]);if(instance.parentObject.autosliding&&!instance.parentObject.isMousePaused&&!instance.parentObject.isVideoPaused)instance.parentObject.slideTimeout.start()}};var WP3DCarousel=function(container,
options,id,lightboxObject){this.container=container;this.options=options;this.id=id;this.lightboxObject=lightboxObject;this.resizeTimeout=null;this.screenstatus="normal";this.preProcess(this)};WP3DCarousel.prototype={initData:function(){this.init()},init:function(){var instance=this;this.initEnv();this.container.css({width:"100%"});this.elemArray=$(".wonderplugin3dcarousel-item",this.container);this.elemLength=this.elemArray.length;if(this.options.random){for(var i=this.elemLength-1;i>0;i--){var index=
Math.floor(Math.random()*i);this.elemArray.eq(index).insertBefore(this.elemArray.eq(i));this.elemArray.eq(i).insertBefore(this.elemArray.eq(index))}this.elemArray=$(".wonderplugin3dcarousel-item",this.container)}this.initWM();this.initAutoSlide();this.legacyMode=false;if(this.isIE6||this.isIE7||this.isIE8||this.isIE9||this.options.legacymode)this.legacyMode=true;if(this.legacyMode)this.carouselObject=new LegacyCarousel(this,this.container,this.options,this.id);else switch(this.options.template){case "3dcarousel":this.carouselObject=
new ThreeDCarousel(this,this.container,this.options,this.id);break;default:this.carouselObject=new ThreeDSlider(this,this.container,this.options,this.id);break}this.initPlayVideo();this.initOverlay();this.createText();this.createArrows();this.enableSwipe();this.createBullets();this.initLightbox();this.carouselObject.init();this.resizeImages();$(window).resize(function(){clearTimeout(instance.resizeTimeout);instance.resizeTimeout=setTimeout(function(){instance.resizeCarousel()},50)});this.container.css({display:"block"})},
preProcess:function(instance){var found=false;var item_index=0;var youtubeapikey="";var youtubeplaylistid="";var youtubeplaylistmaxresults="";$(".wonderplugin3dcarousel-item",this.container).each(function(index){if($(this).data("youtubeapikey")){youtubeapikey=$(this).data("youtubeapikey");youtubeplaylistid=$(this).data("youtubeplaylistid");youtubeplaylistmaxresults=$(this).data("youtubeplaylistmaxresults");item_index=index;found=true;return false}});if(found)instance.getYouTubePlaylist(youtubeapikey,
youtubeplaylistid,youtubeplaylistmaxresults,item_index,item_index,instance.preProcess,instance,null);else instance.initData()},getYouTubePlaylist:function(youtubeapikey,youtubeplaylistid,youtubeplaylistmaxresults,index,insert_index,onsuccess,instance,pagetoken){var youtube_url="https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId="+youtubeplaylistid+"&key="+youtubeapikey;if(youtubeplaylistmaxresults)if(youtubeplaylistmaxresults>50)youtube_url+="&maxResults=50";else youtube_url+=
"&maxResults="+youtubeplaylistmaxresults;if(pagetoken)youtube_url+="&pageToken="+pagetoken;var all_done=true;$.getJSON(youtube_url,function(data){if(data&&data.items){var original_item=$(".wonderplugin3dcarousel-item",instance.container).eq(index);for(var i=0;i<data.items.length;i++){var video_id=data.items[i]["snippet"]["resourceId"]["videoId"];var thumbnail="https://img.youtube.com/vi/"+video_id+"/0.jpg";var image="https://img.youtube.com/vi/"+video_id+"/0.jpg";if(data.items[i]["snippet"]["thumbnails"]&&
data.items[i]["snippet"]["thumbnails"]["maxres"])image=data.items[i]["snippet"]["thumbnails"]["maxres"]["url"];var video="https://www.youtube.com/embed/"+video_id;var title=data.items[i]["snippet"]["title"];var description=data.items[i]["snippet"]["description"];var new_item=original_item.clone();new_item.removeAttr("data-youtubeapikey").removeAttr("data-youtubeplaylistid").removeAttr("data-youtubeplaylistmaxresults");var item_html=new_item.html().replace(/data-srcyt=/g,"src=").replace(/__IMAGE__/g,
image).replace(/__THUMBNAIL__/g,thumbnail).replace(/__VIDEO__/g,video).replace(/__TITLE__/g,title).replace(/__DESCRIPTION__/g,description);new_item.html(item_html);if(instance.options.lightboxobject&&new_item.find("a").hasClass("html5lightbox"))new_item.find("a").each(function(){instance.options.lightboxobject.push(this);$(this).off("click").click(instance.options.lightboxobject.clickHandler)});$(".wonderplugin3dcarousel-item",instance.container).eq(insert_index).after(new_item);insert_index++}}if(data&&
data.nextPageToken&&youtubeplaylistmaxresults&&youtubeplaylistmaxresults>50){all_done=false;instance.getYouTubePlaylist(youtubeapikey,youtubeplaylistid,youtubeplaylistmaxresults-50,index,insert_index,onsuccess,instance,data.nextPageToken)}}).always(function(){if(all_done){$(".wonderplugin3dcarousel-item",instance.container).eq(index).remove();onsuccess(instance)}})},initWM:function(){if(!this.options.mtext)return;var instance=this;$(".wonderplugin3dcarousel-item",this.container).each(function(index){if(index%
3==1)$(this).append('<a href="'+instance.options.marklink+'" target="_blank"><div style="display:block;visibility:visible;position:absolute;top:2px;left:2px;padding:2px 4px;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;background-color:#eee;color:#333;font:12px Arial,sans-serif;">'+instance.options.mark+"</div></a>")})},checkScreen:function(){var screenstatus="normal";var screenWidth=$(window).width();if(screenWidth<=this.options.small_screenwidth)screenstatus="small";else if(screenWidth<=
this.options.medium_screenwidth)screenstatus="medium";return screenstatus},initEnv:function(){this.isOpera=navigator.userAgent.match(/Opera/i)!=null||navigator.userAgent.match(/OPR\//i)!=null;this.isFirefox=navigator.userAgent.match(/Firefox/i)!=null;this.isIE11=navigator.userAgent.match(/Trident\/7/)!=null&&navigator.userAgent.match(/rv:11/)!=null;this.isIE=navigator.userAgent.match(/MSIE/i)!=null&&!this.isOpera;this.isIE10=navigator.userAgent.match(/MSIE 10/i)!=null&&!this.isOpera;this.isIE9=navigator.userAgent.match(/MSIE 9/i)!=
null&&!this.isOpera;this.isIE8=navigator.userAgent.match(/MSIE 8/i)!=null&&!this.isOpera;this.isIE7=navigator.userAgent.match(/MSIE 7/i)!=null&&!this.isOpera;this.isIE6=navigator.userAgent.match(/MSIE 6/i)!=null&&!this.isOpera;this.isAndroid=navigator.userAgent.match(/Android/i)!=null;if(this.isAndroid){var match=navigator.userAgent.match(/Android\s([0-9\.]*)/i);this.androidVersion=match&&match.length>=2?parseInt(match[1],10):-1}var v=document.createElement("video");this.canplaymp4=v&&v.canPlayType&&
v.canPlayType("video/mp4").replace(/no/,"");this.flashInstalled=false;try{if(new ActiveXObject("ShockwaveFlash.ShockwaveFlash"))this.flashInstalled=true}catch(e){if(navigator.mimeTypes["application/x-shockwave-flash"])this.flashInstalled=true}},initAutoSlide:function(){var instance=this;this.autosliding=false;this.isVideoPaused=false;this.isMousePaused=false;this.slideTimeout=new WP3DCarouselTimer(instance.options.slideinterval,function(){if(instance.options.autoslidedir=="right")instance.gotoNext();
else instance.gotoPrev()},null);if(this.options.autoslide)this.autosliding=true;if(this.options.pauseonmouseover)this.container.hover(function(){instance.isMousePaused=true;instance.slideTimeout.stop()},function(){instance.isMousePaused=false;if(instance.autosliding&&!instance.isVideoPaused)instance.slideTimeout.start()})},initPlayVideo:function(){this.initVideoButton()},initVideoButton:function(){if(!this.options.showplayvideo)return;var instance=this;$(".wonderplugin3dcarousel-img",this.container).each(function(index){var isVideo=
false;var isLightboxVideo=false;var videoUrl="";var videoWebmUrl="";if($(this).data("video")){videoUrl=$(this).data("video");if($(this).data("videowebm"))videoWebmUrl=$(this).data("videowebm");isVideo=true}var lightboxLink=$(this).closest("a.wp3dcarousellightbox");if(lightboxLink.length>0){var type=instance.checkVideoType(lightboxLink.attr("href"));if(type==TYPE_VIDEO_YOUTUBE||type==TYPE_VIDEO_VIMEO||type==TYPE_VIDEO_MP4||type==TYPE_VIDEO_WEBM)isLightboxVideo=true}if(instance.options.showplayvideo&&
(isVideo||isLightboxVideo))$(this).closest(".wonderplugin3dcarousel-img-container").append('<div class="wonderplugin3dcarousel-playvideo" style="position:absolute;top:0;left:0;width:100%;height:100%;cursor:pointer;'+"background-image:url('"+instance.options.playvideoimage+"');"+"background-repeat:no-repeat;background-position:"+instance.options.playvideoposition+";"+'"></div>');if(isVideo)$(this).closest(".wonderplugin3dcarousel-img-container").css({cursor:"pointer"}).click(function(){if(!$(this).data("isplayvideo")){$(this).data("isplayvideo",
true);instance.playVideo($(this),videoUrl,videoWebmUrl)}})})},playVideo:function($videoDiv,videoUrl,videoWebmUrl){if(videoUrl.length<=0)return;this.slideTimeout.stop();this.isVideoPaused=true;var type=this.checkVideoType(videoUrl);if(type==TYPE_VIDEO_YOUTUBE)this.playYoutubeVideo(videoUrl,$videoDiv);else if(type==TYPE_VIDEO_VIMEO)this.playVimeoVideo(videoUrl,$videoDiv);else if(type==TYPE_VIDEO_MP4)this.playMp4Video(videoUrl,videoWebmUrl,true,$videoDiv)},playMp4Video:function(href,webmhref,autoplay,
$videoWrapper){this.isHTML5=true;if(this.isIE6||this.isIE7||this.isIE8||this.isIE9)this.isHTML5=false;if(this.isFirefox||this.isOpera)if(!this.canplaymp4&&!webmhref)this.isHTML5=false;if(this.isHTML5){var videoSrc=href;if(this.isFirefox||this.isOpera)if(webmhref)videoSrc=webmhref;this.embedHTML5Video($videoWrapper,videoSrc,autoplay)}else{this.htmlfolder=window.location.href.substr(0,window.location.href.lastIndexOf("/")+1);if(href.charAt(0)!="/"&&href.substring(0,5)!="http:"&&href.substring(0,6)!=
"https:")href=this.htmlfolder+href;this.embedFlash($videoWrapper,"100%","100%",this.options.jsfolder+"html5boxplayer.swf","transparent",{width:"100%",height:"100%",videofile:href,hdfile:"",ishd:"0",autoplay:autoplay?"1":"0",errorcss:".wonderplugin3dcarousel-error{text-align:center;color:#ff0000;}",id:this.id,hidecontrols:"0",hideplaybutton:"0",defaultvolume:1})}},embedHTML5Video:function($container,src,autoPlay){if(this.isFirefox&&this.options.nativecontrolsonfirefox||(this.isIE||this.isIE11)&&this.options.nativecontrolsonie||
this.isIPhone&&this.options.nativecontrolsoniphone||this.isIPad&&this.options.nativecontrolsonipad||this.isAndroid&&this.options.nativecontrolsonandroid)this.options.nativehtml5controls=true;if(this.isIPhone||this.isIPad||this.isAndroid)this.options.nativecontrolsonfullscreen=true;$container.html("<div class='wonderplugin3dcarousel-video-container-"+this.id+"' style='position:relative;display:block;width:100%;height:100%;'><video class='threed-inline-video' style='width:100%;height:100%;margin:0;padding:0;'"+
(this.options.nativehtml5controls&&!this.options.videohidecontrols?" controls='controls'":"")+(this.options.nativecontrolsnodownload?" controlsList='nodownload'":"")+"></div>");$("video",$container).get(0).setAttribute("src",src);if(autoPlay)$("video",$container).get(0).play()},embedFlash:function($container,w,h,src,wmode,flashVars){if(this.flashInstalled){var htmlOptions={pluginspage:"http://www.adobe.com/go/getflashplayer",quality:"high",allowFullScreen:"true",allowScriptAccess:"always",type:"application/x-shockwave-flash"};
htmlOptions.width=w;htmlOptions.height=h;htmlOptions.src=src;htmlOptions.wmode=wmode;htmlOptions.flashVars=$.param(flashVars);var htmlString="";for(var key in htmlOptions)htmlString+=key+"="+htmlOptions[key]+" ";$container.html("<embed "+htmlString+"/>")}else $container.html("<div class='wonderplugin3dcarousel-video-error' style='display:block;position:absolute;text-align:center;width:100%;height:100%;left:0px;top:0px;'><div>The required Adobe Flash Player plugin is not installed</div><br /><div style='display:block;position:relative;text-align:center;width:112px;height:33px;margin:0px auto;'><a href='http://www.adobe.com/go/getflashplayer'><img src='https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' width='112' height='33'></img></a></div>")},
playYoutubeVideo:function(href,$videoWrapper){var src=href+(href.indexOf("?")<0?"?":"&")+"autoplay=1&wmode=transparent&rel=0&autohide=1";$videoWrapper.html("<iframe width='100%' height='100%' src='"+src+"' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>")},playVimeoVideo:function(href,$videoWrapper){var src=href+(href.indexOf("?")<0?"?":"&")+"autoplay=1&api=1";$videoWrapper.html("<iframe width='100%' height='100%' src='"+src+"' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>")},
checkVideoType:function(href){if(!href)return-1;if(href.match(/\.(flv)(.*)?$/i))return TYPE_VIDEO_FLASH;if(href.match(/\.(mp4|m4v)(.*)?$/i))return TYPE_VIDEO_MP4;if(href.match(/\.(ogv|ogg)(.*)?$/i))return TYPE_VIDEO_OGG;if(href.match(/\.(webm)(.*)?$/i))return TYPE_VIDEO_WEBM;if(href.match(/\:\/\/.*(youtube\.com)/i)||href.match(/\:\/\/.*(youtu\.be)/i))return TYPE_VIDEO_YOUTUBE;if(href.match(/\:\/\/.*(vimeo\.com)/i))return TYPE_VIDEO_VIMEO;return 0},initOverlay:function(){var instance=this;if(this.options.addimgoverlay)$(".wonderplugin3dcarousel-item",
this.container).each(function(){$(this).find(".wonderplugin3dcarousel-img").parent().append('<div class="wonderplugin3dcarousel-img-overlay"></div>')});if(this.options.applylinktohoveroverlay){$(".wonderplugin3dcarousel-hoveroverlay",this.container).each(function(){var imgContainer=$(this).parent().find(".wonderplugin3dcarousel-img-container");if(imgContainer.length>0&&imgContainer.parent().is("a"))$(this).css({cursor:"pointer"})});$(".wonderplugin3dcarousel-hoveroverlay",this.container).click(function(){$(this).parent().find(".wonderplugin3dcarousel-img").click()})}$(".wonderplugin3dcarousel-item",
this.container).each(function(){if($(".wonderplugin3dcarousel-hoveroverlay",this).length>0){if($(".wonderplugin3dcarousel-content",this).length>0)$(".wonderplugin3dcarousel-content",this).addClass("wonderplugin3dcarousel-"+instance.options.imghovereffect+"-out");$(".wonderplugin3dcarousel-hoveroverlay",this).addClass("wonderplugin3dcarousel-"+instance.options.imghovereffect+"-in")}})},initLightbox:function(){var instance=this;$(".wp3dcarousellightbox-"+this.id).each(function(){instance.lightboxObject.addItem($(this).attr("href"),
$(this).data("title")?$(this).data("title"):$(this).attr("title"),$(this).data("group"),$(this).data("width"),$(this).data("height"),$(this).data("webm"),$(this).data("ogg"),$(this).data("thumbnail"),$(this).data("description"),$(this).data("mediatype"))});$(".wp3dcarousellightbox-"+this.id).click(function(){var index=$(this).closest(".wonderplugin3dcarousel-item").index();if(index==instance.carouselObject.curItem||!instance.options.onlyenablelightboxoncenter)instance.lightboxObject.showItem($(this).attr("href"));
$(this).closest(".wonderplugin3dcarousel-item").trigger("click");return false});if(instance.options.onlyenablelightboxoncenter)instance.container.on("wonderplugin3dcarousel.beforeswitch",function(){$(".wp3dcarousellightbox-"+instance.id).each(function(){var index=$(this).closest(".wonderplugin3dcarousel-item").index();$(this).css({cursor:index==instance.carouselObject.curItem?"pointer":"default"});var playVideoButton=$(this).find(".wonderplugin3dcarousel-playvideo");if(playVideoButton.length>0)playVideoButton.css({cursor:index==
instance.carouselObject.curItem?"pointer":"default"})})})},resizeCarousel:function(){this.carouselObject.resizeObject()},resizeImgObj:function($img){var imgW=$img.data("rawwidth");var imgH=$img.data("rawheight");if(this.options.donotzoomin&&imgW<=this.carouselObject.itemW&&imgH<=this.carouselObject.itemH){$img.css({"margin-left":this.carouselObject.itemW/2-imgW/2+"px","margin-top":this.carouselObject.itemH/2-imgH/2+"px"});return}if(this.options.scalemode=="fit")if(imgW/imgH>this.carouselObject.itemW/
this.carouselObject.itemH){var margin=this.carouselObject.itemH/2-this.carouselObject.itemW*imgH/imgW/2+"px";$img.css({"max-width":"100%","max-height":"none",width:"100%",height:"auto","margin-left":0,"margin-top":margin});$img.siblings(".wonderplugin3dcarousel-img-overlay").css({width:"100%",left:0,right:0,height:"auto",top:margin,bottom:margin})}else{var margin=this.carouselObject.itemW/2-this.carouselObject.itemH*imgW/imgH/2+"px";$img.css({"max-width":"100%","max-height":"100%",width:"auto",height:"100%",
"margin-left":margin,"margin-top":0});$img.siblings(".wonderplugin3dcarousel-img-overlay").css({width:"auto",left:margin,right:margin,height:"100%",top:0,bottom:0})}else if(imgW/imgH>this.carouselObject.itemW/this.carouselObject.itemH)$img.css({"max-width":"none","max-height":"100%",width:"auto",height:"100%","margin-left":this.carouselObject.itemW/2-this.carouselObject.itemH*imgW/imgH/2+"px","margin-top":0});else $img.css({"max-width":"100%","max-height":"none",width:"100%",height:"auto","margin-left":0,
"margin-top":this.carouselObject.itemH/2-this.carouselObject.itemW*imgH/imgW/2+"px"})},resizeImages:function(){var instance=this;$(".wonderplugin3dcarousel-img",this.container).on("load",function(){var imgWidth=$(this).get(0).naturalWidth?$(this).get(0).naturalWidth:this.width;var imgHeight=$(this).get(0).naturalHeight?$(this).get(0).naturalHeight:this.height;if(!$(this).attr("data-rawwidth"))$(this).data("rawwidth",imgWidth);if(!$(this).attr("data-rawheight"))$(this).data("rawheight",imgHeight);
instance.resizeImgObj($(this))}).each(function(){if(this.complete)$(this).trigger("load")})},gotoPrev:function(){this.carouselObject.gotoPrev()},gotoNext:function(){this.carouselObject.gotoNext()},updateText:function(index){$(".wonderplugin3dcarousel-title",this.container).empty();$(".wonderplugin3dcarousel-description",this.container).empty();if(this.options.showtitle&&this.elemArray.eq(index).data("title"))$(".wonderplugin3dcarousel-title",this.container).html(this.elemArray.eq(index).data("title"));
if(this.options.showdescription&&this.elemArray.eq(index).data("description"))$(".wonderplugin3dcarousel-description",this.container).html(this.elemArray.eq(index).data("description"))},createText:function(){if(this.options.textstyle=="none")return;var carouselText=$('<div class="wonderplugin3dcarousel-text"></div>').appendTo(this.container);if(this.options.showtitle)carouselText.append('<div class="wonderplugin3dcarousel-title"></div>');if(this.options.showdescription)carouselText.append('<div class="wonderplugin3dcarousel-description"></div>');
var instance=this;this.container.on("wonderplugin3dcarousel.switch",function(e,prev,cur){if(cur>=0&&cur<instance.elemLength)if(instance.options.texteffect=="fade")$(".wonderplugin3dcarousel-text",instance.container).fadeOut(function(){instance.updateText(cur);$(".wonderplugin3dcarousel-text",instance.container).fadeIn()});else instance.updateText(cur)})},enableSwipe:function(){if(this.options.enabletouchswipe){var instance=this;var preventDefault=instance.isAndroid&&instance.androidVersion>=0&&instance.androidVersion<=
5?true:false;$(".wonderplugin3dcarousel-list-container",this.container).threedCarouselTouchSwipe({preventWebBrowser:preventDefault,swipeLeft:function(){$(window).trigger("wonderplugin3dcarousel.swipe",[instance.id,instance.carouselObject.curItem,"left"]);instance.gotoNext()},swipeRight:function(){$(window).trigger("wonderplugin3dcarousel.swipe",[instance.id,instance.carouselObject.curItem,"right"]);instance.gotoPrev()}})}},createArrows:function(){if(this.options.arrowstyle=="none")return;var instance=
this;var buttonCode='<div class="wonderplugin3dcarousel-prev"></div><div class="wonderplugin3dcarousel-next"></div>';if(this.options.arrowsinsidelist)$(".wonderplugin3dcarousel-list-container",this.container).append(buttonCode);else this.container.append(buttonCode);var $prevArrow=$(".wonderplugin3dcarousel-prev",this.container);var $nextArrow=$(".wonderplugin3dcarousel-next",this.container);if(this.options.arrowpos=="bottom"){$prevArrow.css({position:"absolute",top:"100%",right:"50%","margin-right":"4px"});
$nextArrow.css({position:"absolute",top:"100%",left:"50%","margin-left":"4px"})}else{$prevArrow.css({position:"absolute",top:"50%","margin-top":"-"+this.options.arrowheight/2+"px",left:0});$nextArrow.css({position:"absolute",top:"50%","margin-top":"-"+this.options.arrowheight/2+"px",right:0})}$prevArrow.css({overflow:"hidden",position:"absolute",cursor:"pointer",width:this.options.arrowwidth+"px",height:this.options.arrowheight+"px",background:'url("'+this.options.arrowimage+'") no-repeat left top'});
$nextArrow.css({overflow:"hidden",position:"absolute",cursor:"pointer",width:this.options.arrowwidth+"px",height:this.options.arrowheight+"px",background:'url("'+this.options.arrowimage+'") no-repeat right top'});$prevArrow.hover(function(){$(this).css({"background-position":"left bottom"})},function(){$(this).css({"background-position":"left top"})});$nextArrow.hover(function(){$(this).css({"background-position":"right bottom"})},function(){$(this).css({"background-position":"right top"})});if(this.options.arrowstyle==
"always"){$prevArrow.css({display:"block"});$nextArrow.css({display:"block"})}else{$prevArrow.css({display:"none"});$nextArrow.css({display:"none"});this.container.hover(function(){if(instance.options.arrowanimation=="slide"){$prevArrow.finish().css({display:"block",opaticy:0,"margin-left":"-"+instance.options.arrowwidth/2+"px"}).animate({opacity:1,"margin-left":0},300);$nextArrow.finish().css({display:"block",opaticy:0,"margin-right":"-"+instance.options.arrowwidth/2+"px"}).animate({opacity:1,"margin-right":0},
300)}else{$prevArrow.fadeIn(300);$nextArrow.fadeIn(300)}},function(){if(instance.options.arrowanimation=="slide"){$prevArrow.finish().animate({opacity:0,"margin-left":"-"+instance.options.arrowwidth/2+"px"},300,function(){$(this).css({display:"none"})});$nextArrow.finish().animate({opacity:0,"margin-right":"-"+instance.options.arrowwidth/2+"px"},300,function(){$(this).css({display:"none"})})}else{$prevArrow.fadeOut(300);$nextArrow.fadeOut(300)}})}$prevArrow.click(function(){$(window).trigger("wonderplugin3dcarousel.arrowclicked",
[instance.id,instance.carouselObject.curItem,"left"]);instance.gotoPrev()});$nextArrow.click(function(){$(window).trigger("wonderplugin3dcarousel.arrowclicked",[instance.id,instance.carouselObject.curItem,"right"]);instance.gotoNext()});this.container.on("wonderplugin3dcarousel.switch",function(e,prev,cur){if(!instance.options.loop&&!instance.options.loopside){$(".wonderplugin3dcarousel-prev",instance.container).css({visibility:cur==0?"hidden":"visible"});$(".wonderplugin3dcarousel-next",instance.container).css({visibility:cur==
instance.elemLength-1?"hidden":"visible"})}})},createBullet:function(index){var instance=this;var bullet=$('<div class="wonderplugin3dcarousel-bullet" data-bulletindex='+index+"></div>");if(this.options.navstyle=="numbering"){bullet.html(String(index+1));bullet.css({display:"inline-block",width:this.options.navwidth,height:this.options.navheight,cursor:"pointer","font":this.options.navnumberingfont,"line-height":this.options.navheight+"px","color":this.options.navnumberingcolor,"background-color":this.options.navnumberingbgcolor,
"text-align":"center",margin:0,padding:0,border:0,"margin-left":index==0?0:this.options.navspacing});if(this.options.navnumberingbground)bullet.css({"border-radius":Math.round(this.options.navheight/2)+"px"})}else bullet.css({display:"inline-block",width:this.options.navwidth,height:this.options.navheight,cursor:"pointer","background-image":'url("'+this.options.navimage+'")',"background-repeat":"no-repeat","background-position":"left top",margin:0,padding:0,border:0,"margin-left":index==0?0:this.options.navspacing});
bullet.hover(function(){instance.bulletHighlight($(this))},function(){instance.bulletNormal($(this))});bullet.click(function(){instance.carouselObject.gotoItem($(this).data("bulletindex"))});return bullet},bulletNormal:function(bullet){if(this.carouselObject.curItem==bullet.data("bulletindex"))return;if(this.options.navstyle=="numbering")bullet.css({"color":this.options.navnumberingcolor,"background-color":this.options.navnumberingbgcolor});else bullet.css({"background-position":"left top"})},bulletHighlight:function(bullet){if(this.options.navstyle==
"numbering")bullet.css({"color":this.options.navnumberingcolorhighlight,"background-color":this.options.navnumberingbgcolorhighlight});else bullet.css({"background-position":"left bottom"})},createBullets:function(){if(this.options.navstyle=="none")return;var instance=this;var nav=$('<div class="wonderplugin3dcarousel-nav"></div>').appendTo(this.container);var bulletWrapper=$('<div class="wonderplugin3dcarousel-bullet-wrapper"></div>').appendTo(nav);var bulletList=$('<div class="wonderplugin3dcarousel-bullet-list"></div>').appendTo(bulletWrapper);
bulletWrapper.css({display:"block",position:"relative","text-align":"center"});bulletList.css({display:"inline-block",position:"relative",margin:"0 auto"});for(var i=0;i<this.elemLength;i++){var bullet=this.createBullet(i);bulletList.append(bullet)}this.container.on("wonderplugin3dcarousel.switch",function(e,prev,cur){var bullets=$(".wonderplugin3dcarousel-bullet",instance.container);if(prev!=cur&&prev>=0)instance.bulletNormal(bullets.eq(prev));if(cur>=0)instance.bulletHighlight(bullets.eq(cur))})}};
options=options||{};for(var key in options)if(key.toLowerCase()!==key){options[key.toLowerCase()]=options[key];delete options[key]}this.each(function(){if($(this).data("donotinit")&&(!options||!options["forceinit"]))return;if($(this).data("inited"))return;$(this).data("inited",1);var defaultOptions={scalemode:"fill",donotzoomin:true,applylinktohoveroverlay:true,itembgcolor:"transparent",itemborder:0,addimgoverlay:true,imghovereffect:"flipy",nativehtml5controls:true,videohidecontrols:false,nativecontrolsonfullscreen:true,
nativecontrolsnodownload:true,nativecontrolsonfirefox:false,nativecontrolsonie:false,nativecontrolsoniphone:true,nativecontrolsonipad:true,nativecontrolsonandroid:true,pausevideonotinmiddle:true,carouselmargin:"48px 0",medium_carouselmargin:"48px 0",small_carouselmargin:"32px 0",textstyle:"always",showtitle:true,showdescription:true,texteffect:"none",enabletouchswipe:true,arrowsinsidelist:true,arrowstyle:"mouseover",arrowpos:"side",arrowimage:"arrows-48-48-1.png",arrowwidth:48,arrowheight:48,arrowanimation:"slide",
navstyle:"bullets",navwidth:16,navheight:16,navspacing:8,navimage:"bullet-16-16-0.png",navnumberingfont:"normal 12px Arial",navnumberingcolor:"#666",navnumberingbgcolor:"#fff",navnumberingcolorhighlight:"#fff",navnumberingbgcolorhighlight:"#333",navnumberingbground:true,showplayvideo:true,playvideoimage:"playvideo-64-64-0.png",playvideoposition:"center center",autoslide:false,autoslidedir:"right",slideinterval:5E3,pauseonmouseover:true,firstitem:0,random:false,onlyenablelightboxoncenter:false,template:"3dslider",
width:400,height:300,legacymode:false,loop:true,loopslide:false,visibleitems:5,perspective:2E3,xdis:350,zdis:200,yrotate:45,transition:"all 0.5s ease-in-out",medium_screenwidth:768,medium_width:400,medium_height:100,small_screenwidth:414,small_width:300,small_height:200,medium_visibleitems:3,medium_perspective:1E3,medium_xdis:350,medium_zdis:200,medium_yrotate:45,medium_transition:"all 0.5s ease-in-out",small_visibleitems:1,small_perspective:1E3,small_xdis:200,small_zdis:150,small_yrotate:45,small_transition:"all 0.5s ease-in-out",
facemode:"circle",itemspace:8,rotatex:-8,scaleratio:1.2,lightboxresponsive:true,lightboxshownavigation:true,lightboxshowtitle:true,lightboxshowdescription:false,lightboxfullscreenmode:false,lightboxcloseonoverlay:true,lightboxvideohidecontrols:false,lightboxtitlestyle:"bottom",lightboximagepercentage:75,lightboxdefaultvideovolume:1,lightboxoverlaybgcolor:"#000",lightboxoverlayopacity:0.9,lightboxbgcolor:"#fff",lightboxtitleprefix:"%NUM / %TOTAL",lightboxtitleinsidecss:"color:#fff; font-size:16px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left;",
lightboxdescriptioninsidecss:"color:#fff; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;",lightboxthumbwidth:90,lightboxthumbheight:60,lightboxthumbtopmargin:12,lightboxthumbbottommargin:4,lightboxbarheight:64,lightboxtitlebottomcss:"{color:#333; font-size:14px; font-family:Armata,sans-serif,Arial; overflow:hidden; text-align:left;}",lightboxdescriptionbottomcss:"{color:#333; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;}",
lightboxautoslide:false,lightboxslideinterval:5E3,lightboxshowtimer:true,lightboxtimerposition:"bottom",lightboxtimerheight:2,lightboxtimercolor:"#dc572e",lightboxtimeropacity:1,lightboxshowplaybutton:true,lightboxalwaysshownavarrows:false,lightboxbordersize:8,lightboxshowtitleprefix:true,lightboxborderradius:0,lightboximagekeepratio:true,lightboxshowsocial:false,lightboxsocialposition:"position:absolute;top:100%;right:0;",lightboxsocialpositionsmallscreen:"position:absolute;top:100%;right:0;left:0;",
lightboxsocialdirection:"horizontal",lightboxsocialbuttonsize:32,lightboxsocialbuttonfontsize:18,lightboxsocialrotateeffect:true,lightboxshowfacebook:true,lightboxshowtwitter:true,lightboxshowpinterest:true,lightboxnavbgcolor:"rgba(0,0,0,0.2)",lightboxshownavcontrol:true,lightboxhidenavdefault:false};this.options=$.extend({},defaultOptions,options);var instance=this;$.each($(this).data(),function(key,value){instance.options[key.toLowerCase()]=value});this.options.mark="WordPress 3D Carousel Free Version";
this.options.marklink="https://www.wonderplugin.com/wordpress-3dcarousel/";var imageList=["arrowimage","navimage","playvideoimage"];for(var i=0;i<imageList.length;i++)if(this.options[imageList[i]].substring(0,7).toLowerCase()!="http://"&&this.options[imageList[i]].substring(0,8).toLowerCase()!="https://")this.options[imageList[i]]=this.options.jsfolder+this.options[imageList[i]];this.options.mtext=false;var lightboxOptions={responsive:this.options.lightboxresponsive,shownavigation:this.options.lightboxshownavigation,
showtitle:this.options.lightboxshowtitle,showdescription:this.options.lightboxshowdescription,thumbwidth:this.options.lightboxthumbwidth,thumbheight:this.options.lightboxthumbheight,thumbtopmargin:this.options.lightboxthumbtopmargin,thumbbottommargin:this.options.lightboxthumbbottommargin,barheight:this.options.lightboxbarheight,titlebottomcss:this.options.lightboxtitlebottomcss,descriptionbottomcss:this.options.lightboxdescriptionbottomcss,fullscreenmode:this.options.lightboxfullscreenmode,closeonoverlay:this.options.lightboxcloseonoverlay,
videohidecontrols:this.options.lightboxvideohidecontrols,titlestyle:this.options.lightboxtitlestyle,imagepercentage:this.options.lightboximagepercentage,defaultvideovolume:this.options.lightboxdefaultvideovolume,overlaybgcolor:this.options.lightboxoverlaybgcolor,overlayopacity:this.options.lightboxoverlayopacity,bgcolor:this.options.lightboxbgcolor,titleprefix:this.options.lightboxtitleprefix,titleinsidecss:this.options.lightboxtitleinsidecss,descriptioninsidecss:this.options.lightboxdescriptioninsidecss,
autoslide:this.options.lightboxautoslide,slideinterval:this.options.lightboxslideinterval,showtimer:this.options.lightboxshowtimer,timerposition:this.options.lightboxtimerposition,timerheight:this.options.lightboxtimerheight,timercolor:this.options.lightboxtimercolor,timeropacity:this.options.lightboxtimeropacity,showplaybutton:this.options.lightboxshowplaybutton,alwaysshownavarrows:this.options.lightboxalwaysshownavarrows,bordersize:this.options.lightboxbordersize,showtitleprefix:this.options.lightboxshowtitleprefix,
borderradius:this.options.lightboxborderradius,imagekeepratio:this.options.lightboximagekeepratio,showsocial:this.options.lightboxshowsocial,socialposition:this.options.lightboxsocialposition,socialpositionsmallscreen:this.options.lightboxsocialpositionsmallscreen,socialdirection:this.options.lightboxsocialdirection,socialbuttonsize:this.options.lightboxsocialbuttonsize,socialbuttonfontsize:this.options.lightboxsocialbuttonfontsize,socialrotateeffect:this.options.lightboxsocialrotateeffect,showfacebook:this.options.lightboxshowfacebook,
showtwitter:this.options.lightboxshowtwitter,showpinterest:this.options.lightboxshowpinterest,navbgcolor:this.options.lightboxnavbgcolor,shownavcontrol:this.options.lightboxshownavcontrol,hidenavdefault:this.options.lightboxhidenavdefault};if($("#wp3dcarousellightbox_advanced_options").length)$.each($("#wp3dcarousellightbox_advanced_options").data(),function(key,value){lightboxOptions[key.toLowerCase()]=value});if($("#wp3dcarousellightbox_advanced_options_"+this.options.carouselid).length)$.each($("#wp3dcarousellightbox_advanced_options_"+
this.options.carouselid).data(),function(key,value){lightboxOptions[key.toLowerCase()]=value});var init3DCarousel=function(inst){var lightboxObject=$("").wp3dcarousellightbox(lightboxOptions);wp3DCarouselLightboxObjects.addObject(lightboxObject);$(inst).data("lightboxobject",lightboxObject);var object=new WP3DCarousel($(inst),inst.options,inst.options.carouselid,lightboxObject);wp3DCarouselObjects.addObject(object);$(inst).data("object",object);$(inst).data("id",inst.options.carouselid)};init3DCarousel(this)})};
$(document).ready(function(){$(".wonderplugin-3dcarousel-engine").css({display:"none"});$(".wonderplugin3dcarousel").wonderplugin3dcarousel()})})(jQuery);
(function($){$.fn.threedCarouselTouchSwipe=function(options){var defaults={preventWebBrowser:false,swipeLeft:null,swipeRight:null,swipeTop:null,swipeBottom:null};if(options)$.extend(defaults,options);return this.each(function(){var startX=-1,startY=-1;var curX=-1,curY=-1;function touchStart(event){var e=event.originalEvent;if(e.targetTouches.length>=1){startX=e.targetTouches[0].pageX;startY=e.targetTouches[0].pageY}else touchCancel(event)}function touchMove(event){if(defaults.preventWebBrowser)event.preventDefault();
var e=event.originalEvent;if(e.targetTouches.length>=1){curX=e.targetTouches[0].pageX;curY=e.targetTouches[0].pageY}else touchCancel(event)}function touchEnd(event){if(curX>0||curY>0){triggerHandler();touchCancel(event)}else touchCancel(event)}function touchCancel(event){startX=-1;startY=-1;curX=-1;curY=-1}function triggerHandler(){if(Math.abs(curX-startX)>Math.abs(curY-startY))if(curX>startX){if(defaults.swipeRight)defaults.swipeRight.call()}else{if(defaults.swipeLeft)defaults.swipeLeft.call()}else if(curY>
startY){if(defaults.swipeBottom)defaults.swipeBottom.call()}else if(defaults.swipeTop)defaults.swipeTop.call()}try{$(this).on("touchstart",touchStart);$(this).on("touchmove",touchMove);$(this).on("touchend",touchEnd);$(this).on("touchcancel",touchCancel)}catch(e){}})}})(jQuery);if(typeof wp3DCarouselLightboxObjects==="undefined")var wp3DCarouselLightboxObjects=new function(){this.objects=[];this.addObject=function(obj){this.objects.push(obj)}};
if(typeof wp3DCarouselObjects==="undefined")var wp3DCarouselObjects=new function(){this.objects=[];this.addObject=function(obj){this.objects.push(obj)}};
;
/* global wc_add_to_cart_params */
jQuery( function( $ ) {

	if ( typeof wc_add_to_cart_params === 'undefined' ) {
		return false;
	}

	/**
	 * AddToCartHandler class.
	 */
	var AddToCartHandler = function() {
		$( document.body )
			.on( 'click', '.add_to_cart_button', this.onAddToCart )
			.on( 'click', '.remove_from_cart_button', this.onRemoveFromCart )
			.on( 'added_to_cart', this.updateButton )
			.on( 'added_to_cart', this.updateCartPage )
			.on( 'added_to_cart removed_from_cart', this.updateFragments );
	};

	/**
	 * Handle the add to cart event.
	 */
	AddToCartHandler.prototype.onAddToCart = function( e ) {
		var $thisbutton = $( this );

		if ( $thisbutton.is( '.ajax_add_to_cart' ) ) {
			if ( ! $thisbutton.attr( 'data-product_id' ) ) {
				return true;
			}

			e.preventDefault();

			$thisbutton.removeClass( 'added' );
			$thisbutton.addClass( 'loading' );

			var data = {};

			$.each( $thisbutton.data(), function( key, value ) {
				data[ key ] = value;
			});

			// Trigger event.
			$( document.body ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

			// Ajax action.
			$.post( wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'add_to_cart' ), data, function( response ) {
				if ( ! response ) {
					return;
				}

				if ( response.error && response.product_url ) {
					window.location = response.product_url;
					return;
				}

				// Redirect to cart option
				if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
					window.location = wc_add_to_cart_params.cart_url;
					return;
				}

				// Trigger event so themes can refresh other areas.
				$( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, $thisbutton ] );
			});
		}
	};

	/**
	 * Update fragments after remove from cart event in mini-cart.
	 */
	AddToCartHandler.prototype.onRemoveFromCart = function( e ) {
		var $thisbutton = $( this ),
			$row        = $thisbutton.closest( '.woocommerce-mini-cart-item' );

		e.preventDefault();

		$row.block({
			message: null,
			overlayCSS: {
				opacity: 0.6
			}
		});

		$.post( wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_from_cart' ), { cart_item_key : $thisbutton.data( 'cart_item_key' ) }, function( response ) {
			if ( ! response || ! response.fragments ) {
				window.location = $thisbutton.attr( 'href' );
				return;
			}
			$( document.body ).trigger( 'removed_from_cart', [ response.fragments, response.cart_hash ] );
		}).fail( function() {
			window.location = $thisbutton.attr( 'href' );
			return;
		});
	};

	/**
	 * Update cart page elements after add to cart events.
	 */
	AddToCartHandler.prototype.updateButton = function( e, fragments, cart_hash, $button ) {
		$button = typeof $button === 'undefined' ? false : $button;

		if ( $button ) {
			$button.removeClass( 'loading' );
			$button.addClass( 'added' );

			// View cart text.
			if ( ! wc_add_to_cart_params.is_cart && $button.parent().find( '.added_to_cart' ).length === 0 ) {
				$button.after( ' <a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' +
					wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
			}

			$( document.body ).trigger( 'wc_cart_button_updated', [ $button ] );
		}
	};

	/**
	 * Update cart page elements after add to cart events.
	 */
	AddToCartHandler.prototype.updateCartPage = function() {
		var page = window.location.toString().replace( 'add-to-cart', 'added-to-cart' );

		$( '.shop_table.cart' ).load( page + ' .shop_table.cart:eq(0) > *', function() {
			$( '.shop_table.cart' ).stop( true ).css( 'opacity', '1' ).unblock();
			$( document.body ).trigger( 'cart_page_refreshed' );
		});

		$( '.cart_totals' ).load( page + ' .cart_totals:eq(0) > *', function() {
			$( '.cart_totals' ).stop( true ).css( 'opacity', '1' ).unblock();
			$( document.body ).trigger( 'cart_totals_refreshed' );
		});
	};

	/**
	 * Update fragments after add to cart events.
	 */
	AddToCartHandler.prototype.updateFragments = function( e, fragments ) {
		if ( fragments ) {
			$.each( fragments, function( key ) {
				$( key )
					.addClass( 'updating' )
					.fadeTo( '400', '0.6' )
					.block({
						message: null,
						overlayCSS: {
							opacity: 0.6
						}
					});
			});

			$.each( fragments, function( key, value ) {
				$( key ).replaceWith( value );
				$( key ).stop( true ).css( 'opacity', '1' ).unblock();
			});

			$( document.body ).trigger( 'wc_fragments_loaded' );
		}
	};

	/**
	 * Init AddToCartHandler.
	 */
	new AddToCartHandler();
});
;
/*!
 * jQuery blockUI plugin
 * Version 2.70.0-2014.11.23
 * Requires jQuery v1.7 or later
 *
 * Examples at: http://malsup.com/jquery/block/
 * Copyright (c) 2007-2013 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Thanks to Amir-Hossein Sobhi for some excellent contributions!
 */
;(function() {
/*jshint eqeqeq:false curly:false latedef:false */
"use strict";

	function setup($) {
		$.fn._fadeIn = $.fn.fadeIn;

		var noOp = $.noop || function() {};

		// this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
		// confusing userAgent strings on Vista)
		var msie = /MSIE/.test(navigator.userAgent);
		var ie6  = /MSIE 6.0/.test(navigator.userAgent) && ! /MSIE 8.0/.test(navigator.userAgent);
		var mode = document.documentMode || 0;
		var setExpr = $.isFunction( document.createElement('div').style.setExpression );

		// global $ methods for blocking/unblocking the entire page
		$.blockUI   = function(opts) { install(window, opts); };
		$.unblockUI = function(opts) { remove(window, opts); };

		// convenience method for quick growl-like notifications  (http://www.google.com/search?q=growl)
		$.growlUI = function(title, message, timeout, onClose) {
			var $m = $('<div class="growlUI"></div>');
			if (title) $m.append('<h1>'+title+'</h1>');
			if (message) $m.append('<h2>'+message+'</h2>');
			if (timeout === undefined) timeout = 3000;

			// Added by konapun: Set timeout to 30 seconds if this growl is moused over, like normal toast notifications
			var callBlock = function(opts) {
				opts = opts || {};

				$.blockUI({
					message: $m,
					fadeIn : typeof opts.fadeIn  !== 'undefined' ? opts.fadeIn  : 700,
					fadeOut: typeof opts.fadeOut !== 'undefined' ? opts.fadeOut : 1000,
					timeout: typeof opts.timeout !== 'undefined' ? opts.timeout : timeout,
					centerY: false,
					showOverlay: false,
					onUnblock: onClose,
					css: $.blockUI.defaults.growlCSS
				});
			};

			callBlock();
			var nonmousedOpacity = $m.css('opacity');
			$m.mouseover(function() {
				callBlock({
					fadeIn: 0,
					timeout: 30000
				});

				var displayBlock = $('.blockMsg');
				displayBlock.stop(); // cancel fadeout if it has started
				displayBlock.fadeTo(300, 1); // make it easier to read the message by removing transparency
			}).mouseout(function() {
				$('.blockMsg').fadeOut(1000);
			});
			// End konapun additions
		};

		// plugin method for blocking element content
		$.fn.block = function(opts) {
			if ( this[0] === window ) {
				$.blockUI( opts );
				return this;
			}
			var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
			this.each(function() {
				var $el = $(this);
				if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked'))
					return;
				$el.unblock({ fadeOut: 0 });
			});

			return this.each(function() {
				if ($.css(this,'position') == 'static') {
					this.style.position = 'relative';
					$(this).data('blockUI.static', true);
				}
				this.style.zoom = 1; // force 'hasLayout' in ie
				install(this, opts);
			});
		};

		// plugin method for unblocking element content
		$.fn.unblock = function(opts) {
			if ( this[0] === window ) {
				$.unblockUI( opts );
				return this;
			}
			return this.each(function() {
				remove(this, opts);
			});
		};

		$.blockUI.version = 2.70; // 2nd generation blocking at no extra cost!

		// override these in your code to change the default behavior and style
		$.blockUI.defaults = {
			// message displayed when blocking (use null for no message)
			message:  '<h1>Please wait...</h1>',

			title: null,		// title string; only used when theme == true
			draggable: true,	// only used when theme == true (requires jquery-ui.js to be loaded)

			theme: false, // set to true to use with jQuery UI themes

			// styles for the message when blocking; if you wish to disable
			// these and use an external stylesheet then do this in your code:
			// $.blockUI.defaults.css = {};
			css: {
				padding:	0,
				margin:		0,
				width:		'30%',
				top:		'40%',
				left:		'35%',
				textAlign:	'center',
				color:		'#000',
				border:		'3px solid #aaa',
				backgroundColor:'#fff',
				cursor:		'wait'
			},

			// minimal style set used when themes are used
			themedCSS: {
				width:	'30%',
				top:	'40%',
				left:	'35%'
			},

			// styles for the overlay
			overlayCSS:  {
				backgroundColor:	'#000',
				opacity:			0.6,
				cursor:				'wait'
			},

			// style to replace wait cursor before unblocking to correct issue
			// of lingering wait cursor
			cursorReset: 'default',

			// styles applied when using $.growlUI
			growlCSS: {
				width:		'350px',
				top:		'10px',
				left:		'',
				right:		'10px',
				border:		'none',
				padding:	'5px',
				opacity:	0.6,
				cursor:		'default',
				color:		'#fff',
				backgroundColor: '#000',
				'-webkit-border-radius':'10px',
				'-moz-border-radius':	'10px',
				'border-radius':		'10px'
			},

			// IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
			// (hat tip to Jorge H. N. de Vasconcelos)
			/*jshint scripturl:true */
			iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',

			// force usage of iframe in non-IE browsers (handy for blocking applets)
			forceIframe: false,

			// z-index for the blocking overlay
			baseZ: 1000,

			// set these to true to have the message automatically centered
			centerX: true, // <-- only effects element blocking (page block controlled via css above)
			centerY: true,

			// allow body element to be stetched in ie6; this makes blocking look better
			// on "short" pages.  disable if you wish to prevent changes to the body height
			allowBodyStretch: true,

			// enable if you want key and mouse events to be disabled for content that is blocked
			bindEvents: true,

			// be default blockUI will supress tab navigation from leaving blocking content
			// (if bindEvents is true)
			constrainTabKey: true,

			// fadeIn time in millis; set to 0 to disable fadeIn on block
			fadeIn:  200,

			// fadeOut time in millis; set to 0 to disable fadeOut on unblock
			fadeOut:  400,

			// time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
			timeout: 0,

			// disable if you don't want to show the overlay
			showOverlay: true,

			// if true, focus will be placed in the first available input field when
			// page blocking
			focusInput: true,

            // elements that can receive focus
            focusableElements: ':input:enabled:visible',

			// suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
			// no longer needed in 2012
			// applyPlatformOpacityRules: true,

			// callback method invoked when fadeIn has completed and blocking message is visible
			onBlock: null,

			// callback method invoked when unblocking has completed; the callback is
			// passed the element that has been unblocked (which is the window object for page
			// blocks) and the options that were passed to the unblock call:
			//	onUnblock(element, options)
			onUnblock: null,

			// callback method invoked when the overlay area is clicked.
			// setting this will turn the cursor to a pointer, otherwise cursor defined in overlayCss will be used.
			onOverlayClick: null,

			// don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
			quirksmodeOffsetHack: 4,

			// class name of the message block
			blockMsgClass: 'blockMsg',

			// if it is already blocked, then ignore it (don't unblock and reblock)
			ignoreIfBlocked: false
		};

		// private data and functions follow...

		var pageBlock = null;
		var pageBlockEls = [];

		function install(el, opts) {
			var css, themedCSS;
			var full = (el == window);
			var msg = (opts && opts.message !== undefined ? opts.message : undefined);
			opts = $.extend({}, $.blockUI.defaults, opts || {});

			if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked'))
				return;

			opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
			css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
			if (opts.onOverlayClick)
				opts.overlayCSS.cursor = 'pointer';

			themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
			msg = msg === undefined ? opts.message : msg;

			// remove the current block (if there is one)
			if (full && pageBlock)
				remove(window, {fadeOut:0});

			// if an existing element is being used as the blocking content then we capture
			// its current place in the DOM (and current display style) so we can restore
			// it when we unblock
			if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
				var node = msg.jquery ? msg[0] : msg;
				var data = {};
				$(el).data('blockUI.history', data);
				data.el = node;
				data.parent = node.parentNode;
				data.display = node.style.display;
				data.position = node.style.position;
				if (data.parent)
					data.parent.removeChild(node);
			}

			$(el).data('blockUI.onUnblock', opts.onUnblock);
			var z = opts.baseZ;

			// blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
			// layer1 is the iframe layer which is used to supress bleed through of underlying content
			// layer2 is the overlay layer which has opacity and a wait cursor (by default)
			// layer3 is the message content that is displayed while blocking
			var lyr1, lyr2, lyr3, s;
			if (msie || opts.forceIframe)
				lyr1 = $('<iframe class="blockUI" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>');
			else
				lyr1 = $('<div class="blockUI" style="display:none"></div>');

			if (opts.theme)
				lyr2 = $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+ (z++) +';display:none"></div>');
			else
				lyr2 = $('<div class="blockUI blockOverlay" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');

			if (opts.theme && full) {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:fixed">';
				if ( opts.title ) {
					s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
				}
				s += '<div class="ui-widget-content ui-dialog-content"></div>';
				s += '</div>';
			}
			else if (opts.theme) {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:absolute">';
				if ( opts.title ) {
					s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
				}
				s += '<div class="ui-widget-content ui-dialog-content"></div>';
				s += '</div>';
			}
			else if (full) {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:'+(z+10)+';display:none;position:fixed"></div>';
			}
			else {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:'+(z+10)+';display:none;position:absolute"></div>';
			}
			lyr3 = $(s);

			// if we have a message, style it
			if (msg) {
				if (opts.theme) {
					lyr3.css(themedCSS);
					lyr3.addClass('ui-widget-content');
				}
				else
					lyr3.css(css);
			}

			// style the overlay
			if (!opts.theme /*&& (!opts.applyPlatformOpacityRules)*/)
				lyr2.css(opts.overlayCSS);
			lyr2.css('position', full ? 'fixed' : 'absolute');

			// make iframe layer transparent in IE
			if (msie || opts.forceIframe)
				lyr1.css('opacity',0.0);

			//$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
			var layers = [lyr1,lyr2,lyr3], $par = full ? $('body') : $(el);
			$.each(layers, function() {
				this.appendTo($par);
			});

			if (opts.theme && opts.draggable && $.fn.draggable) {
				lyr3.draggable({
					handle: '.ui-dialog-titlebar',
					cancel: 'li'
				});
			}

			// ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
			var expr = setExpr && (!$.support.boxModel || $('object,embed', full ? null : el).length > 0);
			if (ie6 || expr) {
				// give body 100% height
				if (full && opts.allowBodyStretch && $.support.boxModel)
					$('html,body').css('height','100%');

				// fix ie6 issue when blocked element has a border width
				if ((ie6 || !$.support.boxModel) && !full) {
					var t = sz(el,'borderTopWidth'), l = sz(el,'borderLeftWidth');
					var fixT = t ? '(0 - '+t+')' : 0;
					var fixL = l ? '(0 - '+l+')' : 0;
				}

				// simulate fixed position
				$.each(layers, function(i,o) {
					var s = o[0].style;
					s.position = 'absolute';
					if (i < 2) {
						if (full)
							s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"');
						else
							s.setExpression('height','this.parentNode.offsetHeight + "px"');
						if (full)
							s.setExpression('width','jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"');
						else
							s.setExpression('width','this.parentNode.offsetWidth + "px"');
						if (fixL) s.setExpression('left', fixL);
						if (fixT) s.setExpression('top', fixT);
					}
					else if (opts.centerY) {
						if (full) s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
						s.marginTop = 0;
					}
					else if (!opts.centerY && full) {
						var top = (opts.css && opts.css.top) ? parseInt(opts.css.top, 10) : 0;
						var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';
						s.setExpression('top',expression);
					}
				});
			}

			// show the message
			if (msg) {
				if (opts.theme)
					lyr3.find('.ui-widget-content').append(msg);
				else
					lyr3.append(msg);
				if (msg.jquery || msg.nodeType)
					$(msg).show();
			}

			if ((msie || opts.forceIframe) && opts.showOverlay)
				lyr1.show(); // opacity is zero
			if (opts.fadeIn) {
				var cb = opts.onBlock ? opts.onBlock : noOp;
				var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
				var cb2 = msg ? cb : noOp;
				if (opts.showOverlay)
					lyr2._fadeIn(opts.fadeIn, cb1);
				if (msg)
					lyr3._fadeIn(opts.fadeIn, cb2);
			}
			else {
				if (opts.showOverlay)
					lyr2.show();
				if (msg)
					lyr3.show();
				if (opts.onBlock)
					opts.onBlock.bind(lyr3)();
			}

			// bind key and mouse events
			bind(1, el, opts);

			if (full) {
				pageBlock = lyr3[0];
				pageBlockEls = $(opts.focusableElements,pageBlock);
				if (opts.focusInput)
					setTimeout(focus, 20);
			}
			else
				center(lyr3[0], opts.centerX, opts.centerY);

			if (opts.timeout) {
				// auto-unblock
				var to = setTimeout(function() {
					if (full)
						$.unblockUI(opts);
					else
						$(el).unblock(opts);
				}, opts.timeout);
				$(el).data('blockUI.timeout', to);
			}
		}

		// remove the block
		function remove(el, opts) {
			var count;
			var full = (el == window);
			var $el = $(el);
			var data = $el.data('blockUI.history');
			var to = $el.data('blockUI.timeout');
			if (to) {
				clearTimeout(to);
				$el.removeData('blockUI.timeout');
			}
			opts = $.extend({}, $.blockUI.defaults, opts || {});
			bind(0, el, opts); // unbind events

			if (opts.onUnblock === null) {
				opts.onUnblock = $el.data('blockUI.onUnblock');
				$el.removeData('blockUI.onUnblock');
			}

			var els;
			if (full) // crazy selector to handle odd field errors in ie6/7
				els = $(document.body).children().filter('.blockUI').add('body > .blockUI');
			else
				els = $el.find('>.blockUI');

			// fix cursor issue
			if ( opts.cursorReset ) {
				if ( els.length > 1 )
					els[1].style.cursor = opts.cursorReset;
				if ( els.length > 2 )
					els[2].style.cursor = opts.cursorReset;
			}

			if (full)
				pageBlock = pageBlockEls = null;

			if (opts.fadeOut) {
				count = els.length;
				els.stop().fadeOut(opts.fadeOut, function() {
					if ( --count === 0)
						reset(els,data,opts,el);
				});
			}
			else
				reset(els, data, opts, el);
		}

		// move blocking element back into the DOM where it started
		function reset(els,data,opts,el) {
			var $el = $(el);
			if ( $el.data('blockUI.isBlocked') )
				return;

			els.each(function(i,o) {
				// remove via DOM calls so we don't lose event handlers
				if (this.parentNode)
					this.parentNode.removeChild(this);
			});

			if (data && data.el) {
				data.el.style.display = data.display;
				data.el.style.position = data.position;
				data.el.style.cursor = 'default'; // #59
				if (data.parent)
					data.parent.appendChild(data.el);
				$el.removeData('blockUI.history');
			}

			if ($el.data('blockUI.static')) {
				$el.css('position', 'static'); // #22
			}

			if (typeof opts.onUnblock == 'function')
				opts.onUnblock(el,opts);

			// fix issue in Safari 6 where block artifacts remain until reflow
			var body = $(document.body), w = body.width(), cssW = body[0].style.width;
			body.width(w-1).width(w);
			body[0].style.width = cssW;
		}

		// bind/unbind the handler
		function bind(b, el, opts) {
			var full = el == window, $el = $(el);

			// don't bother unbinding if there is nothing to unbind
			if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
				return;

			$el.data('blockUI.isBlocked', b);

			// don't bind events when overlay is not in use or if bindEvents is false
			if (!full || !opts.bindEvents || (b && !opts.showOverlay))
				return;

			// bind anchors and inputs for mouse and key events
			var events = 'mousedown mouseup keydown keypress keyup touchstart touchend touchmove';
			if (b)
				$(document).bind(events, opts, handler);
			else
				$(document).unbind(events, handler);

		// former impl...
		//		var $e = $('a,:input');
		//		b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
		}

		// event handler to suppress keyboard/mouse events when blocking
		function handler(e) {
			// allow tab navigation (conditionally)
			if (e.type === 'keydown' && e.keyCode && e.keyCode == 9) {
				if (pageBlock && e.data.constrainTabKey) {
					var els = pageBlockEls;
					var fwd = !e.shiftKey && e.target === els[els.length-1];
					var back = e.shiftKey && e.target === els[0];
					if (fwd || back) {
						setTimeout(function(){focus(back);},10);
						return false;
					}
				}
			}
			var opts = e.data;
			var target = $(e.target);
			if (target.hasClass('blockOverlay') && opts.onOverlayClick)
				opts.onOverlayClick(e);

			// allow events within the message content
			if (target.parents('div.' + opts.blockMsgClass).length > 0)
				return true;

			// allow events for content that is not being blocked
			return target.parents().children().filter('div.blockUI').length === 0;
		}

		function focus(back) {
			if (!pageBlockEls)
				return;
			var e = pageBlockEls[back===true ? pageBlockEls.length-1 : 0];
			if (e)
				e.focus();
		}

		function center(el, x, y) {
			var p = el.parentNode, s = el.style;
			var l = ((p.offsetWidth - el.offsetWidth)/2) - sz(p,'borderLeftWidth');
			var t = ((p.offsetHeight - el.offsetHeight)/2) - sz(p,'borderTopWidth');
			if (x) s.left = l > 0 ? (l+'px') : '0';
			if (y) s.top  = t > 0 ? (t+'px') : '0';
		}

		function sz(el, p) {
			return parseInt($.css(el,p),10)||0;
		}

	}


	/*global define:true */
	if (typeof define === 'function' && define.amd && define.amd.jQuery) {
		define(['jquery'], setup);
	} else {
		setup(jQuery);
	}

})();;
/*!
 * JavaScript Cookie v2.1.4
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader = false;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init (converter) {
		function api (key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				// We're using "expires" because "max-age" is not supported by IE
				attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				var stringifiedAttributes = '';

				for (var attributeName in attributes) {
					if (!attributes[attributeName]) {
						continue;
					}
					stringifiedAttributes += '; ' + attributeName;
					if (attributes[attributeName] === true) {
						continue;
					}
					stringifiedAttributes += '=' + attributes[attributeName];
				}
				return (document.cookie = key + '=' + value + stringifiedAttributes);
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) {}
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));
;
/* global Cookies */
jQuery( function( $ ) {
	// Orderby
	$( '.woocommerce-ordering' ).on( 'change', 'select.orderby', function() {
		$( this ).closest( 'form' ).submit();
	});

	// Target quantity inputs on product pages
	$( 'input.qty:not(.product-quantity input.qty)' ).each( function() {
		var min = parseFloat( $( this ).attr( 'min' ) );

		if ( min >= 0 && parseFloat( $( this ).val() ) < min ) {
			$( this ).val( min );
		}
	});

	// Set a cookie and hide the store notice when the dismiss button is clicked
	$( '.woocommerce-store-notice__dismiss-link' ).click( function() {
		Cookies.set( 'store_notice', 'hidden', { path: '/' } );
		$( '.woocommerce-store-notice' ).hide();
	});

	// Check the value of that cookie and show/hide the notice accordingly
	if ( 'hidden' === Cookies.get( 'store_notice' ) ) {
		$( '.woocommerce-store-notice' ).hide();
	} else {
		$( '.woocommerce-store-notice' ).show();
	}

	// Make form field descriptions toggle on focus.
	$( document.body ).on( 'click', function() {
		$( '.woocommerce-input-wrapper span.description:visible' ).prop( 'aria-hidden', true ).slideUp( 250 );
	} );

	$( '.woocommerce-input-wrapper' ).on( 'click', function( event ) {
		event.stopPropagation();
	} );

	$( '.woocommerce-input-wrapper :input' )
		.on( 'keydown', function( event ) {
			var input       = $( this ),
				parent      = input.parent(),
				description = parent.find( 'span.description' );

			if ( 27 === event.which && description.length && description.is( ':visible' ) ) {
				description.prop( 'aria-hidden', true ).slideUp( 250 );
				event.preventDefault();
				return false;
			}
		} )
		.on( 'click focus', function() {
			var input       = $( this ),
				parent      = input.parent(),
				description = parent.find( 'span.description' );

			parent.addClass( 'currentTarget' );

			$( '.woocommerce-input-wrapper:not(.currentTarget) span.description:visible' ).prop( 'aria-hidden', true ).slideUp( 250 );

			if ( description.length && description.is( ':hidden' ) ) {
				description.prop( 'aria-hidden', false ).slideDown( 250 );
			}

			parent.removeClass( 'currentTarget' );
		} );

	// Common scroll to element code.
	$.scroll_to_notices = function( scrollElement ) {
		if ( scrollElement.length ) {
			$( 'html, body' ).animate( {
				scrollTop: ( scrollElement.offset().top - 100 )
			}, 1000 );
		}
	};
});
;
/* global wc_cart_fragments_params, Cookies */
jQuery( function( $ ) {

	// wc_cart_fragments_params is required to continue, ensure the object exists
	if ( typeof wc_cart_fragments_params === 'undefined' ) {
		return false;
	}

	/* Storage Handling */
	var $supports_html5_storage = true,
		cart_hash_key           = wc_cart_fragments_params.cart_hash_key;

	try {
		$supports_html5_storage = ( 'sessionStorage' in window && window.sessionStorage !== null );
		window.sessionStorage.setItem( 'wc', 'test' );
		window.sessionStorage.removeItem( 'wc' );
		window.localStorage.setItem( 'wc', 'test' );
		window.localStorage.removeItem( 'wc' );
	} catch( err ) {
		$supports_html5_storage = false;
	}

	/* Cart session creation time to base expiration on */
	function set_cart_creation_timestamp() {
		if ( $supports_html5_storage ) {
			sessionStorage.setItem( 'wc_cart_created', ( new Date() ).getTime() );
		}
	}

	/** Set the cart hash in both session and local storage */
	function set_cart_hash( cart_hash ) {
		if ( $supports_html5_storage ) {
			localStorage.setItem( cart_hash_key, cart_hash );
			sessionStorage.setItem( cart_hash_key, cart_hash );
		}
	}

	var $fragment_refresh = {
		url: wc_cart_fragments_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
		type: 'POST',
		success: function( data ) {
			if ( data && data.fragments ) {

				$.each( data.fragments, function( key, value ) {
					$( key ).replaceWith( value );
				});

				if ( $supports_html5_storage ) {
					sessionStorage.setItem( wc_cart_fragments_params.fragment_name, JSON.stringify( data.fragments ) );
					set_cart_hash( data.cart_hash );

					if ( data.cart_hash ) {
						set_cart_creation_timestamp();
					}
				}

				$( document.body ).trigger( 'wc_fragments_refreshed' );
			}
		}
	};

	/* Named callback for refreshing cart fragment */
	function refresh_cart_fragment() {
		$.ajax( $fragment_refresh );
	}

	/* Cart Handling */
	if ( $supports_html5_storage ) {

		var cart_timeout = null,
			day_in_ms    = ( 24 * 60 * 60 * 1000 );

		$( document.body ).on( 'wc_fragment_refresh updated_wc_div', function() {
			refresh_cart_fragment();
		});

		$( document.body ).on( 'added_to_cart removed_from_cart', function( event, fragments, cart_hash ) {
			var prev_cart_hash = sessionStorage.getItem( cart_hash_key );

			if ( prev_cart_hash === null || prev_cart_hash === undefined || prev_cart_hash === '' ) {
				set_cart_creation_timestamp();
			}

			sessionStorage.setItem( wc_cart_fragments_params.fragment_name, JSON.stringify( fragments ) );
			set_cart_hash( cart_hash );
		});

		$( document.body ).on( 'wc_fragments_refreshed', function() {
			clearTimeout( cart_timeout );
			cart_timeout = setTimeout( refresh_cart_fragment, day_in_ms );
		} );

		// Refresh when storage changes in another tab
		$( window ).on( 'storage onstorage', function ( e ) {
			if ( cart_hash_key === e.originalEvent.key && localStorage.getItem( cart_hash_key ) !== sessionStorage.getItem( cart_hash_key ) ) {
				refresh_cart_fragment();
			}
		});

		// Refresh when page is shown after back button (safari)
		$( window ).on( 'pageshow' , function( e ) {
			if ( e.originalEvent.persisted ) {
				$( '.widget_shopping_cart_content' ).empty();
				$( document.body ).trigger( 'wc_fragment_refresh' );
			}
		} );

		try {
			var wc_fragments = $.parseJSON( sessionStorage.getItem( wc_cart_fragments_params.fragment_name ) ),
				cart_hash    = sessionStorage.getItem( cart_hash_key ),
				cookie_hash  = Cookies.get( 'woocommerce_cart_hash'),
				cart_created = sessionStorage.getItem( 'wc_cart_created' );

			if ( cart_hash === null || cart_hash === undefined || cart_hash === '' ) {
				cart_hash = '';
			}

			if ( cookie_hash === null || cookie_hash === undefined || cookie_hash === '' ) {
				cookie_hash = '';
			}

			if ( cart_hash && ( cart_created === null || cart_created === undefined || cart_created === '' ) ) {
				throw 'No cart_created';
			}

			if ( cart_created ) {
				var cart_expiration = ( ( 1 * cart_created ) + day_in_ms ),
					timestamp_now   = ( new Date() ).getTime();
				if ( cart_expiration < timestamp_now ) {
					throw 'Fragment expired';
				}
				cart_timeout = setTimeout( refresh_cart_fragment, ( cart_expiration - timestamp_now ) );
			}

			if ( wc_fragments && wc_fragments['div.widget_shopping_cart_content'] && cart_hash === cookie_hash ) {

				$.each( wc_fragments, function( key, value ) {
					$( key ).replaceWith(value);
				});

				$( document.body ).trigger( 'wc_fragments_loaded' );
			} else {
				throw 'No fragment';
			}

		} catch( err ) {
			refresh_cart_fragment();
		}

	} else {
		refresh_cart_fragment();
	}

	/* Cart Hiding */
	if ( Cookies.get( 'woocommerce_items_in_cart' ) > 0 ) {
		$( '.hide_cart_widget_if_empty' ).closest( '.widget_shopping_cart' ).show();
	} else {
		$( '.hide_cart_widget_if_empty' ).closest( '.widget_shopping_cart' ).hide();
	}

	$( document.body ).on( 'adding_to_cart', function() {
		$( '.hide_cart_widget_if_empty' ).closest( '.widget_shopping_cart' ).show();
	});
});
;
;

		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			jQuery( 'body' ).trigger( 'jetpack-lazy-images-load' );
		} );
	;
function wpa_wcpb_add_to_cart( obj ) {
	if ( obj.hasClass( 'wc-variation-selection-needed' ) ) {
		jQuery('.wpa-error').css('opacity', '1');
		return;
	}
	jQuery('.wpa-error').css('opacity', '0');
	var parent 						= obj.parent(),
		list_product_id 			= '',
		variable 					= obj.attr('variation_id'),
		bundle_variable 			= {};
		bundle_variable_json		= '',
		loader 						= jQuery('.wpa-wcpb-list .loader');
	loader.css('display', 'inline-block');	
	
	jQuery( '.list-select .item', parent ).each(function(){
		var checked = jQuery( 'input[type="checkbox"]:checked', jQuery(this) ).length;
		if ( checked ) {
			list_product_id += jQuery(this).attr('data-product-id') + ',';
		}
	});

	// Get Custom Variable of Bundle
	jQuery('.plt-variations-form').each(function(){
		var bundle_id 				= jQuery(this).data('product_id'),
			bundle_item_vari 		= {},
			check_default 			= jQuery( 'select option:selected', this ).data( 'default' ),
			bundle_price 			= jQuery( 'select option:selected', this ).val(),
			bundle_variation 		= jQuery( 'select option:selected', this ).html();
		if ( check_default != '1' ) {
			bundle_item_vari['price'] = bundle_price;
			bundle_item_vari['variable'] = bundle_variation;
			bundle_variable[bundle_id] = bundle_item_vari;
		}
	});
	bundle_variable_json = JSON.parse(JSON.stringify(bundle_variable));

	if ( list_product_id ) {
		jQuery.ajax( {
			type : "POST",
			url  : wpa_wcpb.ajaxurl,
			data : {
				action : 'wpa_wcpb_add_to_cart',
				list_product_id : list_product_id,
				variable: variable,
				bundle_variable: bundle_variable_json,
				_nonce : wpa_wcpb._nonce,
			},
			success : function( response ) {
				wpa_wcpb_toggleClass_loading('done', loader[0]);
				jQuery('.wpa-message').css('opacity', '1');
				// Update mini cart
				if ( jQuery('.widget_shopping_cart_content').length ){
					jQuery.post(
						wpa_wcpb.ajaxurl,
						{'action': 'wpa_wcpb_update_mini_cart'},
						function(response) {
							jQuery('.widget_shopping_cart_content').html(response);
						}
					);
				}
			}
		} );
	}
}

function wpa_wcpb_toggleClass_loading(toggleClassName, target) {
	var currentClassName = ' '+target.className+' ';
	if(~currentClassName.indexOf(' '+toggleClassName+' ')) {
		target.className = currentClassName.replace(' '+toggleClassName+' ', ' ').trim();
	} else {
		target.className = (currentClassName+' done').trim();
	}
}

function wpa_wcpb_onchange_input_check_total_discount(){
	var total_price = 0,
		wpa_wcpb_list = jQuery('.wpa-wcpb-list'),
		product_bundles = jQuery('.px-product-bundles'),
		input_checked_lenght = jQuery('.px-product-bundles input[type=checkbox]:checked').length,
		product_bundle_data = product_bundles.attr('data-total-discount')
		product_bundle_data_arr = product_bundle_data.split(','),
		bundle_percent = product_bundle_data_arr[input_checked_lenght - 1],
		currencySymbol = '<span class="woocommerce-Price-currencySymbol">' + jQuery('.total.price .current-price span.woocommerce-Price-amount .woocommerce-Price-currencySymbol', wpa_wcpb_list).html() + '</span>';
	if ( ! bundle_percent ) {
		for ( var i = product_bundle_data_arr.length - 1; i >= 0; i-- ) {
			if ( product_bundle_data_arr[i] ) {
				bundle_percent = product_bundle_data_arr[i];
				break;
			}
		};
	}
	
	jQuery('.px-product-bundles input[type=checkbox]:checked').each(function(){
		var parent = jQuery(this).parent().parent(),
			price = parent.attr('data-item-price'),
			new_price = parseFloat( price ) - parseFloat( price ) * parseFloat( bundle_percent ) / 100;
		jQuery('.price > span.woocommerce-Price-amount', parent).html(
			'<span class="woocommerce-Price-currencySymbol">' + jQuery('.price > span.woocommerce-Price-amount .woocommerce-Price-currencySymbol', parent).html() + '</span>' + new_price.toFixed(2)
		);
		total_price += parseFloat( price );
	});
	
	jQuery('.total.price .current-price span.woocommerce-Price-amount', wpa_wcpb_list).html(
		currencySymbol + (parseFloat( total_price ) - parseFloat( total_price ) * parseFloat( bundle_percent ) / 100 ).toFixed(2)
	);
	jQuery('.total.price .old-price span.woocommerce-Price-amount', wpa_wcpb_list).html(
		currencySymbol + total_price.toFixed(2)
	);
	jQuery('.total.price .save-price span.woocommerce-Price-amount', wpa_wcpb_list).html(
		currencySymbol + Math.round( parseFloat( total_price ) - ( parseFloat( total_price ) - parseFloat( total_price ) * parseFloat( bundle_percent ) / 100 ) ).toFixed(2)
	);
	jQuery('.total.price .save-percent', wpa_wcpb_list).html(
		bundle_percent
	);
}

function wpa_wcpb_onchange_input_check_discount_per_item(){
	var total_price = 0,
		bundles_price = 0,
		wpa_wcpb_list = jQuery('.wpa-wcpb-list'),
		product_bundles = jQuery('.px-product-bundles'),
		currencySymbol = '<span class="woocommerce-Price-currencySymbol">' + jQuery('.total.price .current-price span.woocommerce-Price-amount .woocommerce-Price-currencySymbol', wpa_wcpb_list).html() + '</span>';
	
	jQuery('.px-product-bundles input[type=checkbox]:checked').each(function(){
		var parent = jQuery(this).parent().parent(),
			item_price = parent.attr('data-item-price');
			item_percent = ( parent.attr('data-item-percent') ) ? parent.attr('data-item-percent') : 0,
			new_item_price = parseFloat( item_price ) - parseFloat( item_price ) * parseFloat( item_percent ) / 100;
		bundles_price += parseFloat( new_item_price ); 
		total_price += parseFloat( item_price );
	});

	jQuery('.total.price .current-price span.woocommerce-Price-amount', wpa_wcpb_list).html(
		currencySymbol + bundles_price
	);
	jQuery('.total.price .old-price span.woocommerce-Price-amount', wpa_wcpb_list).html(
		currencySymbol + total_price
	);
	jQuery('.total.price .save-price span.woocommerce-Price-amount', wpa_wcpb_list).html(
		currencySymbol + ( total_price - bundles_price )
	);
	jQuery('.total.price .save-percent', wpa_wcpb_list).html(
		parseFloat( 100 - ( bundles_price / total_price * 100 ) )
	);
}


;(function ( $, window, document, undefined ) {
	"use strict";

	$.PLT = $.PLT || {};

	// ======================================================
	// Main Product Variation Select
	// ------------------------------------------------------
	$.PLT.main_product_variation_select = function () {
		if ( !$( '.variations_form' ).length ) return;
		$( '.variations_form .variations select' ).each(function() {
			$(this).blur(function(){
				if ( $( '.variations_form .single_add_to_cart_button' ).hasClass( 'disabled' ) ) {
					$( '.wpa_wcpb_add_to_cart' ).addClass( 'wc-variation-selection-needed' );
					$( '.wpa_wcpb_add_to_cart' ).addClass( 'disabled' );
				}else {
					$( '.wpa_wcpb_add_to_cart' ).removeClass( 'wc-variation-selection-needed' );
					$( '.wpa_wcpb_add_to_cart' ).removeClass( 'disabled' );
				}
				var newattr = $(this).attr( 'name' ),
					newattrval = $(this).val(),
					variation_id = $( 'input[name="variation_id"], input.variation_id', $( '.variations_form' ) ).val();
				$( '.wpa_wcpb_add_to_cart' ).attr( newattr, newattrval );
				$( '.wpa_wcpb_add_to_cart' ).attr( 'variation_id', variation_id );
			});
		});
	}

	// ======================================================
	// Main Product Variation onload Default
	// ------------------------------------------------------
	$.PLT.main_product_variation_load_default = function () {
		if ( !$( '.variations_form' ).length ) return;
		if ( $( '.variations_form .single_add_to_cart_button' ).hasClass( 'disabled' ) ) {
			$( '.wpa_wcpb_add_to_cart' ).addClass( 'wc-variation-selection-needed' );
			$( '.wpa_wcpb_add_to_cart' ).addClass( 'disabled' );
		}else {
			$( '.wpa_wcpb_add_to_cart' ).removeClass( 'wc-variation-selection-needed' );
			$( '.wpa_wcpb_add_to_cart' ).removeClass( 'disabled' );
		}
		var variation_id = $( 'input[name="variation_id"], input.variation_id', $( '.variations_form' ) ).val();
		$( '.wpa_wcpb_add_to_cart' ).attr( 'variation_id', variation_id );
	}

	// ======================================================
	// Product Bundle Variation Select
	// ------------------------------------------------------
	$.PLT.product_bundle_variation_select = function () {
		if ( !$( '.plt-variations-form select' ).length ) return;
		$( '.plt-variations-form select' ).each(function() {
			$(this).change(function(){
				var parent = $(this).parent().parent(),
					bundle_price = $(this).val(),
					wpa_wcpb_list = jQuery('.wpa-wcpb-list'),
					currencySymbol = '<span class="woocommerce-Price-currencySymbol">' + jQuery('.total.price .current-price span.woocommerce-Price-amount .woocommerce-Price-currencySymbol', wpa_wcpb_list).html() + '</span>';;
				parent.attr('data-item-price', bundle_price);
				$('.info-item .price del .woocommerce-Price-amount', parent).html( currencySymbol + Math.round( parseFloat( bundle_price ) ).toFixed(2) );
				wpa_wcpb_onchange_input_check_total_discount();
			});


		});
	}
	
	$( document ).ready( function() {
		$(window).load(function() { 
			$.PLT.main_product_variation_select();
			$.PLT.main_product_variation_load_default();
			$.PLT.product_bundle_variation_select();
		});
	} );
})( jQuery, window, document );




;
;(function ($, window, document, undefined) {
	"use strict";

	var _galleries;
	var _form_data;
	// ======================================================
	// Get images of each attribute
	// ------------------------------------------------------
	function wpa_wcvs_get_images( id ) {
		var galleryKey = '', selectedAttributes = {}, usedImages = [];

		// Get selected size and color
		$( '#product-' + id + ' .swatch select' ).each(function() {
			if ($( this ).parent().parent().hasClass( 'is-color' ) ) {
				galleryKey = '_product_image_gallery_' + $( this ).data( 'attribute_name' ).replace( 'attribute_', '' ) + '-' + $( this ).val();
			}

			selectedAttributes[$( this ).data( 'attribute_name' ).replace( 'attribute_', '' )] = $( this ).val();
		});

		var images;

		var html = '<div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 0; transition: opacity .25s ease-in-out;">';
		html += '<div class="woocommerce-product-gallery__wrapper">';

		if ( typeof( _galleries[galleryKey]) !== "undefined" && _galleries[galleryKey] !== null ) {
			images = _galleries[galleryKey];

			$.each( images, function ( index, image ) {
				if ( image['single'] == undefined ) {
					var img_single = image['thumbnail'];
				} else {
					var img_single = image['single']; 
				}
				
				if ( $.inArray( img_single, usedImages ) === -1 ) {
					html += '<div data-thumb="' + img_single + '" class="woocommerce-product-gallery__image">';
					html += '<a href="' + image['data-src'] + '">';
					html += '<img width="' + image['data-large_image_width'] + '" height="' + image['data-large_image_width'] + '" src="' + image['data-large_image'] + '" class="attachment-shop_single size-shop_single" alt="" title="" data-src="' + image['data-src'] + '" data-large_image="' + image['data-large_image'] + '" data-large_image_width="' + image['data-large_image_width'] + '" data-large_image_height="' + image['data-large_image_height'] + '"/>';
					html += '</a></div>';
					usedImages.push( img_single );
				}
			});
		} else {
			images = _galleries['default_gallery'];

			// Get variation image
			$.each( _form_data, function ( index, variation ) {
				if ( Object.keys(variation['attributes'] ).length === Object.keys( selectedAttributes ).length ) {
					// Flag to check right or wrong variation
					var chooseThisVariation = true;

					$.each( selectedAttributes, function ( attrName, attrValue ) {
						if ( variation['attributes']['attribute_' + attrName + ''] !== '' && variation['attributes']['attribute_' + attrName + ''] !== attrValue) {
							chooseThisVariation = false;
						}
					});
					
					if ( chooseThisVariation ) {
						var image = variation['image'];
						if ( $.inArray( image['thumb_src'], usedImages ) === -1 ) {
							html += '<div data-thumb="' + image['thumb_src'] + '" class="woocommerce-product-gallery__image">';
							html += '<a href="' + image['full_src'] + '">';
							html += '<img width="' + image['src_w'] + '" height="' + image['src_h'] + '" src="' + image['src'] + '" class="attachment-shop_single size-shop_single" alt="" title="" data-src="' + image['full_src'] + '" data-large_image="' + image['src'] + '" data-large_image_width="' + image['src_w'] + '" data-large_image_height="' + image['src_h'] + ' srcset="' + image['srcset'] + '"/>';
							html += '</a></div>';
							usedImages.push( image['thumb_src'] );
							return true;
						}
					}
				}
			});
		}

		html += '</div></div>';

		$( document.body ).trigger( 'wpa_wcvs_update_html', {
			'html': html,
			'pid': id
		});
	};

	// ======================================================
	// Update HTML
	// ------------------------------------------------------
	$( document.body ).bind( 'wpa_wcvs_update_html', function ( event, data ) {
		var html = data.html, pid = data.pid;

		$( '#product-' + pid + ' .images' ).replaceWith( html );

		// Storefront theme trigger gallery
		if ($( '.woocommerce-product-gallery' ).length > 0 ) {
			$( '.woocommerce-product-gallery' ).each( function() {
				$( this ).wc_product_gallery();
			});
		}
	});


	// ======================================================
	// Update HTML
	// ------------------------------------------------------
	function wpa_wcvs_update_images() {
		setTimeout( function() {
			$( '.variations_form select[data-attribute_name]' ).trigger( 'change' );
		});
		
		var selected = [];
		$( 'body' )
			.on( 'click touchstart', '.swatch__list--item', function ( e ) {
				 _form     = $( this ).parents( 'form' );
				_galleries = _form.data( 'galleries' );
				_form_data = _form.data( 'product_variations' );
				_pid       = _form.data( 'product_id' );
				

				var _this      = $( this ),
					_select    = _this.parent().next( '.value' ).find( 'select' ),
					_attr      = _this.parent().data( 'attribute' ),
					_variation = _this.data( 'variation' ),
					_is_color_label = $( this ).parents('.is-color').length;

				_select.trigger( 'focusin' );

				// Check if this combination is available
				if ( ! _select.find( 'option[value="' + _variation + '"]' ).length ) {
					_this.siblings( '.swatch__list--item' ).removeClass( 'is-selected' );
					_select.val( '' ).change();
					_form.trigger( 'wpa_wcvs_no_matching_variations', [_this] );
					return;
				}

				if ( selected.indexOf( _attr ) === -1) {
					selected.push( _attr );
				}

				if ( _this.hasClass( 'is-selected' ) ) {
					_select.val( '' );
					_this.removeClass( 'is-selected' );
					delete selected[selected.indexOf( _attr )];
					_select.change();
				} else {
					_this.addClass( 'is-selected' ).siblings( '.is-selected' ).removeClass( 'is-selected' );
					_select.val( _variation );
					_select.change();
				}
				if (_is_color_label > 0) {
					wpa_wcvs_get_images( _pid );	
				}
			})
			.on( 'click', '.reset_variations', function() {
				$( this ).closest( '.variations_form' ).find( '.swatch__list--item.is-selected' ).removeClass( 'is-selected' );

				selected = [];
			})
			.on( 'wpa_wcvs_no_matching_variations', function() {
				window.alert( wc_add_to_cart_variation_params.i18n_no_matching_variations_text );
			});

		// Update image gallery for default value
		if ( $( '.swatch__list--item' ).hasClass( 'is-selected' ) ) {
			var _this = $( '.swatch__list--item.is-selected' ),
				_form = $( '.variations_form' ),
				_pid = _form.data( 'product_id' ),
				_is_color_label = $( this ).parents('.is-color').length;
			if (_is_color_label > 0) {
				wpa_wcvs_get_images( _pid );
			}
		}

		$( '.variations_form' ).on( 'change', 'select[data-attribute_name]', function() {
			// Refresh options available.
			$( '.swatch__list[data-attribute]' ).each( function( i, e ) {
				( function( e ) {
					setTimeout( function() {
						var option = $( e ).attr( 'data-attribute' ), select = $( 'select#' + option );

						$( e ).children().each( function( i2, e2 ) {
							if ( ! select.children( '[value="' + $( e2 ).attr( 'data-variation' ) + '"]' ).length ) {
								$( e2 ).addClass( 'disabled' );
							} else {
								$( e2 ).removeClass( 'disabled' );
							}
						} );
					}, 50 );
				} )( e );
			} );
		});
	}

	// ======================================================
	// Update images on product list
	// ------------------------------------------------------
	function wpa_wcvs_change_images() {
		$( 'body' ).on( 'click touchstart', 'div.swatch__list span', function() {
			var _src      = $( this ).data( 'thumb' ),
				_src_flip = $( this ).data( 'thumb-flip' ),
				_img      = $( this ).closest( '.product' ).find( '.wp-post-image' ),
				_img_flip = $( this ).closest( '.product' ).find( '.wp-image-flip' );
			
			_img.attr( 'src', _src );
			_img.attr( 'srcset', _src );
			_img_flip.attr( 'src', _src_flip );
			_img_flip.attr( 'srcset', _src_flip );
		});
	}

	$( document ).ready( function() {
		_galleries = $( '.variations_form' ).data( 'galleries' );
		_form_data = $( '.variations_form' ).data( 'product_variations' );

		wpa_wcvs_update_images();
		wpa_wcvs_change_images();
	});
})(jQuery, window, document);;
/* ------------------------------------------------------------------------
	Class: prettyPhoto
	Use: Lightbox clone for jQuery
	Author: Stephane Caron (http://www.no-margin-for-errors.com)
	Version: 3.1.6
------------------------------------------------------------------------- */
(function($) {
	$.prettyPhoto = {version: '3.1.6'};

	$.fn.prettyPhoto = function(pp_settings) {
		pp_settings = jQuery.extend({
			hook: 'rel', /* the attribute tag to use for prettyPhoto hooks. default: 'rel'. For HTML5, use "data-rel" or similar. */
			animation_speed: 'fast', /* fast/slow/normal */
			ajaxcallback: function() {},
			slideshow: 5000, /* false OR interval time in ms */
			autoplay_slideshow: false, /* true/false */
			opacity: 0.80, /* Value between 0 and 1 */
			show_title: true, /* true/false */
			allow_resize: true, /* Resize the photos bigger than viewport. true/false */
			allow_expand: true, /* Allow the user to expand a resized image. true/false */
			default_width: 500,
			default_height: 344,
			counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
			theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			horizontal_padding: 20, /* The padding on each side of the picture */
			hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
			wmode: 'opaque', /* Set the flash wmode attribute */
			autoplay: true, /* Automatically start videos: True/False */
			modal: false, /* If set to true, only the close button will close the window */
			deeplinking: true, /* Allow prettyPhoto to update the url to enable deeplinking. */
			overlay_gallery: true, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
			overlay_gallery_max: 30, /* Maximum number of pictures in the overlay gallery */
			keyboard_shortcuts: true, /* Set to false if you open forms inside prettyPhoto */
			changepicturecallback: function(){}, /* Called everytime an item is shown/changed */
			callback: function(){}, /* Called when prettyPhoto is closed */
			ie6_fallback: true,
			markup: '<div class="pp_pic_holder"> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_top"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details"> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous">Previous</a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next">Next</a> \
											</div> \
											<p class="pp_description"></p> \
											<div class="pp_social">{pp_social}</div> \
											<a class="pp_close" href="#">Close</a> \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>',
			gallery_markup: '<div class="pp_gallery"> \
								<a href="#" class="pp_arrow_previous">Previous</a> \
								<div> \
									<ul> \
										{gallery} \
									</ul> \
								</div> \
								<a href="#" class="pp_arrow_next">Next</a> \
							</div>',
			image_markup: '<img id="fullResImage" src="{path}" />',
			flash_markup: '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="{width}" height="{height}"><param name="wmode" value="{wmode}" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{path}" /><embed src="{path}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="{width}" height="{height}" wmode="{wmode}"></embed></object>',
			quicktime_markup: '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="https://www.apple.com/qtactivex/qtplugin.cab" height="{height}" width="{width}"><param name="src" value="{path}"><param name="autoplay" value="{autoplay}"><param name="type" value="video/quicktime"><embed src="{path}" height="{height}" width="{width}" autoplay="{autoplay}" type="video/quicktime" pluginspage="https://www.apple.com/quicktime/download/"></embed></object>',
			iframe_markup: '<iframe src ="{path}" width="{width}" height="{height}" frameborder="no"></iframe>',
			inline_markup: '<div class="pp_inline">{content}</div>',
			custom_markup: '',
			social_tools: '<div class="twitter"><a href="//twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script></div><div class="facebook"><iframe src="//www.facebook.com/plugins/like.php?locale=en_US&href={location_href}&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:23px;" allowTransparency="true"></iframe></div>' /* html or false to disable */
		}, pp_settings);

		// Global variables accessible only by prettyPhoto
		var matchedObjects = this, percentBased = false, pp_dimensions, pp_open,

		// prettyPhoto container specific
		pp_contentHeight, pp_contentWidth, pp_containerHeight, pp_containerWidth,

		// Window size
		windowHeight = $(window).height(), windowWidth = $(window).width(),

		// Global elements
		pp_slideshow;

		doresize = true, scroll_pos = _get_scroll();

		// Window/Keyboard events
		$(window).unbind('resize.prettyphoto').bind('resize.prettyphoto',function(){ _center_overlay(); _resize_overlay(); });

		if(pp_settings.keyboard_shortcuts) {
			$(document).unbind('keydown.prettyphoto').bind('keydown.prettyphoto',function(e){
				if(typeof $pp_pic_holder != 'undefined'){
					if($pp_pic_holder.is(':visible')){
						switch(e.keyCode){
							case 37:
								$.prettyPhoto.changePage('previous');
								e.preventDefault();
								break;
							case 39:
								$.prettyPhoto.changePage('next');
								e.preventDefault();
								break;
							case 27:
								if(!settings.modal)
								$.prettyPhoto.close();
								e.preventDefault();
								break;
						};
						// return false;
					};
				};
			});
		};

		/**
		* Initialize prettyPhoto.
		*/
		$.prettyPhoto.initialize = function() {

			settings = pp_settings;

			if(settings.theme == 'pp_default') settings.horizontal_padding = 16;

			// Find out if the picture is part of a set
			theRel = $(this).attr(settings.hook);
			galleryRegExp = /\[(?:.*)\]/;
			isSet = (galleryRegExp.exec(theRel)) ? true : false;

			// Put the SRCs, TITLEs, ALTs into an array.
			pp_images = (isSet) ? jQuery.map(matchedObjects, function(n, i){ if($(n).attr(settings.hook).indexOf(theRel) != -1) return $(n).attr('href'); }) : $.makeArray($(this).attr('href'));
			pp_titles = (isSet) ? jQuery.map(matchedObjects, function(n, i){ if($(n).attr(settings.hook).indexOf(theRel) != -1) return ($(n).find('img').attr('alt')) ? $(n).find('img').attr('alt') : ""; }) : $.makeArray($(this).find('img').attr('alt'));
			pp_descriptions = (isSet) ? jQuery.map(matchedObjects, function(n, i){ if($(n).attr(settings.hook).indexOf(theRel) != -1) return ($(n).attr('title')) ? $(n).attr('title') : ""; }) : $.makeArray($(this).attr('title'));

			if(pp_images.length > settings.overlay_gallery_max) settings.overlay_gallery = false;

			set_position = jQuery.inArray($(this).attr('href'), pp_images); // Define where in the array the clicked item is positionned
			rel_index = (isSet) ? set_position : $("a["+settings.hook+"^='"+theRel+"']").index($(this));

			_build_overlay(this); // Build the overlay {this} being the caller

			if(settings.allow_resize)
				$(window).bind('scroll.prettyphoto',function(){ _center_overlay(); });


			$.prettyPhoto.open();

			return false;
		}


		/**
		* Opens the prettyPhoto modal box.
		* @param image {String,Array} Full path to the image to be open, can also be an array containing full images paths.
		* @param title {String,Array} The title to be displayed with the picture, can also be an array containing all the titles.
		* @param description {String,Array} The description to be displayed with the picture, can also be an array containing all the descriptions.
		*/
		$.prettyPhoto.open = function(event) {
			if(typeof settings == "undefined"){ // Means it's an API call, need to manually get the settings and set the variables
				settings = pp_settings;
				pp_images = $.makeArray(arguments[0]);
				pp_titles = (arguments[1]) ? $.makeArray(arguments[1]) : $.makeArray("");
				pp_descriptions = (arguments[2]) ? $.makeArray(arguments[2]) : $.makeArray("");
				isSet = (pp_images.length > 1) ? true : false;
				set_position = (arguments[3])? arguments[3]: 0;
				_build_overlay(event.target); // Build the overlay {this} being the caller
			}

			if(settings.hideflash) $('object,embed,iframe[src*=youtube],iframe[src*=vimeo]').css('visibility','hidden'); // Hide the flash

			_checkPosition($(pp_images).length); // Hide the next/previous links if on first or last images.

			$('.pp_loaderIcon').show();

			if(settings.deeplinking)
				setHashtag();

			// Rebuild Facebook Like Button with updated href
			if(settings.social_tools){
				facebook_like_link = settings.social_tools.replace('{location_href}', encodeURIComponent(location.href));
				$pp_pic_holder.find('.pp_social').html(facebook_like_link);
			}

			// Fade the content in
			if($ppt.is(':hidden')) $ppt.css('opacity',0).show();
			$pp_overlay.show().fadeTo(settings.animation_speed,settings.opacity);

			// Display the current position
			$pp_pic_holder.find('.currentTextHolder').text((set_position+1) + settings.counter_separator_label + $(pp_images).length);

			// Set the description
			if(typeof pp_descriptions[set_position] != 'undefined' && pp_descriptions[set_position] != ""){
				$pp_pic_holder.find('.pp_description').show().html(unescape(pp_descriptions[set_position]));
			}else{
				$pp_pic_holder.find('.pp_description').hide();
			}

			// Get the dimensions
			movie_width = ( parseFloat(getParam('width',pp_images[set_position])) ) ? getParam('width',pp_images[set_position]) : settings.default_width.toString();
			movie_height = ( parseFloat(getParam('height',pp_images[set_position])) ) ? getParam('height',pp_images[set_position]) : settings.default_height.toString();

			// If the size is % based, calculate according to window dimensions
			percentBased=false;
			if(movie_height.indexOf('%') != -1) { movie_height = parseFloat(($(window).height() * parseFloat(movie_height) / 100) - 150); percentBased = true; }
			if(movie_width.indexOf('%') != -1) { movie_width = parseFloat(($(window).width() * parseFloat(movie_width) / 100) - 150); percentBased = true; }

			// Fade the holder
			$pp_pic_holder.fadeIn(function(){
				// Set the title
				(settings.show_title && pp_titles[set_position] != "" && typeof pp_titles[set_position] != "undefined") ? $ppt.html(unescape(pp_titles[set_position])) : $ppt.html('&nbsp;');

				imgPreloader = "";
				skipInjection = false;

				// Inject the proper content
				switch(_getFileType(pp_images[set_position])){
					case 'image':
						imgPreloader = new Image();

						// Preload the neighbour images
						nextImage = new Image();
						if(isSet && set_position < $(pp_images).length -1) nextImage.src = pp_images[set_position + 1];
						prevImage = new Image();
						if(isSet && pp_images[set_position - 1]) prevImage.src = pp_images[set_position - 1];

						$pp_pic_holder.find('#pp_full_res')[0].innerHTML = settings.image_markup.replace(/{path}/g,pp_images[set_position]);

						imgPreloader.onload = function(){
							// Fit item to viewport
							pp_dimensions = _fitToViewport(imgPreloader.width,imgPreloader.height);

							_showContent();
						};

						imgPreloader.onerror = function(){
							alert('Image cannot be loaded. Make sure the path is correct and image exist.');
							$.prettyPhoto.close();
						};

						imgPreloader.src = pp_images[set_position];
					break;

					case 'youtube':
						pp_dimensions = _fitToViewport(movie_width,movie_height); // Fit item to viewport

						// Regular youtube link
						movie_id = getParam('v',pp_images[set_position]);

						// youtu.be link
						if(movie_id == ""){
							movie_id = pp_images[set_position].split('youtu.be/');
							movie_id = movie_id[1];
							if(movie_id.indexOf('?') > 0)
								movie_id = movie_id.substr(0,movie_id.indexOf('?')); // Strip anything after the ?

							if(movie_id.indexOf('&') > 0)
								movie_id = movie_id.substr(0,movie_id.indexOf('&')); // Strip anything after the &
						}

						movie = '//www.youtube.com/embed/'+movie_id;
						(getParam('rel',pp_images[set_position])) ? movie+="?rel="+getParam('rel',pp_images[set_position]) : movie+="?rel=1";

						if(settings.autoplay) movie += "&autoplay=1";

						toInject = settings.iframe_markup.replace(/{width}/g,pp_dimensions['width']).replace(/{height}/g,pp_dimensions['height']).replace(/{wmode}/g,settings.wmode).replace(/{path}/g,movie);
					break;

					case 'vimeo':
						pp_dimensions = _fitToViewport(movie_width,movie_height); // Fit item to viewport

						movie_id = pp_images[set_position];
						var regExp = /http(s?):\/\/(www\.)?vimeo.com\/(\d+)/;
						var match = movie_id.match(regExp);

						movie = '//player.vimeo.com/video/'+ match[3] +'?title=0&amp;byline=0&amp;portrait=0';
						if(settings.autoplay) movie += "&autoplay=1;";

						vimeo_width = pp_dimensions['width'] + '/embed/?moog_width='+ pp_dimensions['width'];

						toInject = settings.iframe_markup.replace(/{width}/g,vimeo_width).replace(/{height}/g,pp_dimensions['height']).replace(/{path}/g,movie);
					break;

					case 'quicktime':
						pp_dimensions = _fitToViewport(movie_width,movie_height); // Fit item to viewport
						pp_dimensions['height']+=15; pp_dimensions['contentHeight']+=15; pp_dimensions['containerHeight']+=15; // Add space for the control bar

						toInject = settings.quicktime_markup.replace(/{width}/g,pp_dimensions['width']).replace(/{height}/g,pp_dimensions['height']).replace(/{wmode}/g,settings.wmode).replace(/{path}/g,pp_images[set_position]).replace(/{autoplay}/g,settings.autoplay);
					break;

					case 'flash':
						pp_dimensions = _fitToViewport(movie_width,movie_height); // Fit item to viewport

						flash_vars = pp_images[set_position];
						flash_vars = flash_vars.substring(pp_images[set_position].indexOf('flashvars') + 10,pp_images[set_position].length);

						filename = pp_images[set_position];
						filename = filename.substring(0,filename.indexOf('?'));

						toInject =  settings.flash_markup.replace(/{width}/g,pp_dimensions['width']).replace(/{height}/g,pp_dimensions['height']).replace(/{wmode}/g,settings.wmode).replace(/{path}/g,filename+'?'+flash_vars);
					break;

					case 'iframe':
						pp_dimensions = _fitToViewport(movie_width,movie_height); // Fit item to viewport

						frame_url = pp_images[set_position];
						frame_url = frame_url.substr(0,frame_url.indexOf('iframe')-1);

						toInject = settings.iframe_markup.replace(/{width}/g,pp_dimensions['width']).replace(/{height}/g,pp_dimensions['height']).replace(/{path}/g,frame_url);
					break;

					case 'ajax':
						doresize = false; // Make sure the dimensions are not resized.
						pp_dimensions = _fitToViewport(movie_width,movie_height);
						doresize = true; // Reset the dimensions

						skipInjection = true;
						$.get(pp_images[set_position],function(responseHTML){
							toInject = settings.inline_markup.replace(/{content}/g,responseHTML);
							$pp_pic_holder.find('#pp_full_res')[0].innerHTML = toInject;
							_showContent();
						});

					break;

					case 'custom':
						pp_dimensions = _fitToViewport(movie_width,movie_height); // Fit item to viewport

						toInject = settings.custom_markup;
					break;

					case 'inline':
						// to get the item height clone it, apply default width, wrap it in the prettyPhoto containers , then delete
						myClone = $(pp_images[set_position]).clone().append('<br clear="all" />').css({'width':settings.default_width}).wrapInner('<div id="pp_full_res"><div class="pp_inline"></div></div>').appendTo($('body')).show();
						doresize = false; // Make sure the dimensions are not resized.
						pp_dimensions = _fitToViewport($(myClone).width(),$(myClone).height());
						doresize = true; // Reset the dimensions
						$(myClone).remove();
						toInject = settings.inline_markup.replace(/{content}/g,$(pp_images[set_position]).html());
					break;
				};

				if(!imgPreloader && !skipInjection){
					$pp_pic_holder.find('#pp_full_res')[0].innerHTML = toInject;

					// Show content
					_showContent();
				};
			});

			return false;
		};


		/**
		* Change page in the prettyPhoto modal box
		* @param direction {String} Direction of the paging, previous or next.
		*/
		$.prettyPhoto.changePage = function(direction){
			currentGalleryPage = 0;

			if(direction == 'previous') {
				set_position--;
				if (set_position < 0) set_position = $(pp_images).length-1;
			}else if(direction == 'next'){
				set_position++;
				if(set_position > $(pp_images).length-1) set_position = 0;
			}else{
				set_position=direction;
			};

			rel_index = set_position;

			if(!doresize) doresize = true; // Allow the resizing of the images
			if(settings.allow_expand) {
				$('.pp_contract').removeClass('pp_contract').addClass('pp_expand');
			}

			_hideContent(function(){ $.prettyPhoto.open(); });
		};


		/**
		* Change gallery page in the prettyPhoto modal box
		* @param direction {String} Direction of the paging, previous or next.
		*/
		$.prettyPhoto.changeGalleryPage = function(direction){
			if(direction=='next'){
				currentGalleryPage ++;

				if(currentGalleryPage > totalPage) currentGalleryPage = 0;
			}else if(direction=='previous'){
				currentGalleryPage --;

				if(currentGalleryPage < 0) currentGalleryPage = totalPage;
			}else{
				currentGalleryPage = direction;
			};

			slide_speed = (direction == 'next' || direction == 'previous') ? settings.animation_speed : 0;

			slide_to = currentGalleryPage * (itemsPerPage * itemWidth);

			$pp_gallery.find('ul').animate({left:-slide_to},slide_speed);
		};


		/**
		* Start the slideshow...
		*/
		$.prettyPhoto.startSlideshow = function(){
			if(typeof pp_slideshow == 'undefined'){
				$pp_pic_holder.find('.pp_play').unbind('click').removeClass('pp_play').addClass('pp_pause').click(function(){
					$.prettyPhoto.stopSlideshow();
					return false;
				});
				pp_slideshow = setInterval($.prettyPhoto.startSlideshow,settings.slideshow);
			}else{
				$.prettyPhoto.changePage('next');
			};
		}


		/**
		* Stop the slideshow...
		*/
		$.prettyPhoto.stopSlideshow = function(){
			$pp_pic_holder.find('.pp_pause').unbind('click').removeClass('pp_pause').addClass('pp_play').click(function(){
				$.prettyPhoto.startSlideshow();
				return false;
			});
			clearInterval(pp_slideshow);
			pp_slideshow=undefined;
		}


		/**
		* Closes prettyPhoto.
		*/
		$.prettyPhoto.close = function(){
			if($pp_overlay.is(":animated")) return;

			$.prettyPhoto.stopSlideshow();

			$pp_pic_holder.stop().find('object,embed').css('visibility','hidden');

			$('div.pp_pic_holder,div.ppt,.pp_fade').fadeOut(settings.animation_speed,function(){ $(this).remove(); });

			$pp_overlay.fadeOut(settings.animation_speed, function(){

				if(settings.hideflash) $('object,embed,iframe[src*=youtube],iframe[src*=vimeo]').css('visibility','visible'); // Show the flash

				$(this).remove(); // No more need for the prettyPhoto markup

				$(window).unbind('scroll.prettyphoto');

				clearHashtag();

				settings.callback();

				doresize = true;

				pp_open = false;

				delete settings;
			});
		};

		/**
		* Set the proper sizes on the containers and animate the content in.
		*/
		function _showContent(){
			$('.pp_loaderIcon').hide();

			// Calculate the opened top position of the pic holder
			projectedTop = scroll_pos['scrollTop'] + ((windowHeight/2) - (pp_dimensions['containerHeight']/2));
			if(projectedTop < 0) projectedTop = 0;

			$ppt.fadeTo(settings.animation_speed,1);

			// Resize the content holder
			$pp_pic_holder.find('.pp_content')
				.animate({
					height:pp_dimensions['contentHeight'],
					width:pp_dimensions['contentWidth']
				},settings.animation_speed);

			// Resize picture the holder
			$pp_pic_holder.animate({
				'top': projectedTop,
				'left': ((windowWidth/2) - (pp_dimensions['containerWidth']/2) < 0) ? 0 : (windowWidth/2) - (pp_dimensions['containerWidth']/2),
				width:pp_dimensions['containerWidth']
			},settings.animation_speed,function(){
				$pp_pic_holder.find('.pp_hoverContainer,#fullResImage').height(pp_dimensions['height']).width(pp_dimensions['width']);

				$pp_pic_holder.find('.pp_fade').fadeIn(settings.animation_speed); // Fade the new content

				// Show the nav
				if(isSet && _getFileType(pp_images[set_position])=="image") { $pp_pic_holder.find('.pp_hoverContainer').show(); }else{ $pp_pic_holder.find('.pp_hoverContainer').hide(); }

				if(settings.allow_expand) {
					if(pp_dimensions['resized']){ // Fade the resizing link if the image is resized
						$('a.pp_expand,a.pp_contract').show();
					}else{
						$('a.pp_expand').hide();
					}
				}

				if(settings.autoplay_slideshow && !pp_slideshow && !pp_open) $.prettyPhoto.startSlideshow();

				settings.changepicturecallback(); // Callback!

				pp_open = true;
			});

			_insert_gallery();
			pp_settings.ajaxcallback();
		};

		/**
		* Hide the content...DUH!
		*/
		function _hideContent(callback){
			// Fade out the current picture
			$pp_pic_holder.find('#pp_full_res object,#pp_full_res embed').css('visibility','hidden');
			$pp_pic_holder.find('.pp_fade').fadeOut(settings.animation_speed,function(){
				$('.pp_loaderIcon').show();

				callback();
			});
		};

		/**
		* Check the item position in the gallery array, hide or show the navigation links
		* @param setCount {integer} The total number of items in the set
		*/
		function _checkPosition(setCount){
			(setCount > 1) ? $('.pp_nav').show() : $('.pp_nav').hide(); // Hide the bottom nav if it's not a set.
		};

		/**
		* Resize the item dimensions if it's bigger than the viewport
		* @param width {integer} Width of the item to be opened
		* @param height {integer} Height of the item to be opened
		* @return An array containin the "fitted" dimensions
		*/
		function _fitToViewport(width,height){
			resized = false;

			_getDimensions(width,height);

			// Define them in case there's no resize needed
			imageWidth = width, imageHeight = height;

			if( ((pp_containerWidth > windowWidth) || (pp_containerHeight > windowHeight)) && doresize && settings.allow_resize && !percentBased) {
				resized = true, fitting = false;

				while (!fitting){
					if((pp_containerWidth > windowWidth)){
						imageWidth = (windowWidth - 200);
						imageHeight = (height/width) * imageWidth;
					}else if((pp_containerHeight > windowHeight)){
						imageHeight = (windowHeight - 200);
						imageWidth = (width/height) * imageHeight;
					}else{
						fitting = true;
					};

					pp_containerHeight = imageHeight, pp_containerWidth = imageWidth;
				};



				if((pp_containerWidth > windowWidth) || (pp_containerHeight > windowHeight)){
					_fitToViewport(pp_containerWidth,pp_containerHeight)
				};

				_getDimensions(imageWidth,imageHeight);
			};

			return {
				width:Math.floor(imageWidth),
				height:Math.floor(imageHeight),
				containerHeight:Math.floor(pp_containerHeight),
				containerWidth:Math.floor(pp_containerWidth) + (settings.horizontal_padding * 2),
				contentHeight:Math.floor(pp_contentHeight),
				contentWidth:Math.floor(pp_contentWidth),
				resized:resized
			};
		};

		/**
		* Get the containers dimensions according to the item size
		* @param width {integer} Width of the item to be opened
		* @param height {integer} Height of the item to be opened
		*/
		function _getDimensions(width,height){
			width = parseFloat(width);
			height = parseFloat(height);

			// Get the details height, to do so, I need to clone it since it's invisible
			$pp_details = $pp_pic_holder.find('.pp_details');
			$pp_details.width(width);
			detailsHeight = parseFloat($pp_details.css('marginTop')) + parseFloat($pp_details.css('marginBottom'));

			$pp_details = $pp_details.clone().addClass(settings.theme).width(width).appendTo($('body')).css({
				'position':'absolute',
				'top':-10000
			});
			detailsHeight += $pp_details.height();
			detailsHeight = (detailsHeight <= 34) ? 36 : detailsHeight; // Min-height for the details
			$pp_details.remove();

			// Get the titles height, to do so, I need to clone it since it's invisible
			$pp_title = $pp_pic_holder.find('.ppt');
			$pp_title.width(width);
			titleHeight = parseFloat($pp_title.css('marginTop')) + parseFloat($pp_title.css('marginBottom'));
			$pp_title = $pp_title.clone().appendTo($('body')).css({
				'position':'absolute',
				'top':-10000
			});
			titleHeight += $pp_title.height();
			$pp_title.remove();

			// Get the container size, to resize the holder to the right dimensions
			pp_contentHeight = height + detailsHeight;
			pp_contentWidth = width;
			pp_containerHeight = pp_contentHeight + titleHeight + $pp_pic_holder.find('.pp_top').height() + $pp_pic_holder.find('.pp_bottom').height();
			pp_containerWidth = width;
		}

		function _getFileType(itemSrc){
			if (itemSrc.match(/youtube\.com\/watch/i) || itemSrc.match(/youtu\.be/i)) {
				return 'youtube';
			}else if (itemSrc.match(/vimeo\.com/i)) {
				return 'vimeo';
			}else if(itemSrc.match(/\b.mov\b/i)){
				return 'quicktime';
			}else if(itemSrc.match(/\b.swf\b/i)){
				return 'flash';
			}else if(itemSrc.match(/\biframe=true\b/i)){
				return 'iframe';
			}else if(itemSrc.match(/\bajax=true\b/i)){
				return 'ajax';
			}else if(itemSrc.match(/\bcustom=true\b/i)){
				return 'custom';
			}else if(itemSrc.substr(0,1) == '#'){
				return 'inline';
			}else{
				return 'image';
			};
		};

		function _center_overlay(){
			if(doresize && typeof $pp_pic_holder != 'undefined') {
				scroll_pos = _get_scroll();
				contentHeight = $pp_pic_holder.height(), contentwidth = $pp_pic_holder.width();

				projectedTop = (windowHeight/2) + scroll_pos['scrollTop'] - (contentHeight/2);
				if(projectedTop < 0) projectedTop = 0;

				if(contentHeight > windowHeight)
					return;

				$pp_pic_holder.css({
					'top': projectedTop,
					'left': (windowWidth/2) + scroll_pos['scrollLeft'] - (contentwidth/2)
				});
			};
		};

		function _get_scroll(){
			if (self.pageYOffset) {
				return {scrollTop:self.pageYOffset,scrollLeft:self.pageXOffset};
			} else if (document.documentElement && document.documentElement.scrollTop) { // Explorer 6 Strict
				return {scrollTop:document.documentElement.scrollTop,scrollLeft:document.documentElement.scrollLeft};
			} else if (document.body) {// all other Explorers
				return {scrollTop:document.body.scrollTop,scrollLeft:document.body.scrollLeft};
			};
		};

		function _resize_overlay() {
			windowHeight = $(window).height(), windowWidth = $(window).width();

			if(typeof $pp_overlay != "undefined") $pp_overlay.height($(document).height()).width(windowWidth);
		};

		function _insert_gallery(){
			if(isSet && settings.overlay_gallery && _getFileType(pp_images[set_position])=="image") {
				itemWidth = 52+5; // 52 beign the thumb width, 5 being the right margin.
				navWidth = (settings.theme == "facebook" || settings.theme == "pp_default") ? 50 : 30; // Define the arrow width depending on the theme

				itemsPerPage = Math.floor((pp_dimensions['containerWidth'] - 100 - navWidth) / itemWidth);
				itemsPerPage = (itemsPerPage < pp_images.length) ? itemsPerPage : pp_images.length;
				totalPage = Math.ceil(pp_images.length / itemsPerPage) - 1;

				// Hide the nav in the case there's no need for links
				if(totalPage == 0){
					navWidth = 0; // No nav means no width!
					$pp_gallery.find('.pp_arrow_next,.pp_arrow_previous').hide();
				}else{
					$pp_gallery.find('.pp_arrow_next,.pp_arrow_previous').show();
				};

				galleryWidth = itemsPerPage * itemWidth;
				fullGalleryWidth = pp_images.length * itemWidth;

				// Set the proper width to the gallery items
				$pp_gallery
					.css('margin-left',-((galleryWidth/2) + (navWidth/2)))
					.find('div:first').width(galleryWidth+5)
					.find('ul').width(fullGalleryWidth)
					.find('li.selected').removeClass('selected');

				goToPage = (Math.floor(set_position/itemsPerPage) < totalPage) ? Math.floor(set_position/itemsPerPage) : totalPage;

				$.prettyPhoto.changeGalleryPage(goToPage);

				$pp_gallery_li.filter(':eq('+set_position+')').addClass('selected');
			}else{
				$pp_pic_holder.find('.pp_content').unbind('mouseenter mouseleave');
				// $pp_gallery.hide();
			}
		}

		function _build_overlay(caller){
			// Inject Social Tool markup into General markup
			if(settings.social_tools)
				facebook_like_link = settings.social_tools.replace('{location_href}', encodeURIComponent(location.href));

			settings.markup = settings.markup.replace('{pp_social}','');

			$('body').append(settings.markup); // Inject the markup

			$pp_pic_holder = $('.pp_pic_holder') , $ppt = $('.ppt'), $pp_overlay = $('div.pp_overlay'); // Set my global selectors

			// Inject the inline gallery!
			if(isSet && settings.overlay_gallery) {
				currentGalleryPage = 0;
				toInject = "";
				for (var i=0; i < pp_images.length; i++) {
					if(!pp_images[i].match(/\b(jpg|jpeg|png|gif)\b/gi)){
						classname = 'default';
						img_src = '';
					}else{
						classname = '';
						img_src = pp_images[i];
					}
					toInject += "<li class='"+classname+"'><a href='#'><img src='" + img_src + "' width='50' alt='' /></a></li>";
				};

				toInject = settings.gallery_markup.replace(/{gallery}/g,toInject);

				$pp_pic_holder.find('#pp_full_res').after(toInject);

				$pp_gallery = $('.pp_pic_holder .pp_gallery'), $pp_gallery_li = $pp_gallery.find('li'); // Set the gallery selectors

				$pp_gallery.find('.pp_arrow_next').click(function(){
					$.prettyPhoto.changeGalleryPage('next');
					$.prettyPhoto.stopSlideshow();
					return false;
				});

				$pp_gallery.find('.pp_arrow_previous').click(function(){
					$.prettyPhoto.changeGalleryPage('previous');
					$.prettyPhoto.stopSlideshow();
					return false;
				});

				$pp_pic_holder.find('.pp_content').hover(
					function(){
						$pp_pic_holder.find('.pp_gallery:not(.disabled)').fadeIn();
					},
					function(){
						$pp_pic_holder.find('.pp_gallery:not(.disabled)').fadeOut();
					});

				itemWidth = 52+5; // 52 beign the thumb width, 5 being the right margin.
				$pp_gallery_li.each(function(i){
					$(this)
						.find('a')
						.click(function(){
							$.prettyPhoto.changePage(i);
							$.prettyPhoto.stopSlideshow();
							return false;
						});
				});
			};


			// Inject the play/pause if it's a slideshow
			if(settings.slideshow){
				$pp_pic_holder.find('.pp_nav').prepend('<a href="#" class="pp_play">Play</a>')
				$pp_pic_holder.find('.pp_nav .pp_play').click(function(){
					$.prettyPhoto.startSlideshow();
					return false;
				});
			}

			$pp_pic_holder.attr('class','pp_pic_holder ' + settings.theme); // Set the proper theme

			$pp_overlay
				.css({
					'opacity':0,
					'height':$(document).height(),
					'width':$(window).width()
					})
				.bind('click',function(){
					if(!settings.modal) $.prettyPhoto.close();
				});

			$('a.pp_close').bind('click',function(){ $.prettyPhoto.close(); return false; });


			if(settings.allow_expand) {
				$('a.pp_expand').bind('click',function(e){
					// Expand the image
					if($(this).hasClass('pp_expand')){
						$(this).removeClass('pp_expand').addClass('pp_contract');
						doresize = false;
					}else{
						$(this).removeClass('pp_contract').addClass('pp_expand');
						doresize = true;
					};

					_hideContent(function(){ $.prettyPhoto.open(); });

					return false;
				});
			}

			$pp_pic_holder.find('.pp_previous, .pp_nav .pp_arrow_previous').bind('click',function(){
				$.prettyPhoto.changePage('previous');
				$.prettyPhoto.stopSlideshow();
				return false;
			});

			$pp_pic_holder.find('.pp_next, .pp_nav .pp_arrow_next').bind('click',function(){
				$.prettyPhoto.changePage('next');
				$.prettyPhoto.stopSlideshow();
				return false;
			});

			_center_overlay(); // Center it
		};

		if(!pp_alreadyInitialized && getHashtag()){
			pp_alreadyInitialized = true;

			// Grab the rel index to trigger the click on the correct element
			hashIndex = getHashtag();
			hashRel = hashIndex;
			hashIndex = hashIndex.substring(hashIndex.indexOf('/')+1,hashIndex.length-1);
			hashRel = hashRel.substring(0,hashRel.indexOf('/'));

			// Little timeout to make sure all the prettyPhoto initialize scripts has been run.
			// Useful in the event the page contain several init scripts.
			setTimeout(function(){ $("a["+pp_settings.hook+"^='"+hashRel+"']:eq("+hashIndex+")").trigger('click'); },50);
		}

		return this.unbind('click.prettyphoto').bind('click.prettyphoto',$.prettyPhoto.initialize); // Return the jQuery object for chaining. The unbind method is used to avoid click conflict when the plugin is called more than once
	};

	function getHashtag(){
		var url = location.href;
		hashtag = (url.indexOf('#prettyPhoto') !== -1) ? decodeURI(url.substring(url.indexOf('#prettyPhoto')+1,url.length)) : false;
		if(hashtag){  hashtag = hashtag.replace(/<|>/g,''); }
		return hashtag;
	};

	function setHashtag(){
		if(typeof theRel == 'undefined') return; // theRel is set on normal calls, it's impossible to deeplink using the API
		location.hash = theRel + '/'+rel_index+'/';
	};

	function clearHashtag(){
		if ( location.href.indexOf('#prettyPhoto') !== -1 ) location.hash = "prettyPhoto";
	}

	function getParam(name,url){
	  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( url );
	  return ( results == null ) ? "" : results[1];
	}

})(jQuery);

var pp_alreadyInitialized = false; // Used for the deep linking to make sure not to call the same function several times.
;
/**
 * jQuery SelectBox
 *
 * v1.2.0
 * github.com/marcj/jquery-selectBox
 */
(function(a){var b=this.SelectBox=function(c,d){if(c instanceof jQuery){if(c.length>0){c=c[0]}else{return}}this.typeTimer=null;this.typeSearch="";this.isMac=navigator.platform.match(/mac/i);d="object"===typeof d?d:{};this.selectElement=c;if(!d.mobile&&navigator.userAgent.match(/iPad|iPhone|Android|IEMobile|BlackBerry/i)){return false}if("select"!==c.tagName.toLowerCase()){return false}this.init(d)};b.prototype.version="1.2.0";b.prototype.init=function(o){var j=a(this.selectElement);if(j.data("selectBox-control")){return false}var f=a('<a class="selectBox" />'),h=j.attr("multiple")||parseInt(j.attr("size"))>1,d=o||{},c=parseInt(j.prop("tabindex"))||0,m=this;f.width(j.outerWidth()).addClass(j.attr("class")).attr("title",j.attr("title")||"").attr("tabindex",c).css("display","inline-block").bind("focus.selectBox",function(){if(this!==document.activeElement&&document.body!==document.activeElement){a(document.activeElement).blur()}if(f.hasClass("selectBox-active")){return}f.addClass("selectBox-active");j.trigger("focus")}).bind("blur.selectBox",function(){if(!f.hasClass("selectBox-active")){return}f.removeClass("selectBox-active");j.trigger("blur")});if(!a(window).data("selectBox-bindings")){a(window).data("selectBox-bindings",true).bind("scroll.selectBox",this.hideMenus).bind("resize.selectBox",this.hideMenus)}if(j.attr("disabled")){f.addClass("selectBox-disabled")}j.bind("click.selectBox",function(p){f.focus();p.preventDefault()});if(h){o=this.getOptions("inline");f.append(o).data("selectBox-options",o).addClass("selectBox-inline selectBox-menuShowing").bind("keydown.selectBox",function(p){m.handleKeyDown(p)}).bind("keypress.selectBox",function(p){m.handleKeyPress(p)}).bind("mousedown.selectBox",function(p){if(1!==p.which){return}if(a(p.target).is("A.selectBox-inline")){p.preventDefault()}if(!f.hasClass("selectBox-focus")){f.focus()}}).insertAfter(j);if(!j[0].style.height){var n=j.attr("size")?parseInt(j.attr("size")):5;var e=f.clone().removeAttr("id").css({position:"absolute",top:"-9999em"}).show().appendTo("body");e.find(".selectBox-options").html("<li><a>\u00A0</a></li>");var l=parseInt(e.find(".selectBox-options A:first").html("&nbsp;").outerHeight());e.remove();f.height(l*n)}this.disableSelection(f)}else{var i=a('<span class="selectBox-label" />'),k=a('<span class="selectBox-arrow" />');i.attr("class",this.getLabelClass()).text(this.getLabelText());o=this.getOptions("dropdown");o.appendTo("BODY");f.data("selectBox-options",o).addClass("selectBox-dropdown").append(i).append(k).bind("mousedown.selectBox",function(p){if(1===p.which){if(f.hasClass("selectBox-menuShowing")){m.hideMenus()}else{p.stopPropagation();o.data("selectBox-down-at-x",p.screenX).data("selectBox-down-at-y",p.screenY);m.showMenu()}}}).bind("keydown.selectBox",function(p){m.handleKeyDown(p)}).bind("keypress.selectBox",function(p){m.handleKeyPress(p)}).bind("open.selectBox",function(q,p){if(p&&p._selectBox===true){return}m.showMenu()}).bind("close.selectBox",function(q,p){if(p&&p._selectBox===true){return}m.hideMenus()}).insertAfter(j);var g=f.width()-k.outerWidth()-parseInt(i.css("paddingLeft"))||0-parseInt(i.css("paddingRight"))||0;i.width(g);this.disableSelection(f)}j.addClass("selectBox").data("selectBox-control",f).data("selectBox-settings",d).hide()};b.prototype.getOptions=function(j){var f;var c=a(this.selectElement);var e=this;var d=function(i,k){i.children("OPTION, OPTGROUP").each(function(){if(a(this).is("OPTION")){if(a(this).length>0){e.generateOptions(a(this),k)}else{k.append("<li>\u00A0</li>")}}else{var l=a('<li class="selectBox-optgroup" />');l.text(a(this).attr("label"));k.append(l);k=d(a(this),k)}});return k};switch(j){case"inline":f=a('<ul class="selectBox-options" />');f=d(c,f);f.find("A").bind("mouseover.selectBox",function(i){e.addHover(a(this).parent())}).bind("mouseout.selectBox",function(i){e.removeHover(a(this).parent())}).bind("mousedown.selectBox",function(i){if(1!==i.which){return}i.preventDefault();if(!c.selectBox("control").hasClass("selectBox-active")){c.selectBox("control").focus()}}).bind("mouseup.selectBox",function(i){if(1!==i.which){return}e.hideMenus();e.selectOption(a(this).parent(),i)});this.disableSelection(f);return f;case"dropdown":f=a('<ul class="selectBox-dropdown-menu selectBox-options" />');f=d(c,f);f.data("selectBox-select",c).css("display","none").appendTo("BODY").find("A").bind("mousedown.selectBox",function(i){if(i.which===1){i.preventDefault();if(i.screenX===f.data("selectBox-down-at-x")&&i.screenY===f.data("selectBox-down-at-y")){f.removeData("selectBox-down-at-x").removeData("selectBox-down-at-y");e.hideMenus()}}}).bind("mouseup.selectBox",function(i){if(1!==i.which){return}if(i.screenX===f.data("selectBox-down-at-x")&&i.screenY===f.data("selectBox-down-at-y")){return}else{f.removeData("selectBox-down-at-x").removeData("selectBox-down-at-y")}e.selectOption(a(this).parent());e.hideMenus()}).bind("mouseover.selectBox",function(i){e.addHover(a(this).parent())}).bind("mouseout.selectBox",function(i){e.removeHover(a(this).parent())});var h=c.attr("class")||"";if(""!==h){h=h.split(" ");for(var g in h){f.addClass(h[g]+"-selectBox-dropdown-menu")}}this.disableSelection(f);return f}};b.prototype.getLabelClass=function(){var c=a(this.selectElement).find("OPTION:selected");return("selectBox-label "+(c.attr("class")||"")).replace(/\s+$/,"")};b.prototype.getLabelText=function(){var c=a(this.selectElement).find("OPTION:selected");return c.text()||"\u00A0"};b.prototype.setLabel=function(){var c=a(this.selectElement);var d=c.data("selectBox-control");if(!d){return}d.find(".selectBox-label").attr("class",this.getLabelClass()).text(this.getLabelText())};b.prototype.destroy=function(){var c=a(this.selectElement);var e=c.data("selectBox-control");if(!e){return}var d=e.data("selectBox-options");d.remove();e.remove();c.removeClass("selectBox").removeData("selectBox-control").data("selectBox-control",null).removeData("selectBox-settings").data("selectBox-settings",null).show()};b.prototype.refresh=function(){var c=a(this.selectElement),e=c.data("selectBox-control"),f=e.hasClass("selectBox-dropdown"),d=e.hasClass("selectBox-menuShowing");c.selectBox("options",c.html());if(f&&d){this.showMenu()}};b.prototype.showMenu=function(){var e=this,d=a(this.selectElement),j=d.data("selectBox-control"),h=d.data("selectBox-settings"),f=j.data("selectBox-options");if(j.hasClass("selectBox-disabled")){return false}this.hideMenus();var g=parseInt(j.css("borderBottomWidth"))||0;f.width(j.innerWidth()).css({top:j.offset().top+j.outerHeight()-g,left:j.offset().left});if(d.triggerHandler("beforeopen")){return false}var i=function(){d.triggerHandler("open",{_selectBox:true})};switch(h.menuTransition){case"fade":f.fadeIn(h.menuSpeed,i);break;case"slide":f.slideDown(h.menuSpeed,i);break;default:f.show(h.menuSpeed,i);break}if(!h.menuSpeed){i()}var c=f.find(".selectBox-selected:first");this.keepOptionInView(c,true);this.addHover(c);j.addClass("selectBox-menuShowing");a(document).bind("mousedown.selectBox",function(k){if(1===k.which){if(a(k.target).parents().andSelf().hasClass("selectBox-options")){return}e.hideMenus()}})};b.prototype.hideMenus=function(){if(a(".selectBox-dropdown-menu:visible").length===0){return}a(document).unbind("mousedown.selectBox");a(".selectBox-dropdown-menu").each(function(){var d=a(this),c=d.data("selectBox-select"),g=c.data("selectBox-control"),e=c.data("selectBox-settings");if(c.triggerHandler("beforeclose")){return false}var f=function(){c.triggerHandler("close",{_selectBox:true})};if(e){switch(e.menuTransition){case"fade":d.fadeOut(e.menuSpeed,f);break;case"slide":d.slideUp(e.menuSpeed,f);break;default:d.hide(e.menuSpeed,f);break}if(!e.menuSpeed){f()}g.removeClass("selectBox-menuShowing")}else{a(this).hide();a(this).triggerHandler("close",{_selectBox:true});a(this).removeClass("selectBox-menuShowing")}})};b.prototype.selectOption=function(d,j){var c=a(this.selectElement);d=a(d);var k=c.data("selectBox-control"),h=c.data("selectBox-settings");if(k.hasClass("selectBox-disabled")){return false}if(0===d.length||d.hasClass("selectBox-disabled")){return false}if(c.attr("multiple")){if(j.shiftKey&&k.data("selectBox-last-selected")){d.toggleClass("selectBox-selected");var e;if(d.index()>k.data("selectBox-last-selected").index()){e=d.siblings().slice(k.data("selectBox-last-selected").index(),d.index())}else{e=d.siblings().slice(d.index(),k.data("selectBox-last-selected").index())}e=e.not(".selectBox-optgroup, .selectBox-disabled");if(d.hasClass("selectBox-selected")){e.addClass("selectBox-selected")}else{e.removeClass("selectBox-selected")}}else{if((this.isMac&&j.metaKey)||(!this.isMac&&j.ctrlKey)){d.toggleClass("selectBox-selected")}else{d.siblings().removeClass("selectBox-selected");d.addClass("selectBox-selected")}}}else{d.siblings().removeClass("selectBox-selected");d.addClass("selectBox-selected")}if(k.hasClass("selectBox-dropdown")){k.find(".selectBox-label").text(d.text())}var f=0,g=[];if(c.attr("multiple")){k.find(".selectBox-selected A").each(function(){g[f++]=a(this).attr("rel")})}else{g=d.find("A").attr("rel")}k.data("selectBox-last-selected",d);if(c.val()!==g){c.val(g);this.setLabel();c.trigger("change")}return true};b.prototype.addHover=function(d){d=a(d);var c=a(this.selectElement),f=c.data("selectBox-control"),e=f.data("selectBox-options");e.find(".selectBox-hover").removeClass("selectBox-hover");d.addClass("selectBox-hover")};b.prototype.getSelectElement=function(){return this.selectElement};b.prototype.removeHover=function(d){d=a(d);var c=a(this.selectElement),f=c.data("selectBox-control"),e=f.data("selectBox-options");e.find(".selectBox-hover").removeClass("selectBox-hover")};b.prototype.keepOptionInView=function(e,d){if(!e||e.length===0){return}var c=a(this.selectElement),j=c.data("selectBox-control"),g=j.data("selectBox-options"),h=j.hasClass("selectBox-dropdown")?g:g.parent(),i=parseInt(e.offset().top-h.position().top),f=parseInt(i+e.outerHeight());if(d){h.scrollTop(e.offset().top-h.offset().top+h.scrollTop()-(h.height()/2))}else{if(i<0){h.scrollTop(e.offset().top-h.offset().top+h.scrollTop())}if(f>h.height()){h.scrollTop((e.offset().top+e.outerHeight())-h.offset().top+h.scrollTop()-h.height())}}};b.prototype.handleKeyDown=function(c){var k=a(this.selectElement),g=k.data("selectBox-control"),l=g.data("selectBox-options"),e=k.data("selectBox-settings"),f=0,h=0;if(g.hasClass("selectBox-disabled")){return}switch(c.keyCode){case 8:c.preventDefault();this.typeSearch="";break;case 9:case 27:this.hideMenus();this.removeHover();break;case 13:if(g.hasClass("selectBox-menuShowing")){this.selectOption(l.find("LI.selectBox-hover:first"),c);if(g.hasClass("selectBox-dropdown")){this.hideMenus()}}else{this.showMenu()}break;case 38:case 37:c.preventDefault();if(g.hasClass("selectBox-menuShowing")){var d=l.find(".selectBox-hover").prev("LI");f=l.find("LI:not(.selectBox-optgroup)").length;h=0;while(d.length===0||d.hasClass("selectBox-disabled")||d.hasClass("selectBox-optgroup")){d=d.prev("LI");if(d.length===0){if(e.loopOptions){d=l.find("LI:last")}else{d=l.find("LI:first")}}if(++h>=f){break}}this.addHover(d);this.selectOption(d,c);this.keepOptionInView(d)}else{this.showMenu()}break;case 40:case 39:c.preventDefault();if(g.hasClass("selectBox-menuShowing")){var j=l.find(".selectBox-hover").next("LI");f=l.find("LI:not(.selectBox-optgroup)").length;h=0;while(0===j.length||j.hasClass("selectBox-disabled")||j.hasClass("selectBox-optgroup")){j=j.next("LI");if(j.length===0){if(e.loopOptions){j=l.find("LI:first")}else{j=l.find("LI:last")}}if(++h>=f){break}}this.addHover(j);this.selectOption(j,c);this.keepOptionInView(j)}else{this.showMenu()}break}};b.prototype.handleKeyPress=function(e){var c=a(this.selectElement),f=c.data("selectBox-control"),d=f.data("selectBox-options");if(f.hasClass("selectBox-disabled")){return}switch(e.keyCode){case 9:case 27:case 13:case 38:case 37:case 40:case 39:break;default:if(!f.hasClass("selectBox-menuShowing")){this.showMenu()}e.preventDefault();clearTimeout(this.typeTimer);this.typeSearch+=String.fromCharCode(e.charCode||e.keyCode);d.find("A").each(function(){if(a(this).text().substr(0,this.typeSearch.length).toLowerCase()===this.typeSearch.toLowerCase()){this.addHover(a(this).parent());this.selectOption(a(this).parent(),e);this.keepOptionInView(a(this).parent());return false}});this.typeTimer=setTimeout(function(){this.typeSearch=""},1000);break}};b.prototype.enable=function(){var c=a(this.selectElement);c.prop("disabled",false);var d=c.data("selectBox-control");if(!d){return}d.removeClass("selectBox-disabled")};b.prototype.disable=function(){var c=a(this.selectElement);c.prop("disabled",true);var d=c.data("selectBox-control");if(!d){return}d.addClass("selectBox-disabled")};b.prototype.setValue=function(f){var c=a(this.selectElement);c.val(f);f=c.val();if(null===f){f=c.children().first().val();c.val(f)}var g=c.data("selectBox-control");if(!g){return}var e=c.data("selectBox-settings"),d=g.data("selectBox-options");this.setLabel();d.find(".selectBox-selected").removeClass("selectBox-selected");d.find("A").each(function(){if(typeof(f)==="object"){for(var h=0;h<f.length;h++){if(a(this).attr("rel")==f[h]){a(this).parent().addClass("selectBox-selected")}}}else{if(a(this).attr("rel")==f){a(this).parent().addClass("selectBox-selected")}}});if(e.change){e.change.call(c)}};b.prototype.setOptions=function(m){var l=a(this.selectElement),f=l.data("selectBox-control"),d=l.data("selectBox-settings"),k;switch(typeof(m)){case"string":l.html(m);break;case"object":l.html("");for(var g in m){if(m[g]===null){continue}if(typeof(m[g])==="object"){var c=a('<optgroup label="'+g+'" />');for(var e in m[g]){c.append('<option value="'+e+'">'+m[g][e]+"</option>")}l.append(c)}else{var h=a('<option value="'+g+'">'+m[g]+"</option>");l.append(h)}}break}if(!f){return}f.data("selectBox-options").remove();k=f.hasClass("selectBox-dropdown")?"dropdown":"inline";m=this.getOptions(k);f.data("selectBox-options",m);switch(k){case"inline":f.append(m);break;case"dropdown":this.setLabel();a("BODY").append(m);break}};b.prototype.disableSelection=function(c){a(c).css("MozUserSelect","none").bind("selectstart",function(d){d.preventDefault()})};b.prototype.generateOptions=function(e,f){var c=a("<li />"),d=a("<a />");c.addClass(e.attr("class"));c.data(e.data());d.attr("rel",e.val()).text(e.text());c.append(d);if(e.attr("disabled")){c.addClass("selectBox-disabled")}if(e.attr("selected")){c.addClass("selectBox-selected")}f.append(c)};a.extend(a.fn,{selectBox:function(e,c){var d;switch(e){case"control":return a(this).data("selectBox-control");case"settings":if(!c){return a(this).data("selectBox-settings")}a(this).each(function(){a(this).data("selectBox-settings",a.extend(true,a(this).data("selectBox-settings"),c))});break;case"options":if(undefined===c){return a(this).data("selectBox-control").data("selectBox-options")}a(this).each(function(){if(d=a(this).data("selectBox")){d.setOptions(c)}});break;case"value":if(undefined===c){return a(this).val()}a(this).each(function(){if(d=a(this).data("selectBox")){d.setValue(c)}});break;case"refresh":a(this).each(function(){if(d=a(this).data("selectBox")){d.refresh()}});break;case"enable":a(this).each(function(){if(d=a(this).data("selectBox")){d.enable(this)}});break;case"disable":a(this).each(function(){if(d=a(this).data("selectBox")){d.disable()}});break;case"destroy":a(this).each(function(){if(d=a(this).data("selectBox")){d.destroy();a(this).data("selectBox",null)}});break;case"instance":return a(this).data("selectBox");default:a(this).each(function(g,f){if(!a(f).data("selectBox")){a(f).data("selectBox",new b(f,e))}});break}return a(this)}})})(jQuery);;
jQuery(document).ready(function(b){function n(){k.off("change");k=b('.wishlist_table tbody input[type="checkbox"]');"undefined"!=typeof b.fn.selectBox&&b("select.selectBox").selectBox();p();l()}function u(){var a=b(".woocommerce-message");0==a.length?b("#yith-wcwl-form").prepend(yith_wcwl_l10n.labels.added_to_cart_message):a.fadeOut(300,function(){b(this).replaceWith(yith_wcwl_l10n.labels.added_to_cart_message).fadeIn()})}function v(a){var c=a.data("product-id"),d=b(".add-to-wishlist-"+c);c={add_to_wishlist:c,
    product_type:a.data("product-type"),action:yith_wcwl_l10n.actions.add_to_wishlist_action};if(yith_wcwl_l10n.multi_wishlist&&yith_wcwl_l10n.is_user_logged_in){var e=a.parents(".yith-wcwl-popup-footer").prev(".yith-wcwl-popup-content"),f=e.find(".wishlist-select"),g=e.find(".wishlist-name");e=e.find(".wishlist-visibility");c.wishlist_id=f.val();c.wishlist_name=g.val();c.wishlist_visibility=e.val()}r()?b.ajax({type:"POST",url:yith_wcwl_l10n.ajax_url,data:c,dataType:"json",beforeSend:function(){a.siblings(".ajax-loading").css("visibility",
        "visible")},complete:function(){a.siblings(".ajax-loading").css("visibility","hidden")},success:function(c){var e=b("#yith-wcwl-popup-message"),f=c.result,g=c.message;if(yith_wcwl_l10n.multi_wishlist&&yith_wcwl_l10n.is_user_logged_in){var h=b("select.wishlist-select");yith_wcwl_l10n.multi_wishlist&&"undefined"!=typeof b.prettyPhoto&&"undefined"!=typeof b.prettyPhoto.close&&b.prettyPhoto.close();h.each(function(a){a=b(this);var d=a.find("option");d=d.slice(1,d.length-1);d.remove();if("undefined"!=
        typeof c.user_wishlists)for(d in d=0,c.user_wishlists)"1"!=c.user_wishlists[d].is_default&&b("<option>").val(c.user_wishlists[d].ID).html(c.user_wishlists[d].wishlist_name).insertBefore(a.find("option:last-child"))})}b("#yith-wcwl-message").html(g);e.css("margin-left","-"+b(e).width()+"px").fadeIn();window.setTimeout(function(){e.fadeOut()},2E3);"true"==f?((!yith_wcwl_l10n.multi_wishlist||!yith_wcwl_l10n.is_user_logged_in||yith_wcwl_l10n.multi_wishlist&&yith_wcwl_l10n.is_user_logged_in&&yith_wcwl_l10n.hide_add_button)&&
    d.find(".yith-wcwl-add-button").hide().removeClass("show").addClass("hide"),d.find(".yith-wcwl-wishlistexistsbrowse").hide().removeClass("show").addClass("hide").find("a").attr("href",c.wishlist_url),d.find(".yith-wcwl-wishlistaddedbrowse").show().removeClass("hide").addClass("show").find("a").attr("href",c.wishlist_url)):"exists"==f?((!yith_wcwl_l10n.multi_wishlist||!yith_wcwl_l10n.is_user_logged_in||yith_wcwl_l10n.multi_wishlist&&yith_wcwl_l10n.is_user_logged_in&&yith_wcwl_l10n.hide_add_button)&&
    d.find(".yith-wcwl-add-button").hide().removeClass("show").addClass("hide"),d.find(".yith-wcwl-wishlistexistsbrowse").show().removeClass("hide").addClass("show").find("a").attr("href",c.wishlist_url),d.find(".yith-wcwl-wishlistaddedbrowse").hide().removeClass("show").addClass("hide").find("a").attr("href",c.wishlist_url)):(d.find(".yith-wcwl-add-button").show().removeClass("hide").addClass("show"),d.find(".yith-wcwl-wishlistexistsbrowse").hide().removeClass("show").addClass("hide"),d.find(".yith-wcwl-wishlistaddedbrowse").hide().removeClass("show").addClass("hide"));
        b("body").trigger("added_to_wishlist",[a,d])}}):alert(yith_wcwl_l10n.labels.cookie_disabled)}function x(a){var c=a.parents(".cart.wishlist_table"),d=c.data("pagination"),e=c.data("per-page"),f=c.data("page"),g=a.parents("[data-row-id]");c.find(".pagination-row");var h=g.data("row-id"),m=c.data("id"),w=c.data("token");d={action:yith_wcwl_l10n.actions.remove_from_wishlist_action,remove_from_wishlist:h,pagination:d,per_page:e,current_page:f,wishlist_id:m,wishlist_token:w};b("#yith-wcwl-message").html("&nbsp;");
    "undefined"!=typeof b.fn.block&&c.fadeTo("400","0.6").block({message:null,overlayCSS:{background:"transparent url("+yith_wcwl_l10n.ajax_loader_url+") no-repeat center",backgroundSize:"16px 16px",opacity:.6}});b("#yith-wcwl-form").load(yith_wcwl_l10n.ajax_url+" #yith-wcwl-form",d,function(){"undefined"!=typeof b.fn.unblock&&c.stop(!0).css("opacity","1").unblock();n();b("body").trigger("removed_from_wishlist",[a,g])})}function y(a,c){var d=a.data("product-id"),e=b(document).find(".cart.wishlist_table"),
    f=e.data("pagination"),g=e.data("per-page"),h=e.data("id"),m=e.data("token");d={action:yith_wcwl_l10n.actions.reload_wishlist_and_adding_elem_action,pagination:f,per_page:g,wishlist_id:h,wishlist_token:m,add_to_wishlist:d,product_type:a.data("product-type")};r()?b.ajax({type:"POST",url:yith_wcwl_l10n.ajax_url,data:d,dataType:"html",beforeSend:function(){"undefined"!=typeof b.fn.block&&e.fadeTo("400","0.6").block({message:null,overlayCSS:{background:"transparent url("+yith_wcwl_l10n.ajax_loader_url+
            ") no-repeat center",backgroundSize:"16px 16px",opacity:.6}})},success:function(a){a=b(a).find("#yith-wcwl-form");c.replaceWith(a);n()}}):alert(yith_wcwl_l10n.labels.cookie_disabled)}function z(a){var c=a.parents(".cart.wishlist_table"),d=c.data("token"),e=c.data("id"),f=a.parents("[data-row-id]"),g=f.data("row-id"),h=a.val(),m=c.data("pagination"),k=c.data("per-page"),l=c.data("page");d={action:yith_wcwl_l10n.actions.move_to_another_wishlist_action,wishlist_token:d,wishlist_id:e,destination_wishlist_token:h,
    item_id:g,pagination:m,per_page:k,current_page:l};""!=h&&("undefined"!=typeof b.fn.block&&c.fadeTo("400","0.6").block({message:null,overlayCSS:{background:"transparent url("+yith_wcwl_l10n.ajax_loader_url+") no-repeat center",backgroundSize:"16px 16px",opacity:.6}}),b("#yith-wcwl-form").load(yith_wcwl_l10n.ajax_url+" #yith-wcwl-form",d,function(){"undefined"!=typeof b.fn.unblock&&c.stop(!0).css("opacity","1").unblock();n();b("body").trigger("moved_to_another_wishlist",[a,f])}))}function t(a){var c=
    b(this);a.preventDefault();c.parents(".wishlist-title").next().show();c.parents(".wishlist-title").hide()}function A(a){var c=b(this);a.preventDefault();c.parents(".hidden-title-form").hide();c.parents(".hidden-title-form").prev().show()}function r(){if(navigator.cookieEnabled)return!0;document.cookie="cookietest=1";var a=-1!=document.cookie.indexOf("cookietest=");document.cookie="cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT";return a}function B(){if(0!=b(".yith-wcwl-add-to-wishlist").length&&
    0==b("#yith-wcwl-popup-message").length){var a=b("<div>").attr("id","yith-wcwl-message");a=b("<div>").attr("id","yith-wcwl-popup-message").html(a).hide();b("body").prepend(a)}}function p(){k.on("change",function(){var a="",c=b(this).parents(".cart.wishlist_table"),d=c.data("id");c=c.data("token");var e=document.URL;k.filter(":checked").each(function(){var d=b(this);a+=0!=a.length?",":"";a+=d.parents("[data-row-id]").data("row-id")});e=q(e,"wishlist_products_to_add_to_cart",a);e=q(e,"wishlist_token",
    c);e=q(e,"wishlist_id",d);b("#custom_add_to_cart").attr("href",e)})}function l(){"undefined"!=typeof b.prettyPhoto&&b('a[data-rel^="prettyPhoto[add_to_wishlist_"]').add('a[data-rel="prettyPhoto[ask_an_estimate]"]').unbind("click").prettyPhoto({hook:"data-rel",social_tools:!1,theme:"pp_woocommerce",horizontal_padding:20,opacity:.8,deeplinking:!1})}function q(a,b,d){d=b+"="+d;a=a.replace(new RegExp("(&|\\?)"+b+"=[^&]*"),"$1"+d);-1<a.indexOf(b+"=")||(a=-1<a.indexOf("?")?a+("&"+d):a+("?"+d));return a}
    var C="undefined"!==typeof wc_add_to_cart_params&&null!==wc_add_to_cart_params?wc_add_to_cart_params.cart_redirect_after_add:"",k=b('.wishlist_table tbody input[type="checkbox"]:not(:disabled)');b(document).on("yith_wcwl_init",function(){var a=b(this),c=b('.wishlist_table tbody input[type="checkbox"]:not(:disabled)');a.on("click",".add_to_wishlist",function(a){var d=b(this);a.preventDefault();v(d);return!1});a.on("click",".remove_from_wishlist",function(a){var d=b(this);a.preventDefault();x(d);return!1});
        a.on("adding_to_cart","body",function(a,b,c){"undefined"!=typeof b&&"undefined"!=typeof c&&0!=b.closest(".wishlist_table").length&&(c.remove_from_wishlist_after_add_to_cart=b.closest("[data-row-id]").data("row-id"),c.wishlist_id=b.closest(".wishlist_table").data("id"),wc_add_to_cart_params.cart_redirect_after_add=yith_wcwl_l10n.redirect_to_cart)});a.on("added_to_cart","body",function(a){wc_add_to_cart_params.cart_redirect_after_add=C;a=b(".wishlist_table");a.find(".added").removeClass("added");a.find(".added_to_cart").remove()});
        a.on("added_to_cart","body",u);a.on("cart_page_refreshed","body",n);a.on("click",".show-title-form",t);a.on("click",".wishlist-title-with-form h2",t);a.on("click",".hide-title-form",A);a.on("change",".change-wishlist",function(a){a=b(this);z(a);return!1});a.on("change",".yith-wcwl-popup-content .wishlist-select",function(a){a=b(this);"new"==a.val()?a.parents(".yith-wcwl-first-row").next(".yith-wcwl-second-row").css("display","table-row"):a.parents(".yith-wcwl-first-row").next(".yith-wcwl-second-row").hide()});
        a.on("change","#bulk_add_to_cart",function(){b(this).is(":checked")?c.attr("checked","checked").change():c.removeAttr("checked").change()});a.on("click","#custom_add_to_cart",function(a){var d=b(this),f=d.parents(".cart.wishlist_table");yith_wcwl_l10n.ajax_add_to_cart_enabled&&(a.preventDefault(),"undefined"!=typeof b.fn.block&&f.fadeTo("400","0.6").block({message:null,overlayCSS:{background:"transparent url("+yith_wcwl_l10n.ajax_loader_url+") no-repeat center",backgroundSize:"16px 16px",opacity:.6}}),
            b("#yith-wcwl-form").load(yith_wcwl_l10n.ajax_url+d.attr("href")+" #yith-wcwl-form",{action:yith_wcwl_l10n.actions.bulk_add_to_cart_action},function(){"undefined"!=typeof b.fn.unblock&&f.stop(!0).css("opacity","1").unblock();c.off("change");c=b('.wishlist_table tbody input[type="checkbox"]');"undefined"!=typeof b.fn.selectBox&&b("select.selectBox").selectBox();p();l()}))});a.on("click",".yith-wfbt-add-wishlist",function(a){a.preventDefault();a=b(this);var c=b("#yith-wcwl-form");b("html, body").animate({scrollTop:c.offset().top},
            500);y(a,c)});B();p();l()}).trigger("yith_wcwl_init");b(document).on("yith_infs_added_elem",function(){l()});"undefined"!=typeof b.fn.selectBox&&b("select.selectBox").selectBox()});;
/*! Magnific Popup - v1.0.1 - 2015-12-30
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2015 Dmitry Semenov; */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):window.jQuery||window.Zepto)}(function(a){var b,c,d,e,f,g,h="Close",i="BeforeClose",j="AfterClose",k="BeforeAppend",l="MarkupParse",m="Open",n="Change",o="mfp",p="."+o,q="mfp-ready",r="mfp-removing",s="mfp-prevent-close",t=function(){},u=!!window.jQuery,v=a(window),w=function(a,c){b.ev.on(o+a+p,c)},x=function(b,c,d,e){var f=document.createElement("div");return f.className="mfp-"+b,d&&(f.innerHTML=d),e?c&&c.appendChild(f):(f=a(f),c&&f.appendTo(c)),f},y=function(c,d){b.ev.triggerHandler(o+c,d),b.st.callbacks&&(c=c.charAt(0).toLowerCase()+c.slice(1),b.st.callbacks[c]&&b.st.callbacks[c].apply(b,a.isArray(d)?d:[d]))},z=function(c){return c===g&&b.currTemplate.closeBtn||(b.currTemplate.closeBtn=a(b.st.closeMarkup.replace("%title%",b.st.tClose)),g=c),b.currTemplate.closeBtn},A=function(){a.magnificPopup.instance||(b=new t,b.init(),a.magnificPopup.instance=b)},B=function(){var a=document.createElement("p").style,b=["ms","O","Moz","Webkit"];if(void 0!==a.transition)return!0;for(;b.length;)if(b.pop()+"Transition"in a)return!0;return!1};t.prototype={constructor:t,init:function(){var c=navigator.appVersion;b.isIE7=-1!==c.indexOf("MSIE 7."),b.isIE8=-1!==c.indexOf("MSIE 8."),b.isLowIE=b.isIE7||b.isIE8,b.isAndroid=/android/gi.test(c),b.isIOS=/iphone|ipad|ipod/gi.test(c),b.supportsTransition=B(),b.probablyMobile=b.isAndroid||b.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),d=a(document),b.popupsCache={}},open:function(c){var e;if(c.isObj===!1){b.items=c.items.toArray(),b.index=0;var g,h=c.items;for(e=0;e<h.length;e++)if(g=h[e],g.parsed&&(g=g.el[0]),g===c.el[0]){b.index=e;break}}else b.items=a.isArray(c.items)?c.items:[c.items],b.index=c.index||0;if(b.isOpen)return void b.updateItemHTML();b.types=[],f="",c.mainEl&&c.mainEl.length?b.ev=c.mainEl.eq(0):b.ev=d,c.key?(b.popupsCache[c.key]||(b.popupsCache[c.key]={}),b.currTemplate=b.popupsCache[c.key]):b.currTemplate={},b.st=a.extend(!0,{},a.magnificPopup.defaults,c),b.fixedContentPos="auto"===b.st.fixedContentPos?!b.probablyMobile:b.st.fixedContentPos,b.st.modal&&(b.st.closeOnContentClick=!1,b.st.closeOnBgClick=!1,b.st.showCloseBtn=!1,b.st.enableEscapeKey=!1),b.bgOverlay||(b.bgOverlay=x("bg").on("click"+p,function(){b.close()}),b.wrap=x("wrap").attr("tabindex",-1).on("click"+p,function(a){b._checkIfClose(a.target)&&b.close()}),b.container=x("container",b.wrap)),b.contentContainer=x("content"),b.st.preloader&&(b.preloader=x("preloader",b.container,b.st.tLoading));var i=a.magnificPopup.modules;for(e=0;e<i.length;e++){var j=i[e];j=j.charAt(0).toUpperCase()+j.slice(1),b["init"+j].call(b)}y("BeforeOpen"),b.st.showCloseBtn&&(b.st.closeBtnInside?(w(l,function(a,b,c,d){c.close_replaceWith=z(d.type)}),f+=" mfp-close-btn-in"):b.wrap.append(z())),b.st.alignTop&&(f+=" mfp-align-top"),b.fixedContentPos?b.wrap.css({overflow:b.st.overflowY,overflowX:"hidden",overflowY:b.st.overflowY}):b.wrap.css({top:v.scrollTop(),position:"absolute"}),(b.st.fixedBgPos===!1||"auto"===b.st.fixedBgPos&&!b.fixedContentPos)&&b.bgOverlay.css({height:d.height(),position:"absolute"}),b.st.enableEscapeKey&&d.on("keyup"+p,function(a){27===a.keyCode&&b.close()}),v.on("resize"+p,function(){b.updateSize()}),b.st.closeOnContentClick||(f+=" mfp-auto-cursor"),f&&b.wrap.addClass(f);var k=b.wH=v.height(),n={};if(b.fixedContentPos&&b._hasScrollBar(k)){var o=b._getScrollbarSize();o&&(n.marginRight=o)}b.fixedContentPos&&(b.isIE7?a("body, html").css("overflow","hidden"):n.overflow="hidden");var r=b.st.mainClass;return b.isIE7&&(r+=" mfp-ie7"),r&&b._addClassToMFP(r),b.updateItemHTML(),y("BuildControls"),a("html").css(n),b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo||a(document.body)),b._lastFocusedEl=document.activeElement,setTimeout(function(){b.content?(b._addClassToMFP(q),b._setFocus()):b.bgOverlay.addClass(q),d.on("focusin"+p,b._onFocusIn)},16),b.isOpen=!0,b.updateSize(k),y(m),c},close:function(){b.isOpen&&(y(i),b.isOpen=!1,b.st.removalDelay&&!b.isLowIE&&b.supportsTransition?(b._addClassToMFP(r),setTimeout(function(){b._close()},b.st.removalDelay)):b._close())},_close:function(){y(h);var c=r+" "+q+" ";if(b.bgOverlay.detach(),b.wrap.detach(),b.container.empty(),b.st.mainClass&&(c+=b.st.mainClass+" "),b._removeClassFromMFP(c),b.fixedContentPos){var e={marginRight:""};b.isIE7?a("body, html").css("overflow",""):e.overflow="",a("html").css(e)}d.off("keyup"+p+" focusin"+p),b.ev.off(p),b.wrap.attr("class","mfp-wrap").removeAttr("style"),b.bgOverlay.attr("class","mfp-bg"),b.container.attr("class","mfp-container"),!b.st.showCloseBtn||b.st.closeBtnInside&&b.currTemplate[b.currItem.type]!==!0||b.currTemplate.closeBtn&&b.currTemplate.closeBtn.detach(),b.st.autoFocusLast&&b._lastFocusedEl&&a(b._lastFocusedEl).focus(),b.currItem=null,b.content=null,b.currTemplate=null,b.prevHeight=0,y(j)},updateSize:function(a){if(b.isIOS){var c=document.documentElement.clientWidth/window.innerWidth,d=window.innerHeight*c;b.wrap.css("height",d),b.wH=d}else b.wH=a||v.height();b.fixedContentPos||b.wrap.css("height",b.wH),y("Resize")},updateItemHTML:function(){var c=b.items[b.index];b.contentContainer.detach(),b.content&&b.content.detach(),c.parsed||(c=b.parseEl(b.index));var d=c.type;if(y("BeforeChange",[b.currItem?b.currItem.type:"",d]),b.currItem=c,!b.currTemplate[d]){var f=b.st[d]?b.st[d].markup:!1;y("FirstMarkupParse",f),f?b.currTemplate[d]=a(f):b.currTemplate[d]=!0}e&&e!==c.type&&b.container.removeClass("mfp-"+e+"-holder");var g=b["get"+d.charAt(0).toUpperCase()+d.slice(1)](c,b.currTemplate[d]);b.appendContent(g,d),c.preloaded=!0,y(n,c),e=c.type,b.container.prepend(b.contentContainer),y("AfterChange")},appendContent:function(a,c){b.content=a,a?b.st.showCloseBtn&&b.st.closeBtnInside&&b.currTemplate[c]===!0?b.content.find(".mfp-close").length||b.content.append(z()):b.content=a:b.content="",y(k),b.container.addClass("mfp-"+c+"-holder"),b.contentContainer.append(b.content)},parseEl:function(c){var d,e=b.items[c];if(e.tagName?e={el:a(e)}:(d=e.type,e={data:e,src:e.src}),e.el){for(var f=b.types,g=0;g<f.length;g++)if(e.el.hasClass("mfp-"+f[g])){d=f[g];break}e.src=e.el.attr("data-mfp-src"),e.src||(e.src=e.el.attr("href"))}return e.type=d||b.st.type||"inline",e.index=c,e.parsed=!0,b.items[c]=e,y("ElementParse",e),b.items[c]},addGroup:function(a,c){var d=function(d){d.mfpEl=this,b._openClick(d,a,c)};c||(c={});var e="click.magnificPopup";c.mainEl=a,c.items?(c.isObj=!0,a.off(e).on(e,d)):(c.isObj=!1,c.delegate?a.off(e).on(e,c.delegate,d):(c.items=a,a.off(e).on(e,d)))},_openClick:function(c,d,e){var f=void 0!==e.midClick?e.midClick:a.magnificPopup.defaults.midClick;if(f||!(2===c.which||c.ctrlKey||c.metaKey||c.altKey||c.shiftKey)){var g=void 0!==e.disableOn?e.disableOn:a.magnificPopup.defaults.disableOn;if(g)if(a.isFunction(g)){if(!g.call(b))return!0}else if(v.width()<g)return!0;c.type&&(c.preventDefault(),b.isOpen&&c.stopPropagation()),e.el=a(c.mfpEl),e.delegate&&(e.items=d.find(e.delegate)),b.open(e)}},updateStatus:function(a,d){if(b.preloader){c!==a&&b.container.removeClass("mfp-s-"+c),d||"loading"!==a||(d=b.st.tLoading);var e={status:a,text:d};y("UpdateStatus",e),a=e.status,d=e.text,b.preloader.html(d),b.preloader.find("a").on("click",function(a){a.stopImmediatePropagation()}),b.container.addClass("mfp-s-"+a),c=a}},_checkIfClose:function(c){if(!a(c).hasClass(s)){var d=b.st.closeOnContentClick,e=b.st.closeOnBgClick;if(d&&e)return!0;if(!b.content||a(c).hasClass("mfp-close")||b.preloader&&c===b.preloader[0])return!0;if(c===b.content[0]||a.contains(b.content[0],c)){if(d)return!0}else if(e&&a.contains(document,c))return!0;return!1}},_addClassToMFP:function(a){b.bgOverlay.addClass(a),b.wrap.addClass(a)},_removeClassFromMFP:function(a){this.bgOverlay.removeClass(a),b.wrap.removeClass(a)},_hasScrollBar:function(a){return(b.isIE7?d.height():document.body.scrollHeight)>(a||v.height())},_setFocus:function(){(b.st.focus?b.content.find(b.st.focus).eq(0):b.wrap).focus()},_onFocusIn:function(c){return c.target===b.wrap[0]||a.contains(b.wrap[0],c.target)?void 0:(b._setFocus(),!1)},_parseMarkup:function(b,c,d){var e;d.data&&(c=a.extend(d.data,c)),y(l,[b,c,d]),a.each(c,function(a,c){if(void 0===c||c===!1)return!0;if(e=a.split("_"),e.length>1){var d=b.find(p+"-"+e[0]);if(d.length>0){var f=e[1];"replaceWith"===f?d[0]!==c[0]&&d.replaceWith(c):"img"===f?d.is("img")?d.attr("src",c):d.replaceWith('<img src="'+c+'" class="'+d.attr("class")+'" />'):d.attr(e[1],c)}}else b.find(p+"-"+a).html(c)})},_getScrollbarSize:function(){if(void 0===b.scrollbarSize){var a=document.createElement("div");a.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(a),b.scrollbarSize=a.offsetWidth-a.clientWidth,document.body.removeChild(a)}return b.scrollbarSize}},a.magnificPopup={instance:null,proto:t.prototype,modules:[],open:function(b,c){return A(),b=b?a.extend(!0,{},b):{},b.isObj=!0,b.index=c||0,this.instance.open(b)},close:function(){return a.magnificPopup.instance&&a.magnificPopup.instance.close()},registerModule:function(b,c){c.options&&(a.magnificPopup.defaults[b]=c.options),a.extend(this.proto,c.proto),this.modules.push(b)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&#215;</button>',tClose:"Close (Esc)",tLoading:"Loading...",autoFocusLast:!0}},a.fn.magnificPopup=function(c){A();var d=a(this);if("string"==typeof c)if("open"===c){var e,f=u?d.data("magnificPopup"):d[0].magnificPopup,g=parseInt(arguments[1],10)||0;f.items?e=f.items[g]:(e=d,f.delegate&&(e=e.find(f.delegate)),e=e.eq(g)),b._openClick({mfpEl:e},d,f)}else b.isOpen&&b[c].apply(b,Array.prototype.slice.call(arguments,1));else c=a.extend(!0,{},c),u?d.data("magnificPopup",c):d[0].magnificPopup=c,b.addGroup(d,c);return d};var C,D,E,F="inline",G=function(){E&&(D.after(E.addClass(C)).detach(),E=null)};a.magnificPopup.registerModule(F,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){b.types.push(F),w(h+"."+F,function(){G()})},getInline:function(c,d){if(G(),c.src){var e=b.st.inline,f=a(c.src);if(f.length){var g=f[0].parentNode;g&&g.tagName&&(D||(C=e.hiddenClass,D=x(C),C="mfp-"+C),E=f.after(D).detach().removeClass(C)),b.updateStatus("ready")}else b.updateStatus("error",e.tNotFound),f=a("<div>");return c.inlineElement=f,f}return b.updateStatus("ready"),b._parseMarkup(d,{},c),d}}});var H,I="ajax",J=function(){H&&a(document.body).removeClass(H)},K=function(){J(),b.req&&b.req.abort()};a.magnificPopup.registerModule(I,{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){b.types.push(I),H=b.st.ajax.cursor,w(h+"."+I,K),w("BeforeChange."+I,K)},getAjax:function(c){H&&a(document.body).addClass(H),b.updateStatus("loading");var d=a.extend({url:c.src,success:function(d,e,f){var g={data:d,xhr:f};y("ParseAjax",g),b.appendContent(a(g.data),I),c.finished=!0,J(),b._setFocus(),setTimeout(function(){b.wrap.addClass(q)},16),b.updateStatus("ready"),y("AjaxContentAdded")},error:function(){J(),c.finished=c.loadError=!0,b.updateStatus("error",b.st.ajax.tError.replace("%url%",c.src))}},b.st.ajax.settings);return b.req=a.ajax(d),""}}});var L,M=function(c){if(c.data&&void 0!==c.data.title)return c.data.title;var d=b.st.image.titleSrc;if(d){if(a.isFunction(d))return d.call(b,c);if(c.el)return c.el.attr(d)||""}return""};a.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var c=b.st.image,d=".image";b.types.push("image"),w(m+d,function(){"image"===b.currItem.type&&c.cursor&&a(document.body).addClass(c.cursor)}),w(h+d,function(){c.cursor&&a(document.body).removeClass(c.cursor),v.off("resize"+p)}),w("Resize"+d,b.resizeImage),b.isLowIE&&w("AfterChange",b.resizeImage)},resizeImage:function(){var a=b.currItem;if(a&&a.img&&b.st.image.verticalFit){var c=0;b.isLowIE&&(c=parseInt(a.img.css("padding-top"),10)+parseInt(a.img.css("padding-bottom"),10)),a.img.css("max-height",b.wH-c)}},_onImageHasSize:function(a){a.img&&(a.hasSize=!0,L&&clearInterval(L),a.isCheckingImgSize=!1,y("ImageHasSize",a),a.imgHidden&&(b.content&&b.content.removeClass("mfp-loading"),a.imgHidden=!1))},findImageSize:function(a){var c=0,d=a.img[0],e=function(f){L&&clearInterval(L),L=setInterval(function(){return d.naturalWidth>0?void b._onImageHasSize(a):(c>200&&clearInterval(L),c++,void(3===c?e(10):40===c?e(50):100===c&&e(500)))},f)};e(1)},getImage:function(c,d){var e=0,f=function(){c&&(c.img[0].complete?(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("ready")),c.hasSize=!0,c.loaded=!0,y("ImageLoadComplete")):(e++,200>e?setTimeout(f,100):g()))},g=function(){c&&(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("error",h.tError.replace("%url%",c.src))),c.hasSize=!0,c.loaded=!0,c.loadError=!0)},h=b.st.image,i=d.find(".mfp-img");if(i.length){var j=document.createElement("img");j.className="mfp-img",c.el&&c.el.find("img").length&&(j.alt=c.el.find("img").attr("alt")),c.img=a(j).on("load.mfploader",f).on("error.mfploader",g),j.src=c.src,i.is("img")&&(c.img=c.img.clone()),j=c.img[0],j.naturalWidth>0?c.hasSize=!0:j.width||(c.hasSize=!1)}return b._parseMarkup(d,{title:M(c),img_replaceWith:c.img},c),b.resizeImage(),c.hasSize?(L&&clearInterval(L),c.loadError?(d.addClass("mfp-loading"),b.updateStatus("error",h.tError.replace("%url%",c.src))):(d.removeClass("mfp-loading"),b.updateStatus("ready")),d):(b.updateStatus("loading"),c.loading=!0,c.hasSize||(c.imgHidden=!0,d.addClass("mfp-loading"),b.findImageSize(c)),d)}}});var N,O=function(){return void 0===N&&(N=void 0!==document.createElement("p").style.MozTransform),N};a.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(a){return a.is("img")?a:a.find("img")}},proto:{initZoom:function(){var a,c=b.st.zoom,d=".zoom";if(c.enabled&&b.supportsTransition){var e,f,g=c.duration,j=function(a){var b=a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),d="all "+c.duration/1e3+"s "+c.easing,e={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},f="transition";return e["-webkit-"+f]=e["-moz-"+f]=e["-o-"+f]=e[f]=d,b.css(e),b},k=function(){b.content.css("visibility","visible")};w("BuildControls"+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.content.css("visibility","hidden"),a=b._getItemToZoom(),!a)return void k();f=j(a),f.css(b._getOffset()),b.wrap.append(f),e=setTimeout(function(){f.css(b._getOffset(!0)),e=setTimeout(function(){k(),setTimeout(function(){f.remove(),a=f=null,y("ZoomAnimationEnded")},16)},g)},16)}}),w(i+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.st.removalDelay=g,!a){if(a=b._getItemToZoom(),!a)return;f=j(a)}f.css(b._getOffset(!0)),b.wrap.append(f),b.content.css("visibility","hidden"),setTimeout(function(){f.css(b._getOffset())},16)}}),w(h+d,function(){b._allowZoom()&&(k(),f&&f.remove(),a=null)})}},_allowZoom:function(){return"image"===b.currItem.type},_getItemToZoom:function(){return b.currItem.hasSize?b.currItem.img:!1},_getOffset:function(c){var d;d=c?b.currItem.img:b.st.zoom.opener(b.currItem.el||b.currItem);var e=d.offset(),f=parseInt(d.css("padding-top"),10),g=parseInt(d.css("padding-bottom"),10);e.top-=a(window).scrollTop()-f;var h={width:d.width(),height:(u?d.innerHeight():d[0].offsetHeight)-g-f};return O()?h["-moz-transform"]=h.transform="translate("+e.left+"px,"+e.top+"px)":(h.left=e.left,h.top=e.top),h}}});var P="iframe",Q="//about:blank",R=function(a){if(b.currTemplate[P]){var c=b.currTemplate[P].find("iframe");c.length&&(a||(c[0].src=Q),b.isIE8&&c.css("display",a?"block":"none"))}};a.magnificPopup.registerModule(P,{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" cookieconsent="marketing" data-src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){b.types.push(P),w("BeforeChange",function(a,b,c){b!==c&&(b===P?R():c===P&&R(!0))}),w(h+"."+P,function(){R()})},getIframe:function(c,d){var e=c.src,f=b.st.iframe;a.each(f.patterns,function(){return e.indexOf(this.index)>-1?(this.id&&(e="string"==typeof this.id?e.substr(e.lastIndexOf(this.id)+this.id.length,e.length):this.id.call(this,e)),e=this.src.replace("%id%",e),!1):void 0});var g={};return f.srcAction&&(g[f.srcAction]=e),b._parseMarkup(d,g,c),b.updateStatus("ready"),d}}});var S=function(a){var c=b.items.length;return a>c-1?a-c:0>a?c+a:a},T=function(a,b,c){return a.replace(/%curr%/gi,b+1).replace(/%total%/gi,c)};a.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var c=b.st.gallery,e=".mfp-gallery",g=Boolean(a.fn.mfpFastClick);return b.direction=!0,c&&c.enabled?(f+=" mfp-gallery",w(m+e,function(){c.navigateByImgClick&&b.wrap.on("click"+e,".mfp-img",function(){return b.items.length>1?(b.next(),!1):void 0}),d.on("keydown"+e,function(a){37===a.keyCode?b.prev():39===a.keyCode&&b.next()})}),w("UpdateStatus"+e,function(a,c){c.text&&(c.text=T(c.text,b.currItem.index,b.items.length))}),w(l+e,function(a,d,e,f){var g=b.items.length;e.counter=g>1?T(c.tCounter,f.index,g):""}),w("BuildControls"+e,function(){if(b.items.length>1&&c.arrows&&!b.arrowLeft){var d=c.arrowMarkup,e=b.arrowLeft=a(d.replace(/%title%/gi,c.tPrev).replace(/%dir%/gi,"left")).addClass(s),f=b.arrowRight=a(d.replace(/%title%/gi,c.tNext).replace(/%dir%/gi,"right")).addClass(s),h=g?"mfpFastClick":"click";e[h](function(){b.prev()}),f[h](function(){b.next()}),b.isIE7&&(x("b",e[0],!1,!0),x("a",e[0],!1,!0),x("b",f[0],!1,!0),x("a",f[0],!1,!0)),b.container.append(e.add(f))}}),w(n+e,function(){b._preloadTimeout&&clearTimeout(b._preloadTimeout),b._preloadTimeout=setTimeout(function(){b.preloadNearbyImages(),b._preloadTimeout=null},16)}),void w(h+e,function(){d.off(e),b.wrap.off("click"+e),b.arrowLeft&&g&&b.arrowLeft.add(b.arrowRight).destroyMfpFastClick(),b.arrowRight=b.arrowLeft=null})):!1},next:function(){b.direction=!0,b.index=S(b.index+1),b.updateItemHTML()},prev:function(){b.direction=!1,b.index=S(b.index-1),b.updateItemHTML()},goTo:function(a){b.direction=a>=b.index,b.index=a,b.updateItemHTML()},preloadNearbyImages:function(){var a,c=b.st.gallery.preload,d=Math.min(c[0],b.items.length),e=Math.min(c[1],b.items.length);for(a=1;a<=(b.direction?e:d);a++)b._preloadItem(b.index+a);for(a=1;a<=(b.direction?d:e);a++)b._preloadItem(b.index-a)},_preloadItem:function(c){if(c=S(c),!b.items[c].preloaded){var d=b.items[c];d.parsed||(d=b.parseEl(c)),y("LazyLoad",d),"image"===d.type&&(d.img=a('<img class="mfp-img" />').on("load.mfploader",function(){d.hasSize=!0}).on("error.mfploader",function(){d.hasSize=!0,d.loadError=!0,y("LazyLoadError",d)}).attr("src",d.src)),d.preloaded=!0}}}});var U="retina";a.magnificPopup.registerModule(U,{options:{replaceSrc:function(a){return a.src.replace(/\.\w+$/,function(a){return"@2x"+a})},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var a=b.st.retina,c=a.ratio;c=isNaN(c)?c():c,c>1&&(w("ImageHasSize."+U,function(a,b){b.img.css({"max-width":b.img[0].naturalWidth/c,width:"100%"})}),w("ElementParse."+U,function(b,d){d.src=a.replaceSrc(d,c)}))}}}}),function(){var b=1e3,c="ontouchstart"in window,d=function(){v.off("touchmove"+f+" touchend"+f)},e="mfpFastClick",f="."+e;a.fn.mfpFastClick=function(e){return a(this).each(function(){var g,h=a(this);if(c){var i,j,k,l,m,n;h.on("touchstart"+f,function(a){l=!1,n=1,m=a.originalEvent?a.originalEvent.touches[0]:a.touches[0],j=m.clientX,k=m.clientY,v.on("touchmove"+f,function(a){m=a.originalEvent?a.originalEvent.touches:a.touches,n=m.length,m=m[0],(Math.abs(m.clientX-j)>10||Math.abs(m.clientY-k)>10)&&(l=!0,d())}).on("touchend"+f,function(a){d(),l||n>1||(g=!0,a.preventDefault(),clearTimeout(i),i=setTimeout(function(){g=!1},b),e())})})}h.on("click"+f,function(){g||e()})})},a.fn.destroyMfpFastClick=function(){a(this).off("touchstart"+f+" click"+f),c&&v.off("touchmove"+f+" touchend"+f)}}(),A()});;
/*!
 * Isotope PACKAGED v2.0.0
 * Filter & sort magical layouts
 * http://isotope.metafizzy.co
 */

(function(t){function e(){}function i(t){function i(e){e.prototype.option||(e.prototype.option=function(e){t.isPlainObject(e)&&(this.options=t.extend(!0,this.options,e))})}function n(e,i){t.fn[e]=function(n){if("string"==typeof n){for(var s=o.call(arguments,1),a=0,u=this.length;u>a;a++){var p=this[a],h=t.data(p,e);if(h)if(t.isFunction(h[n])&&"_"!==n.charAt(0)){var f=h[n].apply(h,s);if(void 0!==f)return f}else r("no such method '"+n+"' for "+e+" instance");else r("cannot call methods on "+e+" prior to initialization; "+"attempted to call '"+n+"'")}return this}return this.each(function(){var o=t.data(this,e);o?(o.option(n),o._init()):(o=new i(this,n),t.data(this,e,o))})}}if(t){var r="undefined"==typeof console?e:function(t){console.error(t)};return t.bridget=function(t,e){i(e),n(t,e)},t.bridget}}var o=Array.prototype.slice;"function"==typeof define&&define.amd?define("jquery-bridget/jquery.bridget",["jquery"],i):i(t.jQuery)})(window),function(t){function e(e){var i=t.event;return i.target=i.target||i.srcElement||e,i}var i=document.documentElement,o=function(){};i.addEventListener?o=function(t,e,i){t.addEventListener(e,i,!1)}:i.attachEvent&&(o=function(t,i,o){t[i+o]=o.handleEvent?function(){var i=e(t);o.handleEvent.call(o,i)}:function(){var i=e(t);o.call(t,i)},t.attachEvent("on"+i,t[i+o])});var n=function(){};i.removeEventListener?n=function(t,e,i){t.removeEventListener(e,i,!1)}:i.detachEvent&&(n=function(t,e,i){t.detachEvent("on"+e,t[e+i]);try{delete t[e+i]}catch(o){t[e+i]=void 0}});var r={bind:o,unbind:n};"function"==typeof define&&define.amd?define("eventie/eventie",r):"object"==typeof exports?module.exports=r:t.eventie=r}(this),function(t){function e(t){"function"==typeof t&&(e.isReady?t():r.push(t))}function i(t){var i="readystatechange"===t.type&&"complete"!==n.readyState;if(!e.isReady&&!i){e.isReady=!0;for(var o=0,s=r.length;s>o;o++){var a=r[o];a()}}}function o(o){return o.bind(n,"DOMContentLoaded",i),o.bind(n,"readystatechange",i),o.bind(t,"load",i),e}var n=t.document,r=[];e.isReady=!1,"function"==typeof define&&define.amd?(e.isReady="function"==typeof requirejs,define("doc-ready/doc-ready",["eventie/eventie"],o)):t.docReady=o(t.eventie)}(this),function(){function t(){}function e(t,e){for(var i=t.length;i--;)if(t[i].listener===e)return i;return-1}function i(t){return function(){return this[t].apply(this,arguments)}}var o=t.prototype,n=this,r=n.EventEmitter;o.getListeners=function(t){var e,i,o=this._getEvents();if(t instanceof RegExp){e={};for(i in o)o.hasOwnProperty(i)&&t.test(i)&&(e[i]=o[i])}else e=o[t]||(o[t]=[]);return e},o.flattenListeners=function(t){var e,i=[];for(e=0;t.length>e;e+=1)i.push(t[e].listener);return i},o.getListenersAsObject=function(t){var e,i=this.getListeners(t);return i instanceof Array&&(e={},e[t]=i),e||i},o.addListener=function(t,i){var o,n=this.getListenersAsObject(t),r="object"==typeof i;for(o in n)n.hasOwnProperty(o)&&-1===e(n[o],i)&&n[o].push(r?i:{listener:i,once:!1});return this},o.on=i("addListener"),o.addOnceListener=function(t,e){return this.addListener(t,{listener:e,once:!0})},o.once=i("addOnceListener"),o.defineEvent=function(t){return this.getListeners(t),this},o.defineEvents=function(t){for(var e=0;t.length>e;e+=1)this.defineEvent(t[e]);return this},o.removeListener=function(t,i){var o,n,r=this.getListenersAsObject(t);for(n in r)r.hasOwnProperty(n)&&(o=e(r[n],i),-1!==o&&r[n].splice(o,1));return this},o.off=i("removeListener"),o.addListeners=function(t,e){return this.manipulateListeners(!1,t,e)},o.removeListeners=function(t,e){return this.manipulateListeners(!0,t,e)},o.manipulateListeners=function(t,e,i){var o,n,r=t?this.removeListener:this.addListener,s=t?this.removeListeners:this.addListeners;if("object"!=typeof e||e instanceof RegExp)for(o=i.length;o--;)r.call(this,e,i[o]);else for(o in e)e.hasOwnProperty(o)&&(n=e[o])&&("function"==typeof n?r.call(this,o,n):s.call(this,o,n));return this},o.removeEvent=function(t){var e,i=typeof t,o=this._getEvents();if("string"===i)delete o[t];else if(t instanceof RegExp)for(e in o)o.hasOwnProperty(e)&&t.test(e)&&delete o[e];else delete this._events;return this},o.removeAllListeners=i("removeEvent"),o.emitEvent=function(t,e){var i,o,n,r,s=this.getListenersAsObject(t);for(n in s)if(s.hasOwnProperty(n))for(o=s[n].length;o--;)i=s[n][o],i.once===!0&&this.removeListener(t,i.listener),r=i.listener.apply(this,e||[]),r===this._getOnceReturnValue()&&this.removeListener(t,i.listener);return this},o.trigger=i("emitEvent"),o.emit=function(t){var e=Array.prototype.slice.call(arguments,1);return this.emitEvent(t,e)},o.setOnceReturnValue=function(t){return this._onceReturnValue=t,this},o._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},o._getEvents=function(){return this._events||(this._events={})},t.noConflict=function(){return n.EventEmitter=r,t},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return t}):"object"==typeof module&&module.exports?module.exports=t:this.EventEmitter=t}.call(this),function(t){function e(t){if(t){if("string"==typeof o[t])return t;t=t.charAt(0).toUpperCase()+t.slice(1);for(var e,n=0,r=i.length;r>n;n++)if(e=i[n]+t,"string"==typeof o[e])return e}}var i="Webkit Moz ms Ms O".split(" "),o=document.documentElement.style;"function"==typeof define&&define.amd?define("get-style-property/get-style-property",[],function(){return e}):"object"==typeof exports?module.exports=e:t.getStyleProperty=e}(window),function(t){function e(t){var e=parseFloat(t),i=-1===t.indexOf("%")&&!isNaN(e);return i&&e}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0,i=s.length;i>e;e++){var o=s[e];t[o]=0}return t}function o(t){function o(t){if("string"==typeof t&&(t=document.querySelector(t)),t&&"object"==typeof t&&t.nodeType){var o=r(t);if("none"===o.display)return i();var n={};n.width=t.offsetWidth,n.height=t.offsetHeight;for(var h=n.isBorderBox=!(!p||!o[p]||"border-box"!==o[p]),f=0,c=s.length;c>f;f++){var d=s[f],l=o[d];l=a(t,l);var y=parseFloat(l);n[d]=isNaN(y)?0:y}var m=n.paddingLeft+n.paddingRight,g=n.paddingTop+n.paddingBottom,v=n.marginLeft+n.marginRight,_=n.marginTop+n.marginBottom,I=n.borderLeftWidth+n.borderRightWidth,L=n.borderTopWidth+n.borderBottomWidth,z=h&&u,S=e(o.width);S!==!1&&(n.width=S+(z?0:m+I));var b=e(o.height);return b!==!1&&(n.height=b+(z?0:g+L)),n.innerWidth=n.width-(m+I),n.innerHeight=n.height-(g+L),n.outerWidth=n.width+v,n.outerHeight=n.height+_,n}}function a(t,e){if(n||-1===e.indexOf("%"))return e;var i=t.style,o=i.left,r=t.runtimeStyle,s=r&&r.left;return s&&(r.left=t.currentStyle.left),i.left=e,e=i.pixelLeft,i.left=o,s&&(r.left=s),e}var u,p=t("boxSizing");return function(){if(p){var t=document.createElement("div");t.style.width="200px",t.style.padding="1px 2px 3px 4px",t.style.borderStyle="solid",t.style.borderWidth="1px 2px 3px 4px",t.style[p]="border-box";var i=document.body||document.documentElement;i.appendChild(t);var o=r(t);u=200===e(o.width),i.removeChild(t)}}(),o}var n=t.getComputedStyle,r=n?function(t){return n(t,null)}:function(t){return t.currentStyle},s=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"];"function"==typeof define&&define.amd?define("get-size/get-size",["get-style-property/get-style-property"],o):"object"==typeof exports?module.exports=o(require("get-style-property")):t.getSize=o(t.getStyleProperty)}(window),function(t,e){function i(t,e){return t[a](e)}function o(t){if(!t.parentNode){var e=document.createDocumentFragment();e.appendChild(t)}}function n(t,e){o(t);for(var i=t.parentNode.querySelectorAll(e),n=0,r=i.length;r>n;n++)if(i[n]===t)return!0;return!1}function r(t,e){return o(t),i(t,e)}var s,a=function(){if(e.matchesSelector)return"matchesSelector";for(var t=["webkit","moz","ms","o"],i=0,o=t.length;o>i;i++){var n=t[i],r=n+"MatchesSelector";if(e[r])return r}}();if(a){var u=document.createElement("div"),p=i(u,"div");s=p?i:r}else s=n;"function"==typeof define&&define.amd?define("matches-selector/matches-selector",[],function(){return s}):window.matchesSelector=s}(this,Element.prototype),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){for(var e in t)return!1;return e=null,!0}function o(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}function n(t,n,r){function a(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}var u=r("transition"),p=r("transform"),h=u&&p,f=!!r("perspective"),c={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"otransitionend",transition:"transitionend"}[u],d=["transform","transition","transitionDuration","transitionProperty"],l=function(){for(var t={},e=0,i=d.length;i>e;e++){var o=d[e],n=r(o);n&&n!==o&&(t[o]=n)}return t}();e(a.prototype,t.prototype),a.prototype._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},a.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},a.prototype.getSize=function(){this.size=n(this.element)},a.prototype.css=function(t){var e=this.element.style;for(var i in t){var o=l[i]||i;e[o]=t[i]}},a.prototype.getPosition=function(){var t=s(this.element),e=this.layout.options,i=e.isOriginLeft,o=e.isOriginTop,n=parseInt(t[i?"left":"right"],10),r=parseInt(t[o?"top":"bottom"],10);n=isNaN(n)?0:n,r=isNaN(r)?0:r;var a=this.layout.size;n-=i?a.paddingLeft:a.paddingRight,r-=o?a.paddingTop:a.paddingBottom,this.position.x=n,this.position.y=r},a.prototype.layoutPosition=function(){var t=this.layout.size,e=this.layout.options,i={};e.isOriginLeft?(i.left=this.position.x+t.paddingLeft+"px",i.right=""):(i.right=this.position.x+t.paddingRight+"px",i.left=""),e.isOriginTop?(i.top=this.position.y+t.paddingTop+"px",i.bottom=""):(i.bottom=this.position.y+t.paddingBottom+"px",i.top=""),this.css(i),this.emitEvent("layout",[this])};var y=f?function(t,e){return"translate3d("+t+"px, "+e+"px, 0)"}:function(t,e){return"translate("+t+"px, "+e+"px)"};a.prototype._transitionTo=function(t,e){this.getPosition();var i=this.position.x,o=this.position.y,n=parseInt(t,10),r=parseInt(e,10),s=n===this.position.x&&r===this.position.y;if(this.setPosition(t,e),s&&!this.isTransitioning)return this.layoutPosition(),void 0;var a=t-i,u=e-o,p={},h=this.layout.options;a=h.isOriginLeft?a:-a,u=h.isOriginTop?u:-u,p.transform=y(a,u),this.transition({to:p,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},a.prototype.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},a.prototype.moveTo=h?a.prototype._transitionTo:a.prototype.goTo,a.prototype.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},a.prototype._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},a.prototype._transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return this._nonTransition(t),void 0;var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var o=this.element.offsetHeight;o=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var m=p&&o(p)+",opacity";a.prototype.enableTransition=function(){this.isTransitioning||(this.css({transitionProperty:m,transitionDuration:this.layout.options.transitionDuration}),this.element.addEventListener(c,this,!1))},a.prototype.transition=a.prototype[u?"_transition":"_nonTransition"],a.prototype.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},a.prototype.onotransitionend=function(t){this.ontransitionend(t)};var g={"-webkit-transform":"transform","-moz-transform":"transform","-o-transform":"transform"};a.prototype.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,o=g[t.propertyName]||t.propertyName;if(delete e.ingProperties[o],i(e.ingProperties)&&this.disableTransition(),o in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[o]),o in e.onEnd){var n=e.onEnd[o];n.call(this),delete e.onEnd[o]}this.emitEvent("transitionEnd",[this])}},a.prototype.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(c,this,!1),this.isTransitioning=!1},a.prototype._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var v={transitionProperty:"",transitionDuration:""};return a.prototype.removeTransitionStyles=function(){this.css(v)},a.prototype.removeElem=function(){this.element.parentNode.removeChild(this.element),this.emitEvent("remove",[this])},a.prototype.remove=function(){if(!u||!parseFloat(this.layout.options.transitionDuration))return this.removeElem(),void 0;var t=this;this.on("transitionEnd",function(){return t.removeElem(),!0}),this.hide()},a.prototype.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options;this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0})},a.prototype.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options;this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:{opacity:function(){this.isHidden&&this.css({display:"none"})}}})},a.prototype.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},a}var r=t.getComputedStyle,s=r?function(t){return r(t,null)}:function(t){return t.currentStyle};"function"==typeof define&&define.amd?define("outlayer/item",["eventEmitter/EventEmitter","get-size/get-size","get-style-property/get-style-property"],n):(t.Outlayer={},t.Outlayer.Item=n(t.EventEmitter,t.getSize,t.getStyleProperty))}(window),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){return"[object Array]"===f.call(t)}function o(t){var e=[];if(i(t))e=t;else if(t&&"number"==typeof t.length)for(var o=0,n=t.length;n>o;o++)e.push(t[o]);else e.push(t);return e}function n(t,e){var i=d(e,t);-1!==i&&e.splice(i,1)}function r(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()}function s(i,s,f,d,l,y){function m(t,i){if("string"==typeof t&&(t=a.querySelector(t)),!t||!c(t))return u&&u.error("Bad "+this.constructor.namespace+" element: "+t),void 0;this.element=t,this.options=e({},this.constructor.defaults),this.option(i);var o=++g;this.element.outlayerGUID=o,v[o]=this,this._create(),this.options.isInitLayout&&this.layout()}var g=0,v={};return m.namespace="outlayer",m.Item=y,m.defaults={containerStyle:{position:"relative"},isInitLayout:!0,isOriginLeft:!0,isOriginTop:!0,isResizeBound:!0,isResizingContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}},e(m.prototype,f.prototype),m.prototype.option=function(t){e(this.options,t)},m.prototype._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),e(this.element.style,this.options.containerStyle),this.options.isResizeBound&&this.bindResize()},m.prototype.reloadItems=function(){this.items=this._itemize(this.element.children)},m.prototype._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,o=[],n=0,r=e.length;r>n;n++){var s=e[n],a=new i(s,this);o.push(a)}return o},m.prototype._filterFindItemElements=function(t){t=o(t);for(var e=this.options.itemSelector,i=[],n=0,r=t.length;r>n;n++){var s=t[n];if(c(s))if(e){l(s,e)&&i.push(s);for(var a=s.querySelectorAll(e),u=0,p=a.length;p>u;u++)i.push(a[u])}else i.push(s)}return i},m.prototype.getItemElements=function(){for(var t=[],e=0,i=this.items.length;i>e;e++)t.push(this.items[e].element);return t},m.prototype.layout=function(){this._resetLayout(),this._manageStamps();var t=void 0!==this.options.isLayoutInstant?this.options.isLayoutInstant:!this._isLayoutInited;this.layoutItems(this.items,t),this._isLayoutInited=!0},m.prototype._init=m.prototype.layout,m.prototype._resetLayout=function(){this.getSize()},m.prototype.getSize=function(){this.size=d(this.element)},m.prototype._getMeasurement=function(t,e){var i,o=this.options[t];o?("string"==typeof o?i=this.element.querySelector(o):c(o)&&(i=o),this[t]=i?d(i)[e]:o):this[t]=0},m.prototype.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},m.prototype._getItemsForLayout=function(t){for(var e=[],i=0,o=t.length;o>i;i++){var n=t[i];n.isIgnored||e.push(n)}return e},m.prototype._layoutItems=function(t,e){function i(){o.emitEvent("layoutComplete",[o,t])}var o=this;if(!t||!t.length)return i(),void 0;this._itemsOn(t,"layout",i);for(var n=[],r=0,s=t.length;s>r;r++){var a=t[r],u=this._getItemLayoutPosition(a);u.item=a,u.isInstant=e||a.isLayoutInstant,n.push(u)}this._processLayoutQueue(n)},m.prototype._getItemLayoutPosition=function(){return{x:0,y:0}},m.prototype._processLayoutQueue=function(t){for(var e=0,i=t.length;i>e;e++){var o=t[e];this._positionItem(o.item,o.x,o.y,o.isInstant)}},m.prototype._positionItem=function(t,e,i,o){o?t.goTo(e,i):t.moveTo(e,i)},m.prototype._postLayout=function(){this.resizeContainer()},m.prototype.resizeContainer=function(){if(this.options.isResizingContainer){var t=this._getContainerSize();t&&(this._setContainerMeasure(t.width,!0),this._setContainerMeasure(t.height,!1))}},m.prototype._getContainerSize=h,m.prototype._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},m.prototype._itemsOn=function(t,e,i){function o(){return n++,n===r&&i.call(s),!0}for(var n=0,r=t.length,s=this,a=0,u=t.length;u>a;a++){var p=t[a];p.on(e,o)}},m.prototype.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},m.prototype.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},m.prototype.stamp=function(t){if(t=this._find(t)){this.stamps=this.stamps.concat(t);for(var e=0,i=t.length;i>e;e++){var o=t[e];this.ignore(o)}}},m.prototype.unstamp=function(t){if(t=this._find(t))for(var e=0,i=t.length;i>e;e++){var o=t[e];n(o,this.stamps),this.unignore(o)}},m.prototype._find=function(t){return t?("string"==typeof t&&(t=this.element.querySelectorAll(t)),t=o(t)):void 0},m.prototype._manageStamps=function(){if(this.stamps&&this.stamps.length){this._getBoundingRect();for(var t=0,e=this.stamps.length;e>t;t++){var i=this.stamps[t];this._manageStamp(i)}}},m.prototype._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},m.prototype._manageStamp=h,m.prototype._getElementOffset=function(t){var e=t.getBoundingClientRect(),i=this._boundingRect,o=d(t),n={left:e.left-i.left-o.marginLeft,top:e.top-i.top-o.marginTop,right:i.right-e.right-o.marginRight,bottom:i.bottom-e.bottom-o.marginBottom};return n},m.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},m.prototype.bindResize=function(){this.isResizeBound||(i.bind(t,"resize",this),this.isResizeBound=!0)},m.prototype.unbindResize=function(){this.isResizeBound&&i.unbind(t,"resize",this),this.isResizeBound=!1},m.prototype.onresize=function(){function t(){e.resize(),delete e.resizeTimeout}this.resizeTimeout&&clearTimeout(this.resizeTimeout);var e=this;this.resizeTimeout=setTimeout(t,100)},m.prototype.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},m.prototype.needsResizeLayout=function(){var t=d(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},m.prototype.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},m.prototype.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},m.prototype.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},m.prototype.reveal=function(t){var e=t&&t.length;if(e)for(var i=0;e>i;i++){var o=t[i];o.reveal()}},m.prototype.hide=function(t){var e=t&&t.length;if(e)for(var i=0;e>i;i++){var o=t[i];o.hide()}},m.prototype.getItem=function(t){for(var e=0,i=this.items.length;i>e;e++){var o=this.items[e];if(o.element===t)return o}},m.prototype.getItems=function(t){if(t&&t.length){for(var e=[],i=0,o=t.length;o>i;i++){var n=t[i],r=this.getItem(n);r&&e.push(r)}return e}},m.prototype.remove=function(t){t=o(t);var e=this.getItems(t);if(e&&e.length){this._itemsOn(e,"remove",function(){this.emitEvent("removeComplete",[this,e])});for(var i=0,r=e.length;r>i;i++){var s=e[i];s.remove(),n(s,this.items)}}},m.prototype.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="";for(var e=0,i=this.items.length;i>e;e++){var o=this.items[e];o.destroy()}this.unbindResize(),delete this.element.outlayerGUID,p&&p.removeData(this.element,this.constructor.namespace)},m.data=function(t){var e=t&&t.outlayerGUID;return e&&v[e]},m.create=function(t,i){function o(){m.apply(this,arguments)}return Object.create?o.prototype=Object.create(m.prototype):e(o.prototype,m.prototype),o.prototype.constructor=o,o.defaults=e({},m.defaults),e(o.defaults,i),o.prototype.settings={},o.namespace=t,o.data=m.data,o.Item=function(){y.apply(this,arguments)},o.Item.prototype=new y,s(function(){for(var e=r(t),i=a.querySelectorAll(".js-"+e),n="data-"+e+"-options",s=0,h=i.length;h>s;s++){var f,c=i[s],d=c.getAttribute(n);try{f=d&&JSON.parse(d)}catch(l){u&&u.error("Error parsing "+n+" on "+c.nodeName.toLowerCase()+(c.id?"#"+c.id:"")+": "+l);continue}var y=new o(c,f);p&&p.data(c,t,y)}}),p&&p.bridget&&p.bridget(t,o),o},m.Item=y,m}var a=t.document,u=t.console,p=t.jQuery,h=function(){},f=Object.prototype.toString,c="object"==typeof HTMLElement?function(t){return t instanceof HTMLElement}:function(t){return t&&"object"==typeof t&&1===t.nodeType&&"string"==typeof t.nodeName},d=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,o=t.length;o>i;i++)if(t[i]===e)return i;return-1};"function"==typeof define&&define.amd?define("outlayer/outlayer",["eventie/eventie","doc-ready/doc-ready","eventEmitter/EventEmitter","get-size/get-size","matches-selector/matches-selector","./item"],s):t.Outlayer=s(t.eventie,t.docReady,t.EventEmitter,t.getSize,t.matchesSelector,t.Outlayer.Item)}(window),function(t){function e(t){function e(){t.Item.apply(this,arguments)}return e.prototype=new t.Item,e.prototype._create=function(){this.id=this.layout.itemGUID++,t.Item.prototype._create.call(this),this.sortData={}},e.prototype.updateSortData=function(){if(!this.isIgnored){this.sortData.id=this.id,this.sortData["original-order"]=this.id,this.sortData.random=Math.random();var t=this.layout.options.getSortData,e=this.layout._sorters;for(var i in t){var o=e[i];this.sortData[i]=o(this.element,this)}}},e}"function"==typeof define&&define.amd?define("isotope/js/item",["outlayer/outlayer"],e):(t.Isotope=t.Isotope||{},t.Isotope.Item=e(t.Outlayer))}(window),function(t){function e(t,e){function i(t){this.isotope=t,t&&(this.options=t.options[this.namespace],this.element=t.element,this.items=t.filteredItems,this.size=t.size)}return function(){function t(t){return function(){return e.prototype[t].apply(this.isotope,arguments)}}for(var o=["_resetLayout","_getItemLayoutPosition","_manageStamp","_getContainerSize","_getElementOffset","needsResizeLayout"],n=0,r=o.length;r>n;n++){var s=o[n];i.prototype[s]=t(s)}}(),i.prototype.needsVerticalResizeLayout=function(){var e=t(this.isotope.element),i=this.isotope.size&&e;return i&&e.innerHeight!==this.isotope.size.innerHeight},i.prototype._getMeasurement=function(){this.isotope._getMeasurement.apply(this,arguments)},i.prototype.getColumnWidth=function(){this.getSegmentSize("column","Width")},i.prototype.getRowHeight=function(){this.getSegmentSize("row","Height")},i.prototype.getSegmentSize=function(t,e){var i=t+e,o="outer"+e;if(this._getMeasurement(i,o),!this[i]){var n=this.getFirstItemSize();this[i]=n&&n[o]||this.isotope.size["inner"+e]}},i.prototype.getFirstItemSize=function(){var e=this.isotope.filteredItems[0];return e&&e.element&&t(e.element)},i.prototype.layout=function(){this.isotope.layout.apply(this.isotope,arguments)},i.prototype.getSize=function(){this.isotope.getSize(),this.size=this.isotope.size},i.modes={},i.create=function(t,e){function o(){i.apply(this,arguments)}return o.prototype=new i,e&&(o.options=e),o.prototype.namespace=t,i.modes[t]=o,o},i}"function"==typeof define&&define.amd?define("isotope/js/layout-mode",["get-size/get-size","outlayer/outlayer"],e):(t.Isotope=t.Isotope||{},t.Isotope.LayoutMode=e(t.getSize,t.Outlayer))}(window),function(t){function e(t,e){var o=t.create("masonry");return o.prototype._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns();var t=this.cols;for(this.colYs=[];t--;)this.colYs.push(0);this.maxY=0},o.prototype.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}this.columnWidth+=this.gutter,this.cols=Math.floor((this.containerWidth+this.gutter)/this.columnWidth),this.cols=Math.max(this.cols,1)},o.prototype.getContainerWidth=function(){var t=this.options.isFitWidth?this.element.parentNode:this.element,i=e(t);this.containerWidth=i&&i.innerWidth},o.prototype._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,o=e&&1>e?"round":"ceil",n=Math[o](t.size.outerWidth/this.columnWidth);n=Math.min(n,this.cols);for(var r=this._getColGroup(n),s=Math.min.apply(Math,r),a=i(r,s),u={x:this.columnWidth*a,y:s},p=s+t.size.outerHeight,h=this.cols+1-r.length,f=0;h>f;f++)this.colYs[a+f]=p;return u},o.prototype._getColGroup=function(t){if(2>t)return this.colYs;for(var e=[],i=this.cols+1-t,o=0;i>o;o++){var n=this.colYs.slice(o,o+t);e[o]=Math.max.apply(Math,n)}return e},o.prototype._manageStamp=function(t){var i=e(t),o=this._getElementOffset(t),n=this.options.isOriginLeft?o.left:o.right,r=n+i.outerWidth,s=Math.floor(n/this.columnWidth);s=Math.max(0,s);var a=Math.floor(r/this.columnWidth);a-=r%this.columnWidth?0:1,a=Math.min(this.cols-1,a);for(var u=(this.options.isOriginTop?o.top:o.bottom)+i.outerHeight,p=s;a>=p;p++)this.colYs[p]=Math.max(u,this.colYs[p])},o.prototype._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this.options.isFitWidth&&(t.width=this._getContainerFitWidth()),t},o.prototype._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},o.prototype.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!==this.containerWidth},o}var i=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,o=t.length;o>i;i++){var n=t[i];if(n===e)return i}return-1};"function"==typeof define&&define.amd?define("masonry/masonry",["outlayer/outlayer","get-size/get-size"],e):t.Masonry=e(t.Outlayer,t.getSize)}(window),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t,i){var o=t.create("masonry"),n=o.prototype._getElementOffset,r=o.prototype.layout,s=o.prototype._getMeasurement;e(o.prototype,i.prototype),o.prototype._getElementOffset=n,o.prototype.layout=r,o.prototype._getMeasurement=s;var a=o.prototype.measureColumns;o.prototype.measureColumns=function(){this.items=this.isotope.filteredItems,a.call(this)};var u=o.prototype._manageStamp;return o.prototype._manageStamp=function(){this.options.isOriginLeft=this.isotope.options.isOriginLeft,this.options.isOriginTop=this.isotope.options.isOriginTop,u.apply(this,arguments)},o}"function"==typeof define&&define.amd?define("isotope/js/layout-modes/masonry",["../layout-mode","masonry/masonry"],i):i(t.Isotope.LayoutMode,t.Masonry)}(window),function(t){function e(t){var e=t.create("fitRows");return e.prototype._resetLayout=function(){this.x=0,this.y=0,this.maxY=0},e.prototype._getItemLayoutPosition=function(t){t.getSize(),0!==this.x&&t.size.outerWidth+this.x>this.isotope.size.innerWidth&&(this.x=0,this.y=this.maxY);var e={x:this.x,y:this.y};return this.maxY=Math.max(this.maxY,this.y+t.size.outerHeight),this.x+=t.size.outerWidth,e},e.prototype._getContainerSize=function(){return{height:this.maxY}},e}"function"==typeof define&&define.amd?define("isotope/js/layout-modes/fit-rows",["../layout-mode"],e):e(t.Isotope.LayoutMode)}(window),function(t){function e(t){var e=t.create("vertical",{horizontalAlignment:0});return e.prototype._resetLayout=function(){this.y=0},e.prototype._getItemLayoutPosition=function(t){t.getSize();var e=(this.isotope.size.innerWidth-t.size.outerWidth)*this.options.horizontalAlignment,i=this.y;return this.y+=t.size.outerHeight,{x:e,y:i}},e.prototype._getContainerSize=function(){return{height:this.y}},e}"function"==typeof define&&define.amd?define("isotope/js/layout-modes/vertical",["../layout-mode"],e):e(t.Isotope.LayoutMode)}(window),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){return"[object Array]"===h.call(t)}function o(t){var e=[];if(i(t))e=t;else if(t&&"number"==typeof t.length)for(var o=0,n=t.length;n>o;o++)e.push(t[o]);else e.push(t);return e}function n(t,e){var i=f(e,t);-1!==i&&e.splice(i,1)}function r(t,i,r,u,h){function f(t,e){return function(i,o){for(var n=0,r=t.length;r>n;n++){var s=t[n],a=i.sortData[s],u=o.sortData[s];if(a>u||u>a){var p=void 0!==e[s]?e[s]:e,h=p?1:-1;return(a>u?1:-1)*h}}return 0}}var c=t.create("isotope",{layoutMode:"masonry",isJQueryFiltering:!0,sortAscending:!0});c.Item=u,c.LayoutMode=h,c.prototype._create=function(){this.itemGUID=0,this._sorters={},this._getSorters(),t.prototype._create.call(this),this.modes={},this.filteredItems=this.items,this.sortHistory=["original-order"];for(var e in h.modes)this._initLayoutMode(e)},c.prototype.reloadItems=function(){this.itemGUID=0,t.prototype.reloadItems.call(this)},c.prototype._itemize=function(){for(var e=t.prototype._itemize.apply(this,arguments),i=0,o=e.length;o>i;i++){var n=e[i];n.id=this.itemGUID++}return this._updateItemsSortData(e),e},c.prototype._initLayoutMode=function(t){var i=h.modes[t],o=this.options[t]||{};this.options[t]=i.options?e(i.options,o):o,this.modes[t]=new i(this)},c.prototype.layout=function(){return!this._isLayoutInited&&this.options.isInitLayout?(this.arrange(),void 0):(this._layout(),void 0)},c.prototype._layout=function(){var t=this._getIsInstant();this._resetLayout(),this._manageStamps(),this.layoutItems(this.filteredItems,t),this._isLayoutInited=!0},c.prototype.arrange=function(t){this.option(t),this._getIsInstant(),this.filteredItems=this._filter(this.items),this._sort(),this._layout()},c.prototype._init=c.prototype.arrange,c.prototype._getIsInstant=function(){var t=void 0!==this.options.isLayoutInstant?this.options.isLayoutInstant:!this._isLayoutInited;return this._isInstant=t,t},c.prototype._filter=function(t){function e(){f.reveal(n),f.hide(r)}var i=this.options.filter;i=i||"*";for(var o=[],n=[],r=[],s=this._getFilterTest(i),a=0,u=t.length;u>a;a++){var p=t[a];if(!p.isIgnored){var h=s(p);h&&o.push(p),h&&p.isHidden?n.push(p):h||p.isHidden||r.push(p)}}var f=this;return this._isInstant?this._noTransition(e):e(),o},c.prototype._getFilterTest=function(t){return s&&this.options.isJQueryFiltering?function(e){return s(e.element).is(t)}:"function"==typeof t?function(e){return t(e.element)}:function(e){return r(e.element,t)}},c.prototype.updateSortData=function(t){this._getSorters(),t=o(t);var e=this.getItems(t);e=e.length?e:this.items,this._updateItemsSortData(e)
},c.prototype._getSorters=function(){var t=this.options.getSortData;for(var e in t){var i=t[e];this._sorters[e]=d(i)}},c.prototype._updateItemsSortData=function(t){for(var e=0,i=t.length;i>e;e++){var o=t[e];o.updateSortData()}};var d=function(){function t(t){if("string"!=typeof t)return t;var i=a(t).split(" "),o=i[0],n=o.match(/^\[(.+)\]$/),r=n&&n[1],s=e(r,o),u=c.sortDataParsers[i[1]];return t=u?function(t){return t&&u(s(t))}:function(t){return t&&s(t)}}function e(t,e){var i;return i=t?function(e){return e.getAttribute(t)}:function(t){var i=t.querySelector(e);return i&&p(i)}}return t}();c.sortDataParsers={parseInt:function(t){return parseInt(t,10)},parseFloat:function(t){return parseFloat(t)}},c.prototype._sort=function(){var t=this.options.sortBy;if(t){var e=[].concat.apply(t,this.sortHistory),i=f(e,this.options.sortAscending);this.filteredItems.sort(i),t!==this.sortHistory[0]&&this.sortHistory.unshift(t)}},c.prototype._mode=function(){var t=this.options.layoutMode,e=this.modes[t];if(!e)throw Error("No layout mode: "+t);return e.options=this.options[t],e},c.prototype._resetLayout=function(){t.prototype._resetLayout.call(this),this._mode()._resetLayout()},c.prototype._getItemLayoutPosition=function(t){return this._mode()._getItemLayoutPosition(t)},c.prototype._manageStamp=function(t){this._mode()._manageStamp(t)},c.prototype._getContainerSize=function(){return this._mode()._getContainerSize()},c.prototype.needsResizeLayout=function(){return this._mode().needsResizeLayout()},c.prototype.appended=function(t){var e=this.addItems(t);if(e.length){var i=this._filterRevealAdded(e);this.filteredItems=this.filteredItems.concat(i)}},c.prototype.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps();var o=this._filterRevealAdded(e);this.layoutItems(i),this.filteredItems=o.concat(this.filteredItems)}},c.prototype._filterRevealAdded=function(t){var e=this._noTransition(function(){return this._filter(t)});return this.layoutItems(e,!0),this.reveal(e),t},c.prototype.insert=function(t){var e=this.addItems(t);if(e.length){var i,o,n=e.length;for(i=0;n>i;i++)o=e[i],this.element.appendChild(o.element);var r=this._filter(e);for(this._noTransition(function(){this.hide(r)}),i=0;n>i;i++)e[i].isLayoutInstant=!0;for(this.arrange(),i=0;n>i;i++)delete e[i].isLayoutInstant;this.reveal(r)}};var l=c.prototype.remove;return c.prototype.remove=function(t){t=o(t);var e=this.getItems(t);if(l.call(this,t),e&&e.length)for(var i=0,r=e.length;r>i;i++){var s=e[i];n(s,this.filteredItems)}},c.prototype._noTransition=function(t){var e=this.options.transitionDuration;this.options.transitionDuration=0;var i=t.call(this);return this.options.transitionDuration=e,i},c}var s=t.jQuery,a=String.prototype.trim?function(t){return t.trim()}:function(t){return t.replace(/^\s+|\s+$/g,"")},u=document.documentElement,p=u.textContent?function(t){return t.textContent}:function(t){return t.innerText},h=Object.prototype.toString,f=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,o=t.length;o>i;i++)if(t[i]===e)return i;return-1};"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","matches-selector/matches-selector","isotope/js/item","isotope/js/layout-mode","isotope/js/layout-modes/masonry","isotope/js/layout-modes/fit-rows","isotope/js/layout-modes/vertical"],r):t.Isotope=r(t.Outlayer,t.getSize,t.matchesSelector,t.Isotope.Item,t.Isotope.LayoutMode)}(window);;
!function(e,t){"function"==typeof define&&define.amd?define(t):"object"==typeof exports?module.exports=t(require,exports,module):e.ScrollReveal=t()}(this,function(e,t,n){return function(){"use strict";var e,t,n;this.ScrollReveal=function(){function i(n){return"undefined"==typeof this||Object.getPrototypeOf(this)!==i.prototype?new i(n):(e=this,e.tools=new t,e.isSupported()?(e.tools.extend(e.defaults,n||{}),o(e.defaults),e.store={elements:{},containers:[]},e.sequences={},e.history=[],e.uid=0,e.initialized=!1):"undefined"!=typeof console&&null!==console,e)}function o(t){var n=t.container;return n&&"string"==typeof n?t.container=window.document.querySelector(n):(n&&!e.tools.isNode(n)&&(t.container=null),null==n&&(t.container=window.document.documentElement),t.container)}function r(){return++e.uid}function s(t,n){t.config?t.config=e.tools.extendClone(t.config,n):t.config=e.tools.extendClone(e.defaults,n),"top"===t.config.origin||"bottom"===t.config.origin?t.config.axis="Y":t.config.axis="X","top"!==t.config.origin&&"left"!==t.config.origin||(t.config.distance="-"+t.config.distance)}function a(e){var t=window.getComputedStyle(e.domEl);e.styles||(e.styles={transition:{},transform:{},computed:{}},e.styles.inline=e.domEl.getAttribute("style")||"",e.styles.inline+="; visibility: visible; ",e.styles.computed.opacity=t.opacity,t.transition&&"all 0s ease 0s"!=t.transition?e.styles.computed.transition=t.transition+", ":e.styles.computed.transition=""),e.styles.transition.instant=l(e,0),e.styles.transition.delayed=l(e,e.config.delay),e.styles.transform.initial=" -webkit-transform:",e.styles.transform.target=" -webkit-transform:",c(e),e.styles.transform.initial+="transform:",e.styles.transform.target+="transform:",c(e)}function l(e,t){var n=e.config;return"-webkit-transition: "+e.styles.computed.transition+"-webkit-transform "+n.duration/1e3+"s "+n.easing+" "+t/1e3+"s, opacity "+n.duration/1e3+"s "+n.easing+" "+t/1e3+"s; transition: "+e.styles.computed.transition+"transform "+n.duration/1e3+"s "+n.easing+" "+t/1e3+"s, opacity "+n.duration/1e3+"s "+n.easing+" "+t/1e3+"s; "}function c(e){var t=e.config,n=e.styles.transform;parseInt(t.distance)&&(n.initial+=" translate"+t.axis+"("+t.distance+")",n.target+=" translate"+t.axis+"(0)"),t.scale&&(n.initial+=" scale("+t.scale+")",n.target+=" scale(1)"),t.rotate.x&&(n.initial+=" rotateX("+t.rotate.x+"deg)",n.target+=" rotateX(0)"),t.rotate.y&&(n.initial+=" rotateY("+t.rotate.y+"deg)",n.target+=" rotateY(0)"),t.rotate.z&&(n.initial+=" rotateZ("+t.rotate.z+"deg)",n.target+=" rotateZ(0)"),n.initial+="; opacity: "+t.opacity+";",n.target+="; opacity: "+e.styles.computed.opacity+";"}function f(t){var n=t.config.container;n&&-1==e.store.containers.indexOf(n)&&e.store.containers.push(t.config.container),e.store.elements[t.id]=t}function u(t,n,i){var o={selector:t,config:n,interval:i};e.history.push(o)}function d(){if(e.isSupported()){p();for(var t=0;t<e.store.containers.length;t++)e.store.containers[t].addEventListener("scroll",y),e.store.containers[t].addEventListener("resize",y);e.initialized||(window.addEventListener("scroll",y),window.addEventListener("resize",y),e.initialized=!0)}return e}function y(){n(p)}function m(){var t,n,i,o;e.tools.forOwn(e.sequences,function(r){o=e.sequences[r],t=!1;for(var s=0;s<o.elemIds.length;s++)i=o.elemIds[s],n=e.store.elements[i],O(n)&&!t&&(t=!0);o.active=t})}function p(){var t,n;m(),e.tools.forOwn(e.store.elements,function(i){n=e.store.elements[i],t=b(n),v(n)?(t?n.domEl.setAttribute("style",n.styles.inline+n.styles.transform.target+n.styles.transition.delayed):n.domEl.setAttribute("style",n.styles.inline+n.styles.transform.target+n.styles.transition.instant),w("reveal",n,t),n.revealing=!0,n.seen=!0,n.sequence&&g(n,t)):h(n)&&(n.domEl.setAttribute("style",n.styles.inline+n.styles.transform.initial+n.styles.transition.instant),w("reset",n),n.revealing=!1)})}function g(t,n){var i=0,o=0,r=e.sequences[t.sequence.id];r.blocked=!0,n&&"onload"==t.config.useDelay&&(o=t.config.delay),t.sequence.timer&&(i=Math.abs(t.sequence.timer.started-new Date),window.clearTimeout(t.sequence.timer)),t.sequence.timer={started:new Date},t.sequence.timer.clock=window.setTimeout(function(){r.blocked=!1,t.sequence.timer=null,y()},Math.abs(r.interval)+o-i)}function w(e,t,n){var i=0,o=0,r="after";switch(e){case"reveal":o=t.config.duration,n&&(o+=t.config.delay),r+="Reveal";break;case"reset":o=t.config.duration,r+="Reset"}t.timer&&(i=Math.abs(t.timer.started-new Date),window.clearTimeout(t.timer.clock)),t.timer={started:new Date},t.timer.clock=window.setTimeout(function(){t.config[r](t.domEl),t.timer=null},o-i)}function v(t){if(t.sequence){var n=e.sequences[t.sequence.id];return n.active&&!n.blocked&&!t.revealing&&!t.disabled}return O(t)&&!t.revealing&&!t.disabled}function b(t){var n=t.config.useDelay;return"always"===n||"onload"===n&&!e.initialized||"once"===n&&!t.seen}function h(t){if(t.sequence){var n=e.sequences[t.sequence.id];return!n.active&&t.config.reset&&t.revealing&&!t.disabled}return!O(t)&&t.config.reset&&t.revealing&&!t.disabled}function x(e){return{width:e.clientWidth,height:e.clientHeight}}function q(e){if(e&&e!==window.document.documentElement){var t=E(e);return{x:e.scrollLeft+t.left,y:e.scrollTop+t.top}}return{x:window.pageXOffset,y:window.pageYOffset}}function E(e){var t=0,n=0,i=e.offsetHeight,o=e.offsetWidth;do isNaN(e.offsetTop)||(t+=e.offsetTop),isNaN(e.offsetLeft)||(n+=e.offsetLeft);while(e=e.offsetParent);return{top:t,left:n,height:i,width:o}}function O(e){function t(){var t=c+a*s,n=f+l*s,i=u-a*s,y=d-l*s,m=r.y+e.config.viewOffset.top,p=r.x+e.config.viewOffset.left,g=r.y-e.config.viewOffset.bottom+o.height,w=r.x-e.config.viewOffset.right+o.width;return g>t&&i>m&&n>p&&w>y}function n(){return"fixed"===window.getComputedStyle(e.domEl).position}var i=E(e.domEl),o=x(e.config.container),r=q(e.config.container),s=e.config.viewFactor,a=i.height,l=i.width,c=i.top,f=i.left,u=c+a,d=f+l;return t()||n()}return i.prototype.defaults={origin:"bottom",distance:"20px",duration:500,delay:0,rotate:{x:0,y:0,z:0},opacity:0,scale:.9,easing:"cubic-bezier(0.6, 0.2, 0.1, 1)",container:null,mobile:!0,reset:!1,useDelay:"always",viewFactor:.2,viewOffset:{top:0,right:0,bottom:0,left:0},afterReveal:function(e){},afterReset:function(e){}},i.prototype.isSupported=function(){var e=document.documentElement.style;return"WebkitTransition"in e&&"WebkitTransform"in e||"transition"in e&&"transform"in e},i.prototype.reveal=function(t,n,i,l){var c,y,m,p,g,w;if(c=n&&n.container?o(n):e.defaults.container,y=e.tools.isNode(t)?[t]:Array.prototype.slice.call(c.querySelectorAll(t)),!y.length)return e;n&&"number"==typeof n&&(i=n,n={}),i&&"number"==typeof i&&(w=r(),g=e.sequences[w]={id:w,interval:i,elemIds:[],active:!1});for(var v=0;v<y.length;v++)p=y[v].getAttribute("data-sr-id"),p?m=e.store.elements[p]:(m={id:r(),domEl:y[v],seen:!1,revealing:!1},m.domEl.setAttribute("data-sr-id",m.id)),g&&(m.sequence={id:g.id,index:g.elemIds.length},g.elemIds.push(m.id)),s(m,n||{}),a(m),f(m),e.tools.isMobile()&&!m.config.mobile||!e.isSupported()?(m.domEl.setAttribute("style",m.styles.inline),m.disabled=!0):m.revealing||m.domEl.setAttribute("style",m.styles.inline+m.styles.transform.initial);return!l&&e.isSupported()&&(u(t,n),e.initTimeout&&window.clearTimeout(e.initTimeout),e.initTimeout=window.setTimeout(d,0)),e},i.prototype.sync=function(){if(e.history.length&&e.isSupported()){for(var t=0;t<e.history.length;t++){var n=e.history[t];e.reveal(n.selector,n.config,n.interval,!0)}d()}return e},i}(),t=function(){function e(){}return e.prototype.isObject=function(e){return null!==e&&"object"==typeof e&&e.constructor==Object},e.prototype.isNode=function(e){return"object"==typeof Node?e instanceof Node:e&&"object"==typeof e&&"number"==typeof e.nodeType&&"string"==typeof e.nodeName},e.prototype.forOwn=function(e,t){if(!this.isObject(e))throw new TypeError('Expected "object", but received "'+typeof e+'".');for(var n in e)e.hasOwnProperty(n)&&t(n)},e.prototype.extend=function(e,t){return this.forOwn(t,function(n){this.isObject(t[n])?(e[n]&&this.isObject(e[n])||(e[n]={}),this.extend(e[n],t[n])):e[n]=t[n]}.bind(this)),e},e.prototype.extendClone=function(e,t){return this.extend(this.extend({},e),t)},e.prototype.isMobile=function(){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)},e}(),n=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame}.call(this),this.ScrollReveal});;
(function(){!function(n){n.countdown=function(t,e){var r,o=this;return this.el=t,this.$el=n(t),this.$el.data("countdown",this),this.init=function(){return o.options=n.extend({},n.countdown.defaultOptions,e),o.options.refresh&&(o.interval=setInterval(function(){return o.render()},o.options.refresh)),o.render(),o},r=function(t){var e,r;return t=Date.parse(n.isPlainObject(o.options.date)?o.options.date:new Date(o.options.date)),r=(t-Date.parse(new Date))/1e3,0>=r&&(r=0,o.interval&&o.stop(),o.options.onEnd.apply(o)),e={years:0,days:0,hours:0,min:0,sec:0,millisec:0},r>=31557600&&(e.years=Math.floor(r/31557600),r-=365.25*e.years*86400),r>=86400&&(e.days=Math.floor(r/86400),r-=86400*e.days),r>=3600&&(e.hours=Math.floor(r/3600),r-=3600*e.hours),r>=60&&(e.min=Math.floor(r/60),r-=60*e.min),e.sec=r,e},this.leadingZeros=function(n,t){for(null==t&&(t=2),n=String(n);n.length<t;)n="0"+n;return n},this.update=function(n){return o.options.date=n,o},this.render=function(){return o.options.render.apply(o,[r(o.options.date)]),o},this.stop=function(){return o.interval&&clearInterval(o.interval),o.interval=null,o},this.start=function(t){return null==t&&(t=o.options.refresh||n.countdown.defaultOptions.refresh),o.interval&&clearInterval(o.interval),o.render(),o.options.refresh=t,o.interval=setInterval(function(){return o.render()},o.options.refresh),o},this.init()},n.countdown.defaultOptions={date:"June 7, 2087 15:03:25",refresh:1e3,onEnd:n.noop,render:function(t){return n(this.el).html(""+t.years+" years, "+t.days+" days, "+this.leadingZeros(t.hours)+" hours, "+this.leadingZeros(t.min)+" min and "+this.leadingZeros(t.sec)+" sec")}},n.fn.countdown=function(t){return n.each(this,function(e,r){var o;return o=n(r),o.data("countdown")?void 0:o.data("countdown",new n.countdown(r,t))})}}(jQuery)}).call(this);;
/*global wc_add_to_cart_variation_params */
;(function ( $, window, document, undefined ) {
	/**
	 * VariationForm class which handles variation forms and attributes.
	 */
	var VariationForm = function( $form ) {
		var self = this;

		self.$form                = $form;
		self.$attributeFields     = $form.find( '.variations select' );
		self.$singleVariation     = $form.find( '.single_variation' );
		self.$singleVariationWrap = $form.find( '.single_variation_wrap' );
		self.$resetVariations     = $form.find( '.reset_variations' );
		self.$product             = $form.closest( '.product' );
		self.variationData        = $form.data( 'product_variations' );
		self.useAjax              = false === self.variationData;
		self.xhr                  = false;
		self.loading              = true;

		// Initial state.
		self.$singleVariationWrap.show();
		self.$form.off( '.wc-variation-form' );

		// Methods.
		self.getChosenAttributes    = self.getChosenAttributes.bind( self );
		self.findMatchingVariations = self.findMatchingVariations.bind( self );
		self.isMatch                = self.isMatch.bind( self );
		self.toggleResetLink        = self.toggleResetLink.bind( self );

		// Events.
		$form.on( 'click.wc-variation-form', '.reset_variations', { variationForm: self }, self.onReset );
		$form.on( 'reload_product_variations', { variationForm: self }, self.onReload );
		$form.on( 'hide_variation', { variationForm: self }, self.onHide );
		$form.on( 'show_variation', { variationForm: self }, self.onShow );
		$form.on( 'click', '.single_add_to_cart_button', { variationForm: self }, self.onAddToCart );
		$form.on( 'reset_data', { variationForm: self }, self.onResetDisplayedVariation );
		$form.on( 'reset_image', { variationForm: self }, self.onResetImage );
		$form.on( 'change.wc-variation-form', '.variations select', { variationForm: self }, self.onChange );
		$form.on( 'found_variation.wc-variation-form', { variationForm: self }, self.onFoundVariation );
		$form.on( 'check_variations.wc-variation-form', { variationForm: self }, self.onFindVariation );
		$form.on( 'update_variation_values.wc-variation-form', { variationForm: self }, self.onUpdateAttributes );

		// Init after gallery.
		setTimeout( function() {
			$form.trigger( 'check_variations' );
			$form.trigger( 'wc_variation_form' );
			self.loading = false;
		}, 100 );
	};

	/**
	 * Reset all fields.
	 */
	VariationForm.prototype.onReset = function( event ) {
		event.preventDefault();
		event.data.variationForm.$attributeFields.val( '' ).change();
		event.data.variationForm.$form.trigger( 'reset_data' );
	};

	/**
	 * Reload variation data from the DOM.
	 */
	VariationForm.prototype.onReload = function( event ) {
		var form           = event.data.variationForm;
		form.variationData = form.$form.data( 'product_variations' );
		form.useAjax       = false === form.variationData;
		form.$form.trigger( 'check_variations' );
	};

	/**
	 * When a variation is hidden.
	 */
	VariationForm.prototype.onHide = function( event ) {
		event.preventDefault();
		event.data.variationForm.$form.find( '.single_add_to_cart_button' ).removeClass( 'wc-variation-is-unavailable' ).addClass( 'disabled wc-variation-selection-needed' );
		event.data.variationForm.$form.find( '.woocommerce-variation-add-to-cart' ).removeClass( 'woocommerce-variation-add-to-cart-enabled' ).addClass( 'woocommerce-variation-add-to-cart-disabled' );
	};

	/**
	 * When a variation is shown.
	 */
	VariationForm.prototype.onShow = function( event, variation, purchasable ) {
		event.preventDefault();
		if ( purchasable ) {
			event.data.variationForm.$form.find( '.single_add_to_cart_button' ).removeClass( 'disabled wc-variation-selection-needed wc-variation-is-unavailable' );
			event.data.variationForm.$form.find( '.woocommerce-variation-add-to-cart' ).removeClass( 'woocommerce-variation-add-to-cart-disabled' ).addClass( 'woocommerce-variation-add-to-cart-enabled' );
		} else {
			event.data.variationForm.$form.find( '.single_add_to_cart_button' ).removeClass( 'wc-variation-selection-needed' ).addClass( 'disabled wc-variation-is-unavailable' );
			event.data.variationForm.$form.find( '.woocommerce-variation-add-to-cart' ).removeClass( 'woocommerce-variation-add-to-cart-enabled' ).addClass( 'woocommerce-variation-add-to-cart-disabled' );
		}
	};

	/**
	 * When the cart button is pressed.
	 */
	VariationForm.prototype.onAddToCart = function( event ) {
		if ( $( this ).is('.disabled') ) {
			event.preventDefault();

			if ( $( this ).is('.wc-variation-is-unavailable') ) {
				window.alert( wc_add_to_cart_variation_params.i18n_unavailable_text );
			} else if ( $( this ).is('.wc-variation-selection-needed') ) {
				window.alert( wc_add_to_cart_variation_params.i18n_make_a_selection_text );
			}
		}
	};

	/**
	 * When displayed variation data is reset.
	 */
	VariationForm.prototype.onResetDisplayedVariation = function( event ) {
		var form = event.data.variationForm;
		form.$product.find( '.product_meta' ).find( '.sku' ).wc_reset_content();
		form.$product.find( '.product_weight' ).wc_reset_content();
		form.$product.find( '.product_dimensions' ).wc_reset_content();
		form.$form.trigger( 'reset_image' );
		form.$singleVariation.slideUp( 200 ).trigger( 'hide_variation' );
	};

	/**
	 * When the product image is reset.
	 */
	VariationForm.prototype.onResetImage = function( event ) {
		event.data.variationForm.$form.wc_variations_image_update( false );
	};

	/**
	 * Looks for matching variations for current selected attributes.
	 */
	VariationForm.prototype.onFindVariation = function( event ) {
		var form              = event.data.variationForm,
			attributes        = form.getChosenAttributes(),
			currentAttributes = attributes.data;

		if ( attributes.count === attributes.chosenCount ) {
			if ( form.useAjax ) {
				if ( form.xhr ) {
					form.xhr.abort();
				}
				form.$form.block( { message: null, overlayCSS: { background: '#fff', opacity: 0.6 } } );
				currentAttributes.product_id  = parseInt( form.$form.data( 'product_id' ), 10 );
				currentAttributes.custom_data = form.$form.data( 'custom_data' );
				form.xhr                      = $.ajax( {
					url: wc_add_to_cart_variation_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_variation' ),
					type: 'POST',
					data: currentAttributes,
					success: function( variation ) {
						if ( variation ) {
							form.$form.trigger( 'found_variation', [ variation ] );
						} else {
							form.$form.trigger( 'reset_data' );
							attributes.chosenCount = 0;

							if ( ! form.loading ) {
								form.$form.find( '.single_variation' ).after( '<p class="wc-no-matching-variations woocommerce-info">' + wc_add_to_cart_variation_params.i18n_no_matching_variations_text + '</p>' );
								form.$form.find( '.wc-no-matching-variations' ).slideDown( 200 );
							}
						}
					},
					complete: function() {
						form.$form.unblock();
					}
				} );
			} else {
				form.$form.trigger( 'update_variation_values' );

				var matching_variations = form.findMatchingVariations( form.variationData, currentAttributes ),
					variation           = matching_variations.shift();

				if ( variation ) {
					form.$form.trigger( 'found_variation', [ variation ] );
				} else {
					form.$form.trigger( 'reset_data' );
					attributes.chosenCount = 0;

					if ( ! form.loading ) {
						form.$form.find( '.single_variation' ).after( '<p class="wc-no-matching-variations woocommerce-info">' + wc_add_to_cart_variation_params.i18n_no_matching_variations_text + '</p>' );
						form.$form.find( '.wc-no-matching-variations' ).slideDown( 200 );
					}
				}
			}
		} else {
			form.$form.trigger( 'update_variation_values' );
			form.$form.trigger( 'reset_data' );
		}

		// Show reset link.
		form.toggleResetLink( attributes.chosenCount > 0 );
	};

	/**
	 * Triggered when a variation has been found which matches all attributes.
	 */
	VariationForm.prototype.onFoundVariation = function( event, variation ) {
		var form           = event.data.variationForm,
			$sku           = form.$product.find( '.product_meta' ).find( '.sku' ),
			$weight        = form.$product.find( '.product_weight' ),
			$dimensions    = form.$product.find( '.product_dimensions' ),
			$qty           = form.$singleVariationWrap.find( '.quantity' ),
			purchasable    = true,
			variation_id   = '',
			template       = false,
			$template_html = '';

		if ( variation.sku ) {
			$sku.wc_set_content( variation.sku );
		} else {
			$sku.wc_reset_content();
		}

		if ( variation.weight ) {
			$weight.wc_set_content( variation.weight_html );
		} else {
			$weight.wc_reset_content();
		}

		if ( variation.dimensions ) {
			$dimensions.wc_set_content( variation.dimensions_html );
		} else {
			$dimensions.wc_reset_content();
		}

		form.$form.wc_variations_image_update( variation );

		if ( ! variation.variation_is_visible ) {
			template = wp.template( 'unavailable-variation-template' );
		} else {
			template     = wp.template( 'variation-template' );
			variation_id = variation.variation_id;
		}

		$template_html = template( {
			variation: variation
		} );
		$template_html = $template_html.replace( '/*<![CDATA[*/', '' );
		$template_html = $template_html.replace( '/*]]>*/', '' );

		form.$singleVariation.html( $template_html );
		form.$form.find( 'input[name="variation_id"], input.variation_id' ).val( variation.variation_id ).change();

		// Hide or show qty input
		if ( variation.is_sold_individually === 'yes' ) {
			$qty.find( 'input.qty' ).val( '1' ).attr( 'min', '1' ).attr( 'max', '' );
			$qty.hide();
		} else {
			$qty.find( 'input.qty' ).attr( 'min', variation.min_qty ).attr( 'max', variation.max_qty );
			$qty.show();
		}

		// Enable or disable the add to cart button
		if ( ! variation.is_purchasable || ! variation.is_in_stock || ! variation.variation_is_visible ) {
			purchasable = false;
		}

		// Reveal
		if ( $.trim( form.$singleVariation.text() ) ) {
			form.$singleVariation.slideDown( 200 ).trigger( 'show_variation', [ variation, purchasable ] );
		} else {
			form.$singleVariation.show().trigger( 'show_variation', [ variation, purchasable ] );
		}
	};

	/**
	 * Triggered when an attribute field changes.
	 */
	VariationForm.prototype.onChange = function( event ) {
		var form = event.data.variationForm;

		form.$form.find( 'input[name="variation_id"], input.variation_id' ).val( '' ).change();
		form.$form.find( '.wc-no-matching-variations' ).remove();

		if ( form.useAjax ) {
			form.$form.trigger( 'check_variations' );
		} else {
			form.$form.trigger( 'woocommerce_variation_select_change' );
			form.$form.trigger( 'check_variations' );
			$( this ).blur();
		}

		// Custom event for when variation selection has been changed
		form.$form.trigger( 'woocommerce_variation_has_changed' );
	};

	/**
	 * Escape quotes in a string.
	 * @param {string} string
	 * @return {string}
	 */
	VariationForm.prototype.addSlashes = function( string ) {
		string = string.replace( /'/g, '\\\'' );
		string = string.replace( /"/g, '\\\"' );
		return string;
	};

	/**
	 * Updates attributes in the DOM to show valid values.
	 */
	VariationForm.prototype.onUpdateAttributes = function( event ) {
		var form              = event.data.variationForm,
			attributes        = form.getChosenAttributes(),
			currentAttributes = attributes.data;

		if ( form.useAjax ) {
			return;
		}

		// Loop through selects and disable/enable options based on selections.
		form.$attributeFields.each( function( index, el ) {
			var current_attr_select     = $( el ),
				current_attr_name       = current_attr_select.data( 'attribute_name' ) || current_attr_select.attr( 'name' ),
				show_option_none        = $( el ).data( 'show_option_none' ),
				option_gt_filter        = ':gt(0)',
				attached_options_count  = 0,
				new_attr_select         = $( '<select/>' ),
				selected_attr_val       = current_attr_select.val() || '',
				selected_attr_val_valid = true;

			// Reference options set at first.
			if ( ! current_attr_select.data( 'attribute_html' ) ) {
				var refSelect = current_attr_select.clone();

				refSelect.find( 'option' ).removeAttr( 'disabled attached' ).removeAttr( 'selected' );

				current_attr_select.data( 'attribute_options', refSelect.find( 'option' + option_gt_filter ).get() ); // Legacy data attribute.
				current_attr_select.data( 'attribute_html', refSelect.html() );
			}

			new_attr_select.html( current_attr_select.data( 'attribute_html' ) );

			// The attribute of this select field should not be taken into account when calculating its matching variations:
			// The constraints of this attribute are shaped by the values of the other attributes.
			var checkAttributes = $.extend( true, {}, currentAttributes );

			checkAttributes[ current_attr_name ] = '';

			var variations = form.findMatchingVariations( form.variationData, checkAttributes );

			// Loop through variations.
			for ( var num in variations ) {
				if ( typeof( variations[ num ] ) !== 'undefined' ) {
					var variationAttributes = variations[ num ].attributes;

					for ( var attr_name in variationAttributes ) {
						if ( variationAttributes.hasOwnProperty( attr_name ) ) {
							var attr_val         = variationAttributes[ attr_name ],
								variation_active = '';

							if ( attr_name === current_attr_name ) {
								if ( variations[ num ].variation_is_active ) {
									variation_active = 'enabled';
								}

								if ( attr_val ) {
									// Decode entities and add slashes.
									attr_val = $( '<div/>' ).html( attr_val ).text();

									// Attach.
									new_attr_select.find( 'option[value="' + form.addSlashes( attr_val ) + '"]' ).addClass( 'attached ' + variation_active );
								} else {
									// Attach all apart from placeholder.
									new_attr_select.find( 'option:gt(0)' ).addClass( 'attached ' + variation_active );
								}
							}
						}
					}
				}
			}

			// Count available options.
			attached_options_count = new_attr_select.find( 'option.attached' ).length;

			// Check if current selection is in attached options.
			if ( selected_attr_val && ( attached_options_count === 0 || new_attr_select.find( 'option.attached.enabled[value="' + form.addSlashes( selected_attr_val ) + '"]' ).length === 0 ) ) {
				selected_attr_val_valid = false;
			}

			// Detach the placeholder if:
			// - Valid options exist.
			// - The current selection is non-empty.
			// - The current selection is valid.
			// - Placeholders are not set to be permanently visible.
			if ( attached_options_count > 0 && selected_attr_val && selected_attr_val_valid && ( 'no' === show_option_none ) ) {
				new_attr_select.find( 'option:first' ).remove();
				option_gt_filter = '';
			}

			// Detach unattached.
			new_attr_select.find( 'option' + option_gt_filter + ':not(.attached)' ).remove();

			// Finally, copy to DOM and set value.
			current_attr_select.html( new_attr_select.html() );
			current_attr_select.find( 'option' + option_gt_filter + ':not(.enabled)' ).prop( 'disabled', true );

			// Choose selected value.
			if ( selected_attr_val ) {
				// If the previously selected value is no longer available, fall back to the placeholder (it's going to be there).
				if ( selected_attr_val_valid ) {
					current_attr_select.val( selected_attr_val );
				} else {
					current_attr_select.val( '' ).change();
				}
			} else {
				current_attr_select.val( '' ); // No change event to prevent infinite loop.
			}
		});

		// Custom event for when variations have been updated.
		form.$form.trigger( 'woocommerce_update_variation_values' );
	};

	/**
	 * Get chosen attributes from form.
	 * @return array
	 */
	VariationForm.prototype.getChosenAttributes = function() {
		var data   = {};
		var count  = 0;
		var chosen = 0;

		this.$attributeFields.each( function() {
			var attribute_name = $( this ).data( 'attribute_name' ) || $( this ).attr( 'name' );
			var value          = $( this ).val() || '';

			if ( value.length > 0 ) {
				chosen ++;
			}

			count ++;
			data[ attribute_name ] = value;
		});

		return {
			'count'      : count,
			'chosenCount': chosen,
			'data'       : data
		};
	};

	/**
	 * Find matching variations for attributes.
	 */
	VariationForm.prototype.findMatchingVariations = function( variations, attributes ) {
		var matching = [];
		for ( var i = 0; i < variations.length; i++ ) {
			var variation = variations[i];

			if ( this.isMatch( variation.attributes, attributes ) ) {
				matching.push( variation );
			}
		}
		return matching;
	};

	/**
	 * See if attributes match.
	 * @return {Boolean}
	 */
	VariationForm.prototype.isMatch = function( variation_attributes, attributes ) {
		var match = true;
		for ( var attr_name in variation_attributes ) {
			if ( variation_attributes.hasOwnProperty( attr_name ) ) {
				var val1 = variation_attributes[ attr_name ];
				var val2 = attributes[ attr_name ];
				if ( val1 !== undefined && val2 !== undefined && val1.length !== 0 && val2.length !== 0 && val1 !== val2 ) {
					match = false;
				}
			}
		}
		return match;
	};

	/**
	 * Show or hide the reset link.
	 */
	VariationForm.prototype.toggleResetLink = function( on ) {
		if ( on ) {
			if ( this.$resetVariations.css( 'visibility' ) === 'hidden' ) {
				this.$resetVariations.css( 'visibility', 'visible' ).hide().fadeIn();
			}
		} else {
			this.$resetVariations.css( 'visibility', 'hidden' );
		}
	};

	/**
	 * Function to call wc_variation_form on jquery selector.
	 */
	$.fn.wc_variation_form = function() {
		new VariationForm( this );
		return this;
	};

	/**
	 * Stores the default text for an element so it can be reset later
	 */
	$.fn.wc_set_content = function( content ) {
		if ( undefined === this.attr( 'data-o_content' ) ) {
			this.attr( 'data-o_content', this.text() );
		}
		this.text( content );
	};

	/**
	 * Stores the default text for an element so it can be reset later
	 */
	$.fn.wc_reset_content = function() {
		if ( undefined !== this.attr( 'data-o_content' ) ) {
			this.text( this.attr( 'data-o_content' ) );
		}
	};

	/**
	 * Stores a default attribute for an element so it can be reset later
	 */
	$.fn.wc_set_variation_attr = function( attr, value ) {
		if ( undefined === this.attr( 'data-o_' + attr ) ) {
			this.attr( 'data-o_' + attr, ( ! this.attr( attr ) ) ? '' : this.attr( attr ) );
		}
		if ( false === value ) {
			this.removeAttr( attr );
		} else {
			this.attr( attr, value );
		}
	};

	/**
	 * Reset a default attribute for an element so it can be reset later
	 */
	$.fn.wc_reset_variation_attr = function( attr ) {
		if ( undefined !== this.attr( 'data-o_' + attr ) ) {
			this.attr( attr, this.attr( 'data-o_' + attr ) );
		}
	};

	/**
	 * Reset the slide position if the variation has a different image than the current one
	 */
	$.fn.wc_maybe_trigger_slide_position_reset = function( variation ) {
		var $form                = $( this ),
			$product             = $form.closest( '.product' ),
			$product_gallery     = $product.find( '.images' ),
			reset_slide_position = false,
			new_image_id         = ( variation && variation.image_id ) ? variation.image_id : '';

		if ( $form.attr( 'current-image' ) !== new_image_id ) {
			reset_slide_position = true;
		}

		$form.attr( 'current-image', new_image_id );

		if ( reset_slide_position ) {
			$product_gallery.trigger( 'woocommerce_gallery_reset_slide_position' );
		}
	};

	/**
	 * Sets product images for the chosen variation
	 */
	$.fn.wc_variations_image_update = function( variation ) {
		var $form             = this,
			$product          = $form.closest( '.product' ),
			$product_gallery  = $product.find( '.images' ),
			$gallery_nav      = $product.find( '.flex-control-nav' ),
			$gallery_img      = $gallery_nav.find( 'li:eq(0) img' ),
			$product_img_wrap = $product_gallery.find( '.woocommerce-product-gallery__image, .woocommerce-product-gallery__image--placeholder' ).eq( 0 ),
			$product_img      = $product_img_wrap.find( '.wp-post-image' ),
			$product_link     = $product_img_wrap.find( 'a' ).eq( 0 );

		if ( variation && variation.image && variation.image.src && variation.image.src.length > 1 ) {
			// See if the gallery has an image with the same original src as the image we want to switch to.
			var galleryHasImage = $gallery_nav.find( 'li img[data-o_src="' + variation.image.gallery_thumbnail_src + '"]' ).length > 0;

			// If the gallery has the image, reset the images. We'll scroll to the correct one.
			if ( galleryHasImage ) {
				$form.wc_variations_image_reset();
			}

			// See if gallery has a matching image we can slide to.
			var slideToImage = $gallery_nav.find( 'li img[src="' + variation.image.gallery_thumbnail_src + '"]' );

			if ( slideToImage.length > 0 ) {
				slideToImage.trigger( 'click' );
				$form.attr( 'current-image', variation.image_id );
				window.setTimeout( function() {
					$( window ).trigger( 'resize' );
					$product_gallery.trigger( 'woocommerce_gallery_init_zoom' );
				}, 20 );
				return;
			}

			$product_img.wc_set_variation_attr( 'src', variation.image.src );
			$product_img.wc_set_variation_attr( 'height', variation.image.src_h );
			$product_img.wc_set_variation_attr( 'width', variation.image.src_w );
			$product_img.wc_set_variation_attr( 'srcset', variation.image.srcset );
			$product_img.wc_set_variation_attr( 'sizes', variation.image.sizes );
			$product_img.wc_set_variation_attr( 'title', variation.image.title );
			$product_img.wc_set_variation_attr( 'data-caption', variation.image.caption );
			$product_img.wc_set_variation_attr( 'alt', variation.image.alt );
			$product_img.wc_set_variation_attr( 'data-src', variation.image.full_src );
			$product_img.wc_set_variation_attr( 'data-large_image', variation.image.full_src );
			$product_img.wc_set_variation_attr( 'data-large_image_width', variation.image.full_src_w );
			$product_img.wc_set_variation_attr( 'data-large_image_height', variation.image.full_src_h );
			$product_img_wrap.wc_set_variation_attr( 'data-thumb', variation.image.src );
			$gallery_img.wc_set_variation_attr( 'src', variation.image.gallery_thumbnail_src );
			$product_link.wc_set_variation_attr( 'href', variation.image.full_src );
		} else {
			$form.wc_variations_image_reset();
		}

		window.setTimeout( function() {
			$( window ).trigger( 'resize' );
			$form.wc_maybe_trigger_slide_position_reset( variation );
			$product_gallery.trigger( 'woocommerce_gallery_init_zoom' );
		}, 20 );
	};

	/**
	 * Reset main image to defaults.
	 */
	$.fn.wc_variations_image_reset = function() {
		var $form             = this,
			$product          = $form.closest( '.product' ),
			$product_gallery  = $product.find( '.images' ),
			$gallery_nav      = $product.find( '.flex-control-nav' ),
			$gallery_img      = $gallery_nav.find( 'li:eq(0) img' ),
			$product_img_wrap = $product_gallery.find( '.woocommerce-product-gallery__image, .woocommerce-product-gallery__image--placeholder' ).eq( 0 ),
			$product_img      = $product_img_wrap.find( '.wp-post-image' ),
			$product_link     = $product_img_wrap.find( 'a' ).eq( 0 );

		$product_img.wc_reset_variation_attr( 'src' );
		$product_img.wc_reset_variation_attr( 'width' );
		$product_img.wc_reset_variation_attr( 'height' );
		$product_img.wc_reset_variation_attr( 'srcset' );
		$product_img.wc_reset_variation_attr( 'sizes' );
		$product_img.wc_reset_variation_attr( 'title' );
		$product_img.wc_reset_variation_attr( 'data-caption' );
		$product_img.wc_reset_variation_attr( 'alt' );
		$product_img.wc_reset_variation_attr( 'data-src' );
		$product_img.wc_reset_variation_attr( 'data-large_image' );
		$product_img.wc_reset_variation_attr( 'data-large_image_width' );
		$product_img.wc_reset_variation_attr( 'data-large_image_height' );
		$product_img_wrap.wc_reset_variation_attr( 'data-thumb' );
		$gallery_img.wc_reset_variation_attr( 'src' );
		$product_link.wc_reset_variation_attr( 'href' );
	};

	$(function() {
		if ( typeof wc_add_to_cart_variation_params !== 'undefined' ) {
			$( '.variations_form' ).each( function() {
				$( this ).wc_variation_form();
			});
		}
	});

	/**
	 * Matches inline variation objects to chosen attributes
	 * @deprecated 2.6.9
	 * @type {Object}
	 */
	var wc_variation_form_matcher = {
		find_matching_variations: function( product_variations, settings ) {
			var matching = [];
			for ( var i = 0; i < product_variations.length; i++ ) {
				var variation    = product_variations[i];

				if ( wc_variation_form_matcher.variations_match( variation.attributes, settings ) ) {
					matching.push( variation );
				}
			}
			return matching;
		},
		variations_match: function( attrs1, attrs2 ) {
			var match = true;
			for ( var attr_name in attrs1 ) {
				if ( attrs1.hasOwnProperty( attr_name ) ) {
					var val1 = attrs1[ attr_name ];
					var val2 = attrs2[ attr_name ];
					if ( val1 !== undefined && val2 !== undefined && val1.length !== 0 && val2.length !== 0 && val1 !== val2 ) {
						match = false;
					}
				}
			}
			return match;
		}
	};

})( jQuery, window, document );
;
(function( $ ) {
	"use strict";

	// Check images loaded
	$.fn.JAS_ImagesLoaded = function( callback ) {
		var JAS_Images = function ( src, callback ) {
			var img = new Image;
			img.onload = callback;
			img.src = src;
		}
		var images = this.find( 'img' ).toArray().map( function( el ) {
			return el.src;
		});
		var loaded = 0;
		$( images ).each(function( i, src ) {
			JAS_Images( src, function() {
				loaded++;
				if ( loaded == images.length ) {
					callback();
				}
			})
		})
	}

	// Check is mobile
	var isMobile = function() {
		return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
	}

	// Init slick carousel
	var initCarousel = function() {
		$( '.jas-carousel' ).not( '.slick-initialized' ).slick();

		// Reset the index of image on product variation
		$( 'body' ).on( 'found_variation', '.variations_form', function( ev, variation ) {
			if ( variation && variation.image && variation.image.src && variation.image.src.length > 1 ) {
				$( '.jas-carousel' ).slick( 'slickGoTo', 0 );
				wcInitImageZoom();
			}
		});
	}

	// Init masonry layout
	var initMasonry = function() {
		var el = $( '.jas-masonry' );

		el.each( function( i, val ) {
			var _option = $( this ).data( 'masonry' );

			if ( _option !== undefined ) {
				var _selector = _option.selector,
					_width    = _option.columnWidth,
					_layout   = _option.layoutMode,
					_rtl      = _option.rtl;
					
				$( this ).imagesLoaded( function() {
					$( val ).isotope( {
						layoutMode : _layout,
						itemSelector: _selector,
						percentPosition: true,
						isOriginLeft: _rtl,
						masonry: {
							columnWidth: _width
						}
					} );
				});

				$( '.jas-filter a' ).click( function() {
					var selector = $( this ).data( 'filter' ),
						parent   = $( this ).parents( '.jas-filter' );

					$( val ).isotope({ filter: selector });

					// don't proceed if already selected
					if ( $( this ).hasClass( 'selected' ) ) {
						return false;
					}
					parent.find( '.selected' ).removeClass( 'selected' );
					$( this ).addClass( 'selected' );

					return false;
				});
			}
		});
	}

	// Initialize search form in header.
	var initSearchForm = function() {
		var HS = $( '.header__search' );

		// Open search form
		$( '.sf-open' ).on( 'click', function( e ) {
			e.preventDefault();
			HS.fadeIn();
			HS.find( 'input[type="text"]' ).focus();
		});
		$( '#sf-close' ).on( 'click', function( e ) {
			e.preventDefault();
			HS.fadeOut();
		});
	}

	// Init push on header
	var initPushMenu = function() {
		$( 'a.jas-push-menu-btn' ).on( 'click', function (e) {
			e.preventDefault();
			var mask = '<div class="mask-overlay">';

			$( 'body' ).toggleClass( 'menu-opened' );
			$(mask).hide().appendTo( 'body' ).fadeIn( 'fast' );
			$( '.mask-overlay, .close-menu' ).on( 'click', function() {
				$( 'body' ).removeClass( 'menu-opened' );
				$( '.mask-overlay' ).remove();
			});
		});
	}

	// Accordion mobile menu
	var initDropdownMenu = function() {
		$( '#jas-mobile-menu ul li.has-sub' ).append( '<span class="holder"></span>' );
		$( 'body' ).on('click','.holder',function() {
			var el = $( this ).closest( 'li' );
			if ( el.hasClass( 'open' ) ) {
				el.removeClass( 'open' );
				el.find( 'li' ).removeClass( 'open' );
				el.find( 'ul' ).slideUp();
			} else {
				el.addClass( 'open' );
				el.children( 'ul' ).slideDown();
				el.siblings( 'li' ).children( 'ul' ).slideUp();
				el.siblings( 'li' ).removeClass( 'open' );
				el.siblings( 'li' ).find( 'li' ).removeClass( 'open' );
				el.siblings( 'li' ).find( 'ul' ).slideUp();
			}
		});
	}

	// Sticky menu
	var initStickyMenu = function() {
		if ( JAS_Data_Js != undefined && JAS_Data_Js[ 'header_sticky' ] ) {
			var header          = document.getElementById( 'jas-header' ),
				headerMid       = document.getElementsByClassName( 'header__mid' )[0],
				headerMidHeight = $( '.header__mid' ).outerHeight(),
				headerTopHeight = $( '.header__top' ).outerHeight(),
				headerBotHeight = $( '.header__bot' ).outerHeight(),
				headerHeight    = headerMidHeight + headerTopHeight + headerBotHeight,
				adminBar        = $( '.admin-bar' ).length ? $( '#wpadminbar' ).height() : 0;
				
				if ( headerMid == undefined ) return;

				header.setAttribute( 'style', 'height:' + headerHeight + 'px' );

			$( window ).scroll( function() {
				if ( $( this ).scrollTop() > headerTopHeight ) {
					header.classList.add( 'header-sticky' );
					headerMid.setAttribute( 'style', 'position: fixed;top:' + adminBar + 'px' );
				} else {
					header.classList.remove( 'header-sticky' );
					headerMid.removeAttribute( 'style' );
				}
			});
		}
	}

	// Init right to left menu
	var initRTLMenu = function() {
		var menu = $( '.sub-menu li' ), subMenu = menu.find( ' > .sub-menu');
		menu.on( 'mouseenter', function () {
			if ( subMenu.length ) {
				if ( subMenu.outerWidth() > ( $( window ).outerWidth() - subMenu.offset().left ) ) {
					subMenu.addClass( 'rtl-menu' );
				}
			}
		});
	}

	// Initialize WC quantity adjust.
	var wcQuantityAdjust = function() {
		$( '.quantity .input-text' ).keyup(function() {
			var $button = $( this ).parent().next().next(),
			$max = parseInt($(this).attr( 'max' )),
			$val = parseInt($( this ).val());
			if ($val <= $max) {
				$button.attr( 'data-quantity', $val );	
			}
		});
		$( 'body' ).on( 'click', '.quantity .plus', function( e ) {
			var $input   = $( this ).parent().parent().find( 'input.input-text' ),
				$quantity  = parseInt( $input.attr( 'max' ) ),
				$step      = parseInt( $input.attr( 'step' ) ),
				$val       = parseInt( $input.val() ),
				$button    = $( this ).parent().parent().next().next();
			if ( ( $quantity !== '' ) && ( $quantity <= $val + $step ) ) {
				$( '.quantity .plus' ).css( 'pointer-events', 'none' );
			}
			if ($val + $step > $quantity) return;
			$input.val( $val + $step );

			$input.trigger( 'change' );
			if ( $( '.btn-atc' ).hasClass( 'atc-popup' ) ) {
				$button.attr( 'data-quantity', $val + $step );
			}
		});
		$( 'body' ).on( 'click', '.quantity .minus', function( e ) {
			var $input  = $( this ).parent().parent().find( 'input.input-text' ),
				$step   = parseInt( $input.attr( 'step' ) ),
				$val    = parseInt( $input.val() ) - $step,
				$button = $( this ).parent().parent().next().next();

			if ( $val < $step ) $val = $step;
			$input.val( $val );

			$( '.quantity .plus' ).removeAttr( 'style' );

			$input.trigger( 'change' );
			if ( $( '.btn-atc' ).hasClass( 'atc-popup' ) ) {
				$button.attr( 'data-quantity', $val );
			}
		});
	}

	// Back to top button
	var backToTop = function() {
		var el = $( '#jas-backtop' );
		$( window ).scroll(function() {
			if( $( this ).scrollTop() > $( window ).height() ) {
				el.show();
			} else {
				el.hide();
			}
		});
		el.click( function() {
			$( 'body,html' ).animate({
				scrollTop: 0
			}, 500);
			return false;
		});
	}

	// Init Magnific Popup
	var initMagnificPopup = function() {
		if ( $( '.jas-magnific-image' ).length > 0 ) {
			$( '.jas-magnific-image' ).magnificPopup({
				type: 'image',
				image: {
					verticalFit: true
				},
				mainClass: 'mfp-fade',
				removalDelay: 800,
				callbacks: {
					beforeOpen: function() {
						$( '#jas-wrapper' ).after( '<div class="loader"><div class="loader-inner"></div></div>' );
					},
					open: function() {
						$( '.loader' ).remove();
					},
				}
			});
		}
	}

	// Product quick view
	window._inQuickview = false;
	var initQuickView = function() {
		$( 'body' ).on( 'click', '.btn-quickview', function(e) {
			var _this = $( this ),
				id    = _this.attr( 'data-prod' ),
				data  = { action: 'jas_quickview', product: id };

			$( '#jas-wrapper' ).after( '<div class="loader"><div class="loader-inner"></div></div>' );

			$.post( JASAjaxURL, data, function( response ) {
				$.magnificPopup.open({
					items: {
						src: response,
						type: 'inline',
					},
					mainClass: 'mfp-fade',
					removalDelay: 800
				});

				setTimeout(function() {
					if ( $( '.product-quickview form' ).hasClass( 'variations_form' ) ) {
						$( '.product-quickview form.variations_form' ).wc_variation_form();
						$( '.product-quickview select' ).trigger( 'change' );
					}
				}, 100);

				setTimeout(function() {
					$.PLT.main_product_variation_select();
					$.PLT.main_product_variation_load_default();
					$.PLT.product_bundle_variation_select();
				}, 1000);
				
				$( '.loader' ).remove();

				initCarousel();

				$( '.images' ).imagesLoaded( function() {
					var imgHeight = $( '.product-quickview .images' ).outerHeight();

					$( '.product-quickview .jas-row > div' ).css({
						'height': imgHeight
					});
				});
			});
			e.preventDefault();
			e.stopPropagation();
			window._inQuickview = true;
		});
	}

	// Extra content on single product ( Help & Shipping - Return )
	var wcExtraContent = function() {
		$( 'body' ).on( 'click', '.jas-wc-help', function(e) {
			$( '#jas-wrapper' ).after( '<div class="loader"><div class="loader-inner"></div></div>' );
			
			var data = { action: 'jas_shipping_return' }

			$.post( JASAjaxURL, data, function( response ) {
				$.magnificPopup.open({
					items: {
						src: response
					},
				});
				$( '.loader' ).remove();
			});
			e.preventDefault();
			e.stopPropagation();
		});
	}

	// Init mini cart on header
	var initMiniCart = function() {
		$( 'body' ).on( 'click', '.jas-icon-cart > a', function (e) {
			e.preventDefault();
			var mask = '<div class="mask-overlay">';

			$( 'body' ).toggleClass( 'cart-opened' );
			$( mask ).hide().appendTo( 'body' ).fadeIn( 'fast' );
			$( '.mask-overlay, .close-cart' ).on( 'click', function() {
				$( 'body' ).removeClass( 'cart-opened' );
				$( '.mask-overlay' ).remove();
			});

			$( 'body' ).removeClass( 'popup-opened' );
		});
	}

	// Refesh mini cart on ajax event
	var refreshMiniCart = function( openCart ) {
		openCart = openCart || true;
		$.ajax({
			type: 'POST',
			url: JASAjaxURL,
			dataType: 'json',
			data: { action: 'load_mini_cart' },
			success: function( data ) {
				if ( data.message != null && $( data.message.error ).length > 0 ) {
					$( 'body' ).append( '<div class="woocommerce-error">' + data.message.error + '</div>' );
					$( '.woocommerce-message' ).remove();
				} else {
					var cartContent = $( '.jas-mini-cart .widget_shopping_cart_content' );
					if ( data.cart_html.length > 0 ) {
						cartContent.html( data.cart_html );
					}

					$( '.jas-icon-cart .count' ).text( data.cart_total );
					if (openCart) {
						var mask = '<div class="mask-overlay">';

						$( 'body' ).toggleClass( 'cart-opened' );
						$( mask ).hide().appendTo( 'body' ).fadeIn( 'fast' );
						$( '.mask-overlay, .close-cart' ).on( 'click', function() {
							$( 'body' ).removeClass( 'cart-opened' );
							$( '.mask-overlay' ).remove();
						});	
					}
				}
			}
		});
	}

	// Trigger add to cart button
	var initAddToCart = function() {
		var _input = $( '.quantity input' ), _quantity = _input.attr( 'max' );

		if ( _quantity != '' ) {
			_input.bind( 'keyup mouseup change click', function () {
				if ( parseInt( $( this ).val() ) > parseInt( _quantity ) ) {
					$( '.single_add_to_cart_button' ).addClass( 'disabled' );
				} else {
					$( '.single_add_to_cart_button' ).removeClass( 'disabled' );
				}
			});
		}

		// Single add to cart button
		$( 'body' ).on( 'click', '.atc-slide .single_add_to_cart_button', function() {
			var _this = $( this ), _form = _this.parents( 'form' );
			if ( _this.hasClass( 'disabled' ) || $( '.btn-atc' ).hasClass( 'no-ajax' ) ) return;

			$.ajax({
				type: 'POST',
				url: JASSiteURL + "?wc-ajax=jas-ajax",
				dataType: 'html',
				data: _form.serialize(),
				cache: false,
				headers: { 'cache-control': 'no-cache' },
				beforeSend: function() {
					_this.append( '<i class="fa fa-spinner fa-pulse pa"></i>' );
				},
				success:function() {
					setTimeout( function() {
						$( '.fa-spinner' ).remove();
					}, 480)
				}
			});

			return false;
		});

		// Product bundle add to cart button
		$( 'body' ).on( 'click', '.wpa_wcpb_add_to_cart', function() {
			var _this = $( this ), _form = _this.parents( 'form' );
			if ( _this.hasClass( 'disabled' ) ) return;

			$.ajax({
				type: 'POST',
				url: JASSiteURL + "?wc-ajax=jas-ajax",
				dataType: 'html',
				data: _form.serialize(),
				cache: false,
				headers: { 'cache-control': 'no-cache' },
				beforeSend: function() {
					_this.append( '<i class="fa fa-spinner fa-pulse pa"></i>' );
				},
				success:function() {
					setTimeout( function() {
						$( '.fa-spinner' ).remove();
					}, 480)
				}
			});

			return false;
		});

		if ( $( '.cart-moved' ).length > 0 ) {
			$( '.btn-atc' ).appendTo( '.cart-moved' );
		}
	}

	// Switch wc currency
	var initSwitchCurrency = function() {
		$( 'body' ).on( 'click', '.currency-item', function() {
			var currency = $( this ).data( 'currency' );
			$.cookie( 'jas_currency', currency, { path: '/' });
			location.reload();
			$( document.body ).trigger( 'wc_fragment_refresh' );
		});
	}

	// Open video in popup
	var wcInitPopupVideo = function() {
		$(document).on('click', '.p-video .jas-popup-url', function(event){
			event.preventDefault();
			$.magnificPopup.open({
				disableOn: 0,
			  items: {
				src: $(this).attr('href')
			  },
			  type: 'iframe'
			});
		});
		$(document).on('click', '.p-video .jas-popup-mp4', function(event){
			event.preventDefault();
			$.magnificPopup.open({
				disableOn: 0,
			  items: {
				src: $(this).attr('href')
			  },
			  type: 'inline'
			});
		});
	}

	// Init ajax search
	var wcLiveSearch = function() {
		if ( ! $.fn.autocomplete || ( $( '.jas-ajax-search' ).length === 0 ) ) return;
		
		$( '.jas-ajax-search' ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: JASAjaxURL ,
					dataType: 'json',
					data: {
						key: request.term,
						action: 'jas_claue_live_search'
					},
					success: function( data ) {
						response( data );
					}
				});
			},
			minLength: 2,
			select: function( event, ui ) {
				window.location = ui.item.url;
			},
			open: function() {
				$( this ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-top' );
			},
			close: function() {
				$( this ).removeClass( 'ui-corner-top' ).addClass( 'ui-corner-all' );
			}
		}).data( 'ui-autocomplete' )._renderItem = function( ul, item ) {
			return $( '<li class="oh mt__10 pt__10">' )
			.data( 'ui-autocomplete-item', item )
			.attr( 'data-url', item.url )
			.append( "<img class='ajax-result-item fl' src='" + item.thumb + "'><div class='oh pl__15'><a class='ajax-result-item-name' href='" + item.url + "'>" + item.label + "</a><p>" + item.except + "</p></div>" )
			.appendTo( ul );
		};
	}

	// init ajax load
	var initAjaxLoad = function() {
		var button = $( '.jas-ajax-load' );

		button.each( function( i, val ) {
			var _option = $( this ).data( 'load-more' );

			if ( _option !== undefined ) {
				var page      = _option.page,
					container = _option.container,
					layout    = _option.layout,
					isLoading = false,
					anchor    = $( val ).find( 'a' ),
					next      = $( anchor ).attr( 'href' ),
					i         = 2;

				if ( layout == 'loadmore' ) {
					$( val ).on( 'click', 'a', function( e ) {
						e.preventDefault();
						anchor = $( val ).find( 'a' );
						next   = $( anchor ).attr( 'href' );

						$( anchor ).html( '<i class="fa fa-circle-o-notch fa-spin"></i>' );

						getData();
					});
				} else {
					var animationFrame = function() {
						anchor = $( val ).find( 'a' );
						next   = $( anchor ).attr( 'href' );

						var bottomOffset = $( '.' + container ).offset().top + $( '.' + container ).height() - $( window ).scrollTop();

						if ( bottomOffset < window.innerHeight && bottomOffset > 0 && ! isLoading ) {
							if ( ! next )
								return;
							isLoading = true;
							$( anchor ).html( '<i class="fa fa-circle-o-notch fa-spin"></i>' );

							getData();
						}
					}

					var scrollHandler = function() {
						requestAnimationFrame( animationFrame );
					};

					$( window ).scroll( scrollHandler );
				}

				var getData = function() {
					$.get( next + '', function( data ) {
						var content    = $( '.' + container, data ).wrapInner( '' ).html(),
							newElement = $( '.' + container, data ).find( '.portfolio-item, .product' ),
							col		   = $('.wc-col-switch a.active').length ? $('.wc-col-switch a.active').data('col') : JAS_Data_Js['wc-column'];
							newElement = newElement.removeClass( 'jas-col-md-2 jas-col-md-3 jas-col-md-4 jas-col-md-6' ).addClass( 'jas-col-md-' + col );
						$( content ).imagesLoaded( function() {
							next = $( anchor, data ).attr( 'href' );
							$( '.' + container ).append( newElement ).isotope( 'appended', newElement );
						});

						$( anchor ).text( JAS_Data_Js['load_more'] );

						if ( page > i ) {
							if ( JAS_Data_Js != undefined && JAS_Data_Js[ 'permalink' ] == 'plain' ) {
								var link = next.replace( /paged=+[0-9]+/gi, 'paged=' + ( i + 1 ) );
							} else {
								var link = next.replace( 'page/' + i, 'page/' + ( i + 1 ) );
							}

							$( anchor ).attr( 'href', link );
						} else {
							$( anchor ).text( JAS_Data_Js['no_more_item'] );
							$( anchor ).removeAttr( 'href' ).addClass( 'disabled' );
						}
						isLoading = false;
						i++;
					});
				}
			}
		});

		if ( $( '.yith-wcan' ).length > 0 && button.length > 0 ) {
			$( 'body' ).on( 'click', '.yith-wcan a', function() {
				$( document ).ajaxComplete(function() {
					window.location.reload();
				});
			});
		}
	}

	// Init Scroll Reveal
	var initScrollReveal = function() {
		window.sr = ScrollReveal().reveal( '.jas-animated', {
			duration: 700
		});
	}

	// Init Countdown
	function initCountdown() {
		var $el = $( '.jas-countdown' );

		$el.each( function( i, val ) {
			var _option = $( this ).data( 'time' );

			if ( _option !== undefined ) {
				var _day   = _option.day,
					_month = _option.month,
					_year  = _option.year,
					_end   = _month + ' ' + _day + ', ' + _year + ' 00:00:00';

				$( val ).countdown( {
					date: _end,
					render: function(data) {
						$( this.$el ).html("<div class='pr'><span class='db cw fs__16 mt__10'>" + this.leadingZeros(data.days, 2) + "</span><span class='db'>" + JAS_Data_Js['days'] + "</span></div><div class='pr'><span class='db cw fs__16 mt__10'>" + this.leadingZeros(data.hours, 2) + "</span><span class='db'>" + JAS_Data_Js['hrs'] + "</span></div><div class='pr'><span class='db cw fs__16 mt__10'>" + this.leadingZeros(data.min, 2) + "</span><span class='db'>" + JAS_Data_Js['mins'] + "</span></div><div class='pr'><span class='db cw fs__16 mt__10'>" + this.leadingZeros(data.sec, 2) + "</span><span class='db'>" + JAS_Data_Js['secs'] + "</span></div>");
					}
				});
			}
		});
	}

	// Init wc switch layout
	var wcInitSwitchLayout = function() {
		$( 'body' ).on( 'click', '.wc-col-switch a', function(e) {
			e.preventDefault();

			var _this     = $( this ),
				_col      = _this.data( 'col' ),
				_parent   = _this.closest( '.wc-col-switch' ),
				_products = $( '.products .product' ),
				_sizer    = $( '.products .grid-sizer' );

			if ( _this.hasClass( 'active' ) ) {
				return;
			}

			_parent.find( 'a' ).removeClass( 'active' );
			_this.addClass( 'active' );

			_products.removeClass( 'jas-col-md-2 jas-col-md-3 jas-col-md-4 jas-col-md-6' ).addClass( 'jas-col-md-' + _col );
			_sizer.removeClass( 'size-2 size-3 size-4 size-6 size-12' ).addClass( 'size-' + _col )

			if ( $( '.products' ).hasClass( 'jas-masonry' ) ) {
				initMasonry();
			}
		});
	}

	// Init sidebar filter
	var wcInitSidebarFilter = function() {
		$( 'body' ).on( 'click', '.filter-trigger', function(e) {
			$( '.jas-filter-wrap' ).toggleClass( 'opened' );
			$( '.close-filter' ).on( 'click', function() {
				$( '.jas-filter-wrap' ).removeClass( 'opened' );
			});
			e.preventDefault();
		});
	}

	// Init sidebar filter
	var wcTopSidebarMenu = function() {
		$( 'body' ).on( 'click', '.jas-mobile .shop-top-sidebar .cat-parent > a', function(e) {
			$( this ).parent().toggleClass( 'opened' );
			e.preventDefault();
		});
	}

	// Init product accordion
	function wcAccordion() {
		$( '.wc-accordions .tab-heading' ).click( function( e ) {
			e.preventDefault();

			var _this = $( this );
			var parent = _this.closest( '.wc-accordion' );
			var parent_top = _this.closest( '.wc-accordions' );

			if ( parent.hasClass( 'active' ) ) {
				parent.removeClass( 'active' );
				parent.find( '.entry-content' ).stop( true, true ).slideUp();
			} else {
				parent_top.find( '.wc-accordion' ).removeClass( 'active' );
				parent.addClass( 'active' );
				parent_top.find( '.entry-content' ).stop( true, true ).slideUp();
				parent.find( '.entry-content' ).stop( true, true ).slideDown();
			}
		} );
	}

	// Sticky sidebar for single product layout 3, 4
	function wcStickySidebar() {
		if ( $( '.jas-sidebar-sticky' ).length > 0 && ! isMobile() ) {
			$( '.jas-sidebar-sticky' ).stick_in_parent();
		}
	}

	// Init prettyPhoto for WC 3.0
	function initPrettyPhoto() {
		if ( typeof $.fn.prettyPhoto == "function" ) {
			$( 'a.zoom' ).prettyPhoto({
				hook: 'data-rel',
				social_tools: false,
				theme: 'pp_woocommerce',
				horizontal_padding: 20,
				opacity: 0.8,
				deeplinking: false
			});
			$( 'a[data-rel^="prettyPhoto"]' ).prettyPhoto({
				hook: 'data-rel',
				social_tools: false,
				theme: 'pp_woocommerce',
				horizontal_padding: 20,
				opacity: 0.8,
				deeplinking: false
			});
		}
	}

	// Init openswatch update images
	function initOpenswatch() {
		$( document.body ).off( 'openswatch_update_images' ).bind( 'openswatch_update_images',function( event, data ) {
			var data_html = data.html;
			var productId = data.productId;

			$( '#product-' + productId + ' .single-product-thumbnail' ).html( data_html );

			setTimeout(function() {
				initCarousel();
				initPrettyPhoto();
			}, 10 );
		});
		$( 'body' ).on( 'click', '.product-list-color-swatch a', function() {
			var src = $( this ).data( 'thumb' );
			if ( src != '' ) {
				$( this ).closest( '.product' ).find( 'img.wp-post-image' ).first().attr( 'src', src );
				$( this ).closest( '.product' ).find( 'img.wp-post-image' ).first().attr( 'srcset', src );
			}
		});
	}

	// Sticky add to cart function
	var wcStickyAddtocart = function() {
		var cart_button = $( '.jas-wc-single .single_add_to_cart_button' ),
			sticky_dom  = $( '.jas-sticky-atc' );

		if ( cart_button.length == 0 || sticky_dom == 0 )
			return;

		var footer      = $( '#jas-footer' ),
			cart_button = $( '.jas-wc-single .single_add_to_cart_button' ),
			adminBar    = $( '.admin-bar' ).length ? $( '#wpadminbar' ).height() : 0,
			cartTop     = cart_button.offset().top + cart_button.height() - adminBar,
			fh          = footer.height(),
			wh          = $( window ).height(),
			dh          = $( document ).height(),
			ww          = $( window ).width();

		var AddtocartFrame = function() {
			var scrollTop = $( window ).scrollTop(),
				max       = footer.offset().top - sticky_dom.height() - 15,
				top       = max - scrollTop;

			if ( scrollTop > cartTop ) {
				sticky_dom.slideDown();
			} else {
				sticky_dom.slideUp();
			}

			if ( scrollTop + wh < dh - fh ) {
				sticky_dom.css({
					'top': 'auto',
					'bottom': ( ww > 480 ) ? 10 : 0
				} );
			} else {
				sticky_dom.css( {
					'bottom': 'auto',
					'top': top
				} );
			}
		};

		var AddtocartScroll = function() {
			requestAnimationFrame( AddtocartFrame );
		};

		$( window ).scroll( AddtocartScroll );

		$( 'body' ).on( 'click', '.jas-sticky-atc .button.disabled', function( e ) {
			e.preventDefault();
			$( 'html, body' ).animate({
				scrollTop: $( 'form.variations_form' ).offset().top - adminBar
			}, 800 );
		});

		$( '.btn-atc .variations_form select' ).on( 'change', function() {
			var attr_name        = $( this ).attr( 'id' ),
				selected         = $( this ).val(),
				select_stick     = $( '.jas-sticky-atc .variations_form select#' + attr_name );
				
				select_stick.val( selected ).change();
		});
	}

	// Preloader
	function initPreLoader() {
		var loader = $( '.preloader' );

		if ( loader.length ) {
			$( window ).on( 'pageshow', function( event ) {
				if ( event.originalEvent != undefined && event.originalEvent.persisted ) {
					loader.fadeIn( 500, function() {
						loader.hide();
						loader.children().hide();
					});
				}
			});
				
			$( window ).on( 'beforeunload', function() {
				loader.fadeIn( 500, function() {
					loader.children().fadeIn( 100 )
				});
			});

			loader.fadeOut( 800 );
			loader.children().fadeOut();
		}
	}

	// Custom 3rd-party plugin
	function customThirdParties() {
		// Reinit carousel on VC tabs
		$( 'body' ).on( 'click', '.vc_tta-panel-title a', function( e ) {
			if ( $( '.jas-carousel' ).length > 0 ) {
				$( '.jas-carousel' ).slick( 'unslick' );
				
				setTimeout( function() {
				   $( '.jas-carousel' ).not( '.slick-initialized' ).slick();
				}, 50);
			}
		});

		/**
		 * Sets product images for the chosen variation
		 */
		$.fn.wc_variations_image_update = function( variation ) {
			var $form             = this,
				$product          = $form.closest( '.product' ),
				$product_gallery  = $product.find( '.images' ),
				$gallery_img      = $product.find( '.p-nav .slick-slide[data-slick-index="0"] img' ),
				$product_img_wrap = $product_gallery.find( '.woocommerce-product-gallery__image, .woocommerce-product-gallery__image--placeholder' ).eq( 0 ),
				$product_img      = $product_img_wrap.find( '.wp-post-image' ),
				$product_link     = $product_img_wrap.find( 'a' ).eq( 0 );

			if ( variation && variation.image && variation.image.src && variation.image.src.length > 1 ) {
				var exist = $product.find('.p-nav img[data-large_image="'+variation.image.full_src+'"]');
				if (exist.length > 0) {
					exist.trigger('click');	
				} else {
					$product_img.wc_set_variation_attr( 'src', variation.image.src );
					$product_img.wc_set_variation_attr( 'height', variation.image.src_h );
					$product_img.wc_set_variation_attr( 'width', variation.image.src_w );
					$product_img.wc_set_variation_attr( 'srcset', variation.image.srcset );
					$product_img.wc_set_variation_attr( 'sizes', variation.image.sizes );
					$product_img.wc_set_variation_attr( 'title', variation.image.title );
					$product_img.wc_set_variation_attr( 'alt', variation.image.alt );
					$product_img.wc_set_variation_attr( 'data-src', variation.image.full_src );
					$product_img.wc_set_variation_attr( 'data-large_image', variation.image.full_src );
					$product_img.wc_set_variation_attr( 'data-large_image_width', variation.image.full_src_w );
					$product_img.wc_set_variation_attr( 'data-large_image_height', variation.image.full_src_h );
					$product_img_wrap.wc_set_variation_attr( 'data-thumb', variation.image.src );
					//$gallery_img.wc_set_variation_attr( 'src', variation.image.thumb_src );
					$product_link.wc_set_variation_attr( 'href', variation.image.full_src );
					window.setTimeout( function() {
						$product_img_wrap.find('.zoomImg').remove();
						$product_img_wrap.zoom();
					}, 1500);
				}
			} else {
				$product_img.wc_reset_variation_attr( 'src' );
				$product_img.wc_reset_variation_attr( 'width' );
				$product_img.wc_reset_variation_attr( 'height' );
				$product_img.wc_reset_variation_attr( 'srcset' );
				$product_img.wc_reset_variation_attr( 'sizes' );
				$product_img.wc_reset_variation_attr( 'title' );
				$product_img.wc_reset_variation_attr( 'alt' );
				$product_img.wc_reset_variation_attr( 'data-src' );
				$product_img.wc_reset_variation_attr( 'data-large_image' );
				$product_img.wc_reset_variation_attr( 'data-large_image_width' );
				$product_img.wc_reset_variation_attr( 'data-large_image_height' );
				$product_img_wrap.wc_reset_variation_attr( 'data-thumb' );
				$gallery_img.wc_reset_variation_attr( 'src' );
				$product_link.wc_reset_variation_attr( 'href' );
			}

			window.setTimeout( function() {
				$product_gallery.trigger( 'woocommerce_gallery_init_zoom' );
				$form.wc_maybe_trigger_slide_position_reset( variation );
				$( window ).trigger( 'resize' );
			}, 10 );
		};
	}

	// Init image zoom
	var wcInitImageZoom = function() {
		if ( $( '.jas-image-zoom' ).length > 0 && ! window._inQuickview ) {
			
			var img = $( '.jas-image-zoom' ).each(function(index, el){
				if (!$(el).hasClass('zoomed')) {
					$(el).zoom({
						touch: false
					});
					$(el).addClass('zoomed');
				}
			});
		}
	}

	// Add to cart popup
	function wcPopupAddtocart() {
		$( 'body' ).on( 'click', '.atc-popup .single_add_to_cart_button', function( e ) {
			var _btn  = $( this ),
				_form = $( 'form.cart' ),
				product_data = {},
				ajaxs = [],
				ajaxCall = function(data, step) { // step: next call ajax
					if (typeof step != 'undefined') {
						var url = JASAjaxURL; 	
					} else {
						var url = JASAjaxURL+'?claucartf5=1'; 
					}
					
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							action: 'jas_claue_popup_update_cart',
							product_data: data
						},
						beforeSend: function() {
							_btn.append( '<i class="fa fa-spinner fa-pulse pa"></i>' );
						},
						success: function( response, status, jqXHR ) {
							if (typeof step != 'undefined') {
								var next = step + 1;
								if (typeof ajaxs[next] != 'undefined') {
									if (next == ajaxs.length - 1) {
										ajaxCall(ajaxs[next]);	
									} else {
										ajaxCall(ajaxs[next], next);
									}
								}
							} else {
								$( '.fa-spinner' ).remove();
								if ( typeof response == 'object' ) {
									$.magnificPopup.open({
										items: {
											src: '<div class="product-quickview cart__popup pr">' + response.output + '</div>',
											type: 'inline'
										},
										mainClass: 'mfp-fade',
										removalDelay: 800,
									});
								} else { // error label
									$('body').append( response );
								}

								$( 'body' ).addClass( 'popup-opened' );
								setTimeout( function() {
									$( '.mask-overlay' ).remove();
									$( 'body' ).removeClass( 'cart-opened' );
								}, 1000)
							}
						}
					});
				};

			if ( _btn.hasClass( 'disabled' ) || $( '.btn-atc' ).hasClass( 'no-ajax' ) ) return;

			if (  _btn.parents( 'form' ).hasClass( 'variations_form' ) ) {
				var variation_id = parseInt( _form.find( '[name=variation_id]' ).val() ),
					product_id   = parseInt( _form.find('[name=add-to-cart]' ).val() ),
					quantity     = parseInt( _btn.next().attr( 'data-quantity' ) );
					

				if ( variation_id ) {
					var attributes_select = _form.find( '.variations select' ),
						attributes = {};

					attributes_select.each( function() {
						attributes[ $( this ).data( 'attribute_name' ) ] = $( this ).val();
					});

					attributes = JSON.stringify( attributes );
					product_data['attributes']   = attributes;
					product_data['variation_id'] = variation_id;
				}

				product_data['product_id'] = product_id;
				product_data['quantity']   = quantity || 1;

			} else if (_btn.parents( 'form' ).hasClass( 'group_table' )) {
				var formGroupData = jQuery('.group_table').serializeArray();
				jQuery.each(formGroupData, function(index, el){
					if (el.name.indexOf('quantity[') == 0) {
						var pid = el.name.replace('quantity[', '').replace(']', ''),
						product_data = {};
						product_data['product_id']   = pid;
						product_data['variation_id'] = 0;
						product_data['quantity']     = el.value || 1;
						product_data = JSON.stringify( product_data );
						ajaxs.push(product_data);
					}
				});
				if (ajaxs.length > 0) {
					ajaxCall(ajaxs[0], 0);
				}
				e.preventDefault();
				return;
			} else {
				var product_id   = _btn.data( 'product_id' ),
					quantity     = _btn.attr( 'data-quantity' ),
					product_data = {};

				product_data['product_id']   = product_id;
				product_data['variation_id'] = 0;
				product_data['quantity']     = quantity || 1;
			}

			product_data = JSON.stringify( product_data );

			ajaxCall(product_data);

			e.preventDefault();
		});

		$( 'body' ).on( 'click', '.modal_btn_add_to_cart', function( e ) {
			e.preventDefault();

			var _btn = $( this );
			if ( _btn.hasClass( 'product_type_variable' ) ) { return; }

			var product_id = _btn.data( 'product_id' ), quantity = 1, product_data = {};

			product_data['product_id']   = product_id;
			product_data['variation_id'] = 0;
			product_data['quantity']     = quantity;

			product_data = JSON.stringify( product_data );
			
			$.ajax({
				url: JASAjaxURL,
				type: 'POST',
				data: {
					action: 'jas_claue_popup_update_cart',
					product_data: product_data
				},
				beforeSend: function() {
					$( '.cart__popup' ).addClass( 'loading' );
				},
				success: function( response, status, jqXHR ) {
					$( '.cart__popup' ).html( response.output );
					$( '.cart__popup' ).removeClass( 'loading' );
				}
			});
		})

		function wcPopupUpdateAjax( cart_key, new_qty, undo_item ) {
			return $.ajax({
				url: JASAjaxURL,
				type: 'POST',
				data: {
					action: 'jas_claue_popup_update_ajax',
					cart_key: cart_key, 
					new_qty: new_qty,
					undo_item: undo_item || false
				}
			});
		}
		function wcPopupUpdateCart( _this, new_qty ) {
			var _pwrap	  = _this.parents( '.cart__popup-item' ),
				_pdata	  = _pwrap.data( 'cart-item' ),
				_cartkey  = _pdata.key,
				_pname 	  = _pdata.pname,
				_qtyinput = _pwrap.find( '.cart__popup-qty--input' );

			$( '.cart__popup' ).addClass( 'loading' );

			wcPopupUpdateAjax( _cartkey, new_qty ).done( function( response, status, jqXHR ) {
				if ( jqXHR.getResponseHeader( 'content-type' ).indexOf( 'text/html' ) >= 0 ) {
					_qtyinput.val( focus_qty );
				} else {
					_pwrap.find( '.cart__popup-total' ).html( response.ptotal );
					$( '.cart__popup-stotal' ).html( response.cart_total );
					
					_qtyinput.val( new_qty );
				}
				$( '.cart__popup' ).removeClass( 'loading' );
			});
		}
		
		$( 'body' ).on( 'change','.cart__popup-qty--input', function( e ) {
			var _this   = $( this ),
				new_qty = parseInt( _this.val( ) ),
				_step   = parseInt( _this.attr( 'step' ) ),
				_min    = parseInt( _this.attr( 'min' ) ),
				_max    = parseInt( _this.attr( 'max' ) ),
				invalid = false;

			if ( new_qty === 0 ) {
				_this.parents( '.cart__popup-item' ).find( '.cart__popup-remove' ).trigger( 'click' );
				return;

			} else if ( isNaN( new_qty ) || new_qty < 0 ) {
				invalid = true;

			} else if ( new_qty > _max && _max > 0 ) {
				alert( 'Maximum product is: ' + _max );
				invalid = true;

			} else if ( new_qty < _min ) {
				invalid = true;

			} else if ( ( new_qty % _step ) !== 0 ) {
				alert( 'Quantity can only be purchased in multiple of ' + _step );
				invalid = true;

			} else {
				wcPopupUpdateCart( _this, new_qty );
			}

			if ( invalid === true ) {
				_this.val( focus_qty );
			}
		});

		$( 'body' ).on( 'click', '.cart__popup-qty' ,function() {
			var _this     = $(this),
				_qty      = _this.siblings( '.cart__popup-qty--input' ),
				_qtyinput = parseInt( _qty.val() ),
				_step     = parseInt( _qty.attr( 'step' ) ),
				_min      = parseInt( _qty.attr( 'min' ) ),
				_max      = parseInt( _qty.attr( 'max' ) );

			_qty.trigger( 'focusin' );

			if ( _this.hasClass( 'cart__popup-qty--plus' ) ) {
				var _newqty = _qtyinput + _step;
			
				if ( _newqty > _max && _max > 0 ) {
					alert( 'Maximum Quantity: ' + _max );
					return;
				}
			} else if ( _this.hasClass( 'cart__popup-qty--minus' ) ) {
				var _newqty = _qtyinput - _step;
				if ( _newqty === 0 ) {
					_this.parents( '.cart__popup-item' ).find( '.cart__popup-remove' ).trigger( 'click' );
					return;
				} else if ( _newqty < _min ) {
					return;
				} else if ( _qtyinput < 0 ) {
					alert( 'Invalid' );
					return;
				}
			}
			wcPopupUpdateCart( _this, _newqty );
		})

		// Remove item from the cart
		$( 'body' ).on( 'click', '.cart__popup-remove',function() {
			$( '.cart__popup' ).addClass( 'loading' );

			var _this 	 = $( this ),
				_pwrap   = _this.parents( '.cart__popup-item' ),
				_pdata	 = _pwrap.data( 'cart-item' ),
				_ckey    = _pdata.key,
				new_qty	 = 0,
				_pname   = _pdata.pname;

			wcPopupUpdateAjax( _ckey, 0 ).done( function( response ) {
				_pwrap.after( '<div class="cart__popup-empty center-xs mt__15 mb__15">' + _pname + ' ' + JAS_Data_Js['popup_remove'] + ' <span class="cart__popup-undo fwb cb">' + JAS_Data_Js['popup_undo'] + '</span></div>' )
				  .hide()
				  .attr( 'class', 'cart__popup-undo--active' );

		
				$( '.cart__popup-stotal' ).html( response.cart_total );
				$( '.cart__popup' ).removeClass( 'loading' );
			});
		});

		// Undo the product removed
		$( 'body' ).on( 'click', '.cart__popup-undo', function() {
			var _this 	= $( this ),
				_pwrap  = _this.parents( '.cart__popup-empty' ),
				_pwraps = _pwrap.prev( '.cart__popup-undo--active' );

			if ( _pwraps.length === 0 ) {
				return;
			}
			$( '.cart__popup' ).addClass( 'loading' );

			var _data = _pwraps.data( 'cart-item' ), _cartkey = _data.key, _pid = _data._pid, qty = 10;

			wcPopupUpdateAjax( _cartkey, qty, true ).done( function( response ) {
				_pwraps.attr( 'class','cart__popup-item flex middle-xs' )
					.show()
					.find( '.cart__popup-quantity input' ).val( response.quantity );

				_pwrap.remove();

				$( '.cart__popup' ).removeClass( 'loading' );
				$( '.cart__popup-stotal' ).html( response.cart_total );
			});
		});

		// Update cart number with custom add to cart and show mini cart with default add to cart
		$( document ).ajaxComplete( function( event, xhr, settings ) {
			if ( settings.url.indexOf( 'claucartf5' ) > 0 ) {
				refreshMiniCart( false );
			}
			if ( settings.url.indexOf( 'jas-ajax' ) > 0 || settings.url.indexOf( 'add_to_cart' ) > 0 ) { // catch the default add to cart ajax
				$.magnificPopup.close();
				if ( $( 'body').hasClass( 'jan-atc-behavior-popup' ) ) { // popup
					// Need call an ajax request to get custom cart content in popup. 
					$.ajax({
						url: JASAjaxURL,
						type: 'POST',
						data: {
							action: 'jas_claue_popup_content_ajax',
						},
						success: function(response) {
							$.magnificPopup.open({
								items: {
									src: '<div class="product-quickview cart__popup pr">' + response + '</div>',
									type: 'inline'
								},
								mainClass: 'mfp-fade',
								removalDelay: 800
							});
						}
					});
				} else { // slide
					refreshMiniCart();		
				}
			}
		});
	}

	/**
	 * DOMready event.
	 */
	$( document ).ready( function() {
		initCarousel();
		initMasonry();
		initSearchForm();
		initDropdownMenu();
		initPushMenu();
		initRTLMenu();
		initQuickView();
		initAddToCart();
		initMiniCart();
		initAjaxLoad();
		initScrollReveal();
		initCountdown();
		initOpenswatch();
		backToTop();
		initMagnificPopup();
		initSwitchCurrency();
		initPreLoader();
		wcInitPopupVideo();
		wcLiveSearch();
		wcInitSwitchLayout();
		wcQuantityAdjust();
		wcExtraContent();
		wcInitSidebarFilter();
		wcTopSidebarMenu();
		wcAccordion();
		wcInitImageZoom();
		wcStickyAddtocart();
		wcPopupAddtocart();
		initPrettyPhoto();
		customThirdParties();
	});
	$( window ).load( function() {
		initStickyMenu();
		wcStickySidebar();
	});

})( jQuery );;
;
var JASAjaxURL = "https://clone.wr1.com/wp-admin/admin-ajax.php";var JASSiteURL = "https://clone.wr1.com/index.php";;
/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD (Register as an anonymous module)
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {},
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling $.cookie().
			cookies = document.cookie ? document.cookie.split('; ') : [],
			i = 0,
			l = cookies.length;

		for (; i < l; i++) {
			var parts = cookies[i].split('='),
				name = decode(parts.shift()),
				cookie = parts.join('=');

			if (key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));
;
/**
 * WordPress inline HTML embed
 *
 * @since 4.4.0
 *
 * This file cannot have ampersands in it. This is to ensure
 * it can be embedded in older versions of WordPress.
 * See https://core.trac.wordpress.org/changeset/35708.
 */
(function ( window, document ) {
	'use strict';

	var supportedBrowser = false,
		loaded = false;

		if ( document.querySelector ) {
			if ( window.addEventListener ) {
				supportedBrowser = true;
			}
		}

	/** @namespace wp */
	window.wp = window.wp || {};

	if ( !! window.wp.receiveEmbedMessage ) {
		return;
	}

	window.wp.receiveEmbedMessage = function( e ) {
		var data = e.data;
		if ( ! ( data.secret || data.message || data.value ) ) {
			return;
		}

		if ( /[^a-zA-Z0-9]/.test( data.secret ) ) {
			return;
		}

		var iframes = document.querySelectorAll( 'iframe[data-secret="' + data.secret + '"]' ),
			blockquotes = document.querySelectorAll( 'blockquote[data-secret="' + data.secret + '"]' ),
			i, source, height, sourceURL, targetURL;

		for ( i = 0; i < blockquotes.length; i++ ) {
			blockquotes[ i ].style.display = 'none';
		}

		for ( i = 0; i < iframes.length; i++ ) {
			source = iframes[ i ];

			if ( e.source !== source.contentWindow ) {
				continue;
			}

			source.removeAttribute( 'style' );

			/* Resize the iframe on request. */
			if ( 'height' === data.message ) {
				height = parseInt( data.value, 10 );
				if ( height > 1000 ) {
					height = 1000;
				} else if ( ~~height < 200 ) {
					height = 200;
				}

				source.height = height;
			}

			/* Link to a specific URL on request. */
			if ( 'link' === data.message ) {
				sourceURL = document.createElement( 'a' );
				targetURL = document.createElement( 'a' );

				sourceURL.href = source.getAttribute( 'src' );
				targetURL.href = data.value;

				/* Only continue if link hostname matches iframe's hostname. */
				if ( targetURL.host === sourceURL.host ) {
					if ( document.activeElement === source ) {
						window.top.location.href = data.value;
					}
				}
			}
		}
	};

	function onLoad() {
		if ( loaded ) {
			return;
		}

		loaded = true;

		var isIE10 = -1 !== navigator.appVersion.indexOf( 'MSIE 10' ),
			isIE11 = !!navigator.userAgent.match( /Trident.*rv:11\./ ),
			iframes = document.querySelectorAll( 'iframe.wp-embedded-content' ),
			iframeClone, i, source, secret;

		for ( i = 0; i < iframes.length; i++ ) {
			source = iframes[ i ];

			if ( ! source.getAttribute( 'data-secret' ) ) {
				/* Add secret to iframe */
				secret = Math.random().toString( 36 ).substr( 2, 10 );
				source.src += '#?secret=' + secret;
				source.setAttribute( 'data-secret', secret );
			}

			/* Remove security attribute from iframes in IE10 and IE11. */
			if ( ( isIE10 || isIE11 ) ) {
				iframeClone = source.cloneNode( true );
				iframeClone.removeAttribute( 'security' );
				source.parentNode.replaceChild( iframeClone, source );
			}
		}
	}

	if ( supportedBrowser ) {
		window.addEventListener( 'message', window.wp.receiveEmbedMessage, false );
		document.addEventListener( 'DOMContentLoaded', onLoad, false );
		window.addEventListener( 'load', onLoad, false );
	}
})( window, document );
;
function vc_js(){vc_toggleBehaviour(),vc_tabsBehaviour(),vc_accordionBehaviour(),vc_teaserGrid(),vc_carouselBehaviour(),vc_slidersBehaviour(),vc_prettyPhoto(),vc_googleplus(),vc_pinterest(),vc_progress_bar(),vc_plugin_flexslider(),vc_google_fonts(),vc_gridBehaviour(),vc_rowBehaviour(),vc_prepareHoverBox(),vc_googleMapsPointer(),vc_ttaActivation(),jQuery(document).trigger("vc_js"),window.setTimeout(vc_waypoints,500)}function getSizeName(){var screen_w=jQuery(window).width();return 1170<screen_w?"desktop_wide":960<screen_w&&1169>screen_w?"desktop":768<screen_w&&959>screen_w?"tablet":300<screen_w&&767>screen_w?"mobile":300>screen_w?"mobile_portrait":""}function loadScript(url,$obj,callback){var script=document.createElement("script");script.type="text/javascript",script.readyState&&(script.onreadystatechange=function(){"loaded"!==script.readyState&&"complete"!==script.readyState||(script.onreadystatechange=null,callback())}),script.src=url,$obj.get(0).appendChild(script)}function vc_ttaActivation(){jQuery("[data-vc-accordion]").on("show.vc.accordion",function(e){var $=window.jQuery,ui={};ui.newPanel=$(this).data("vc.accordion").getTarget(),window.wpb_prepare_tab_content(e,ui)})}function vc_accordionActivate(event,ui){if(ui.newPanel.length&&ui.newHeader.length){var $pie_charts=ui.newPanel.find(".vc_pie_chart:not(.vc_ready)"),$round_charts=ui.newPanel.find(".vc_round-chart"),$line_charts=ui.newPanel.find(".vc_line-chart"),$carousel=ui.newPanel.find('[data-ride="vc_carousel"]');void 0!==jQuery.fn.isotope&&ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"),ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),vc_carouselBehaviour(ui.newPanel),vc_plugin_flexslider(ui.newPanel),$pie_charts.length&&jQuery.fn.vcChat&&$pie_charts.vcChat(),$round_charts.length&&jQuery.fn.vcRoundChart&&$round_charts.vcRoundChart({reload:!1}),$line_charts.length&&jQuery.fn.vcLineChart&&$line_charts.vcLineChart({reload:!1}),$carousel.length&&jQuery.fn.carousel&&$carousel.carousel("resizeAction"),ui.newPanel.parents(".isotope").length&&ui.newPanel.parents(".isotope").each(function(){jQuery(this).isotope("layout")})}}function initVideoBackgrounds(){return window.console&&window.console.warn&&window.console.warn("this function is deprecated use vc_initVideoBackgrounds"),vc_initVideoBackgrounds()}function vc_initVideoBackgrounds(){jQuery("[data-vc-video-bg]").each(function(){var youtubeUrl,youtubeId,$element=jQuery(this);$element.data("vcVideoBg")?(youtubeUrl=$element.data("vcVideoBg"),youtubeId=vcExtractYoutubeId(youtubeUrl),youtubeId&&($element.find(".vc_video-bg").remove(),insertYoutubeVideoAsBackground($element,youtubeId)),jQuery(window).on("grid:items:added",function(event,$grid){$element.has($grid).length&&vcResizeVideoBackground($element)})):$element.find(".vc_video-bg").remove()})}function insertYoutubeVideoAsBackground($element,youtubeId,counter){if("undefined"==typeof YT||void 0===YT.Player)return 100<(counter=void 0===counter?0:counter)?void console.warn("Too many attempts to load YouTube api"):void setTimeout(function(){insertYoutubeVideoAsBackground($element,youtubeId,counter++)},100);var $container=$element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");new YT.Player($container[0],{width:"100%",height:"100%",videoId:youtubeId,playerVars:{playlist:youtubeId,iv_load_policy:3,enablejsapi:1,disablekb:1,autoplay:1,controls:0,showinfo:0,rel:0,loop:1,wmode:"transparent"},events:{onReady:function(event){event.target.mute().setLoop(!0)}}}),vcResizeVideoBackground($element),jQuery(window).bind("resize",function(){vcResizeVideoBackground($element)})}function vcResizeVideoBackground($element){var iframeW,iframeH,marginLeft,marginTop,containerW=$element.innerWidth(),containerH=$element.innerHeight();containerW/containerH<16/9?(iframeW=containerH*(16/9),iframeH=containerH,marginLeft=-Math.round((iframeW-containerW)/2)+"px",marginTop=-Math.round((iframeH-containerH)/2)+"px",iframeW+="px",iframeH+="px"):(iframeW=containerW,iframeH=containerW*(9/16),marginTop=-Math.round((iframeH-containerH)/2)+"px",marginLeft=-Math.round((iframeW-containerW)/2)+"px",iframeW+="px",iframeH+="px"),$element.find(".vc_video-bg iframe").css({maxWidth:"1000%",marginLeft:marginLeft,marginTop:marginTop,width:iframeW,height:iframeH})}function vcExtractYoutubeId(url){if(void 0===url)return!1;var id=url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);return null!==id&&id[1]}function vc_googleMapsPointer(){var $=window.jQuery,$wpbGmapsWidget=$(".wpb_gmaps_widget");$wpbGmapsWidget.click(function(){$("iframe",this).css("pointer-events","auto")}),$wpbGmapsWidget.mouseleave(function(){$("iframe",this).css("pointer-events","none")}),$(".wpb_gmaps_widget iframe").css("pointer-events","none")}function vc_setHoverBoxPerspective(hoverBox){hoverBox.each(function(){var $this=jQuery(this),width=$this.width(),perspective=4*width+"px";$this.css("perspective",perspective)})}function vc_setHoverBoxHeight(hoverBox){hoverBox.each(function(){var $this=jQuery(this),hoverBoxInner=$this.find(".vc-hoverbox-inner");hoverBoxInner.css("min-height",0);var frontHeight=$this.find(".vc-hoverbox-front-inner").outerHeight(),backHeight=$this.find(".vc-hoverbox-back-inner").outerHeight(),hoverBoxHeight=frontHeight>backHeight?frontHeight:backHeight;hoverBoxHeight<250&&(hoverBoxHeight=250),hoverBoxInner.css("min-height",hoverBoxHeight+"px")})}function vc_prepareHoverBox(){var hoverBox=jQuery(".vc-hoverbox");vc_setHoverBoxHeight(hoverBox),vc_setHoverBoxPerspective(hoverBox)}document.documentElement.className+=" js_active ",document.documentElement.className+="ontouchstart"in document.documentElement?" vc_mobile ":" vc_desktop ",function(){for(var prefix=["-webkit-","-moz-","-ms-","-o-",""],i=0;i<prefix.length;i++)prefix[i]+"transform"in document.documentElement.style&&(document.documentElement.className+=" vc_transform ")}(),"function"!=typeof window.vc_plugin_flexslider&&(window.vc_plugin_flexslider=function($parent){($parent?$parent.find(".wpb_flexslider"):jQuery(".wpb_flexslider")).each(function(){var this_element=jQuery(this),sliderTimeout=1e3*parseInt(this_element.attr("data-interval")),sliderFx=this_element.attr("data-flex_fx"),slideshow=!0;0===sliderTimeout&&(slideshow=!1),this_element.is(":visible")&&this_element.flexslider({animation:sliderFx,slideshow:slideshow,slideshowSpeed:sliderTimeout,sliderSpeed:800,smoothHeight:!0})})}),"function"!=typeof window.vc_googleplus&&(window.vc_googleplus=function(){0<jQuery(".wpb_googleplus").length&&function(){var po=document.createElement("script");po.type="text/javascript",po.async=!0,po.src="//apis.google.com/js/plusone.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(po,s)}()}),"function"!=typeof window.vc_pinterest&&(window.vc_pinterest=function(){0<jQuery(".wpb_pinterest").length&&function(){var po=document.createElement("script");po.type="text/javascript",po.async=!0,po.src="//assets.pinterest.com/js/pinit.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(po,s)}()}),"function"!=typeof window.vc_progress_bar&&(window.vc_progress_bar=function(){void 0!==jQuery.fn.waypoint&&jQuery(".vc_progress_bar").waypoint(function(){jQuery(this).find(".vc_single_bar").each(function(index){var $this=jQuery(this),bar=$this.find(".vc_bar"),val=bar.data("percentage-value");setTimeout(function(){bar.css({width:val+"%"})},200*index)})},{offset:"85%"})}),"function"!=typeof window.vc_waypoints&&(window.vc_waypoints=function(){void 0!==jQuery.fn.waypoint&&jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function(){jQuery(this).addClass("wpb_start_animation animated")},{offset:"85%"})}),"function"!=typeof window.vc_toggleBehaviour&&(window.vc_toggleBehaviour=function($el){function event(e){e&&e.preventDefault&&e.preventDefault();var title=jQuery(this),element=title.closest(".vc_toggle"),content=element.find(".vc_toggle_content");element.hasClass("vc_toggle_active")?content.slideUp({duration:300,complete:function(){element.removeClass("vc_toggle_active")}}):content.slideDown({duration:300,complete:function(){element.addClass("vc_toggle_active")}})}$el?$el.hasClass("vc_toggle_title")?$el.unbind("click").click(event):$el.find(".vc_toggle_title").unbind("click").click(event):jQuery(".vc_toggle_title").unbind("click").on("click",event)}),"function"!=typeof window.vc_tabsBehaviour&&(window.vc_tabsBehaviour=function($tab){if(jQuery.ui){var $call=$tab||jQuery(".wpb_tabs, .wpb_tour"),ver=jQuery.ui&&jQuery.ui.version?jQuery.ui.version.split("."):"1.10",old_version=1===parseInt(ver[0])&&9>parseInt(ver[1]);$call.each(function(index){var $tabs,interval=jQuery(this).attr("data-interval"),tabs_array=[];if($tabs=jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({show:function(event,ui){wpb_prepare_tab_content(event,ui)},beforeActivate:function(event,ui){1!==ui.newPanel.index()&&ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")},activate:function(event,ui){wpb_prepare_tab_content(event,ui)}}),interval&&0<interval)try{$tabs.tabs("rotate",1e3*interval)}catch(e){window.console&&window.console.log&&console.log(e)}jQuery(this).find(".wpb_tab").each(function(){tabs_array.push(this.id)}),jQuery(this).find(".wpb_tabs_nav li").click(function(e){return e.preventDefault(),old_version?$tabs.tabs("select",jQuery("a",this).attr("href")):$tabs.tabs("option","active",jQuery(this).index()),!1}),jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function(e){if(e.preventDefault(),old_version){var index=$tabs.tabs("option","selected");jQuery(this).parent().hasClass("wpb_next_slide")?index++:index--,0>index?index=$tabs.tabs("length")-1:index>=$tabs.tabs("length")&&(index=0),$tabs.tabs("select",index)}else{var index=$tabs.tabs("option","active"),length=$tabs.find(".wpb_tab").length;index=jQuery(this).parent().hasClass("wpb_next_slide")?index+1>=length?0:index+1:0>index-1?length-1:index-1,$tabs.tabs("option","active",index)}})})}}),"function"!=typeof window.vc_accordionBehaviour&&(window.vc_accordionBehaviour=function(){jQuery(".wpb_accordion").each(function(index){var $tabs,$this=jQuery(this),active_tab=($this.attr("data-interval"),!isNaN(jQuery(this).data("active-tab"))&&0<parseInt($this.data("active-tab"))&&parseInt($this.data("active-tab"))-1),collapsible=!1===active_tab||"yes"===$this.data("collapsible");$tabs=$this.find(".wpb_accordion_wrapper").accordion({header:"> div > h3",autoHeight:!1,heightStyle:"content",active:active_tab,collapsible:collapsible,navigation:!0,activate:vc_accordionActivate,change:function(event,ui){void 0!==jQuery.fn.isotope&&ui.newContent.find(".isotope").isotope("layout"),vc_carouselBehaviour(ui.newPanel)}}),!0===$this.data("vcDisableKeydown")&&($tabs.data("uiAccordion")._keydown=function(){})})}),"function"!=typeof window.vc_teaserGrid&&(window.vc_teaserGrid=function(){var layout_modes={fitrows:"fitRows",masonry:"masonry"};jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function(){var $container=jQuery(this),$thumbs=$container.find(".wpb_thumbnails"),layout_mode=$thumbs.attr("data-layout-mode");$thumbs.isotope({itemSelector:".isotope-item",layoutMode:void 0===layout_modes[layout_mode]?"fitRows":layout_modes[layout_mode]}),$container.find(".categories_filter a").data("isotope",$thumbs).click(function(e){e.preventDefault();var $thumbs=jQuery(this).data("isotope");jQuery(this).parent().parent().find(".active").removeClass("active"),jQuery(this).parent().addClass("active"),$thumbs.isotope({filter:jQuery(this).attr("data-filter")})}),jQuery(window).bind("load resize",function(){$thumbs.isotope("layout")})})}),"function"!=typeof window.vc_carouselBehaviour&&(window.vc_carouselBehaviour=function($parent){($parent?$parent.find(".wpb_carousel"):jQuery(".wpb_carousel")).each(function(){var $this=jQuery(this);if(!0!==$this.data("carousel_enabled")&&$this.is(":visible")){$this.data("carousel_enabled",!0),getColumnsCount(jQuery(this)),jQuery(this).hasClass("columns_count_1");var carousele_li=jQuery(this).find(".wpb_thumbnails-fluid li");carousele_li.css({"margin-right":carousele_li.css("margin-left"),"margin-left":0});var fluid_ul=jQuery(this).find("ul.wpb_thumbnails-fluid");fluid_ul.width(fluid_ul.width()+300),jQuery(window).resize(function(){var before_resize=screen_size;screen_size=getSizeName(),before_resize!=screen_size&&window.setTimeout("location.reload()",20)})}})}),"function"!=typeof window.vc_slidersBehaviour&&(window.vc_slidersBehaviour=function(){jQuery(".wpb_gallery_slides").each(function(index){var $imagesGrid,this_element=jQuery(this);if(this_element.hasClass("wpb_slider_nivo")){var sliderTimeout=1e3*this_element.attr("data-interval");0===sliderTimeout&&(sliderTimeout=9999999999),this_element.find(".nivoSlider").nivoSlider({effect:"boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",slices:15,boxCols:8,boxRows:4,animSpeed:800,pauseTime:sliderTimeout,startSlide:0,directionNav:!0,directionNavHide:!0,controlNav:!0,keyboardNav:!1,pauseOnHover:!0,manualAdvance:!1,prevText:"Prev",nextText:"Next"})}else this_element.hasClass("wpb_image_grid")&&(jQuery.fn.imagesLoaded?$imagesGrid=this_element.find(".wpb_image_grid_ul").imagesLoaded(function(){$imagesGrid.isotope({itemSelector:".isotope-item",layoutMode:"fitRows"})}):this_element.find(".wpb_image_grid_ul").isotope({itemSelector:".isotope-item",layoutMode:"fitRows"}))})}),"function"!=typeof window.vc_prettyPhoto&&(window.vc_prettyPhoto=function(){try{jQuery&&jQuery.fn&&jQuery.fn.prettyPhoto&&jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({animationSpeed:"normal",hook:"data-rel",padding:15,opacity:.7,showTitle:!0,allowresize:!0,counter_separator_label:"/",hideflash:!1,deeplinking:!1,modal:!1,callback:function(){location.href.indexOf("#!prettyPhoto")>-1&&(location.hash="")},social_tools:""})}catch(err){window.console&&window.console.log&&console.log(err)}}),"function"!=typeof window.vc_google_fonts&&(window.vc_google_fonts=function(){return!1}),window.vcParallaxSkroll=!1,"function"!=typeof window.vc_rowBehaviour&&(window.vc_rowBehaviour=function(){function fullWidthRow(){var $elements=$('[data-vc-full-width="true"]');$.each($elements,function(key,item){var $el=$(this);$el.addClass("vc_hidden");var $el_full=$el.next(".vc_row-full-width");if($el_full.length||($el_full=$el.parent().next(".vc_row-full-width")),$el_full.length){var el_margin_left=parseInt($el.css("margin-left"),10),el_margin_right=parseInt($el.css("margin-right"),10),offset=0-$el_full.offset().left-el_margin_left,width=$(window).width();if($el.css({position:"relative",left:offset,"box-sizing":"border-box",width:$(window).width()}),!$el.data("vcStretchContent")){var padding=-1*offset;0>padding&&(padding=0);var paddingRight=width-padding-$el_full.width()+el_margin_left+el_margin_right;0>paddingRight&&(paddingRight=0),$el.css({"padding-left":padding+"px","padding-right":paddingRight+"px"})}$el.attr("data-vc-full-width-init","true"),$el.removeClass("vc_hidden"),$(document).trigger("vc-full-width-row-single",{el:$el,offset:offset,marginLeft:el_margin_left,marginRight:el_margin_right,elFull:$el_full,width:width})}}),$(document).trigger("vc-full-width-row",$elements)}function fullHeightRow(){var $element=$(".vc_row-o-full-height:first");if($element.length){var $window,windowHeight,offsetTop,fullHeight;$window=$(window),windowHeight=$window.height(),offsetTop=$element.offset().top,offsetTop<windowHeight&&(fullHeight=100-offsetTop/(windowHeight/100),$element.css("min-height",fullHeight+"vh"))}$(document).trigger("vc-full-height-row",$element)}var $=window.jQuery;$(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour",fullWidthRow).on("resize.vcRowBehaviour",fullHeightRow),fullWidthRow(),fullHeightRow(),function(){(window.navigator.userAgent.indexOf("MSIE ")>0||navigator.userAgent.match(/Trident.*rv\:11\./))&&$(".vc_row-o-full-height").each(function(){"flex"===$(this).css("display")&&$(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')})}(),vc_initVideoBackgrounds(),function(){var vcSkrollrOptions,callSkrollInit=!1;window.vcParallaxSkroll&&window.vcParallaxSkroll.destroy(),$(".vc_parallax-inner").remove(),$("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"),$("[data-vc-parallax]").each(function(){var skrollrSpeed,skrollrSize,skrollrStart,skrollrEnd,$parallaxElement,parallaxImage,youtubeId;callSkrollInit=!0,"on"===$(this).data("vcParallaxOFade")&&$(this).children().attr("data-5p-top-bottom","opacity:0;").attr("data-30p-top-bottom","opacity:1;"),skrollrSize=100*$(this).data("vcParallax"),$parallaxElement=$("<div />").addClass("vc_parallax-inner").appendTo($(this)),$parallaxElement.height(skrollrSize+"%"),parallaxImage=$(this).data("vcParallaxImage"),youtubeId=vcExtractYoutubeId(parallaxImage),youtubeId?insertYoutubeVideoAsBackground($parallaxElement,youtubeId):void 0!==parallaxImage&&$parallaxElement.css("background-image","url("+parallaxImage+")"),skrollrSpeed=skrollrSize-100,skrollrStart=-skrollrSpeed,skrollrEnd=0,$parallaxElement.attr("data-bottom-top","top: "+skrollrStart+"%;").attr("data-top-bottom","top: "+skrollrEnd+"%;")}),!(!callSkrollInit||!window.skrollr)&&(vcSkrollrOptions={forceHeight:!1,smoothScrolling:!1,mobileCheck:function(){return!1}},window.vcParallaxSkroll=skrollr.init(vcSkrollrOptions),window.vcParallaxSkroll)}()}),"function"!=typeof window.vc_gridBehaviour&&(window.vc_gridBehaviour=function(){jQuery.fn.vcGrid&&jQuery("[data-vc-grid]").vcGrid()}),"function"!=typeof window.getColumnsCount&&(window.getColumnsCount=function(el){for(var find=!1,i=1;!1===find;){if(el.hasClass("columns_count_"+i))return find=!0,i;i++}});var screen_size=getSizeName();"function"!=typeof window.wpb_prepare_tab_content&&(window.wpb_prepare_tab_content=function(event,ui){var $ui_panel,$google_maps,panel=ui.panel||ui.newPanel,$pie_charts=panel.find(".vc_pie_chart:not(.vc_ready)"),$round_charts=panel.find(".vc_round-chart"),$line_charts=panel.find(".vc_line-chart"),$carousel=panel.find('[data-ride="vc_carousel"]');if(vc_carouselBehaviour(),vc_plugin_flexslider(panel),ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),$pie_charts.length&&jQuery.fn.vcChat&&$pie_charts.vcChat(),$round_charts.length&&jQuery.fn.vcRoundChart&&$round_charts.vcRoundChart({reload:!1}),$line_charts.length&&jQuery.fn.vcLineChart&&$line_charts.vcLineChart({reload:!1}),$carousel.length&&jQuery.fn.carousel&&$carousel.carousel("resizeAction"),$ui_panel=panel.find(".isotope, .wpb_image_grid_ul"),$google_maps=panel.find(".wpb_gmaps_widget"),0<$ui_panel.length&&$ui_panel.isotope("layout"),$google_maps.length&&!$google_maps.is(".map_ready")){var $frame=$google_maps.find("iframe");$frame.attr("src",$frame.attr("src")),$google_maps.addClass("map_ready")}panel.parents(".isotope").length&&panel.parents(".isotope").each(function(){jQuery(this).isotope("layout")})}),window.vc_googleMapsPointer,jQuery(document).ready(vc_prepareHoverBox),jQuery(window).resize(vc_prepareHoverBox),jQuery(document).ready(function($){window.vc_js()});;
/*! skrollr 0.6.30 (2015-06-19) | Alexander Prinzhorn - https://github.com/Prinzhorn/skrollr | Free to use under terms of MIT license */
!function(a,b,c){"use strict";function d(c){if(e=b.documentElement,f=b.body,T(),ha=this,c=c||{},ma=c.constants||{},c.easing)for(var d in c.easing)W[d]=c.easing[d];ta=c.edgeStrategy||"set",ka={beforerender:c.beforerender,render:c.render,keyframe:c.keyframe},la=c.forceHeight!==!1,la&&(Ka=c.scale||1),na=c.mobileDeceleration||y,pa=c.smoothScrolling!==!1,qa=c.smoothScrollingDuration||A,ra={targetTop:ha.getScrollTop()},Sa=(c.mobileCheck||function(){return/Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent||navigator.vendor||a.opera)})(),Sa?(ja=b.getElementById(c.skrollrBody||z),ja&&ga(),X(),Ea(e,[s,v],[t])):Ea(e,[s,u],[t]),ha.refresh(),wa(a,"resize orientationchange",function(){var a=e.clientWidth,b=e.clientHeight;(b!==Pa||a!==Oa)&&(Pa=b,Oa=a,Qa=!0)});var g=U();return function h(){$(),va=g(h)}(),ha}var e,f,g={get:function(){return ha},init:function(a){return ha||new d(a)},VERSION:"0.6.29"},h=Object.prototype.hasOwnProperty,i=a.Math,j=a.getComputedStyle,k="touchstart",l="touchmove",m="touchcancel",n="touchend",o="skrollable",p=o+"-before",q=o+"-between",r=o+"-after",s="skrollr",t="no-"+s,u=s+"-desktop",v=s+"-mobile",w="linear",x=1e3,y=.004,z="skrollr-body",A=200,B="start",C="end",D="center",E="bottom",F="___skrollable_id",G=/^(?:input|textarea|button|select)$/i,H=/^\s+|\s+$/g,I=/^data(?:-(_\w+))?(?:-?(-?\d*\.?\d+p?))?(?:-?(start|end|top|center|bottom))?(?:-?(top|center|bottom))?$/,J=/\s*(@?[\w\-\[\]]+)\s*:\s*(.+?)\s*(?:;|$)/gi,K=/^(@?[a-z\-]+)\[(\w+)\]$/,L=/-([a-z0-9_])/g,M=function(a,b){return b.toUpperCase()},N=/[\-+]?[\d]*\.?[\d]+/g,O=/\{\?\}/g,P=/rgba?\(\s*-?\d+\s*,\s*-?\d+\s*,\s*-?\d+/g,Q=/[a-z\-]+-gradient/g,R="",S="",T=function(){var a=/^(?:O|Moz|webkit|ms)|(?:-(?:o|moz|webkit|ms)-)/;if(j){var b=j(f,null);for(var c in b)if(R=c.match(a)||+c==c&&b[c].match(a))break;if(!R)return void(R=S="");R=R[0],"-"===R.slice(0,1)?(S=R,R={"-webkit-":"webkit","-moz-":"Moz","-ms-":"ms","-o-":"O"}[R]):S="-"+R.toLowerCase()+"-"}},U=function(){var b=a.requestAnimationFrame||a[R.toLowerCase()+"RequestAnimationFrame"],c=Ha();return(Sa||!b)&&(b=function(b){var d=Ha()-c,e=i.max(0,1e3/60-d);return a.setTimeout(function(){c=Ha(),b()},e)}),b},V=function(){var b=a.cancelAnimationFrame||a[R.toLowerCase()+"CancelAnimationFrame"];return(Sa||!b)&&(b=function(b){return a.clearTimeout(b)}),b},W={begin:function(){return 0},end:function(){return 1},linear:function(a){return a},quadratic:function(a){return a*a},cubic:function(a){return a*a*a},swing:function(a){return-i.cos(a*i.PI)/2+.5},sqrt:function(a){return i.sqrt(a)},outCubic:function(a){return i.pow(a-1,3)+1},bounce:function(a){var b;if(.5083>=a)b=3;else if(.8489>=a)b=9;else if(.96208>=a)b=27;else{if(!(.99981>=a))return 1;b=91}return 1-i.abs(3*i.cos(a*b*1.028)/b)}};d.prototype.refresh=function(a){var d,e,f=!1;for(a===c?(f=!0,ia=[],Ra=0,a=b.getElementsByTagName("*")):a.length===c&&(a=[a]),d=0,e=a.length;e>d;d++){var g=a[d],h=g,i=[],j=pa,k=ta,l=!1;if(f&&F in g&&delete g[F],g.attributes){for(var m=0,n=g.attributes.length;n>m;m++){var p=g.attributes[m];if("data-anchor-target"!==p.name)if("data-smooth-scrolling"!==p.name)if("data-edge-strategy"!==p.name)if("data-emit-events"!==p.name){var q=p.name.match(I);if(null!==q){var r={props:p.value,element:g,eventType:p.name.replace(L,M)};i.push(r);var s=q[1];s&&(r.constant=s.substr(1));var t=q[2];/p$/.test(t)?(r.isPercentage=!0,r.offset=(0|t.slice(0,-1))/100):r.offset=0|t;var u=q[3],v=q[4]||u;u&&u!==B&&u!==C?(r.mode="relative",r.anchors=[u,v]):(r.mode="absolute",u===C?r.isEnd=!0:r.isPercentage||(r.offset=r.offset*Ka))}}else l=!0;else k=p.value;else j="off"!==p.value;else if(h=b.querySelector(p.value),null===h)throw'Unable to find anchor target "'+p.value+'"'}if(i.length){var w,x,y;!f&&F in g?(y=g[F],w=ia[y].styleAttr,x=ia[y].classAttr):(y=g[F]=Ra++,w=g.style.cssText,x=Da(g)),ia[y]={element:g,styleAttr:w,classAttr:x,anchorTarget:h,keyFrames:i,smoothScrolling:j,edgeStrategy:k,emitEvents:l,lastFrameIndex:-1},Ea(g,[o],[])}}}for(Aa(),d=0,e=a.length;e>d;d++){var z=ia[a[d][F]];z!==c&&(_(z),ba(z))}return ha},d.prototype.relativeToAbsolute=function(a,b,c){var d=e.clientHeight,f=a.getBoundingClientRect(),g=f.top,h=f.bottom-f.top;return b===E?g-=d:b===D&&(g-=d/2),c===E?g+=h:c===D&&(g+=h/2),g+=ha.getScrollTop(),g+.5|0},d.prototype.animateTo=function(a,b){b=b||{};var d=Ha(),e=ha.getScrollTop(),f=b.duration===c?x:b.duration;return oa={startTop:e,topDiff:a-e,targetTop:a,duration:f,startTime:d,endTime:d+f,easing:W[b.easing||w],done:b.done},oa.topDiff||(oa.done&&oa.done.call(ha,!1),oa=c),ha},d.prototype.stopAnimateTo=function(){oa&&oa.done&&oa.done.call(ha,!0),oa=c},d.prototype.isAnimatingTo=function(){return!!oa},d.prototype.isMobile=function(){return Sa},d.prototype.setScrollTop=function(b,c){return sa=c===!0,Sa?Ta=i.min(i.max(b,0),Ja):a.scrollTo(0,b),ha},d.prototype.getScrollTop=function(){return Sa?Ta:a.pageYOffset||e.scrollTop||f.scrollTop||0},d.prototype.getMaxScrollTop=function(){return Ja},d.prototype.on=function(a,b){return ka[a]=b,ha},d.prototype.off=function(a){return delete ka[a],ha},d.prototype.destroy=function(){var a=V();a(va),ya(),Ea(e,[t],[s,u,v]);for(var b=0,d=ia.length;d>b;b++)fa(ia[b].element);e.style.overflow=f.style.overflow="",e.style.height=f.style.height="",ja&&g.setStyle(ja,"transform","none"),ha=c,ja=c,ka=c,la=c,Ja=0,Ka=1,ma=c,na=c,La="down",Ma=-1,Oa=0,Pa=0,Qa=!1,oa=c,pa=c,qa=c,ra=c,sa=c,Ra=0,ta=c,Sa=!1,Ta=0,ua=c};var X=function(){var d,g,h,j,o,p,q,r,s,t,u,v;wa(e,[k,l,m,n].join(" "),function(a){var e=a.changedTouches[0];for(j=a.target;3===j.nodeType;)j=j.parentNode;switch(o=e.clientY,p=e.clientX,t=a.timeStamp,G.test(j.tagName)||a.preventDefault(),a.type){case k:d&&d.blur(),ha.stopAnimateTo(),d=j,g=q=o,h=p,s=t;break;case l:G.test(j.tagName)&&b.activeElement!==j&&a.preventDefault(),r=o-q,v=t-u,ha.setScrollTop(Ta-r,!0),q=o,u=t;break;default:case m:case n:var f=g-o,w=h-p,x=w*w+f*f;if(49>x){if(!G.test(d.tagName)){d.focus();var y=b.createEvent("MouseEvents");y.initMouseEvent("click",!0,!0,a.view,1,e.screenX,e.screenY,e.clientX,e.clientY,a.ctrlKey,a.altKey,a.shiftKey,a.metaKey,0,null),d.dispatchEvent(y)}return}d=c;var z=r/v;z=i.max(i.min(z,3),-3);var A=i.abs(z/na),B=z*A+.5*na*A*A,C=ha.getScrollTop()-B,D=0;C>Ja?(D=(Ja-C)/B,C=Ja):0>C&&(D=-C/B,C=0),A*=1-D,ha.animateTo(C+.5|0,{easing:"outCubic",duration:A})}}),a.scrollTo(0,0),e.style.overflow=f.style.overflow="hidden"},Y=function(){var a,b,c,d,f,g,h,j,k,l,m,n=e.clientHeight,o=Ba();for(j=0,k=ia.length;k>j;j++)for(a=ia[j],b=a.element,c=a.anchorTarget,d=a.keyFrames,f=0,g=d.length;g>f;f++)h=d[f],l=h.offset,m=o[h.constant]||0,h.frame=l,h.isPercentage&&(l*=n,h.frame=l),"relative"===h.mode&&(fa(b),h.frame=ha.relativeToAbsolute(c,h.anchors[0],h.anchors[1])-l,fa(b,!0)),h.frame+=m,la&&!h.isEnd&&h.frame>Ja&&(Ja=h.frame);for(Ja=i.max(Ja,Ca()),j=0,k=ia.length;k>j;j++){for(a=ia[j],d=a.keyFrames,f=0,g=d.length;g>f;f++)h=d[f],m=o[h.constant]||0,h.isEnd&&(h.frame=Ja-h.offset+m);a.keyFrames.sort(Ia)}},Z=function(a,b){for(var c=0,d=ia.length;d>c;c++){var e,f,i=ia[c],j=i.element,k=i.smoothScrolling?a:b,l=i.keyFrames,m=l.length,n=l[0],s=l[l.length-1],t=k<n.frame,u=k>s.frame,v=t?n:s,w=i.emitEvents,x=i.lastFrameIndex;if(t||u){if(t&&-1===i.edge||u&&1===i.edge)continue;switch(t?(Ea(j,[p],[r,q]),w&&x>-1&&(za(j,n.eventType,La),i.lastFrameIndex=-1)):(Ea(j,[r],[p,q]),w&&m>x&&(za(j,s.eventType,La),i.lastFrameIndex=m)),i.edge=t?-1:1,i.edgeStrategy){case"reset":fa(j);continue;case"ease":k=v.frame;break;default:case"set":var y=v.props;for(e in y)h.call(y,e)&&(f=ea(y[e].value),0===e.indexOf("@")?j.setAttribute(e.substr(1),f):g.setStyle(j,e,f));continue}}else 0!==i.edge&&(Ea(j,[o,q],[p,r]),i.edge=0);for(var z=0;m-1>z;z++)if(k>=l[z].frame&&k<=l[z+1].frame){var A=l[z],B=l[z+1];for(e in A.props)if(h.call(A.props,e)){var C=(k-A.frame)/(B.frame-A.frame);C=A.props[e].easing(C),f=da(A.props[e].value,B.props[e].value,C),f=ea(f),0===e.indexOf("@")?j.setAttribute(e.substr(1),f):g.setStyle(j,e,f)}w&&x!==z&&("down"===La?za(j,A.eventType,La):za(j,B.eventType,La),i.lastFrameIndex=z);break}}},$=function(){Qa&&(Qa=!1,Aa());var a,b,d=ha.getScrollTop(),e=Ha();if(oa)e>=oa.endTime?(d=oa.targetTop,a=oa.done,oa=c):(b=oa.easing((e-oa.startTime)/oa.duration),d=oa.startTop+b*oa.topDiff|0),ha.setScrollTop(d,!0);else if(!sa){var f=ra.targetTop-d;f&&(ra={startTop:Ma,topDiff:d-Ma,targetTop:d,startTime:Na,endTime:Na+qa}),e<=ra.endTime&&(b=W.sqrt((e-ra.startTime)/qa),d=ra.startTop+b*ra.topDiff|0)}if(sa||Ma!==d){La=d>Ma?"down":Ma>d?"up":La,sa=!1;var h={curTop:d,lastTop:Ma,maxTop:Ja,direction:La},i=ka.beforerender&&ka.beforerender.call(ha,h);i!==!1&&(Z(d,ha.getScrollTop()),Sa&&ja&&g.setStyle(ja,"transform","translate(0, "+-Ta+"px) "+ua),Ma=d,ka.render&&ka.render.call(ha,h)),a&&a.call(ha,!1)}Na=e},_=function(a){for(var b=0,c=a.keyFrames.length;c>b;b++){for(var d,e,f,g,h=a.keyFrames[b],i={};null!==(g=J.exec(h.props));)f=g[1],e=g[2],d=f.match(K),null!==d?(f=d[1],d=d[2]):d=w,e=e.indexOf("!")?aa(e):[e.slice(1)],i[f]={value:e,easing:W[d]};h.props=i}},aa=function(a){var b=[];return P.lastIndex=0,a=a.replace(P,function(a){return a.replace(N,function(a){return a/255*100+"%"})}),S&&(Q.lastIndex=0,a=a.replace(Q,function(a){return S+a})),a=a.replace(N,function(a){return b.push(+a),"{?}"}),b.unshift(a),b},ba=function(a){var b,c,d={};for(b=0,c=a.keyFrames.length;c>b;b++)ca(a.keyFrames[b],d);for(d={},b=a.keyFrames.length-1;b>=0;b--)ca(a.keyFrames[b],d)},ca=function(a,b){var c;for(c in b)h.call(a.props,c)||(a.props[c]=b[c]);for(c in a.props)b[c]=a.props[c]},da=function(a,b,c){var d,e=a.length;if(e!==b.length)throw"Can't interpolate between \""+a[0]+'" and "'+b[0]+'"';var f=[a[0]];for(d=1;e>d;d++)f[d]=a[d]+(b[d]-a[d])*c;return f},ea=function(a){var b=1;return O.lastIndex=0,a[0].replace(O,function(){return a[b++]})},fa=function(a,b){a=[].concat(a);for(var c,d,e=0,f=a.length;f>e;e++)d=a[e],c=ia[d[F]],c&&(b?(d.style.cssText=c.dirtyStyleAttr,Ea(d,c.dirtyClassAttr)):(c.dirtyStyleAttr=d.style.cssText,c.dirtyClassAttr=Da(d),d.style.cssText=c.styleAttr,Ea(d,c.classAttr)))},ga=function(){ua="translateZ(0)",g.setStyle(ja,"transform",ua);var a=j(ja),b=a.getPropertyValue("transform"),c=a.getPropertyValue(S+"transform"),d=b&&"none"!==b||c&&"none"!==c;d||(ua="")};g.setStyle=function(a,b,c){var d=a.style;if(b=b.replace(L,M).replace("-",""),"zIndex"===b)isNaN(c)?d[b]=c:d[b]=""+(0|c);else if("float"===b)d.styleFloat=d.cssFloat=c;else try{R&&(d[R+b.slice(0,1).toUpperCase()+b.slice(1)]=c),d[b]=c}catch(e){}};var ha,ia,ja,ka,la,ma,na,oa,pa,qa,ra,sa,ta,ua,va,wa=g.addEvent=function(b,c,d){var e=function(b){return b=b||a.event,b.target||(b.target=b.srcElement),b.preventDefault||(b.preventDefault=function(){b.returnValue=!1,b.defaultPrevented=!0}),d.call(this,b)};c=c.split(" ");for(var f,g=0,h=c.length;h>g;g++)f=c[g],b.addEventListener?b.addEventListener(f,d,!1):b.attachEvent("on"+f,e),Ua.push({element:b,name:f,listener:d})},xa=g.removeEvent=function(a,b,c){b=b.split(" ");for(var d=0,e=b.length;e>d;d++)a.removeEventListener?a.removeEventListener(b[d],c,!1):a.detachEvent("on"+b[d],c)},ya=function(){for(var a,b=0,c=Ua.length;c>b;b++)a=Ua[b],xa(a.element,a.name,a.listener);Ua=[]},za=function(a,b,c){ka.keyframe&&ka.keyframe.call(ha,a,b,c)},Aa=function(){var a=ha.getScrollTop();Ja=0,la&&!Sa&&(f.style.height=""),Y(),la&&!Sa&&(f.style.height=Ja+e.clientHeight+"px"),Sa?ha.setScrollTop(i.min(ha.getScrollTop(),Ja)):ha.setScrollTop(a,!0),sa=!0},Ba=function(){var a,b,c=e.clientHeight,d={};for(a in ma)b=ma[a],"function"==typeof b?b=b.call(ha):/p$/.test(b)&&(b=b.slice(0,-1)/100*c),d[a]=b;return d},Ca=function(){var a,b=0;return ja&&(b=i.max(ja.offsetHeight,ja.scrollHeight)),a=i.max(b,f.scrollHeight,f.offsetHeight,e.scrollHeight,e.offsetHeight,e.clientHeight),a-e.clientHeight},Da=function(b){var c="className";return a.SVGElement&&b instanceof a.SVGElement&&(b=b[c],c="baseVal"),b[c]},Ea=function(b,d,e){var f="className";if(a.SVGElement&&b instanceof a.SVGElement&&(b=b[f],f="baseVal"),e===c)return void(b[f]=d);for(var g=b[f],h=0,i=e.length;i>h;h++)g=Ga(g).replace(Ga(e[h])," ");g=Fa(g);for(var j=0,k=d.length;k>j;j++)-1===Ga(g).indexOf(Ga(d[j]))&&(g+=" "+d[j]);b[f]=Fa(g)},Fa=function(a){return a.replace(H,"")},Ga=function(a){return" "+a+" "},Ha=Date.now||function(){return+new Date},Ia=function(a,b){return a.frame-b.frame},Ja=0,Ka=1,La="down",Ma=-1,Na=Ha(),Oa=0,Pa=0,Qa=!1,Ra=0,Sa=!1,Ta=0,Ua=[];"function"==typeof define&&define.amd?define([],function(){return g}):"undefined"!=typeof module&&module.exports?module.exports=g:a.skrollr=g}(window,document);;
jQuery.fn.nextInArray = function(element) {
    var nextId = 0;
    for(var i = 0; i < this.length; i++) {
        if(this[i] == element) {
            nextId = i + 1;
            break;
        }
    }
    if(nextId > this.length-1)
        nextId = 0;
    return this[nextId];
}
jQuery.fn.clearForm = function() {
	return this.each(function() {
		var type = this.type, tag = this.tagName.toLowerCase();
		if (tag == 'form')
			return jQuery(':input', this).clearForm();
		if (type == 'text' || type == 'password' || tag == 'textarea')
			this.value = '';
		else if (type == 'checkbox' || type == 'radio')
			this.checked = false;
		else if (tag == 'select') 
			this.selectedIndex = -1;
	});
}
jQuery.fn.tagName = function() {
    return this.get(0).tagName;
}
jQuery.fn.exists = function(){
    return (jQuery(this).size() > 0 ? true : false);
}
function isNumber(val) {
    return /^\d+/.test(val);
}
function pushDataToParam(data, pref) {
	pref = pref ? pref : '';
	var res = [];
	for(var key in data) {
		var name = pref && pref != '' ? pref+ '['+ key+ ']' : key;
		if(typeof(data[key]) === 'array' || typeof(data[key]) === 'object') {
			res = jQuery.merge(res, pushDataToParam(data[key], name));
		} else {
			res.push(name+ "="+ data[key]);
		}
	}
	return res;
}
jQuery.fn.serializeAnythingPts = function(addData) {
    var toReturn    = [];
    var els         = jQuery(this).find(':input').get();
    jQuery.each(els, function() {
        if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type))) {
            var val = jQuery(this).val();
            toReturn.push( encodeURIComponent(this.name) + "=" + encodeURIComponent( val ) );
        }
    });
    if(typeof(addData) != 'undefined') {
		toReturn = jQuery.merge(toReturn, pushDataToParam(addData));
    }
    return toReturn.join("&").replace(/%20/g, "+");
};
jQuery.fn.serializeAssoc = function() {
	var data = [ ];
	jQuery.each( this.serializeArray(), function( key, obj ) {
	  var a = obj.name.match(/(.*?)\[(.*?)\]/);
	  if(a !== null)
	  {
		var subName = a[1];
		var subKey = a[2];
		if( !data[subName] ) data[subName] = [ ];
		  if( data[subName][subKey] ) {
			if( jQuery.isArray( data[subName][subKey] ) ) {
			  data[subName][subKey].push( obj.value );
			} else {
			  data[subName][subKey] = [ ];
			  data[subName][subKey].push( obj.value );
			};
		  } else {
			data[subName][subKey] = obj.value;
		  };  
		} else {
		  if( data[obj.name] ) {
			if( jQuery.isArray( data[obj.name] ) ) {
			  data[obj.name].push( obj.value );
			} else {
			  data[obj.name] = [ ];
			  data[obj.name].push( obj.value );
			};
		  } else {
			data[obj.name] = obj.value;
		  };
		};
	});
	return data;
};
jQuery.fn.serializeObject = function(){
	var obj = {};

	jQuery.each( this.serializeArray(), function(i,o){
		var n = o.name,
			v = o.value;

		obj[n] = obj[n] === undefined ? v
			: jQuery.isArray( obj[n] ) ? obj[n].concat( v )
				: [ obj[n], v ];
	});

	return obj;
};
str_replace_all = function(str, token, newToken, ignoreCase ) {
	var _token;
	str += "";
	var i = -1;

	if ( typeof token === "string" ) {
		if ( ignoreCase ) {
       		_token = token.toLowerCase();

		    while( (
		        i = str.toLowerCase().indexOf(
		            _token, i >= 0 ? i + newToken.length : 0
		        ) ) !== -1
		    ) {
		        str = str.substring( 0, i ) +
		            newToken +
		            str.substring( i + token.length );
		    }

		} else {
		    return str.split( token ).join( newToken );
		}

	}

	return str;
};
function str_replace(haystack, needle, replacement) { 
	var temp = haystack.split(needle); 
	return temp.join(replacement); 
}
function str_repeat(input, multiplier) {
	var buf = '';
	for (var i = 0; i < multiplier; i++) {
		buf += input;
	}
	return buf;
}

function str_split ( f_string, f_split_length, f_backwards ){	// Convert a string to an array
	// 
	// +	 original by: Martijn Wieringa

	if(f_backwards == undefined) {
		f_backwards = false;
	}

	if(f_split_length > 0){
		var result = new Array();

		if(f_backwards) {
			var r = (f_string.length % f_split_length);

			if(r > 0){
				result[result.length] = f_string.substring(0, r);
				f_string = f_string.substring(r);
			}
		}

		while(f_string.length > f_split_length) {
			result[result.length] = f_string.substring(0, f_split_length);
			f_string = f_string.substring(f_split_length);
		}

		result[result.length] = f_string;
		return result;
	}

	return false;
}
function hexdec(hex_string) {
  //  discuss at: http://phpjs.org/functions/hexdec/
  // original by: Philippe Baumann
  //   example 1: hexdec('that');
  //   returns 1: 10
  //   example 2: hexdec('a0');
  //   returns 2: 160

  hex_string = (hex_string + '')
    .replace(/[^a-f0-9]/gi, '');
  return parseInt(hex_string, 16);
}
function dechex(number) {
  //  discuss at: http://phpjs.org/functions/dechex/
  // original by: Philippe Baumann
  // bugfixed by: Onno Marsman
  // improved by: http://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
  //    input by: pilus
  //   example 1: dechex(10);
  //   returns 1: 'a'
  //   example 2: dechex(47);
  //   returns 2: '2f'
  //   example 3: dechex(-1415723993);
  //   returns 3: 'ab9dc427'

  if (number < 0) {
    number = 0xFFFFFFFF + number + 1;
  }
  return parseInt(number, 10)
    .toString(16);
}
function str_pad( input, pad_length, pad_string, pad_type ) {	// Pad a string to a certain length with another string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// + namespaced by: Michael White (http://crestidg.com)

	var half = '', pad_to_go;

	var str_pad_repeater = function(s, len){
			var collect = '', i;

			while(collect.length < len) collect += s;
			collect = collect.substr(0,len);

			return collect;
		};

	if (pad_type != 'STR_PAD_LEFT' && pad_type != 'STR_PAD_RIGHT' && pad_type != 'STR_PAD_BOTH') { pad_type = 'STR_PAD_RIGHT'; }
	if ((pad_to_go = pad_length - input.length) > 0) {
		if (pad_type == 'STR_PAD_LEFT') { input = str_pad_repeater(pad_string, pad_to_go) + input; }
		else if (pad_type == 'STR_PAD_RIGHT') { input = input + str_pad_repeater(pad_string, pad_to_go); }
		else if (pad_type == 'STR_PAD_BOTH') {
			half = str_pad_repeater(pad_string, Math.ceil(pad_to_go/2));
			input = half + input + half;
			input = input.substr(0, pad_length);
		}
	}

	return input;
}

/**
 * @see php html::nameToClassId($name) method
 **/
function nameToClassId(name) {
    return str_replace(
        str_replace(name, ']', ''), 
            '[', ''
    );
}
function strpos( haystack, needle, offset){
    var i = haystack.indexOf( needle, offset ); // returns -1
    return i >= 0 ? i : false;
}
function extend(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
}
function toeRedirect(url, newWnd) {
    if(newWnd) {
		var win = window.open(url, '_blank');
		win.focus();
	} else {
		document.location.href = url;
	}
}
function toeReload(url) {
	if(url)
		toeRedirect(url);
    document.location.reload();
}
jQuery.fn.toeRebuildSelect = function(data, useIdAsValue, val) {
    if(jQuery(this).tagName() == 'SELECT' && typeof(data) == 'object') {
        if(jQuery(data).size() > 0) {
            if(typeof(val) == 'undefined')
                val = false;
            if(jQuery(this).children('option').length) {
                jQuery(this).children('option').remove();
            }
            if(typeof(useIdAsValue) == 'undefined')
                useIdAsValue = false;
            var selected = '';
            for(var id in data) {
                selected = '';
                if(val && ((useIdAsValue && id == val) || (data[id] == val)))
                    selected = 'selected';
                jQuery(this).append('<option value="'+ (useIdAsValue ? id : data[id])+ '" '+ selected+ '>'+ data[id]+ '</option>');
            }
        }
    }
}
/**
 * We will not use just jQUery.inArray because it is work incorrect for objects
 * @return mixed - key that was found element or -1 if not
 */
function toeInArray(needle, haystack) {
    if(typeof(haystack) == 'object') {
        for(var k in haystack) {
            if(haystack[ k ] == needle)
                return k;
        }
    } else if(typeof(haystack) == 'array') {
        return jQuery.inArray(needle, haystack);
    }
    return -1;
}
jQuery.fn.setReadonly = function() {
	jQuery(this).addClass('toeReadonly').attr('readonly', 'readonly');
}
jQuery.fn.unsetReadonly = function() {
	jQuery(this).removeClass('toeReadonly').removeAttr('readonly', 'readonly');
}
jQuery.fn.getClassId = function(pref, test) {
	var classId = jQuery(this).attr('class');
	classId = classId.substr( strpos(classId, pref+ '_') );
	if(strpos(classId, ' '))
		classId = classId.substr( 0, strpos(classId, ' ') );
	classId = classId.split('_');
	classId = classId[1];
	return classId;
}
function toeTextIncDec(textFieldId, inc) {
	var value = parseInt(jQuery('#'+ textFieldId).val());
	if(isNaN(value))
		value = 0;
	if(!(inc < 0 && value < 1)) {
		value += inc;
	}
	jQuery('#'+ textFieldId).val(value);
}

/**
 * Make first letter of string in upper case
 * @param str string - string to convert
 * @return string converted string - first letter in upper case
 */
function toeStrFirstUp(str) {
	str += '';
	var f = str.charAt(0).toUpperCase();
	return f + str.substr(1);
}
function parseStr (str, array) {
  // http://kevin.vanzonneveld.net
  // +   original by: Cagri Ekin
  // +   improved by: Michael White (http://getsprink.com)
  // +    tweaked by: Jack
  // +   bugfixed by: Onno Marsman
  // +   reimplemented by: stag019
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: stag019
  // +   input by: Dreamer
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: MIO_KODUKI (http://mio-koduki.blogspot.com/)
  // +   input by: Zaide (http://zaidesthings.com/)
  // +   input by: David Pesta (http://davidpesta.com/)
  // +   input by: jeicquest
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // %        note 1: When no argument is specified, will put variables in global scope.
  // %        note 1: When a particular argument has been passed, and the returned value is different parse_str of PHP. For example, a=b=c&d====c
  // *     example 1: var arr = {};
  // *     example 1: parse_str('first=foo&second=bar', arr);
  // *     results 1: arr == { first: 'foo', second: 'bar' }
  // *     example 2: var arr = {};
  // *     example 2: parse_str('str_a=Jack+and+Jill+didn%27t+see+the+well.', arr);
  // *     results 2: arr == { str_a: "Jack and Jill didn't see the well." }
  // *     example 3: var abc = {3:'a'};
  // *     example 3: parse_str('abc[a][b]["c"]=def&abc[q]=t+5');
  // *     results 3: JSON.stringify(abc) === '{"3":"a","a":{"b":{"c":"def"}},"q":"t 5"}';
	var strArr = String(str).replace(/^&/, '').replace(/&$/, '').split('&'),
	sal = strArr.length,
	i, j, ct, p, lastObj, obj, lastIter, undef, chr, tmp, key, value,
	postLeftBracketPos, keys, keysLen,
	fixStr = function (str) {
		return decodeURIComponent(str.replace(/\+/g, '%20'));
	};
	// Comented by Alexey Bolotov
	/*
	if (!array) {
	array = this.window;
	}*/
	if (!array) {
		array = {};
	}

	for (i = 0; i < sal; i++) {
		tmp = strArr[i].split('=');
		key = fixStr(tmp[0]);
		value = (tmp.length < 2) ? '' : fixStr(tmp[1]);

		while (key.charAt(0) === ' ') {
			key = key.slice(1);
		}
		if (key.indexOf('\x00') > -1) {
			key = key.slice(0, key.indexOf('\x00'));
		}
		if (key && key.charAt(0) !== '[') {
			keys = [];
			postLeftBracketPos = 0;
			for (j = 0; j < key.length; j++) {
				if (key.charAt(j) === '[' && !postLeftBracketPos) {
					postLeftBracketPos = j + 1;
				} else if (key.charAt(j) === ']') {
					if (postLeftBracketPos) {
						if (!keys.length) {
							keys.push(key.slice(0, postLeftBracketPos - 1));
						}
						keys.push(key.substr(postLeftBracketPos, j - postLeftBracketPos));
						postLeftBracketPos = 0;
						if (key.charAt(j + 1) !== '[') {
							break;
						}
					}
				}
			}
			if (!keys.length) {
				keys = [key];
			}
			for (j = 0; j < keys[0].length; j++) {
				chr = keys[0].charAt(j);
				if (chr === ' ' || chr === '.' || chr === '[') {
					keys[0] = keys[0].substr(0, j) + '_' + keys[0].substr(j + 1);
				}
				if (chr === '[') {
					break;
				}
			}

			obj = array;
			for (j = 0, keysLen = keys.length; j < keysLen; j++) {
				key = keys[j].replace(/^['"]/, '').replace(/['"]$/, '');
				lastIter = j !== keys.length - 1;
				lastObj = obj;
				if ((key !== '' && key !== ' ') || j === 0) {
					if (obj[key] === undef) {
						obj[key] = {};
					}
					obj = obj[key];
				} else { // To insert new dimension
					ct = -1;
					for (p in obj) {
						if (obj.hasOwnProperty(p)) {
							if (+p > ct && p.match(/^\d+$/g)) {
								ct = +p;
							}
						}
					}
					key = ct + 1;
				}
			}
			lastObj[key] = value;
		}
	}
	return array;
}

function toeListablePts(params) {
	this.params			= jQuery.extend({}, params);
	this.table			= jQuery(this.params.table);
	this.paging			= jQuery(this.params.paging);
	this.perPage		= this.params.perPage;
	this.list			= this.params.list;
	this.count			= this.params.count;
	this.page			= this.params.page;
	this.pagingCallback	= this.params.pagingCallback;
	var self			= this;
	
	this.draw = function(list, count) {
		this.table.find('tr').not('.ptsExample, .ptsTblHeader').remove();
		var exampleRow = this.table.find('.ptsExample');
		for(var i in list) {
			var newRow = exampleRow.clone();
			for(var key in list[i]) {
				var element = newRow.find('.'+ key);
				if(element.size()) {
					var valueTo = element.attr('valueTo');
					if(valueTo) {
						var newValue = list[i][key];
						var prevValue = element.attr(valueTo);
						if(prevValue)
							newValue = prevValue+ ' '+ newValue;
						element.attr(valueTo, newValue);
					} else
						element.html(list[i][key]);
				}
			}
			newRow.removeClass('ptsExample').show();
			this.table.append(newRow);
		}
		if(this.paging) {
			this.paging.html('');
			if(count && count > list.length && this.perPage) {
				for(var i = 1; i <= Math.ceil(count/this.perPage); i++) {
					var newPageId = i-1
					,	newElement = (newPageId == this.page) ? jQuery('<b/>') : jQuery('<a/>');
					if(newPageId != this.page) {
						newElement.attr('href', '#'+ newPageId)
						.click(function(){
							if(self.pagingCallback && typeof(self.pagingCallback) == 'function') {
								self.pagingCallback(parseInt(jQuery(this).attr('href').replace('#', '')));
								return false;
							}
						});
					}
					newElement.addClass('toePagingElement').html(i);
					this.paging.append(newElement);
					if(i%20 == 0 && i)
						this.paging.append('<br />');
				}
			}
		}
	};
	if(this.list)
		this.draw(this.list, this.count);
}

function setCookiePts(c_name, value, exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var value_prepared = '';
	if(typeof(value) == 'array' || typeof(value) == 'object') {
		value_prepared = '_JSON:'+ JSON.stringify( value );
	} else {
		value_prepared = value;
	}
	var c_value = escape(value_prepared)+ ((exdays==null) ? "" : "; expires="+exdate.toUTCString())+ '; path=/';
	document.cookie = c_name+ "="+ c_value;
}

function getCookiePts(name) {
	var parts = document.cookie.split(name + "=");
	if (parts.length == 2) {
		var value = unescape(parts.pop().split(";").shift());
		if(value.indexOf('_JSON:') === 0) {
			value = JSON.parse(value.split("_JSON:").pop());
		}
		return value;
	}
	return null;
}

function delCookiePts( name ) {
  document.cookie = name+ '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function callUserFuncArray(cb, parameters) {
	// http://kevin.vanzonneveld.net
	// +   original by: Thiago Mata (http://thiagomata.blog.com)
	// +   revised  by: Jon Hohle
	// +   improved by: Brett Zamir (http://brett-zamir.me)
	// +   improved by: Diplom@t (http://difane.com/)
	// +   improved by: Brett Zamir (http://brett-zamir.me)
	// *     example 1: call_user_func_array('isNaN', ['a']);
	// *     returns 1: true
	// *     example 2: call_user_func_array('isNaN', [1]);
	// *     returns 2: false
	var func;

	if (typeof cb === 'string') {
		func = (typeof this[cb] === 'function') ? this[cb] : func = (new Function(null, 'return ' + cb))();
	}
	else if (Object.prototype.toString.call(cb) === '[object Array]') {
		func = (typeof cb[0] == 'string') ? eval(cb[0] + "['" + cb[1] + "']") : func = cb[0][cb[1]];
	}
	else if (typeof cb === 'function') {
		func = cb;
	}

	if (typeof func !== 'function') {
		throw new Error(func + ' is not a valid function');
	}

	return (typeof cb[0] === 'string') ? func.apply(eval(cb[0]), parameters) : (typeof cb[0] !== 'object') ? func.apply(null, parameters) : func.apply(cb[0], parameters);
}
jQuery.fn.zoom = function(level, position) {
	position = position ? position : 'center center';
	var scaleCss = level == 1 ? 'none' : 'scale('+ level+ ')';
	jQuery(this).data('zoom', level);
	return jQuery(this).css({
	/*	'zoom': level	// Didn't worked correctly for mobiles
	,*/	'-moz-transform': scaleCss
	,	'-moz-transform-origin': position
	,	'-o-transform': scaleCss
	,	'-o-transform-origin': position
	,	'-webkit-transform': scaleCss
	,	'-webkit-transform-origin': position
	,	'transform': scaleCss
	,	'transform-origin': position
	});
};
jQuery.fn.scrollWidth = function() {
	var inner = document.createElement('p');
	inner.style.width = "100%";
	inner.style.height = "200px";

	var outer = document.createElement('div');
	outer.style.position = "absolute";
	outer.style.top = "0px";
	outer.style.left = "0px";
	outer.style.visibility = "hidden";
	outer.style.width = "200px";
	outer.style.height = "150px";
	outer.style.overflow = "hidden";
	outer.appendChild (inner);

	document.body.appendChild (outer);
	var w1 = inner.offsetWidth;
	outer.style.overflow = 'scroll';
	var w2 = inner.offsetWidth;
	if (w1 == w2) w2 = outer.clientWidth;

	document.body.removeChild (outer);

	return (w1 - w2);
};
/**
 * Retrive worptsess attach ID from image, using img classes
 * @param {htmlObj} img Image to get ID from
 */
function toeGetImgAttachId(img) {
	var classesStr = jQuery(img).attr('class')
	,	aid = 0;
	if(classesStr && classesStr != '') {
		var matches = classesStr.match(/wp-image-(\d+)/);
		if(matches && matches[1]) {
			aid = parseInt(matches[1]);
		}
	}
	return aid;
}
function toeGetHashParams() {
	var hashArr = window.location.hash.split('#')
	,	res = [];
	for(var i in hashArr) {
		if(hashArr[i] && hashArr[i] != '') {
			res.push(hashArr[i]);
		}
	}
	return res;
}
/*Replace text in DOM functions*/
// Reusable generic function
function traverseElement(el, regex, textReplacerFunc, to) {
    // script and style elements are left alone
    if (!/^(script|style)$/.test(el.tagName)) {
        var child = el.lastChild;
        while (child) {
            if (child.nodeType == 1) {
                traverseElement(child, regex, textReplacerFunc, to);
            } else if (child.nodeType == 3) {
                textReplacerFunc(child, regex, to);
            }
            child = child.previousSibling;
        }
    }
}

// This function does the replacing for every matched piece of text
// and can be customized to do what you like
function textReplacerFunc(textNode, regex, to) {
	textNode.data = textNode.data.replace(regex, to);
}

// The main function
function replaceWords(html, words) {
    var container = document.createElement("div");
    container.innerHTML = html;

    // Replace the words one at a time to ensure each one gets matched
	for(var replace in words) {
		traverseElement(container, new RegExp(replace, "g"), textReplacerFunc, words[ replace ]);
	}
    return container.innerHTML;
}
/*****/
function toeSelectText(element) {
    var doc = document
	,	text = jQuery(element).get(0)
	,	range, selection;    
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();        
        range = doc.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}
jQuery.fn.animationDuration = function(seconds, isMili) {
	if(isMili) {
		seconds = parseFloat(seconds) / 1000;
	}
	var secondsStr = seconds+ 's';
	return jQuery(this).css({
		'webkit-animation-duration': secondsStr
	,	'-moz-animation-duration': secondsStr
	,	'-o-animation-duration': secondsStr
	,	'animation-duration': secondsStr
	});
};
/**
 * Convert Date string (in common - mm/dd/yyyy) - to miliseconds
 * @param {string} strDate date string
 * @return {int} miliseconds
 */
function ptsStrToMs(strDate) {
	var dateHours = strDate.split(' ');
	if(dateHours.length == 2) {
		strDate = dateHours[0]+ ' ';
		var hms = dateHours[1].split(':');
		
		for(var i = 0; i < 3; i++) {
			strDate += hms[ i ] ? hms[ i ] : '00';
			if(i < 2)
				strDate += ':';
		}
	}
	var date = new Date( str_replace(strDate, '-', '/') )
	,	res = 0;
	if(date) {
		res = date.getTime();
	}
	return res;
}
function mtRand(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
// Simulates PHP's date function
Date.prototype.format=function(e){var t="";var n=Date.replaceChars;for(var r=0;r<e.length;r++){var i=e.charAt(r);if(r-1>=0&&e.charAt(r-1)=="\\"){t+=i}else if(n[i]){t+=n[i].call(this)}else if(i!="\\"){t+=i}}return t};Date.replaceChars={shortMonths:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Pts","Nov","Dec"],longMonths:["January","February","March","April","May","June","July","August","September","Ptsober","November","December"],shortDays:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],longDays:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],d:function(){return(this.getDate()<10?"0":"")+this.getDate()},D:function(){return Date.replaceChars.shortDays[this.getDay()]},j:function(){return this.getDate()},l:function(){return Date.replaceChars.longDays[this.getDay()]},N:function(){return this.getDay()+1},S:function(){return this.getDate()%10==1&&this.getDate()!=11?"st":this.getDate()%10==2&&this.getDate()!=12?"nd":this.getDate()%10==3&&this.getDate()!=13?"rd":"th"},w:function(){return this.getDay()},z:function(){var e=new Date(this.getFullYear(),0,1);return Math.ceil((this-e)/864e5)},W:function(){var e=new Date(this.getFullYear(),0,1);return Math.ceil(((this-e)/864e5+e.getDay()+1)/7)},F:function(){return Date.replaceChars.longMonths[this.getMonth()]},m:function(){return(this.getMonth()<9?"0":"")+(this.getMonth()+1)},M:function(){return Date.replaceChars.shortMonths[this.getMonth()]},n:function(){return this.getMonth()+1},t:function(){var e=new Date;return(new Date(e.getFullYear(),e.getMonth(),0)).getDate()},L:function(){var e=this.getFullYear();return e%400==0||e%100!=0&&e%4==0},o:function(){var e=new Date(this.valueOf());e.setDate(e.getDate()-(this.getDay()+6)%7+3);return e.getFullYear()},Y:function(){return this.getFullYear()},y:function(){return(""+this.getFullYear()).substr(2)},a:function(){return this.getHours()<12?"am":"pm"},A:function(){return this.getHours()<12?"AM":"PM"},B:function(){return Math.floor(((this.getUTCHours()+1)%24+this.getUTCMinutes()/60+this.getUTCSeconds()/3600)*1e3/24)},g:function(){return this.getHours()%12||12},G:function(){return this.getHours()},h:function(){return((this.getHours()%12||12)<10?"0":"")+(this.getHours()%12||12)},H:function(){return(this.getHours()<10?"0":"")+this.getHours()},i:function(){return(this.getMinutes()<10?"0":"")+this.getMinutes()},s:function(){return(this.getSeconds()<10?"0":"")+this.getSeconds()},u:function(){var e=this.getMilliseconds();return(e<10?"00":e<100?"0":"")+e},e:function(){return"Not Yet Supported"},I:function(){var e=null;for(var t=0;t<12;++t){var n=new Date(this.getFullYear(),t,1);var r=n.getTimezoneOffset();if(e===null)e=r;else if(r<e){e=r;break}else if(r>e)break}return this.getTimezoneOffset()==e|0},O:function(){return(-this.getTimezoneOffset()<0?"-":"+")+(Math.abs(this.getTimezoneOffset()/60)<10?"0":"")+Math.abs(this.getTimezoneOffset()/60)+"00"},P:function(){return(-this.getTimezoneOffset()<0?"-":"+")+(Math.abs(this.getTimezoneOffset()/60)<10?"0":"")+Math.abs(this.getTimezoneOffset()/60)+":00"},T:function(){var e=this.getMonth();this.setMonth(0);var t=this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/,"$1");this.setMonth(e);return t},Z:function(){return-this.getTimezoneOffset()*60},c:function(){return this.format("Y-m-d\\TH:i:sP")},r:function(){return this.toString()},U:function(){return this.getTime()/1e3}}
function ptsInitCustomCheckRadio(selector) {
	if(!selector)
		selector = document;
	jQuery(selector).find('input:not(".ptsCpInpFlag")').iCheck('destroy').iCheck({
		checkboxClass: 'icheckbox_minimal'
	,	radioClass: 'iradio_minimal'
	}).on('ifChanged', function(e){
		// for checkboxHiddenVal type, see class htmlPts
		jQuery(this).trigger('change');
		if(jQuery(this).hasClass('cbox')) {
			var parentRow = jQuery(this).parents('.jqgrow:first');
			if(parentRow && parentRow.size()) {
				jQuery(this).parents('td:first').trigger('click');
			} else {
				var checkId = jQuery(this).attr('id');
				if(checkId && checkId != '' && strpos(checkId, 'cb_') === 0) {
					var parentTblId = str_replace(checkId, 'cb_', '');
					if(parentTblId && parentTblId != '' && jQuery('#'+ parentTblId).size()) {
						jQuery('#'+ parentTblId).find('input[type=checkbox]').iCheck('update');
					}
				}
			}
		}
	}).on('ifClicked', function(e){
		jQuery(this).trigger('click');
	});
}
function ptsCheckUpdate(checkbox) {
	jQuery(checkbox).iCheck('update');
}
function ptsCheckUpdateArea(selector) {
	jQuery(selector).find('input[type=checkbox]').iCheck('update');
	jQuery(selector).find('input[type=radio]').iCheck('update');
}
function ptsCallWpMedia(params) {
	params = params || {};
	if ( typeof wp !== 'undefined' && wp.media && wp.media.editor )
		wp.media.editor.open( params.id );
	//_original_send = wp.media.editor.send.attachment;
	// For posibility to insert from "Insert from URL" option
	wp.media.editor.insert = function(html) {
		// wp.media.editor.send.attachment was called before - don't do here nothing
		if(wp.media.editor._attachSent) {
			wp.media.editor._attachSent = false;
			return;
		}
		if(html && html != '') {
			var imgUrl = jQuery(html).attr('src');
			if(imgUrl) {
				params.clb( {}, {}, imgUrl );
			}
		}
	};
	wp.media.editor.send.attachment = function(opts, attach) {
		wp.media.editor._attachSent = true;
		var imgUrl = opts.size && attach.sizes[ opts.size ] && attach.sizes[ opts.size ].url 
			? attach.sizes[ opts.size ].url
			: attach.url;
		params.clb( opts, attach, imgUrl );
	};
	window.original_send_to_editor = window.send_to_editor; 
	window.send_to_editor = function(html) {
		// html argument might not be useful in this case
		// use the data from var b (attachment) here to make your own ajax call or use data from b and send it back to your defined input fields etc.
	};
}
function ptsMceMoveToolbar(editor, clientX) {
	/*if(editor._ptsLastOpenedMenu) {
		editor._ptsHidingForMove = true;
		editor._ptsLastOpenedMenu.hide();
		editor._ptsLastOpenedMenu = null;
	}*/
	var panel = editor.theme.panel;
	if(!panel) return;	// Element was clicked, but panel not opened - for drag-&-drop for example cases
	var panelRect = panel.layoutRect()
	,	bodyElement = jQuery(editor.bodyElement)
	,	bodyOffset = bodyElement.offset()
	,	newX = clientX - panelRect.w / 2
	,	newY = bodyOffset.top - 30;
	if(newY <= 150) {	// Panel at the bottom of the element
		newY += bodyElement.height() + 30;
		//jQuery('#'+ panel._id).addClass('mce-tinymce-inline-bottom');
	}
	if(newX < 0)
		newX = 0;
	panel.moveTo(newX, panelRect.y ? panelRect.y : newY);
	panel._ptsOriginalTop = newY
}
/*function ptsMceOnShowMoveMainToolbar(e, editor) {
	var panel = editor.theme.panel
	,	prevRect = panel.layoutRect();
	if(typeof(panel._ptsOriginalTop) === 'undefined') {
		panel._ptsOriginalTop = prevRect.y;
	}
	// If we showld move it- but it is not in original position	- move it back
	if(prevRect.y != panel._ptsOriginalTop) {
		panel.ptsSetCanMove();
		panel.moveTo(prevRect.x, panel._ptsOriginalTop);
		panel._ptsRefreshedOriginal = true;
		//panel._ptsOriginalTop = prevRect.y;
	}
	var panelRect = panel.layoutRect()
	,	controlHeight = jQuery('#'+ e.control._id).height()
	,	panelOnBottom = jQuery('#'+ panel._id).hasClass('mce-tinymce-inline-bottom')
	,	newY = panelOnBottom ? panelRect.y + controlHeight + prevRect.h - 7 : panelRect.y - controlHeight - 5;

	panel.ptsSetCanMove();
	panel.moveTo(panelRect.x, newY);

	jQuery('#'+ panel._id).addClass('mce-submenu-opened').addClass(panelOnBottom ? 'mce-submenu-opened-bottom' : 'mce-submenu-opened-top');
}
function ptsMceOnHideMoveMainToolbar(e, editor) {
	var panel = editor.theme.panel;
	if(!editor._ptsHidingForMove && !panel._ptsRefreshedOriginal) {
		var selectionCoords = getSelectionCoords();
		var newX = 0;
		if(selectionCoords) {
			newX = selectionCoords.x;
		} else {
			newX = panel.layoutRect().x;
		}
		ptsMceMoveToolbar(editor, newX);
	}
	editor._ptsHidingForMove = false;
	panel._ptsRefreshedOriginal = false;
	return;
	var panel = editor.theme.panel
	,	moveTop = jQuery('#'+ e.control._id).height() + 5
	,	panelOnBottom = jQuery('#'+ panel._id).hasClass('mce-tinymce-inline-bottom');
	if(panelOnBottom) {
		moveTop *= -1;
	}
	panel.ptsSetCanMove();
	panel.moveBy(0, moveTop);
	jQuery('#'+ panel._id).removeClass('mce-submenu-opened mce-submenu-opened-top mce-submenu-opened-bottom');
}
function ptsMceMoveToolbar(editor, clientX) {
	if(editor._ptsLastOpenedMenu) {
		editor._ptsHidingForMove = true;
		editor._ptsLastOpenedMenu.hide();
		editor._ptsLastOpenedMenu = null;
	}
	var panel = editor.theme.panel;
	if(!panel) return;	// Element was clicked, but panel not opened - for drag-&-drop for example cases
	var panelRect = panel.layoutRect()
	,	bodyElement = jQuery(editor.bodyElement)
	,	bodyOffset = bodyElement.offset()
	,	newX = clientX - panelRect.w / 2
	,	newY = bodyOffset.top - 30;
	if(newY <= 150) {	// Panel at the bottom of the element
		newY += bodyElement.height() + 30;
		jQuery('#'+ panel._id).addClass('mce-tinymce-inline-bottom');
	}
	if(newX < 0)
		newX = 0;
	panel.ptsSetCanMove();
	panel.moveTo(newX, newY);
	panel._ptsOriginalTop = newY
}
function ptsMceOnShowSubMenu(e, editor) {
	
	var rootRect = e.control.getRoot().layoutRect()
	,	panel = editor.theme.panel
	,	panelOnBottom = jQuery('#'+ panel._id).hasClass('mce-tinymce-inline-bottom')
	,	controlHeight = jQuery('#'+ e.control._id).height()
	,	newX = rootRect.x
	,	newY = (panel._ptsOriginalTop ? panel._ptsOriginalTop : rootRect.y) - controlHeight + 10;
	
	if(panelOnBottom) {
		newY += controlHeight + 20;
		jQuery('#'+ e.control._id).addClass('mce-tinymce-subpanel-bottom');
	}
	e.control.ptsSetCanMove();
	e.control.moveTo( newX, newY );
	
	var finalControlHeight = jQuery('#'+ e.control._id).height();
	if(controlHeight != finalControlHeight) {
		var deltaH = -1 * (finalControlHeight - controlHeight);
		if(panelOnBottom) {
			deltaH *= -1;
			deltaH = 0;	// TODO: Fix his and make it work in correct and logical way. p.s.Strange? Really......
		}
		e.control.ptsSetCanMove();
		e.control.moveBy( 0, deltaH );
	}
	editor._ptsLastOpenedMenu = e.control;
}*/
function getSelectionCoords(win) {
    win = win || window;
    var doc = win.document;
    var sel = doc.selection, range, rects, rect;
    var x = 0, y = 0;
    if (sel) {
        if (sel.type != "Control") {
            range = sel.createRange();
            range.collapse(true);
            x = range.boundingLeft;
            y = range.boundingTop;
        }
    } else if (win.getSelection) {
        sel = win.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0).cloneRange();
            if (range.getClientRects) {
                range.collapse(true);
                rects = range.getClientRects();
                if (rects.length > 0) {
                    rect = range.getClientRects()[0];
                }
				if(!rect) {
					return false;
					//rect = jQuery(range.commonAncestorContainer).offset();
				}
                x = rect.left;
                y = rect.top;
            }
            // Fall back to inserting a temporary element
            if (x == 0 && y == 0) {
                var span = doc.createElement("span");
                if (span.getClientRects) {
                    // Ensure span has dimensions and position by
                    // adding a zero-width space character
                    span.appendChild( doc.createTextNode("\u200b") );
                    range.insertNode(span);
                    rect = span.getClientRects()[0];
                    x = rect.left;
                    y = rect.top;
                    var spanParent = span.parentNode;
                    spanParent.removeChild(span);

                    // Glue any broken text nodes back together
                    spanParent.normalize();
                }
            }
        }
    }
    return { x: x, y: y };
}
function get_class(obj) {	// Returns the name of the class of an object
	// 
	// +   original by: Ates Goral (http://magnetiq.com)
	// +   improved by: David James

	if (obj instanceof Object && !(obj instanceof Array) &&
		!(obj instanceof Function) && obj.constructor) {
		var arr = obj.constructor.toString().match(/function\s*(\w+)/);

		if (arr && arr.length == 2) {
			return arr[1];
		}
	}

	return false;
}

function serialize( mixed_val ) {    // Generates a storable representation of a value
    // 
    // +   original by: Ates Goral (http://magnetiq.com)
    // +   adapted for IE: Ilia Kantor (http://javascript.ru)
 
    switch (typeof(mixed_val)){
        case "number":
            if (isNaN(mixed_val) || !isFinite(mixed_val)){
                return false;
            } else {
                return (Math.floor(mixed_val) == mixed_val ? "i" : "d") + ":" + mixed_val + ";";
            }
        case "string":
            return "s:" + mixed_val.length + ":\"" + mixed_val + "\";";
        case "boolean":
            return "b:" + (mixed_val ? "1" : "0") + ";";
        case "object":
            if (mixed_val == null) {
				return "N;";
            } else if (mixed_val instanceof Array) {
                var idxobj = { idx: -1 };
				var map = []
				for(var i=0; i<mixed_val.length;i++) {
					idxobj.idx++;
					var ser = serialize(mixed_val[i]);
					if (ser) {
						map.push(serialize(idxobj.idx) + ser)
					}
				}                             
                return "a:" + mixed_val.length + ":{" + map.join("") + "}"
            } else {
                var class_name = get_class(mixed_val);
                if (class_name == undefined){
					return false;
                }
                var props = new Array();
                for (var prop in mixed_val) {
                    var ser = serialize(mixed_val[prop]);
                    if (ser) {
						props.push(serialize(prop) + ser);
                    }
                }
                return "O:" + class_name.length + ":\"" + class_name + "\":" + props.length + ":{" + props.join("") + "}";
            }
        case "undefined":
            return "N;";
    }
 
    return false;
}
function unserialize ( inp ) {	// Creates a PHP value from a stored representation
	// 
	// +   original by: Arpad Ray (mailto:arpad@php.net)

	var error = 0
	,	errormsg = '';
	if (inp == "" || inp.length < 2) {
		errormsg = "input is too short";
		return;
	}
	var val, kret, vret, cval;
	var type = inp.charAt(0);
	var cont = inp.substring(2);
	var size = 0, divpos = 0, endcont = 0, rest = "", next = "";

	switch (type) {
		case "N": // null
			if (inp.charAt(1) != ";") {
				errormsg = "missing ; for null";
			}
			// leave val undefined
			rest = cont;
			break;
		case "b": // boolean
			if (!/[01];/.test(cont.substring(0,2))) {
				errormsg = "value not 0 or 1, or missing ; for boolean";
			}
			val = (cont.charAt(0) == "1");
			rest = cont.substring(1);
			break;
		case "s": // string
			val = "";
			divpos = cont.indexOf(":");
			if (divpos == -1) {
				errormsg = "missing : for string";
				break;
			}
			size = parseInt(cont.substring(0, divpos));
			if (size == 0) {
				if (cont.length - divpos < 4) {
					errormsg = "string is too short";
					break;
				}
				rest = cont.substring(divpos + 4);
				break;
			}
			if ((cont.length - divpos - size) < 4) {
				errormsg = "string is too short";
				break;
			}
			if (cont.substring(divpos + 2 + size, divpos + 4 + size) != "\";") {
				errormsg = "string is too long, or missing \";";
			}
			val = cont.substring(divpos + 2, divpos + 2 + size);
			rest = cont.substring(divpos + 4 + size);
			break;
		case "i": // integer
		case "d": // float
			var dotfound = 0;
			for (var i = 0; i < cont.length; i++) {
				cval = cont.charAt(i);
				if (isNaN(parseInt(cval)) && !(type == "d" && cval == "." && !dotfound++)) {
					endcont = i;
					break;
				}
			}
			if (!endcont || cont.charAt(endcont) != ";") {
				errormsg = "missing or invalid value, or missing ; for int/float";
			}
			val = cont.substring(0, endcont);
			val = (type == "i" ? parseInt(val) : parseFloat(val));
			rest = cont.substring(endcont + 1);
			break;
		case "a": // array
			if (cont.length < 4) {
				errormsg = "array is too short";
				return;
			}
			divpos = cont.indexOf(":", 1);
			if (divpos == -1) {
				errormsg = "missing : for array";
				return;
			}
			size = parseInt(cont.substring(1, divpos - 1));
			cont = cont.substring(divpos + 2);
			val = new Array();
			if (cont.length < 1) {
				errormsg = "array is too short";
				return;
			}
			for (var i = 0; i + 1 < size * 2; i += 2) {
				kret = unserialize(cont, 1);
				if (error || kret[0] == undefined || kret[1] == "") {
					errormsg = "missing or invalid key, or missing value for array";
					return;
				}
				vret = unserialize(kret[1], 1);
				if (error) {
					errormsg = "invalid value for array";
					return;
				}
				val[kret[0]] = vret[0];
				cont = vret[1];
			}
			if (cont.charAt(0) != "}") {
				errormsg = "missing ending }, or too many values for array";
				return;
			}
			rest = cont.substring(1);
			break;
		case "O": // object
			divpos = cont.indexOf(":");
			if (divpos == -1) {
				errormsg = "missing : for object";
				return;
			}
			size = parseInt(cont.substring(0, divpos));
			var objname = cont.substring(divpos + 2, divpos + 2 + size);
			if (cont.substring(divpos + 2 + size, divpos + 4 + size) != "\":") {
				errormsg = "object name is too long, or missing \":";
				return;
			}
			var objprops = unserialize("a:" + cont.substring(divpos + 4 + size), 1);
			if (error) {
				errormsg = "invalid object properties";
				return;
			}
			rest = objprops[1];
			var objout = "function " + objname + "(){";
			for (key in objprops[0]) {
				objout += "" + key + "=objprops[0]['" + key + "'];";
			}
			objout += "}val=new " + objname + "();";
			eval(objout);
			break;
		default:
			errormsg = "invalid input type";
	}
	return (arguments.length == 1 ? val : [val, rest]);
}

function splitNode(node, start, end) {
  var parent = node.parentNode;
  //var parentOffset = getNodeIndex(parent, limit);

  var doc = node.ownerDocument;  
  var leftRange = doc.createRange();
  leftRange.setStart(parent, parentOffset);
  leftRange.setEnd(node, offset);
  var left = leftRange.extractContents();
  parent.insertBefore(left, limit);
  
	var doc = node.ownerDocument;  
	var leftRange = doc.createRange();
	leftRange.setStart(node, start);
	leftRange.setEnd(node, end);
	var left = leftRange.extractContents();
}

function getNodeIndex(parent, node) {
  var index = parent.childNodes.length;
  while (index--) {
    if (node === parent.childNodes[index]) {
      break;
    }
  }
  return index;
}
function ptsChangeElAttrs(scope) {
	this._scope = scope;
	this._itemSubMenu = null;
	this._element = null;
	this._isChangedAttrText = false;
	this._re = {
		attrName : /[A-Za-z0-9-_]*/,
		attrVal: /[A-Za-z0-9-_\s]*/
	};
	this._allowedAttrs = ['id', 'style', 'class'];

	this._initMenuItem();
	this._initSubMenuItem();
	this._getAttrToSubMenu();
};
ptsChangeElAttrs.prototype._initMenuItem = function() {
	var self = this._scope,
		$parent = self._$.find('.ptsElMenuMainPanel'),
		$newEl = jQuery('#ptsAddHtmlAttribute');

	$newEl = $newEl
		.clone()
		.removeAttr('id')
		.addClass('ptsElMenuBtn');
	$parent.prepend('<div class="ptsElMenuBtnDelimiter"></div>');
	$parent.prepend($newEl);
};
ptsChangeElAttrs.prototype._initSubMenuItem = function() {
	var self = this._scope,
		$parent = self._$.find('.ptsElMenuContent'),
		$newEl = jQuery('#ptsSubMenyAddHtmlAttr');

	$newEl = $newEl
		.clone()
		.removeAttr('id');
	$parent.append($newEl);

	this._itemSubMenu = $parent;
};
ptsChangeElAttrs.prototype._getAttrToSubMenu = function () {

	var me = this,
		self = me._scope,
		$el = self._element._$.hasClass('ptsEl')
			? self._element._$
			: self._element._$.find('.ptsEl').eq(0),
		$parent = this._itemSubMenu,
		$newEl = null,
		allowedAttr = this._allowedAttrs;
	me._element = $el;

	var $attrNames = {}, $attrs = {};

	$parent.find('.ptsElMenuSubPanel[data-sub-panel="add_attr"] .ptsElMenuSubPanelRow').remove();

	$el.each(function(){
		$attrNames = this.attributes;
		$parent = $parent.find('.ptsElMenuSubPanel[data-sub-panel="add_attr"]');

		for (var key in allowedAttr) {
			// for compatibility with other plugin
			if(typeof allowedAttr[key] == 'string') {
				$attrs[allowedAttr[key]] = jQuery(this).attr(allowedAttr[key]);

				$newEl = jQuery('#ptsRowAddHtmlAttr')
					.clone()
					.removeAttr('id');
				$newEl.attr('data-id', allowedAttr[key]);
				$newEl.find('span').text(allowedAttr[key]);
				$newEl.find('input').val($attrs[allowedAttr[key]]);
				$newEl.find('input').attr('name', allowedAttr[key]);
				$parent.append($newEl);
			}
		}

		me._elAttributes = $attrs;
		me._setEventChangeElVal();
		me._setEventUpdateAttrs();
	});

};
ptsChangeElAttrs.prototype._updateElAttrs = function() {
	this._isChangedAttrText = true;
};
ptsChangeElAttrs.prototype._setEventChangeElVal = function() {
	var me = this,
		$parent = this._itemSubMenu;

	function callBackFn() {
		var thatAttr = jQuery(this).attr('name'),
			thatVal = jQuery(this).val();
		me._element.attr(thatAttr, thatVal);
		me._updateElAttrs();
	}

	$parent.find('div[data-sub-panel="add_attr"] input:not(".ptsCpInpFlag")').unbind('keyup');
	$parent.find('div[data-sub-panel="add_attr"] input:not(".ptsCpInpFlag")').on('keyup', callBackFn);
};
ptsChangeElAttrs.prototype._setEventUpdateAttrs = function() {
	var me = this,
		$parent = this._itemSubMenu;

	function callBackFn() {
		if (me._isChangedAttrText) {
			me._getAttrToSubMenu();
			me._isChangedAttrText = false;
		}
	}

	$parent.find('input').unbind('blur');
	$parent.find('input').on('blur', callBackFn);
};

/**
	@source: https://github.com/customd/jquery-visible
**/
!function(t){var i=t(window);t.fn.visible=function(t,e,o){if(!(this.length<1)){var r=this.length>1?this.eq(0):this,n=r.get(0),f=i.width(),h=i.height(),o=o?o:"both",l=e===!0?n.offsetWidth*n.offsetHeight:!0;if("function"==typeof n.getBoundingClientRect){var g=n.getBoundingClientRect(),u=g.top>=0&&g.top<h,s=g.bottom>0&&g.bottom<=h,c=g.left>=0&&g.left<f,a=g.right>0&&g.right<=f,v=t?u||s:u&&s,b=t?c||a:c&&a;if("both"===o)return l&&v&&b;if("vertical"===o)return l&&v;if("horizontal"===o)return l&&b}else{var d=i.scrollTop(),p=d+h,w=i.scrollLeft(),m=w+f,y=r.offset(),z=y.top,B=z+r.height(),C=y.left,R=C+r.width(),j=t===!0?B:z,q=t===!0?z:B,H=t===!0?R:C,L=t===!0?C:R;if("both"===o)return!!l&&p>=q&&j>=d&&m>=L&&H>=w;if("vertical"===o)return!!l&&p>=q&&j>=d;if("horizontal"===o)return!!l&&m>=L&&H>=w}}}}(jQuery);

(function(window){
	window.htmlentities = {
		/**
		 * Converts a string to its html characters completely.
		 *
		 * @param {String} str String with unescaped HTML characters
		 **/
		encode : function(str) {
			var buf = [];

			for (var i=str.length-1;i>=0;i--) {
				buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
			}

			return buf.join('');
		},
		/**
		 * Converts an html characterSet into its original character.
		 *
		 * @param {String} str htmlSet entities
		 **/
		decode : function(str) {
			return str.replace(/&#(\d+);/g, function(match, dec) {
				return String.fromCharCode(dec);
			});
		}
	};
})(window);;
/*! iCheck v1.0.2 by Damir Sultanov, http://git.io/arlzeA, MIT Licensed */
(function(f){function A(a,b,d){var c=a[0],g=/er/.test(d)?_indeterminate:/bl/.test(d)?n:k,e=d==_update?{checked:c[k],disabled:c[n],indeterminate:"true"==a.attr(_indeterminate)||"false"==a.attr(_determinate)}:c[g];if(/^(ch|di|in)/.test(d)&&!e)x(a,g);else if(/^(un|en|de)/.test(d)&&e)q(a,g);else if(d==_update)for(var f in e)e[f]?x(a,f,!0):q(a,f,!0);else if(!b||"toggle"==d){if(!b)a[_callback]("ifClicked");e?c[_type]!==r&&q(a,g):x(a,g)}}function x(a,b,d){var c=a[0],g=a.parent(),e=b==k,u=b==_indeterminate,
v=b==n,s=u?_determinate:e?y:"enabled",F=l(a,s+t(c[_type])),B=l(a,b+t(c[_type]));if(!0!==c[b]){if(!d&&b==k&&c[_type]==r&&c.name){var w=a.closest("form"),p='input[name="'+c.name+'"]',p=w.length?w.find(p):f(p);p.each(function(){this!==c&&f(this).data(m)&&q(f(this),b)})}u?(c[b]=!0,c[k]&&q(a,k,"force")):(d||(c[b]=!0),e&&c[_indeterminate]&&q(a,_indeterminate,!1));D(a,e,b,d)}c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"default");g[_add](B||l(a,b)||"");g.attr("role")&&!u&&g.attr("aria-"+(v?n:k),"true");
g[_remove](F||l(a,s)||"")}function q(a,b,d){var c=a[0],g=a.parent(),e=b==k,f=b==_indeterminate,m=b==n,s=f?_determinate:e?y:"enabled",q=l(a,s+t(c[_type])),r=l(a,b+t(c[_type]));if(!1!==c[b]){if(f||!d||"force"==d)c[b]=!1;D(a,e,s,d)}!c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"pointer");g[_remove](r||l(a,b)||"");g.attr("role")&&!f&&g.attr("aria-"+(m?n:k),"false");g[_add](q||l(a,s)||"")}function E(a,b){if(a.data(m)){a.parent().html(a.attr("style",a.data(m).s||""));if(b)a[_callback](b);a.off(".i").unwrap();
f(_label+'[for="'+a[0].id+'"]').add(a.closest(_label)).off(".i")}}function l(a,b,f){if(a.data(m))return a.data(m).o[b+(f?"":"Class")]}function t(a){return a.charAt(0).toUpperCase()+a.slice(1)}function D(a,b,f,c){if(!c){if(b)a[_callback]("ifToggled");a[_callback]("ifChanged")[_callback]("if"+t(f))}}var m="iCheck",C=m+"-helper",r="radio",k="checked",y="un"+k,n="disabled";_determinate="determinate";_indeterminate="in"+_determinate;_update="update";_type="type";_click="click";_touch="touchbegin.i touchend.i";
_add="addClass";_remove="removeClass";_callback="trigger";_label="label";_cursor="cursor";_mobile=/ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);f.fn[m]=function(a,b){var d='input[type="checkbox"], input[type="'+r+'"]',c=f(),g=function(a){a.each(function(){var a=f(this);c=a.is(d)?c.add(a):c.add(a.find(d))})};if(/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a=a.toLowerCase(),g(this),c.each(function(){var c=
f(this);"destroy"==a?E(c,"ifDestroyed"):A(c,!0,a);f.isFunction(b)&&b()});if("object"!=typeof a&&a)return this;var e=f.extend({checkedClass:k,disabledClass:n,indeterminateClass:_indeterminate,labelHover:!0},a),l=e.handle,v=e.hoverClass||"hover",s=e.focusClass||"focus",t=e.activeClass||"active",B=!!e.labelHover,w=e.labelHoverClass||"hover",p=(""+e.increaseArea).replace("%","")|0;if("checkbox"==l||l==r)d='input[type="'+l+'"]';-50>p&&(p=-50);g(this);return c.each(function(){var a=f(this);E(a);var c=this,
b=c.id,g=-p+"%",d=100+2*p+"%",d={position:"absolute",top:g,left:g,display:"block",width:d,height:d,margin:0,padding:0,background:"#fff",border:0,opacity:0},g=_mobile?{position:"absolute",visibility:"hidden"}:p?d:{position:"absolute",opacity:0},l="checkbox"==c[_type]?e.checkboxClass||"icheckbox":e.radioClass||"i"+r,z=f(_label+'[for="'+b+'"]').add(a.closest(_label)),u=!!e.aria,y=m+"-"+Math.random().toString(36).substr(2,6),h='<div class="'+l+'" '+(u?'role="'+c[_type]+'" ':"");u&&z.each(function(){h+=
'aria-labelledby="';this.id?h+=this.id:(this.id=y,h+=y);h+='"'});h=a.wrap(h+"/>")[_callback]("ifCreated").parent().append(e.insert);d=f('<ins class="'+C+'"/>').css(d).appendTo(h);a.data(m,{o:e,s:a.attr("style")}).css(g);e.inheritClass&&h[_add](c.className||"");e.inheritID&&b&&h.attr("id",m+"-"+b);"static"==h.css("position")&&h.css("position","relative");A(a,!0,_update);if(z.length)z.on(_click+".i mouseover.i mouseout.i "+_touch,function(b){var d=b[_type],e=f(this);if(!c[n]){if(d==_click){if(f(b.target).is("a"))return;
A(a,!1,!0)}else B&&(/ut|nd/.test(d)?(h[_remove](v),e[_remove](w)):(h[_add](v),e[_add](w)));if(_mobile)b.stopPropagation();else return!1}});a.on(_click+".i focus.i blur.i keyup.i keydown.i keypress.i",function(b){var d=b[_type];b=b.keyCode;if(d==_click)return!1;if("keydown"==d&&32==b)return c[_type]==r&&c[k]||(c[k]?q(a,k):x(a,k)),!1;if("keyup"==d&&c[_type]==r)!c[k]&&x(a,k);else if(/us|ur/.test(d))h["blur"==d?_remove:_add](s)});d.on(_click+" mousedown mouseup mouseover mouseout "+_touch,function(b){var d=
b[_type],e=/wn|up/.test(d)?t:v;if(!c[n]){if(d==_click)A(a,!1,!0);else{if(/wn|er|in/.test(d))h[_add](e);else h[_remove](e+" "+t);if(z.length&&B&&e==v)z[/ut|nd/.test(d)?_remove:_add](w)}if(_mobile)b.stopPropagation();else return!1}})})}})(window.jQuery||window.Zepto);
;
if(typeof(PTS_DATA) == 'undefined')
	var PTS_DATA = {};
if(isNumber(PTS_DATA.animationSpeed)) 
    PTS_DATA.animationSpeed = parseInt(PTS_DATA.animationSpeed);
else if(jQuery.inArray(PTS_DATA.animationSpeed, ['fast', 'slow']) == -1)
    PTS_DATA.animationSpeed = 'fast';
PTS_DATA.showSubscreenOnCenter = parseInt(PTS_DATA.showSubscreenOnCenter);
var sdLoaderImgPts = '<img src="'+ PTS_DATA.loader+ '" />';
var g_ptsAnimationSpeed = 300;

jQuery.fn.showLoaderPts = function() {
    return jQuery(this).html( sdLoaderImgPts );
};
jQuery.fn.appendLoaderPts = function() {
    jQuery(this).append( sdLoaderImgPts );
};
jQuery.sendFormPts = function(params) {
	// Any html element can be used here
	return jQuery('<br />').sendFormPts(params);
};
jQuery.fn.removeClassWild = function(mask) {
	return this.removeClass(function(index, cls) {
	    var re = mask.replace(/\*/g, '\\S+');
	    return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
	});
};
/**
 * Send form or just data to server by ajax and route response
 * @param string params.fid form element ID, if empty - current element will be used
 * @param string params.msgElID element ID to store result messages, if empty - element with ID "msg" will be used. Can be "noMessages" to not use this feature
 * @param function params.onSuccess funstion to do after success receive response. Be advised - "success" means that ajax response will be success
 * @param array params.data data to send if You don't want to send Your form data, will be set instead of all form data
 * @param array params.appendData data to append to sending request. In contrast to params.data will not erase form data
 * @param string params.inputsWraper element ID for inputs wraper, will be used if it is not a form
 * @param string params.clearMsg clear msg element after receive data, if is number - will use it to set time for clearing, else - if true - will clear msg element after 5 seconds
 */
jQuery.fn.sendFormPts = function(params) {
    var form = null;
    if(!params)
        params = {fid: false, msgElID: false, onSuccess: false};

    if(params.fid)
        form = jQuery('#'+ fid);
    else
        form = jQuery(this);
    
    /* This method can be used not only from form data sending, it can be used just to send some data and fill in response msg or errors*/
    var sentFromForm = (jQuery(form).tagName() == 'FORM');
    var data = new Array();
    if(params.data)
        data = params.data;
    else if(sentFromForm)
        data = jQuery(form).serialize();
    
	params.errorClass = 'ptsErrorMsg'+ (params.errorClass ? ' '+ params.errorClass : '');
	params.successClass = 'ptsSuccessMsg'+ (params.successClass ? ' '+ params.successClass : '');
	
    if(params.appendData) {
		var dataIsString = typeof(data) == 'string';
		var addStrData = [];
        for(var i in params.appendData) {
			if(dataIsString) {
				addStrData.push(i+ '='+ params.appendData[i]);
			} else
            data[i] = params.appendData[i];
        }
		if(dataIsString)
			data += '&'+ addStrData.join('&');
    }
    var msgEl = null;
    if(params.msgElID) {
        if(params.msgElID == 'noMessages')
            msgEl = false;
        else if(typeof(params.msgElID) == 'object')
           msgEl = params.msgElID;
       else
            msgEl = jQuery('#'+ params.msgElID);
    }
	if(typeof(params.inputsWraper) == 'string') {
		form = jQuery('#'+ params.inputsWraper);
		sentFromForm = true;
	}
	if(sentFromForm && form) {
        jQuery(form).find('*').removeClass('ptsInputError');
    }
	if(msgEl && !params.btn) {
		jQuery(msgEl)
			.removeClass( params.errorClass )
			.removeClass( params.successClass );
		if(params.hideLoader) {
			jQuery(msgEl).html('');
		} else {
			jQuery(msgEl).showLoaderPts();
		}
	}
	if(params.btn) {
		var btnWasDisabled = jQuery(params.btn).attr('disabled') ? true : false;
		jQuery(params.btn).attr('disabled', 'disabled');
		// Font awesome usage
		params.btnIconElement = jQuery(params.btn).find('.fa').size() ? jQuery(params.btn).find('.fa') : jQuery(params.btn);
		if(jQuery(params.btn).find('.fa').size()) {
			if(!btnWasDisabled) {
				params.btnIconElement
					.data('prev-class', params.btnIconElement.attr('class'));
			}
			params.btnIconElement
				.attr('class', 'fa fa-spinner fa-spin');
		}
	}
    var url = '';
	if(typeof(params.url) != 'undefined')
		url = params.url;
    else if(typeof(ajaxurl) == 'undefined')
        url = PTS_DATA.ajaxurl;
    else
        url = ajaxurl;
    
    jQuery('.ptsErrorForField').hide(PTS_DATA.animationSpeed);
	var dataType = params.dataType ? params.dataType : 'json';
	// Set plugin orientation
	if(typeof(data) == 'string') {
		data += '&pl='+ PTS_DATA.PTS_CODE;
		data += '&reqType=ajax';
	} else {
		data['pl'] = PTS_DATA.PTS_CODE;
		data['reqType'] = 'ajax';
	}
	if(params.onBeforeSend && typeof(params.onBeforeSend) === 'function') {
		params.onBeforeSend();
	}
    jQuery.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: dataType,
        success: function(res) {
            toeProcessAjaxResponsePts(res, msgEl, form, sentFromForm, params);
			if(params.clearMsg) {
				setTimeout(function(){
					if(msgEl)
						jQuery(msgEl).animateClear();
				}, typeof(params.clearMsg) == 'boolean' ? 5000 : params.clearMsg);
			}
        }
    });
};
/**
 * Hide content in element and then clear it
 */
jQuery.fn.animateClear = function() {
	var newContent = jQuery('<span>'+ jQuery(this).html()+ '</span>');
	jQuery(this).html( newContent );
	jQuery(newContent).hide(PTS_DATA.animationSpeed, function(){
		jQuery(newContent).remove();
	});
};
/**
 * Hide content in element and then remove it
 */
jQuery.fn.animateRemovePts = function(animationSpeed, onSuccess) {
	animationSpeed = animationSpeed == undefined ? PTS_DATA.animationSpeed : animationSpeed;
	jQuery(this).hide(animationSpeed, function(){
		jQuery(this).remove();
		if(typeof(onSuccess) === 'function')
			onSuccess();
	});
};
function toeProcessAjaxResponsePts(res, msgEl, form, sentFromForm, params) {
    if(typeof(params) == 'undefined')
        params = {};
    if(typeof(msgEl) == 'string')
        msgEl = jQuery('#'+ msgEl);
    if(msgEl)
        jQuery(msgEl).html('');
	if(params.btn) {
		jQuery(params.btn).removeAttr('disabled');
		if(params.btnIconElement) {
			params.btnIconElement.attr('class', params.btnIconElement.data('prev-class'));
		}
	}
    /*if(sentFromForm) {
        jQuery(form).find('*').removeClass('ptsInputError');
    }*/
    if(typeof(res) == 'object') {
		if(msgEl) {
			msgEl.show();	// Make sure that it is visible
		}
		if(msgEl && params.msgCloseBtn) {
			var closeBtn = jQuery('<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			msgEl.append( closeBtn );
			closeBtn.click(function(){
				msgEl.slideUp(g_ptsAnimationSpeed, function(){
					msgEl.html('');
				});
				return false;
			});
		}
        if(res.error) {
            if(msgEl) {
                jQuery(msgEl)
					.removeClass( params.successClass )
					.addClass( params.errorClass );
            }
			var errorsArr = [];
            for(var name in res.errors) {
                if(sentFromForm) {
					var inputError = jQuery(form).find('[name*="'+ name+ '"]');
                    inputError.addClass('ptsInputError');
					if(inputError.attr('placeholder')) {
						//inputError.attr('placeholder', res.errors[ name ]);
					}
					if(!inputError.data('keyup-error-remove-binded')) {
						inputError.keydown(function(){
							jQuery(this).removeClass('ptsInputError');
						}).data('keyup-error-remove-binded', 1);
					}
                }
                if(jQuery('.ptsErrorForField.toe_'+ nameToClassId(name)+ '').exists())
                    jQuery('.ptsErrorForField.toe_'+ nameToClassId(name)+ '').show().html(res.errors[name]);
                else if(msgEl)
                    jQuery(msgEl).append(res.errors[name]).append('<br />');
				else
					errorsArr.push( res.errors[name] );
            }
			if(errorsArr.length && params.btn && jQuery.fn.dialog) {
				jQuery('<div title="'+ toeLangPts("Really small warning :)")+ '" />').html( errorsArr.join('<br />') ).appendTo('body').dialog({
					modal: true
				,	width: '500px'
				});
			}
        } else if(res.messages.length) {
            if(msgEl) {
                jQuery(msgEl)
					.removeClass( params.errorClass )
					.addClass( params.successClass );
                for(var i = 0; i < res.messages.length; i++) {
                    jQuery(msgEl).append(res.messages[i]).append('<br />');
                }
            }
        }
    }
    if(params.onSuccess && typeof(params.onSuccess) == 'function') {
        params.onSuccess(res);
    }
}

function getDialogElementPts() {
	return jQuery('<div/>').appendTo(jQuery('body'));
}

function toeOptionPts(key) {
	if(PTS_DATA.options && PTS_DATA.options[ key ])
		return PTS_DATA.options[ key ];
	return false;
}
function toeLangPts(key) {
	if(PTS_DATA.siteLang && PTS_DATA.siteLang[key])
		return PTS_DATA.siteLang[key];
	return key;
}
function toePagesPts(key) {
	if(typeof(PTS_DATA) != 'undefined' && PTS_DATA[key])
		return PTS_DATA[key];
	return false;;
}
/**
 * This function will help us not to hide desc right now, but wait - maybe user will want to select some text or click on some link in it.
 */
function toeOptTimeoutHideDescriptionPts() {
	jQuery('#ptsOptDescription').removeAttr('toeFixTip');
	setTimeout(function(){
		if(!jQuery('#ptsOptDescription').attr('toeFixTip'))
			toeOptHideDescriptionPts();
	}, 500);
}
/**
 * Show description for options
 */
function toeOptShowDescriptionPts(description, x, y, moveToLeft) {
    if(typeof(description) != 'undefined' && description != '') {
        if(!jQuery('#ptsOptDescription').size()) {
            jQuery('body').append('<div id="ptsOptDescription"></div>');
        }
		if(moveToLeft)
			jQuery('#ptsOptDescription').css('right', jQuery(window).width() - (x - 10));	// Show it on left side of target
		else
			jQuery('#ptsOptDescription').css('left', x + 10);
        jQuery('#ptsOptDescription').css('top', y);
        jQuery('#ptsOptDescription').show(200);
        jQuery('#ptsOptDescription').html(description);
    }
}
/**
 * Hide description for options
 */
function toeOptHideDescriptionPts() {
	jQuery('#ptsOptDescription').removeAttr('toeFixTip');
    jQuery('#ptsOptDescription').hide(200);
}
function toeInArrayPts(needle, haystack) {
	if(haystack) {
		for(var i in haystack) {
			if(haystack[i] == needle)
				return true;
		}
	}
	return false;
}
function toeShowDialogCustomized(element, options) {
	options = jQuery.extend({
		resizable: false
	,	width: 500
	,	height: 300
	,	closeOnEscape: true
	,	open: function(event, ui) {
			jQuery('.ui-dialog-titlebar').css({
				'background-color': '#222222'
			,	'background-image': 'none'
			,	'border': 'none'
			,	'margin': '0'
			,	'padding': '0'
			,	'border-radius': '0'
			,	'color': '#CFCFCF'
			,	'height': '27px'
			});
			jQuery('.ui-dialog-titlebar-close').css({
				'background': 'url("'+ PTS_DATA.cssPath+ 'img/tb-close.png") no-repeat scroll 0 0 transparent'
			,	'border': '0'
			,	'width': '15px'
			,	'height': '15px'
			,	'padding': '0'
			,	'border-radius': '0'
			,	'margin': '7px 7px 0'
			}).html('');
			jQuery('.ui-dialog').css({
				'border-radius': '3px'
			,	'background-color': '#FFFFFF'
			,	'background-image': 'none'
			,	'padding': '1px'
			,	'z-index': '300000'
			,	'position': 'fixed'
			,	'top': '60px'
			});
			jQuery('.ui-dialog-buttonpane').css({
				'background-color': '#FFFFFF'
			});
			jQuery('.ui-dialog-title').css({
				'color': '#CFCFCF'
			,	'font': '12px sans-serif'
			,	'padding': '6px 10px 0'
			});
			if(options.openCallback && typeof(options.openCallback) == 'function') {
				options.openCallback(event, ui);
			}
			jQuery('.ui-widget-overlay').css({
				'z-index': jQuery( event.target ).parents('.ui-dialog:first').css('z-index') - 1
			,	'background-image': 'none'
			});
			if(options.modal && options.closeOnBg) {
				jQuery('.ui-widget-overlay').unbind('click').bind('click', function() {
					jQuery( element ).dialog('close');
				});
			}
		}
	}, options);
	return jQuery(element).dialog(options);
}
/**
 * @see html::slider();
 **/
function toeSliderMove(event, ui) {
    var id = jQuery(event.target).attr('id');
    jQuery('#toeSliderDisplay_'+ id).html( ui.value );
    jQuery('#toeSliderInput_'+ id).val( ui.value ).change();
}
/**
 * Javascript extend functionality
 * @param {function/class} Child child elemnt for exnted
 * @param {function/class} Parent parent elemnt to extend from
 */
function extendPts(Child, Parent) {
	var F = function() { };
	F.prototype = Parent.prototype;
	Child.prototype = new F();
	Child.prototype.constructor = Child;
	Child.superclass = Parent.prototype;
}
;
/* Tooltipster v3.3.0 */;(function(e,t,n){function s(t,n){this.bodyOverflowX;this.callbacks={hide:[],show:[]};this.checkInterval=null;this.Content;this.$el=e(t);this.$elProxy;this.elProxyPosition;this.enabled=true;this.options=e.extend({},i,n);this.mouseIsOverProxy=false;this.namespace="tooltipster-"+Math.round(Math.random()*1e5);this.Status="hidden";this.timerHide=null;this.timerShow=null;this.$tooltip;this.options.iconTheme=this.options.iconTheme.replace(".","");this.options.theme=this.options.theme.replace(".","");this._init()}function o(t,n){var r=true;e.each(t,function(e,i){if(typeof n[e]==="undefined"||t[e]!==n[e]){r=false;return false}});return r}function f(){return!a&&u}function l(){var e=n.body||n.documentElement,t=e.style,r="transition";if(typeof t[r]=="string"){return true}v=["Moz","Webkit","Khtml","O","ms"],r=r.charAt(0).toUpperCase()+r.substr(1);for(var i=0;i<v.length;i++){if(typeof t[v[i]+r]=="string"){return true}}return false}var r="tooltipster",i={animation:"fade",arrow:true,arrowColor:"",autoClose:true,content:null,contentAsHTML:false,contentCloning:true,debug:true,delay:200,minWidth:0,maxWidth:null,functionInit:function(e,t){},functionBefore:function(e,t){t()},functionReady:function(e,t){},functionAfter:function(e){},hideOnClick:false,icon:"(?)",iconCloning:true,iconDesktop:false,iconTouch:false,iconTheme:"tooltipster-icon",interactive:false,interactiveTolerance:350,multiple:false,offsetX:0,offsetY:0,onlyOne:false,position:"top",positionTracker:false,positionTrackerCallback:function(e){if(this.option("trigger")=="hover"&&this.option("autoClose")){this.hide()}},restoration:"current",speed:350,timer:0,theme:"tooltipster-default",touchDevices:true,trigger:"hover",updateAnimation:true};s.prototype={_init:function(){var t=this;if(n.querySelector){var r=null;if(t.$el.data("tooltipster-initialTitle")===undefined){r=t.$el.attr("title");if(r===undefined)r=null;t.$el.data("tooltipster-initialTitle",r)}if(t.options.content!==null){t._content_set(t.options.content)}else{t._content_set(r)}var i=t.options.functionInit.call(t.$el,t.$el,t.Content);if(typeof i!=="undefined")t._content_set(i);t.$el.addClass("tooltipstered");if(!u&&t.options.iconDesktop||u&&t.options.iconTouch){if(typeof t.options.icon==="string"){t.$elProxy=e('<span class="'+t.options.iconTheme+'"></span>');t.$elProxy.text(t.options.icon)}else{if(t.options.iconCloning)t.$elProxy=t.options.icon.clone(true);else t.$elProxy=t.options.icon}t.$elProxy.insertAfter(t.$el)}else{t.$elProxy=t.$el}if(t.options.trigger=="hover"){t.$elProxy.on("mouseenter."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=true;t._show()}}).on("mouseleave."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=false}});if(u&&t.options.touchDevices){t.$elProxy.on("touchstart."+t.namespace,function(){t._showNow()})}}else if(t.options.trigger=="click"){t.$elProxy.on("click."+t.namespace,function(){if(!f()||t.options.touchDevices){t._show()}})}}},_show:function(){var e=this;if(e.Status!="shown"&&e.Status!="appearing"){if(e.options.delay){e.timerShow=setTimeout(function(){if(e.options.trigger=="click"||e.options.trigger=="hover"&&e.mouseIsOverProxy){e._showNow()}},e.options.delay)}else e._showNow()}},_showNow:function(n){var r=this;r.options.functionBefore.call(r.$el,r.$el,function(){if(r.enabled&&r.Content!==null){if(n)r.callbacks.show.push(n);r.callbacks.hide=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;if(r.options.onlyOne){e(".tooltipstered").not(r.$el).each(function(t,n){var r=e(n),i=r.data("tooltipster-ns");e.each(i,function(e,t){var n=r.data(t),i=n.status(),s=n.option("autoClose");if(i!=="hidden"&&i!=="disappearing"&&s){n.hide()}})})}var i=function(){r.Status="shown";e.each(r.callbacks.show,function(e,t){t.call(r.$el)});r.callbacks.show=[]};if(r.Status!=="hidden"){var s=0;if(r.Status==="disappearing"){r.Status="appearing";if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-"+r.options.animation+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.stop().fadeIn(i)}}else if(r.Status==="shown"){i()}}else{r.Status="appearing";var s=r.options.speed;r.bodyOverflowX=e("body").css("overflow-x");e("body").css("overflow-x","hidden");var o="tooltipster-"+r.options.animation,a="-webkit-transition-duration: "+r.options.speed+"ms; -webkit-animation-duration: "+r.options.speed+"ms; -moz-transition-duration: "+r.options.speed+"ms; -moz-animation-duration: "+r.options.speed+"ms; -o-transition-duration: "+r.options.speed+"ms; -o-animation-duration: "+r.options.speed+"ms; -ms-transition-duration: "+r.options.speed+"ms; -ms-animation-duration: "+r.options.speed+"ms; transition-duration: "+r.options.speed+"ms; animation-duration: "+r.options.speed+"ms;",f=r.options.minWidth?"min-width:"+Math.round(r.options.minWidth)+"px;":"",c=r.options.maxWidth?"max-width:"+Math.round(r.options.maxWidth)+"px;":"",h=r.options.interactive?"pointer-events: auto;":"";r.$tooltip=e('<div class="tooltipster-base '+r.options.theme+'" style="'+f+" "+c+" "+h+" "+a+'"><div class="tooltipster-content"></div></div>');if(l())r.$tooltip.addClass(o);r._content_insert();r.$tooltip.appendTo("body");r.reposition();r.options.functionReady.call(r.$el,r.$el,r.$tooltip);if(l()){r.$tooltip.addClass(o+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.css("display","none").fadeIn(r.options.speed,i)}r._interval_set();e(t).on("scroll."+r.namespace+" resize."+r.namespace,function(){r.reposition()});if(r.options.autoClose){e("body").off("."+r.namespace);if(r.options.trigger=="hover"){if(u){setTimeout(function(){e("body").on("touchstart."+r.namespace,function(){r.hide()})},0)}if(r.options.interactive){if(u){r.$tooltip.on("touchstart."+r.namespace,function(e){e.stopPropagation()})}var p=null;r.$elProxy.add(r.$tooltip).on("mouseleave."+r.namespace+"-autoClose",function(){clearTimeout(p);p=setTimeout(function(){r.hide()},r.options.interactiveTolerance)}).on("mouseenter."+r.namespace+"-autoClose",function(){clearTimeout(p)})}else{r.$elProxy.on("mouseleave."+r.namespace+"-autoClose",function(){r.hide()})}if(r.options.hideOnClick){r.$elProxy.on("click."+r.namespace+"-autoClose",function(){r.hide()})}}else if(r.options.trigger=="click"){setTimeout(function(){e("body").on("click."+r.namespace+" touchstart."+r.namespace,function(){r.hide()})},0);if(r.options.interactive){r.$tooltip.on("click."+r.namespace+" touchstart."+r.namespace,function(e){e.stopPropagation()})}}}}if(r.options.timer>0){r.timerHide=setTimeout(function(){r.timerHide=null;r.hide()},r.options.timer+s)}}})},_interval_set:function(){var t=this;t.checkInterval=setInterval(function(){if(e("body").find(t.$el).length===0||e("body").find(t.$elProxy).length===0||t.Status=="hidden"||e("body").find(t.$tooltip).length===0){if(t.Status=="shown"||t.Status=="appearing")t.hide();t._interval_cancel()}else{if(t.options.positionTracker){var n=t._repositionInfo(t.$elProxy),r=false;if(o(n.dimension,t.elProxyPosition.dimension)){if(t.$elProxy.css("position")==="fixed"){if(o(n.position,t.elProxyPosition.position))r=true}else{if(o(n.offset,t.elProxyPosition.offset))r=true}}if(!r){t.reposition();t.options.positionTrackerCallback.call(t,t.$el)}}}},200)},_interval_cancel:function(){clearInterval(this.checkInterval);this.checkInterval=null},_content_set:function(e){if(typeof e==="object"&&e!==null&&this.options.contentCloning){e=e.clone(true)}this.Content=e},_content_insert:function(){var e=this,t=this.$tooltip.find(".tooltipster-content");if(typeof e.Content==="string"&&!e.options.contentAsHTML){t.text(e.Content)}else{t.empty().append(e.Content)}},_update:function(e){var t=this;t._content_set(e);if(t.Content!==null){if(t.Status!=="hidden"){t._content_insert();t.reposition();if(t.options.updateAnimation){if(l()){t.$tooltip.css({width:"","-webkit-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-moz-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-o-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-ms-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms",transition:"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms"}).addClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!="hidden"){t.$tooltip.removeClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!=="hidden"){t.$tooltip.css({"-webkit-transition":t.options.speed+"ms","-moz-transition":t.options.speed+"ms","-o-transition":t.options.speed+"ms","-ms-transition":t.options.speed+"ms",transition:t.options.speed+"ms"})}},t.options.speed)}},t.options.speed)}else{t.$tooltip.fadeTo(t.options.speed,.5,function(){if(t.Status!="hidden"){t.$tooltip.fadeTo(t.options.speed,1)}})}}}}else{t.hide()}},_repositionInfo:function(e){return{dimension:{height:e.outerHeight(false),width:e.outerWidth(false)},offset:e.offset(),position:{left:parseInt(e.css("left")),top:parseInt(e.css("top"))}}},hide:function(n){var r=this;if(n)r.callbacks.hide.push(n);r.callbacks.show=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;var i=function(){e.each(r.callbacks.hide,function(e,t){t.call(r.$el)});r.callbacks.hide=[]};if(r.Status=="shown"||r.Status=="appearing"){r.Status="disappearing";var s=function(){r.Status="hidden";if(typeof r.Content=="object"&&r.Content!==null){r.Content.detach()}r.$tooltip.remove();r.$tooltip=null;e(t).off("."+r.namespace);e("body").off("."+r.namespace).css("overflow-x",r.bodyOverflowX);e("body").off("."+r.namespace);r.$elProxy.off("."+r.namespace+"-autoClose");r.options.functionAfter.call(r.$el,r.$el);i()};if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-"+r.options.animation+"-show").addClass("tooltipster-dying");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(s)}else{r.$tooltip.stop().fadeOut(r.options.speed,s)}}else if(r.Status=="hidden"){i()}return r},show:function(e){this._showNow(e);return this},update:function(e){return this.content(e)},content:function(e){if(typeof e==="undefined"){return this.Content}else{this._update(e);return this}},reposition:function(){var n=this;if(e("body").find(n.$tooltip).length!==0){n.$tooltip.css("width","");n.elProxyPosition=n._repositionInfo(n.$elProxy);var r=null,i=e(t).width(),s=n.elProxyPosition,o=n.$tooltip.outerWidth(false),u=n.$tooltip.innerWidth()+1,a=n.$tooltip.outerHeight(false);if(n.$elProxy.is("area")){var f=n.$elProxy.attr("shape"),l=n.$elProxy.parent().attr("name"),c=e('img[usemap="#'+l+'"]'),h=c.offset().left,p=c.offset().top,d=n.$elProxy.attr("coords")!==undefined?n.$elProxy.attr("coords").split(","):undefined;if(f=="circle"){var v=parseInt(d[0]),m=parseInt(d[1]),g=parseInt(d[2]);s.dimension.height=g*2;s.dimension.width=g*2;s.offset.top=p+m-g;s.offset.left=h+v-g}else if(f=="rect"){var v=parseInt(d[0]),m=parseInt(d[1]),y=parseInt(d[2]),b=parseInt(d[3]);s.dimension.height=b-m;s.dimension.width=y-v;s.offset.top=p+m;s.offset.left=h+v}else if(f=="poly"){var w=[],E=[],S=0,x=0,T=0,N=0,C="even";for(var k=0;k<d.length;k++){var L=parseInt(d[k]);if(C=="even"){if(L>T){T=L;if(k===0){S=T}}if(L<S){S=L}C="odd"}else{if(L>N){N=L;if(k==1){x=N}}if(L<x){x=L}C="even"}}s.dimension.height=N-x;s.dimension.width=T-S;s.offset.top=p+x;s.offset.left=h+S}else{s.dimension.height=c.outerHeight(false);s.dimension.width=c.outerWidth(false);s.offset.top=p;s.offset.left=h}}var A=0,O=0,M=0,_=parseInt(n.options.offsetY),D=parseInt(n.options.offsetX),P=n.options.position;function H(){var n=e(t).scrollLeft();if(A-n<0){r=A-n;A=n}if(A+o-n>i){r=A-(i+n-o);A=i+n-o}}function B(n,r){if(s.offset.top-e(t).scrollTop()-a-_-12<0&&r.indexOf("top")>-1){P=n}if(s.offset.top+s.dimension.height+a+12+_>e(t).scrollTop()+e(t).height()&&r.indexOf("bottom")>-1){P=n;M=s.offset.top-a-_-12}}if(P=="top"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left+D-j/2;M=s.offset.top-a-_-12;H();B("bottom","top")}if(P=="top-left"){A=s.offset.left+D;M=s.offset.top-a-_-12;H();B("bottom-left","top-left")}if(P=="top-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top-a-_-12;H();B("bottom-right","top-right")}if(P=="bottom"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left-j/2+D;M=s.offset.top+s.dimension.height+_+12;H();B("top","bottom")}if(P=="bottom-left"){A=s.offset.left+D;M=s.offset.top+s.dimension.height+_+12;H();B("top-left","bottom-left")}if(P=="bottom-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top+s.dimension.height+_+12;H();B("top-right","bottom-right")}if(P=="left"){A=s.offset.left-D-o-12;O=s.offset.left+D+s.dimension.width+12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A<0&&O+o>i){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=o+A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);A=s.offset.left-D-q-12-I;F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A<0){A=s.offset.left+D+s.dimension.width+12;r="left"}}if(P=="right"){A=s.offset.left+D+s.dimension.width+12;O=s.offset.left-D-o-12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A+o>i&&O<0){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=i-A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A+o>i){A=s.offset.left-D-o-12;r="right"}}if(n.options.arrow){var R="tooltipster-arrow-"+P;if(n.options.arrowColor.length<1){var U=n.$tooltip.css("background-color")}else{var U=n.options.arrowColor}if(!r){r=""}else if(r=="left"){R="tooltipster-arrow-right";r=""}else if(r=="right"){R="tooltipster-arrow-left";r=""}else{r="left:"+Math.round(r)+"px;"}if(P=="top"||P=="top-left"||P=="top-right"){var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}else if(P=="bottom"||P=="bottom-left"||P=="bottom-right"){var z=parseFloat(n.$tooltip.css("border-top-width")),W=n.$tooltip.css("border-top-color")}else if(P=="left"){var z=parseFloat(n.$tooltip.css("border-right-width")),W=n.$tooltip.css("border-right-color")}else if(P=="right"){var z=parseFloat(n.$tooltip.css("border-left-width")),W=n.$tooltip.css("border-left-color")}else{var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}if(z>1){z++}var X="";if(z!==0){var V="",J="border-color: "+W+";";if(R.indexOf("bottom")!==-1){V="margin-top: -"+Math.round(z)+"px;"}else if(R.indexOf("top")!==-1){V="margin-bottom: -"+Math.round(z)+"px;"}else if(R.indexOf("left")!==-1){V="margin-right: -"+Math.round(z)+"px;"}else if(R.indexOf("right")!==-1){V="margin-left: -"+Math.round(z)+"px;"}X='<span class="tooltipster-arrow-border" style="'+V+" "+J+';"></span>'}n.$tooltip.find(".tooltipster-arrow").remove();var K='<div class="'+R+' tooltipster-arrow" style="'+r+'">'+X+'<span style="border-color:'+U+';"></span></div>';n.$tooltip.append(K)}n.$tooltip.css({top:Math.round(M)+"px",left:Math.round(A)+"px"})}return n},enable:function(){this.enabled=true;return this},disable:function(){this.hide();this.enabled=false;return this},destroy:function(){var t=this;t.hide();if(t.$el[0]!==t.$elProxy[0]){t.$elProxy.remove()}t.$el.removeData(t.namespace).off("."+t.namespace);var n=t.$el.data("tooltipster-ns");if(n.length===1){var r=null;if(t.options.restoration==="previous"){r=t.$el.data("tooltipster-initialTitle")}else if(t.options.restoration==="current"){r=typeof t.Content==="string"?t.Content:e("<div></div>").append(t.Content).html()}if(r){t.$el.attr("title",r)}t.$el.removeClass("tooltipstered").removeData("tooltipster-ns").removeData("tooltipster-initialTitle")}else{n=e.grep(n,function(e,n){return e!==t.namespace});t.$el.data("tooltipster-ns",n)}return t},elementIcon:function(){return this.$el[0]!==this.$elProxy[0]?this.$elProxy[0]:undefined},elementTooltip:function(){return this.$tooltip?this.$tooltip[0]:undefined},option:function(e,t){if(typeof t=="undefined")return this.options[e];else{this.options[e]=t;return this}},status:function(){return this.Status}};e.fn[r]=function(){var t=arguments;if(this.length===0){if(typeof t[0]==="string"){var n=true;switch(t[0]){case"setDefaults":e.extend(i,t[1]);break;default:n=false;break}if(n)return true;else return this}else{return this}}else{if(typeof t[0]==="string"){var r="#*$~&";this.each(function(){var n=e(this).data("tooltipster-ns"),i=n?e(this).data(n[0]):null;if(i){if(typeof i[t[0]]==="function"){var s=i[t[0]](t[1],t[2])}else{throw new Error('Unknown method .tooltipster("'+t[0]+'")')}if(s!==i){r=s;return false}}else{throw new Error("You called Tooltipster's \""+t[0]+'" method on an uninitialized element')}});return r!=="#*$~&"?r:this}else{var o=[],u=t[0]&&typeof t[0].multiple!=="undefined",a=u&&t[0].multiple||!u&&i.multiple,f=t[0]&&typeof t[0].debug!=="undefined",l=f&&t[0].debug||!f&&i.debug;this.each(function(){var n=false,r=e(this).data("tooltipster-ns"),i=null;if(!r){n=true}else if(a){n=true}else if(l){console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.')}if(n){i=new s(this,t[0]);if(!r)r=[];r.push(i.namespace);e(this).data("tooltipster-ns",r);e(this).data(i.namespace,i)}o.push(i)});if(a)return o;else return this}}};var u=!!("ontouchstart"in t);var a=false;e("body").one("mousemove",function(){a=true})})(jQuery,window,document);;
(function( $ ){

  $.fn.responsiveText = function( options ) {

    // Setup options
    var settings = $.extend({
          'minFontSize' : Number.NEGATIVE_INFINITY,
          'maxFontSize' : Number.POSITIVE_INFINITY
        }, options);

    return this.each(function(){

      // Store the object
      var $this = $(this),
          text = $this.get(0);

      if (!text) return;

      $this.data('original-font-size', parseFloat(
        window.getComputedStyle(text).fontSize
      ), 10);

      // Resizer() resizes items based on the object width divided by the compressor * 10
      var resizer = function () {
        var ratio = null,
            originalWidth = window.screen.availWidth,
            currentWidth = window.innerWidth,
            size = $this.data('original-font-size');

        if (originalWidth == currentWidth || !size) return;

        if (currentWidth != originalWidth)
          ratio = originalWidth / currentWidth;

        if (currentWidth > originalWidth)
          size *= ratio;
        else
          size /= ratio;

        size = Math.max(
          Math.min(
            size, 
            settings.maxFontSize
          ), 
          settings.minFontSize
        );

        $this.css('font-size', size + 'px');
      };

      resizer();

      // Call on resize. Opera debounces their resize by default.
      $(window).on('resize.responsiveText orientationchange.responsiveText', resizer);

    });

  };

})( jQuery );;
/**
 * Blocks fabric - main object for whole blocks manipulations
 */
function ptsBlockFabric() {
	this._blocks = [];
	this._isSorting = false;
	this._animationSpeed = g_ptsAnimationSpeed;
}
ptsBlockFabric.prototype.addFromHtml = function(blockData, jqueryHtml) {
	var block = this.add( blockData );
	block.setRaw( jqueryHtml );
};
ptsBlockFabric.prototype.add = function(blockData) {
	var blockData = jQuery.extend({}, blockData);
	if(!blockData.original_id) {
		blockData.original_id = blockData.id;
		blockData.id = 0;
	}
	var blockClass = window[ 'ptsBlock_'+ blockData.cat_code ];
	if(blockClass) {
		var block = new blockClass( blockData );
		var blockIter = this._blocks.push( block );
		block.setIter( blockIter - 1 );
		return block;
	} else {
		console.log('Block Class For '+ blockData.cat_code+ ' Not Found!!!');
	}
};
ptsBlockFabric.prototype.getByViewId = function(viewId) {
	if(this._blocks && this._blocks.length) {
		for(var i = 0; i < this._blocks.length; i++) {
			if(this._blocks[ i ].get('view_id') == viewId) {
				return this._blocks[ i ];
			}
		}
	}
	return false;
};
;
/**
 * Base block object - for extending
 * @param {object} blockData all block data from database (block database row)
 */
function ptsBlockBase(blockData) {
	this._data = blockData;
	this._$ = null;
	this._original$ = null;
	this._id = 0;
	this._iter = 0;
	this._elements = [];
	this._animationSpeed = 300;
	this._disableContentChange = false;
	//this._oneTimeElementsInited = false;
}
ptsBlockBase.prototype.get = function(key) {
	return this._data[ key ];
};
ptsBlockBase.prototype.getParam = function(key) {
	return this._data.params[ key ] ? this._data.params[ key ].val : false;
};
ptsBlockBase.prototype.setParam = function(key, value) {
	if(!this._data.params[ key ]) this._data.params[ key ] = {};
	this._data.params[ key ].val = value;
};
ptsBlockBase.prototype.getRaw = function() {
	return this._$;
};
/**
 * Alias for getRaw method
 */
ptsBlockBase.prototype.$ = function() {
	return this.getRaw();
};
ptsBlockBase.prototype.setRaw = function(jqueryHtml) {
	this._$ = jqueryHtml;
	this._resetElements();
	this._initHtml();
	if(this.getParam('font_family')) {
		this._setFont( this.getParam('font_family') );
	}
};
ptsBlockBase.prototype._initElements = function() {
	this._initElementsForArea( this._$ );
};
ptsBlockBase.prototype._initElementsForArea = function(area) {
	var block = this
	,	addedElements = [];
	var initElement = function(htmlEl) {
		var elementCode = jQuery(htmlEl).data('el')
		,	elementClass = window[ 'ptsElement_'+ elementCode ];
		if(elementClass) {
			var newElement = new elementClass(jQuery(htmlEl), block);
			newElement._setCode(elementCode);
			var newIterNum = block._elements.push( newElement );
			addedElements.push( newElement );
			newElement.setIterNum( newIterNum - 1 );	// newIterNum == new length of _elements array, iterator number for element - is new length - 1
		} else {
			if(g_ptsEdit)
				console.log('Undefined Element ['+ elementCode+ '] !!!');
		}
	};
	jQuery( area ).find('.ptsEl').each(function(){
		initElement(this);
	});
	if(jQuery( area ).hasClass('ptsEl')) {
		initElement( area );
	}
	this._afterInitElements();
	return addedElements;
};
ptsBlockBase.prototype._afterInitElements = function() {
};
ptsBlockBase.prototype._resetElements = function() {
	this._clearElements();
	this._initElements();
};
ptsBlockBase.prototype._clearElements = function() {
	if(this._elements && this._elements.length) {
		for(var i = 0; i < this._elements.length; i++) {
			this._elements[ i ].destroy();
		}
		this._elements = [];
	}
};
ptsBlockBase.prototype.getElements = function() {
	return this._elements;
};
ptsBlockBase.prototype._initHtml = function() {

};
/**
 * ID number in list of canvas elements
 * @param {numeric} iter Iterator - number in all blocks array - for this element
 */
ptsBlockBase.prototype.setIter = function(iter) {
	this._iter = iter;
};
ptsBlockBase.prototype.showLoader = function(txt) {
	var loaderHtml = jQuery('#ptsBlockLoader');
	txt = txt ? txt : loaderHtml.data('base-txt');
	loaderHtml.find('.ptsBlockLoaderTxt').html( txt );
	loaderHtml.css({
		'height': this._$.height()
	,	'top': this._$.offset().top
	}).addClass('active');
};
ptsBlockBase.prototype.hideLoader = function() {
	var loaderHtml = jQuery('#ptsBlockLoader');
	loaderHtml.removeClass('active');
};
ptsBlockBase.prototype._setFont = function(fontFamily) {
	var $fontLink = this._getFontLink();
	// If this is not standard font - it should be Google font, so - load it from Google Fonts server here

	if(toeInArrayPts(fontFamily, ptsBuildConst.standardFonts) === false) {
		$fontLink.attr({
			'href': 'https://fonts.googleapis.com/css?family='+ encodeURIComponent(fontFamily)
		//,	'data-font-family': fontFamily
		});
	}
	this._$.css({
		'font-family': fontFamily
	});
	this.setParam('font_family', fontFamily);
};
ptsBlockBase.prototype._getFontLink = function() {
	var $link = this._$.find('link.ptsFont');
	if(!$link.size()) {
		$link = jQuery('<link class="ptsFont" rel="stylesheet" type="text/css"/>').appendTo( this._$ );
	}
	return $link;
};
ptsBlockBase.prototype.getElementByIterNum = function(iterNum) {
	return this._elements[ iterNum ];
};
ptsBlockBase.prototype.removeElementByIterNum = function(iterNum) {
	this._elements.splice( iterNum, 1 );
	if(this._elements && this._elements.length) {
		for(var i = 0; i < this._elements.length; i++) {
			this._elements[ i ].setIterNum( i );
		}
	}
};
ptsBlockBase.prototype.destroyElementByIterNum = function(iterNum, clb) {
	this.getElementByIterNum( iterNum ).destroy( clb );	// It will call removeElementByIterNum() inside element destroy method
};
/**
 * Price table block base class
 */
function ptsBlock_price_table(blockData) {
	this._increaseHoverFontPerc = 20;	// Increase font on hover effect, %
	this._$lastHoveredCol = null;
	this._refreshColsBinded = false;
	this._onloadHandle = false;
	this._isAlreadyShowed = false;
	this._isResponsiveDescInit = false;
	ptsBlock_price_table.superclass.constructor.apply(this, arguments);
}
extendPts(ptsBlock_price_table, ptsBlockBase);
ptsBlock_price_table.prototype._getColsContainer = function() {
	return this._$.find('.ptsColsWrapper:first');
};
ptsBlock_price_table.prototype._getCols = function(includeDescCol) {
	return this._getColsContainer().find('.ptsCol'+ (includeDescCol ? '' : ':not(.ptsTableDescCol)'));
};
ptsBlock_price_table.prototype._afterInitElements = function() {
	ptsBlock_price_table.superclass._afterInitElements.apply(this, arguments);
	if(parseInt(this.getParam('enb_hover_animation'))) {
		this._initHoverEffect();
	}
	if (this.getParam('table_align'))
		this._$.addClass('ptsTableAlign_' + this.getParam('table_align'));
	if (this.getParam('text_align'))
		this._$.addClass('ptsAlign_' + this.getParam('text_align'));
	if(!this._disableContentChange) {
		this._refreshCellsHeight();
	}
	if(!this._refreshColsBinded) {
		this._$.bind('ptsBlockContentChanged', jQuery.proxy(function(){
			this._refreshCellsHeight();
		}, this));
		this._refreshColsBinded = true;
	}
	if(!_ptsIsEditMode()) { // Not for edit mode unfortunatelly ....
		var $tooltipstedCells = this._$.find('.ptsCell[title]');

		if(this.getParam('disable_custom_tooltip_style') != '1') {
			if($tooltipstedCells && $tooltipstedCells.size()) {
				// TODO: Move this to Options and make this part more flexible
				var tooltipsterSettings = {
					contentAsHTML: true
					,	interactive: true
					,	speed: 250
					,	delay: 0
					,	animation: 'swing'
					,	maxWidth: 450
					,	position: 'top'
				};
				$tooltipstedCells.tooltipster( tooltipsterSettings );
			}
		}
		var self = this
		,	PTS_VISIBLE_SET_HEIGHT_KEY = 'PTS-VISIBLE-SET-HEIGHT-KEY';

		this._fixResponsive();

		jQuery(window).resize(function(){
			if (! self._$.is(':visible')) {
				self._$.data(PTS_VISIBLE_SET_HEIGHT_KEY, false);
			} else {
				self._fixResponsive();
				self._refreshCellsHeight();
			}
		});
		jQuery(window).on('toggleChangeCellHeight', function () {
			self._refreshCellsHeight();
		});
		jQuery(function () {
			var isEnableLazyLoad = function () { 
				return self._$.find('img[data-lazy-src]:not(.lazyloaded)').size() > 0;
			}, checkedLazyLoadLib = false;

			if (isEnableLazyLoad())
				checkedLazyLoadLib = true;

			document.body.addEventListener("DOMSubtreeModified", function (e) {
				if (checkedLazyLoadLib 
					&& e.target.nodeType == 1 
					&& e.target.nodeName == "IMG" 
					&& jQuery.contains(self._$.get(0), e.target)
				) {
					var isLoadedImages = true;

					self._$.find('img[data-lazy-src]').each(function () {
						var $this = jQuery(this);

						if (!$this.hasClass('lazyloaded') && !$this.hasClass('lazyload'))
							isLoadedImages = false;
					});

					if (isLoadedImages) {
						checkedLazyLoadLib = false;

						self._$.find('img[data-lazy-src]').on('load', function () {
							self._fixResponsive();
							self._refreshCellsHeight();
						});
					}
				}

				if (self._isAlreadyShowed || ! self._$) return;

				if (self._$.visible()) {
			  		self._isAlreadyShowed = true;
			  		self._fixResponsive();
			  		self._refreshCellsHeight();
			  	}
			}, false);

			self.setCalcWidth();
		});
	} else {
		this.columnChidrensWidthCalc();
	}

	var self = this;

	if (! this._onloadHandle) {
		this._onloadHandle  = true;
		jQuery(window).load(function(){
			self._refreshCellsHeight();
		});
	}
};
ptsBlock_price_table.prototype._initHoverEffect = function() {
	if(_ptsIsEditMode()) {
		this.setParam('enb_hover_animation', 1);
		return;
	}
	var $cols = this._getCols()
	,	self = this;
	this._disableHoverEffect( $cols );
	$cols.bind('hover.animation', function(e){
		switch(e.type) {
			case 'mouseenter': case 'mousein':
				self._increaseHoverFont( jQuery(this) );
				break;
			case 'mouseleave': case 'mouseout':
				self._backHoverFont( jQuery(this) );
				break;
		}
	});
	this.setParam('enb_hover_animation', 1);
};
ptsBlock_price_table.prototype._increaseHoverFont = function($col) {
	var self = this;
	if(_ptsIsEditMode()) return;	// Not for edit mode unfortunatelly ....
	var $descCell = $col.find('.ptsColDesc');
	$col.height($col.height()); // Reset height on frontend.tables.js (listeners: window resize)
	$descCell.find('span').each(function(){
		var newFontSize = jQuery(this).data('new-font-size');
		if(!newFontSize) {
			var prevFontSize = jQuery(this).css('font-size')
			,	fontUnits = prevFontSize.replace(/\d+/, '')
			,	fontSize = parseInt(str_replace(prevFontSize, fontUnits, ''));
			if(fontSize && fontUnits) {
				newFontSize = Math.ceil(fontSize + (self._increaseHoverFontPerc * fontSize / 100));
				jQuery(this)
					.data('prev-font-size', prevFontSize)
					.data('font-units', fontUnits)
					.data('new-font-size', newFontSize);
			}
		}
		if(newFontSize) {
			jQuery(this).css('font-size', newFontSize+ jQuery(this).data('font-units'));
		}
	});

	if(this.getParam('is_horisontal_row_type') != '1') {

        if(!$descCell.attr('data-prev-height')){
        	var descHeight = $descCell.outerHeight();
            $descCell.attr('data-prev-height', descHeight);
		}else{
            descHeight =  $descCell.attr('data-prev-height');
		}
		$descCell.css({
			'min-height': descHeight
			,	'height': 'auto'
		});
	}

	$col.addClass('hover');
	if(_ptsIsEditMode()) {
		setTimeout(function(){
			var colElement = self.getElementByIterNum($col.data('iter-num'));
			if(colElement) {
				colElement.repositeMenu();
			}
		}, g_ptsHoverAnim);	// 300 - standard animation speed
	}
};
ptsBlock_price_table.prototype._backHoverFont = function($col) {
	if(_ptsIsEditMode()) return;	// Not for edit mode unfortunatelly ....
	$col.removeClass('hover');
	var $descCell = $col.find('.ptsColDesc');
	$descCell.find('span').each(function(){
		var prevFontSize = jQuery(this).data('prev-font-size');
		if(prevFontSize) {
			jQuery(this).css('font-size', prevFontSize);
		}
	});
	setTimeout(function(){
		$descCell.outerHeight( $descCell.data('prev-height') );
	}, 300);	// time is set in css styles, but it always is something around 300ms
};
ptsBlock_price_table.prototype._disableHoverEffect = function($cols) {
	this.setParam('enb_hover_animation', 0);
	if(_ptsIsEditMode()) return;	// Not for edit mode unfortunatelly ....
	$cols = $cols ? $cols : this._getCols();
	$cols.unbind('hover.animation');
};
ptsBlock_price_table.prototype.getColSelectors = function() {
	return {
			header: {sel: '.ptsColHeader'}
		,	desc: {sel: '.ptsColDesc'}
		,	rows: {sel: '.ptsRows'}
		,	cells: {sel: '.ptsCell'}
		,	footer: {sel: '.ptsColFooter'}
	};
};
ptsBlock_price_table.prototype.getMaxColsSizes = function( widthDesc ) {
	var $cols = this._getCols( widthDesc )
	,	sizes = this.getColSelectors();

	$cols.each(function(){
		//fix effects from bug with large min-height in frontend #266
		jQuery(this).css({'min-height' : 'auto'});
		for(var key in sizes) {
			if(key == 'rows') continue;
			var $entity = jQuery(this).find(sizes[ key ].sel);
			if($entity && $entity.size()) {
				if(key == 'cells') {
					if(!sizes[ key ].height)
						sizes[ key ].height = [];
					var cellNum = 0;
					$entity.each(function(){
						var prevHeight = jQuery(this).outerHeight();

						jQuery(this).css('height', 'auto');
						var height = jQuery(this).outerHeight();

						if(!sizes[ key ].height[ cellNum ] || sizes[ key ].height[ cellNum ] < height) {
							sizes[ key ].height[ cellNum ] = height;
						}
						//jQuery(this).outerHeight( prevHeight );
						cellNum++;
					});
				} else {
					var prevHeight = $entity.outerHeight();

					$entity.css('height', 'auto');
					var height = $entity.outerHeight();

					if(!sizes[ key ].height || sizes[ key ].height < height) {
						sizes[ key ].height = height;
					}
					$entity.outerHeight( prevHeight );
				}
			}
		}
	});
	return sizes;
};
ptsBlock_price_table.prototype.getColumnWithInfo = function(strWidthAttr) {
	var trimAttr = strWidthAttr.trim();
	var number = trimAttr.match('\\d+');
	var isPerc = trimAttr.match('%');
	var isPx = trimAttr.match('\\d+');
	if(number === null || (isPerc === null && isPx === null)) {
		return null;
	}
	return new Object({
		'num': (number.length && number.length > 0 && !isNaN(parseInt(number[0]))) ? parseInt(number[0]) : null,
		'isPerc': isPerc === null ? false : true
	});
};
ptsBlock_price_table.prototype.setColsWidth = function(width, perc) {
	var thatObj = this;
	if(thatObj.getParam('is_horisontal_row_type') != '1') {
		if (this.getParam('dsbl_responsive') === '1') {
			var tableWidth = this._$.width(),
				fixedValueTableWidth = this._$.width(),
				$cols = this._getCols(true),
				notSettedColumnWidthArr = new Array();
			$cols.each(function () {
				var col1 = jQuery(this);
				if (col1.length > 0 && col1[0].style && col1[0].style.width) {
					var colWidthObj = thatObj.getColumnWithInfo(col1[0].style.width);
					if (colWidthObj && 'num' in colWidthObj && 'isPerc' in colWidthObj) {
						var calcColWidth = 0;
						if (colWidthObj.isPerc) {
							calcColWidth = fixedValueTableWidth * colWidthObj.num / 100;
						} else {
							calcColWidth = colWidthObj.num;
						}
						var colPdL = parseFloat(col1.css('padding-left')),
							colPdR = parseFloat(col1.css('padding-right')),
							colMgL = parseFloat(col1.css('margin-left')),
							colMgR = parseFloat(col1.css('margin-right')),
							colSumMarginPadding = 0;
						if (!isNaN(colPdL)) {
							colSumMarginPadding += colPdL;
						}
						if (!isNaN(colPdR)) {
							colSumMarginPadding += colPdR;
						}
						if (!isNaN(colMgL)) {
							colSumMarginPadding += colMgL;
						}
						if (!isNaN(colMgR)) {
							colSumMarginPadding += colMgR;
						}
						tableWidth -= calcColWidth;
						calcColWidth = Math.floor(calcColWidth - colSumMarginPadding);
						if (calcColWidth < 0) {
							calcColWidth = 0;
						}
						col1.width(calcColWidth);
					} else {
						notSettedColumnWidthArr[notSettedColumnWidthArr.length] = col1;
					}
				} else {
					notSettedColumnWidthArr[notSettedColumnWidthArr.length] = col1;
				}
			});
			if (tableWidth > 0 && notSettedColumnWidthArr.length > 0) {
				var calcWidthForNsCol = Math.round(tableWidth / notSettedColumnWidthArr.length);
				for (var oneNsColumn in notSettedColumnWidthArr) {
					// set width for Other columns
					notSettedColumnWidthArr[oneNsColumn].width(calcWidthForNsCol);
				}
			}
		} else {
			width = parseFloat(width);
			if (width) {
				if (!perc) {
					this.setParam('col_width', width);
				}
				var $cols = this._getCols(true);
				if (perc) {
					width += '%';
				} else {
					width += 'px';
				}
				$cols.css({
					'width': width
				});
			}
		}
	} else {
		this.columnChidrensWidthCalc();
	}
};
ptsBlock_price_table.prototype.columnChidrensWidthCalc = function() {
	if(this.getParam('is_horisontal_row_type') != '1') {
		return;
	}
	// only for HORIZONTAL ROW
	var $cols = this._getCols(true);
	$cols.each(function (ind, oneCol) {
		var currColumn = jQuery(oneCol)
		,	emptyChilds = currColumn.find('.ptsTableElementContent').children(':not(:has(">div")):not(".ptsColBadge")')
		,	level1Childs = currColumn.find('.ptsTableElementContent').children(':has(">div"):not(".ptsColBadge")')
		,	level1ChildRows = currColumn.find('.ptsTableElementContent').children('.ptsRows')
		,	level2ChildRows = level1ChildRows.children()
		,	allChildCount = level1Childs.length
		,	level2ChildCount = 1;

		if(level1ChildRows.length > 0 && level2ChildRows.length > 0) {
			allChildCount += level2ChildRows.length -1;
			level2ChildCount = level2ChildRows.length;
		}
		//
		emptyChilds.css({
			'width': "0%",
			'margin': '0',
			'padding': '0',
		});
		var oneChildWidth = Math.floor(95 / allChildCount);

		level1Childs.css('width', oneChildWidth + '%');
		var widthInPixel = level1Childs.css('width');
		level1ChildRows.css('width', (level2ChildCount*oneChildWidth+2) + '%');
		level2ChildRows.css('width', widthInPixel);
	});
}
ptsBlock_price_table.prototype.checkColWidthPerc = function() {
	if(this.getParam('calc_width') === 'table') {
		this.setColWidthPerc();
	}
};
ptsBlock_price_table.prototype.setColWidthPerc = function() {
	var $cols = this._getCols( parseInt(this.getParam('enb_desc_col')) );
	this.setColsWidth( 100 / $cols.size(), true );
};
ptsBlock_price_table.prototype.setTableWidth = function(width, measure) {
	if(width && parseInt(width)) {
		width = parseInt(width);
		this.setParam('table_width', width);
	} else {
		width = this.getParam('table_width');
	}
	if(measure) {
		this.setParam('table_width_measure', measure);
	} else {
		measure = this.getParam('table_width_measure');
	}
	this._$.width( width+ measure );

};
ptsBlock_price_table.prototype.setTableVertPadding = function(padding, measure) {
	if(padding && parseInt(padding)) {
		padding = parseInt(padding);
		this.setParam('vert_padding', padding);
	} else {
		padding = this.getParam('vert_padding');
	}
	if(measure) {
		this.setParam('vert_padding_measure', measure);
	} else {
		measure = this.getParam('vert_padding_measure');
	}
	this._$.find('.ptsCol').css('paddingBottom', padding + 'px');
};
ptsBlock_price_table.prototype.setCalcWidth = function(type) {
	if(type) {
		this.setParam('calc_width', type);
	} else {
		type = this.getParam('calc_width');
	}
	switch(type) {
		case 'table':
			this.setTableWidth();
			this.setColWidthPerc();
			break;
		case 'col':
			var enb_desc_col = this.getParam('enb_desc_col')  != 0 ? true : false;
			this._$.width(this._getCols(enb_desc_col).size() * this.getParam('col_width') );
			this.setColsWidth( this.getParam('col_width') );
			break;
	}
};
ptsBlock_price_table.prototype._fixResponsive = function() {
	if(this.getParam('is_horisontal_row_type') == '1') {
		return;
	}
	var $parent = this._$.parents('.ptsTableFrontedShell:first').parent()
	,	parentWidth = $parent.width()
	,	widthMeasure = this.getParam('table_width_measure')
	,	calcWidth = this.getParam('calc_width')
	,	includeDesc = parseInt(this.getParam('enb_desc_col'))
	,	$cols = this._getCols( includeDesc )
	,	actualTblWidth = this._$.width()
	,	criticalColWidth = isNaN(parseInt(this.getParam('resp_min_col_width'))) ? 150 : parseInt(this.getParam('resp_min_col_width'))
	,	dsblResponsive = parseInt(this.getParam('dsbl_responsive'));
	this._$.removeClass('ptsBlockMobile');
	switch(calcWidth) {
		case 'table':
			switch(widthMeasure) {
				case '%':
					var self =  this
					,	removeOtherDescCol = function () {

						if (!dsblResponsive
							&& !_ptsIsEditMode()
							&& self._$.find('.ptsTableDescCol').size() > 1
							&& includeDesc)
						{
							var $descCols = self._$.find('.ptsTableDescCol')
							,	firstCol = false;

							$descCols.each(function () {
								var $this = jQuery(this);

								if (! firstCol) {
									firstCol = true;

									return;
								}

								$this.remove();
							});
						}
					};

					//removeOtherDescCol();

					$cols = this._getCols( includeDesc );

					var colsNum = $cols.size()
						,	currWidth = actualTblWidth / colsNum;
					if(currWidth <= criticalColWidth && !dsblResponsive) {
						$cols.css('width', '100%');

						if (!_ptsIsEditMode() && includeDesc) {
							var $descColumn = this._$.find('.ptsTableDescCol').first()
								,	$columns = this._$.find('.ptsCol:not(.ptsTableDescCol)')
								,	firstCol = false;
							removeOtherDescCol();

							var i = 0;
							$columns.each(function () {
								var $this = jQuery(this);

								if (! firstCol) {
									firstCol = true;
									return;
								}

								i++;
								$descColumn.clone().insertBefore($this);
							});

							this._isResponsiveDescInit = true;
							this._$.find('.ptsCol').css('width', '50%');
						}

						this._$.addClass('ptsBlockMobile');
						this.setParam('went_to_responsive', 1);
					} else {
						removeOtherDescCol();
						if(this.getParam('went_to_responsive')) {
							this.setColWidthPerc();
							this.setParam('went_to_responsive', 0);
						}
					}
					break;
				case 'px':
					if(actualTblWidth > parentWidth) {
						this.setParam('went_to_responsive', this.getParam('table_width'));
						this.setTableWidth(100, '%');	// make it 100% in this case
						this._fixResponsive();	// Repeat - to check case '%':
					} else if(this.getParam('went_to_responsive')) {
						this.setTableWidth(this.getParam('went_to_responsive'), 'px');	// Back to px width
						this.setParam('went_to_responsive', 0);
					}
					break;
			}
			break;
		case 'col':
			var colsNum = $cols.size()
			,	currWidth = parseFloat(this.getParam('col_width'));
			if(currWidth * colsNum >= parentWidth) {
				this.setParam('went_to_responsive', currWidth);
				this.setParam('table_width', 100);	// Set table width to 100%
				this.setParam('table_width_measure', '%');
				this.setCalcWidth('table');
				this._fixResponsive();	// Repeat - to check case '%':
			} else if(this.getParam('went_to_responsive')) {
				this.setCalcWidth('col');
				this.setParam('col_width', this.getParam('went_to_responsive'));	// Back to px width
				this.setParam('went_to_responsive', 0);
			}
			break;
	}
};
ptsBlock_price_table.prototype._refreshCellsHeight = function() {
	// Really important  - keep all this functionality as light as possible, as it can slow down whole builder work
	//console.time('_refreshCellsHeight');
	var $cols = this._getCols( true )
	,	self = this
	,	sizes = this.getMaxColsSizes( true );

	$cols.each(function(){
		for(var key in sizes) {

			var $entity = jQuery(this).find(sizes[ key ].sel);
			if(self.getParam('is_horisontal_row_type') == '1') {
				$entity.css({'height': 'auto'});
			} else {
				if(key == 'rows') {
					$entity.css({'height': 'auto'});
					continue;
				}
				if($entity && $entity.size()) {
					if(key == 'cells') {
						var cellNum = 0;
						$entity.each(function(){
							jQuery(this).css('height', sizes[ key ].height[ cellNum ] );
							cellNum++;
						});
					} else {
						$entity.outerHeight( sizes[ key ].height );

						if ($entity.outerHeight() != sizes[ key ].height) {
							$entity.css('height', sizes[ key ].height);
						}
					}
				}
			}
		}
	});
	//console.timeEnd('_refreshCellsHeight');
};;
function ptsElementBase(jqueryHtml, block) {
	this._iterNum = 0;
	this._id = 'el_'+ mtRand(1, 999999);
	this._animationSpeed = g_ptsAnimationSpeed;
	this._$ = jqueryHtml;
	this._block = block;
	if(typeof(this._menuOriginalId) === 'undefined') {
		this._menuOriginalId = '';
	}
	this._innerImgsCount = 0;
	this._innerImgsLoaded = 0;
	//this._$menu = null;
	this._menu = null;
	this._menuClbs = {};
	if(typeof(this._menuClass) === 'undefined') {
		this._menuClass = 'ptsElementMenu';
	}
	this._menuOnBottom = false;
	this._code = 'base';

	this._initedComplete = false;
	this._editArea = null;
	if(typeof(this._isMovable) === 'undefined') {
		this._isMovable = false;
	}
	this._moveHandler = null;
	this._sortInProgress = false;
	if(typeof(this._showMenuEvent) === 'undefined') {
		this._showMenuEvent = 'click';
	}
	if(typeof(this._changeable) === 'undefined') {
		this._changeable = false;
	}
	if(g_ptsEdit) {
		this._init();
		this._initMenuClbs();
		this._initMenu();

		var images = this._$.find('img');
		if(images && (this._innerImgsCount = images.size())) {
			this._innerImgsLoaded = 0;
			var self = this;
			images.load(function(){
				self._innerImgsLoaded++;
				if(self._$.find('img').size() == self._innerImgsLoaded) {
					self._afterFullContentLoad();
				}
			});
		}
	}
	this._onlyFirstHtmlInit();
	this._initedComplete = true;
}
ptsElementBase.prototype.getId = function() {
	return this._id;
};
ptsElementBase.prototype.getBlock = function() {
	return this._block;
};
ptsElementBase.prototype._onlyFirstHtmlInit = function() {
	if(this._$ && !this._$.data('first-inited')) {
		this._$.data('first-inited', 1);
		return true;
	}
	return false;
};
ptsElementBase.prototype.setIterNum = function(num) {
	this._iterNum = num;
	this._$.data('iter-num', num);
};
ptsElementBase.prototype.getIterNum = function() {
	return this._iterNum;
};
ptsElementBase.prototype.$ = function() {
	return this._$;
};
ptsElementBase.prototype.getCode = function() {
	return this._code;
};
ptsElementBase.prototype._setCode = function(code) {
	this._code = code;
};
ptsElementBase.prototype._init = function() {
	this._beforeInit();
};
ptsElementBase.prototype._beforeInit = function() {
	
};
ptsElementBase.prototype.destroy = function() {
	
};
ptsElementBase.prototype.get = function(opt) {
	return jQuery('<div/>').html(this._$.attr( 'data-'+ opt )).text();	// not .data() - as it should be saved even after page reload, .data() will not create element attribute
};
ptsElementBase.prototype.set = function(opt, val) {
	this._$.attr( 'data-'+ opt, jQuery('<div/>').text(val).html() );	// not .data() - as it should be saved even after page reload, .data() will not create element attribute
};
ptsElementBase.prototype._getEditArea = function() {
	if(!this._editArea) {
		this._editArea = this._$.children('.ptsElArea');
		if(!this._editArea.size()) {
			this._editArea = this._$.find('.ptsInputShell');
		}
	}
	return this._editArea;
};
ptsElementBase.prototype._getOverlay = function() {
	return this._$.find('.ptsElOverlay');
};
/**
 * Standart button item
 */
function ptsElement_btn(jqueryHtml, block) {
	if(typeof(this._menuOriginalId) === 'undefined') {
		this._menuOriginalId = 'ptsElMenuBtnExl';
	}
	this._menuClass = 'ptsElementMenu_btn';
	this._haveAdditionBgEl = null;
	this._changeable = true;
	this.includePostLinks = true;
	ptsElement_btn.superclass.constructor.apply(this, arguments);
}
extendPts(ptsElement_btn, ptsElementBase);
ptsElement_btn.prototype._onlyFirstHtmlInit = function() {
	if(ptsElement_btn.superclass._onlyFirstHtmlInit.apply(this, arguments)) {
		if(this.get('customhover-clb')) {
			var clbName = this.get('customhover-clb');
			if(typeof(this[clbName]) === 'function') {
				var self = this;
				this._getEditArea().hover(function(){
					self[clbName](true, this);
				}, function(){
					self[clbName](false, this);
				});
			}
		}
	}
};
ptsElement_btn.prototype._hoverChangeFontColor = function( hover, element ) {
	if(hover) {
		jQuery(element)
			.data('original-color', this._getEditArea().css('color'))
			.css('color', jQuery(element).parents('.ptsEl:first').attr('data-bgcolor'));	// Ugly, but only one way to get this value in dynamic way for now
	} else {
		jQuery(element)
			.css('color', jQuery(element).data('original-color'));
	}
};
ptsElement_btn.prototype._hoverChangeBgColor = function( hover, element ) {
	var parentElement = jQuery(element).parents('.ptsEl:first');	// Actual element html
	if(hover) {
		parentElement
			.data('original-color', parentElement.css('background-color'))
			.css('background-color', parentElement.attr('data-bgcolor'));	// Ugly, but only one way to get this value in dynamic way for now
	} else {
		parentElement
			.css('background-color', parentElement.data('original-color'));
	}
};
ptsElement_btn.prototype._hoverBorderColor = function( hover, element ) {
	//var parentElement = jQuery(element).parents('.ptsEl:first');	// Actual element html
	if(hover) {
		jQuery(element)
			.data('original-color', jQuery(element).css('border-color'))
			.css('border-color', jQuery(element).parents('.ptsEl:first').attr('data-bgcolor'));	// Ugly, but only one way to get this value in dynamic way for now
	} else {
		jQuery(element)
			.css('border-color', jQuery(element).data('original-color'));
	}
};
/**
 * Table column element
 */
function ptsElement_table_col(jqueryHtml, block) {
	if(typeof(this._menuOriginalId) === 'undefined') {
		this._menuOriginalId = 'ptsElMenuTableColExl';
	}
	if(typeof(this._menuClass) === 'undefined') {
		this._menuClass = 'ptsElementMenu_table_col';
	}
	if(typeof(this._isMovable) === 'undefined') {
		this._isMovable = true;
	}
	this._showMenuEvent = 'hover';
	this._colNum = 0;
	ptsElement_table_col.superclass.constructor.apply(this, arguments);
	/*if(parseInt(this.get('enb-schedule'))) {
		console.log(this.getIterNum());
	}*/
}
extendPts(ptsElement_table_col, ptsElementBase);
ptsElement_table_col.prototype.setIterNum = function() {
	ptsElement_table_col.superclass.setIterNum.apply(this, arguments);
	if(!g_ptsEdit && typeof(ptsScheduleCheck) !== 'undefined') {
		ptsScheduleCheck(this);
	}
};
/**
 * Table description column element
 */
function ptsElement_table_col_desc(jqueryHtml, block) {
	this._isMovable = false;
	ptsElement_table_col_desc.superclass.constructor.apply(this, arguments);
	//this.refreshHeight();
	//var self = this;
	/*this.getBlock().$().bind('ptsBlockContentChanged', function(){
		self.refreshHeight();
	});*/
}
extendPts(ptsElement_table_col_desc, ptsElement_table_col);
/*ptsElement_table_col_desc.prototype.refreshHeight = function() {
	var sizes = this.getBlock().getMaxColsSizes();
	for(var key in sizes) {
		var $entity = this._$.find(sizes[ key ].sel);
		if($entity && $entity.size()) {
			if(key == 'cells' &&  sizes[ key ].height) {
				var cellNum = 0;
				$entity.each(function(){
					if(typeof(sizes[ key ].height[ cellNum ]) !== 'undefined') {
						jQuery(this).css('height', sizes[ key ].height[ cellNum ]);
					}
					cellNum++;
				});
			} else {
				$entity.css('height', sizes[ key ].height);
			}
		}
	}
};*/

/**
 * Text element
 */
function ptsElement_table_cell_txt(jqueryHtml, block) {
	if (block.getParam('responsive_text')) {
		jqueryHtml.find('span, p').responsiveText({ minFontSize: 14 });
	}
	this.includePostLinks = true;
	ptsElement_table_cell_txt.superclass.constructor.apply(this, arguments);
}
extendPts(ptsElement_table_cell_txt, ptsElementBase);;
(function ($) {
	'use strict';

	$.fn.sModal = function(options) {
		var self = this,
			settings = $.extend({
				width: 640,
				height: 480,
				buttons: [],
			}, options),
			$modalOverlay,
			$modalContainer,
			$closeModalButton,
			$modalContent,
			$modalButtons,
			modalOverlayStyle = {
				position: 'fixed',
				top: 0,
				left: 0,
				background: 'rgba(0,0,0,.75)',
				width: '100%',
				height: '100%',
				'z-index': '100000',
				display: 'none',
				'overflow-y': 'auto',
			},
			modalContainerStyle = {
				width: settings.width,
				position: 'absolute',
				top: '50%',
				left: '50%',
				'max-width': 'calc(100% - 1.5em)',
				'margin-right': '-50%',
				'margin-bottom': '20px',
				transform: 'translate(-50%, -50%)',
				background: '#fff',
				padding: '1em',
			},
			modalContentStyle = {
				height: 'calc(100% - 3.6em)',
				'overflow-y': 'auto',
				'overflow-x': 'hidden',
				padding: '1em',
			},
			closeButtonStyle = {
				position: 'absolute',
				right: '10px',
				top: '10px',
				'font-size': '35px',
				color: '#fff',
				cursor: 'pointer',
			},
			modalButtonsStyle = {
				position: 'relative',
				width: '100%',
				'text-align': 'right',
				'margin-top': '1em',
			};


		function closeModal() {
			self.trigger('close.before');
			if (jQuery('.sModal:visible').length < 2) {
				jQuery('body').css('overflow', 'auto');
			}
			$modalOverlay.fadeOut();
			self.trigger('close.after');
		}

		function openModal() {
			jQuery('body').css('overflow', 'hidden');
			$modalOverlay.show(0, function() {
				resizeModal();
				$modalOverlay.hide().fadeIn();
			});
			
			if( parseInt(jQuery('.sc-modal-container').css('top')) > $(window).height()){
				var offset = $(window).height() / 2 + 40;
				jQuery('.sc-modal-container').css('top' , offset);
			}
			
		}

		function resizeModal() {
			var offset = '50%';
			if ($modalContainer.height() > $(window).height()) {
				offset = $modalContainer.height() / 2 + 40;
			}
			$modalContainer.css('top', offset);
		}

		function createInstance($modalTemplate) {

			$modalOverlay = jQuery('<div class="sc-modal-overlay" tabindex="0">').css(modalOverlayStyle);
			
			$modalContainer = jQuery('<div class="sc-modal-container">').css(modalContainerStyle);
			
			$closeModalButton = jQuery('<div class="sc-modal-close-button">&times;</div>').css(closeButtonStyle);
			$closeModalButton.appendTo($modalOverlay);

			$modalContent = jQuery('<div class="sc-modal-content">').css(modalContentStyle);
			$modalContent.appendTo($modalContainer);

			$modalTemplate.appendTo($modalContent).show();

			$modalContainer.appendTo($modalOverlay);

			$modalOverlay.appendTo('body');
			$modalOverlay.add($closeModalButton).on('click', function(event) {
				if ($(this).is($(event.target))) {
					closeModal();
				}
			});

			$(document).on('keyup', function(event) {
				if (event.which == 27 && $modalOverlay.is(':visible')) {
					closeModal();
				}
			});

			var resizeTimer;

			$(window).on('resize', function(event) {
				clearTimeout(resizeTimer);
				resizeTimer = setTimeout(function() {
					resizeModal();
				}, 250);
			});

			if (settings.buttons.length > 0) {
				$modalButtons = jQuery('<div class="sc-modal-action-buttons">').css(modalButtonsStyle);

				for (var i = 0; i < settings.buttons.length; i++) {
					jQuery('<button>').attr('class', settings.buttons[i].class || null)
					.html(settings.buttons[i].content || null)
					.on('click', $.proxy(settings.buttons[i].event, $modalTemplate))
					.appendTo($modalButtons);
				}

				self.$buttons = $modalButtons;

				$modalButtons.appendTo($modalContainer);
			}
			
		}

		createInstance(this);

		$.extend(this, {
			open: function() {
				this.trigger('open.before');
				openModal();
				this.trigger('open.after');
				return this;
			},
			close: function() {
				this.trigger('close.before');
				closeModal();
				this.trigger('close.after');
				return this;
			}
		});

		return this;
	};

})(jQuery);
;
var g_ptsEdit = false
,	g_ptsBlockFabric = null
,	g_ptsHoverAnim = 300	// Table hover animation lenght, ms - hardcoded for now
,	g_ptsHoverMargin = 20;	// Table hover margin displace, px - hardcoded for now

//array with unique_id template that without enabled options header row
//overlap toggle elements
var uniqueIdArray = ['wnOq3v2K', 'sa0DHT4h', 'B1gpCofW', 'ptmJ4YnA', 'r0OL4vCy'];

jQuery(document).ready(function(){
	_ptsInitFabric();
	if(typeof(ptsTables) !== 'undefined' && ptsTables && ptsTables.length) {
		for(var i = 0; i < ptsTables.length; i++) {
			g_ptsBlockFabric.addFromHtml(ptsTables[ i ], jQuery('#'+ ptsTables[ i ].view_id));
			jQuery('body').trigger('set_default_position');
			//for fix horizontal element size bugs frontend.pro.tables.js
			if(ptsTables[i].unique_id === "7m6k5X0i"){
				jQuery('#'+ ptsTables[ i ].view_id).attr('data-unique_id', ptsTables[i].unique_id);
			}

			// fix overlap toggle elements
            if(uniqueIdArray.indexOf(ptsTables[i].unique_id) != -1){
                jQuery('#'+ ptsTables[ i ].view_id).attr('data-unique_id', ptsTables[i].unique_id);
                //if undefined that means options is enabled by default.

                var enableHeader = '1';
				if (typeof ptsTables[i].params.enb_head_row !== 'undefined') {
                    enableHeader = typeof (ptsTables[i].params.enb_head_row.val) !== "undefined" ? '0' : '1';
                }
                jQuery(document.body).trigger( "overlay_toggle", [ ptsTables[i].unique_id , enableHeader] );
            }
			jQuery(".ptsTableFrontedShell .tooltipstered").removeAttr("title");
		}
		jQuery(window).bind('load', function() {
			if(typeof(g_ptsAllowAddUndo) !== 'undefined') {
				setTimeout(function() {
					g_ptsAllowAddUndo = true;
      				_ptsAddUndoBuffer();
    			}, 200);
			}
		});
	}
});
jQuery(window).load(function() {
	setTimeout(function() { jQuery('body').trigger('resize'); }, 500);
});
//in case images are loading dynamically
jQuery('.ptsEl.ptsCol[data-el="table_col"] img').on('load', function() {
	jQuery('body').trigger('resize');
});
function _ptsInitFabric() {
	g_ptsBlockFabric = new ptsBlockFabric();
}
function ptsGetFabric() {
	return g_ptsBlockFabric;
}
function _ptsIsEditMode() {
	return (typeof(g_ptsEditMode) !== 'undefined' && g_ptsEditMode);
};
