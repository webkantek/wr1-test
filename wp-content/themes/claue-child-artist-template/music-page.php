<?php
/**
 * Template Name: Music Page Template
 * 
 * Credit: Smashicons
 * Instagram share creative: https://www.instagram.com/p/BemFFcsF-5c/
 */
 
    get_header();

?>
 
<div id="main-content" class="main-content">
	<div id="primary" class="content-area jas-container">
		<div id="music-album-content" class="site-content vc_row" role="main">
            
        

        <!-- ************************************************* 
        * WR1 Custom code-break 
        ************************************************* -->
        <?php 
            //echo 'Main content';

            // Register the script
            wp_register_script( 'mix-it-up-js', get_stylesheet_directory_uri() . '/custom_js/vendor/mixitup.min.js' );
            
            // Enqueue the script
            wp_enqueue_script('mix-it-up-js', get_stylesheet_directory_uri() . '/custom_js/vendor/mixitup.min.js');
            


            // Get all music albums
            $music_albums = get_posts( array(
                'numberposts' => -1,
                'post_type'   => 'music_album', // Subscription post type
                //'post_status' => 'wc-active', // Active subscription
                'orderby' => 'post_date', // ordered by date
                'order' => 'DESC',
                /*'date_query' => array( // Start & end date
                    array(
                        'after'     => $from_date,
                        'before'    => $to_date,
                        'inclusive' => true,
                    ),
                ),*/
            ));

            $album_count = sizeOf( $music_albums );
            //echo 'Album Count: ' . $album_count . '<br/><br/>';


            $page_title = '<h2 class="music-page-title">Music</h2>';
            echo $page_title;


            /**********************************************
             * Render sorting buttons
            **********************************************/
            $sort_button_el = '<div class="sorting-container"><button type="button" data-filter="all">All</button><button type="button" data-filter=".album">Albums</button><button type="button" data-filter=".single">Singles</button></div>';
            echo $sort_button_el;


            /* 
            * Start of albums container
            */
            echo '<div class="container">';

            /**********************************************
             * Render filtering buttons
            **********************************************/
            //echo '<button type="button" data-sort="order:asc">Ascending</button><button type="button" data-sort="order:descending">Descending</button><button type="button" data-sort="random">Random</button>';

            $s = 0;

            // Going through each current album order
            foreach ( $music_albums as $album ) {
                
                // Get Album ID
                $album_id = $album->ID; 
                //echo 'Album ID: '.$album_id.'<br/>';

                // Get Album Permalink
                $album_permalink = get_permalink($album_id); 
                //echo 'Album Permalink: '.$album_permalink.'<br/>';

                // Get album date
                $album_date = array_shift( explode( ' ', $album->post_date ) );
                //echo 'Album Date: '.$album_date.'<br/>';

                // Get album title data
                $album_title = $album->post_title;
                //echo 'Album Title: '.$album_title.'<br/>';

                // Get album content
                $album_content = $album->post_content;
                //echo 'Album Content: '.$album_content.'<br/><br/>';

                // Get album meta data
                $album_meta_data = get_post_meta( $album_id );
                //echo $album_meta_data;
                /*foreach($album_meta_data as $key => $value) {
                    echo $key . ' : ' . $value . '<br/><br/>';
                    
                    foreach($value as $key2 => $value2) {
                        //echo $key2 . ' : ' . $value2 . '<br/><br/>';
                    }
                }*/


                /**********************************************
                 * Get album data
                **********************************************/
                $album_type_data = $album_meta_data['type_of_album'];
                if ( is_array($album_type_data) ) {

                    // Get album type
                    foreach($album_type_data as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        
                        $album_type = $value;
                        //echo 'Album Type: ' . $album_type . '<br/>';
                    }

                    // Get album featured image
                    $album_featured_image = $album_meta_data['album_featured_image'];
                    if ( is_array($album_featured_image) ) {

                        foreach($album_featured_image as $key => $value) {
                            //echo $key . ' : ' . $value . '<br/><br/>';
                            
                            $album_image_url = $value;
                            $album_image = wp_get_attachment_image_src($album_image_url, $size = 'full');
                            //echo 'Album Featured Image: ' . $album_image[0] . '<br/>';

                            //$album_image = wp_get_attachment_image_src($album_image_url);
                            //$post_thumbnail_id = get_post_thumbnail_id( $album_id );
                            //echo 'Album Post Image: ' . $post_thumbnail_id . '<br/>';
                        }
                    }

                    //echo 'Одного ' . $album_type;
                    $album_type_lowercase = strtolower($album_type);
                
                    //$music_album = '<div class="music-album-cover mix ' . $album_type_lowercase . '" data-order="' . $key . '"><a href="' . $album_permalink . '" title="' . $album_title . '" target="_self"><h3>' . $album_title . '</h3><img src="' . $album_image[0] . '" alt="' . $album_title . '" /></a></div>';

                    //$music_album = '<div class="music-album mix ' . $album_type_lowercase . '" data-order="' . $s . '"><a href="' . $album_permalink . '" title="' . $album_title . '" target="_self"><div class="album-cover" style="background-image: url(' . $album_image[0] . ')"><div class="album-overlay"><div class="overlay-content"><p>Listen to</p><h3>' . $album_title . '</h3></div></div></div></a></div>';

                    $music_album = '<div class="music-album mix ' . $album_type_lowercase . '" data-order="' . $s . '"><div class="album-cover" style="background-image: url(' . $album_image[0] . ')"><div class="album-overlay"><div class="overlay-content"><a class="album-sleeve-cta" href="' . $album_permalink . '" title="' . $album_title . '" target="_self">Listen to<br/>' . $album_title . '</a></div></div></div></div>';

                    echo $music_album;
                }

                $s++;
            }

            /* 
            * End of albums container
            */
            echo '</div>';
        ?>

            <script type="text/javascript">

                function activateMixItUp(containerEl) {
                    var mixer = mixitup(containerEl, {
                        selectors: {
                            //target: '.blog-item'
                        },
                        animation: {
                            duration: 300
                        }
                    });
                }

                setTimeout(function(){ 
			        activateMixItUp('.container');
                }, 300);
                
            </script>
            

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->


<?php


//get_sidebar();
get_footer();